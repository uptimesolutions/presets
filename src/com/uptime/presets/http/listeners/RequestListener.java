/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.http.listeners;

import com.sun.net.httpserver.HttpServer;
import com.uptime.presets.http.handlers.BearingCatalogHandler;
import com.uptime.presets.http.handlers.CustomerApSetHandler;
import com.uptime.presets.http.handlers.GlobalApAlSetsHandler;
import com.uptime.presets.http.handlers.GlobalAssetPresetHandler;
import com.uptime.presets.http.handlers.GlobalAssetTypesHandler;
import com.uptime.presets.http.handlers.GlobalDeviceHandler;
import com.uptime.presets.http.handlers.GlobalFaultFrequenciesHandler;
import com.uptime.presets.http.handlers.GlobalPtLocationNamesHandler;
import com.uptime.presets.http.handlers.GlobalTachometersHandler;
import com.uptime.presets.http.handlers.KafkaTachometersHandler;
import com.uptime.presets.http.handlers.SiteApAlSetsHandler;
import com.uptime.presets.http.handlers.SiteAssetPresetHandler;
import com.uptime.presets.http.handlers.SiteBearingFavoritesHandler;
import com.uptime.presets.http.handlers.SiteDeviceHandler;
import com.uptime.presets.http.handlers.SiteFFSetFavoritesHandler;
import com.uptime.presets.http.handlers.SystemHandler;
import com.uptime.presets.http.handlers.SiteFaultFrequenciesHandler;
import com.uptime.presets.http.handlers.SitePtLocationNamesHandler;
import com.uptime.presets.http.handlers.SiteTachometersHandler;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class RequestListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(RequestListener.class.getName());
    private int port = 0;
    HttpServer server;

    /**
     * Parameterized Constructor
     *
     * @param port, int
     */
    public RequestListener(int port) {
        this.port = port;
    }

    /**
     * Stop the Http server
     */
    public void stop() {
        LOGGER.info("Stopping request listener...");
        server.stop(10);
    }

    /**
     * Start the Http Server with the needed handlers
     *
     * @throws Exception
     */
    public void start() throws Exception {
        server = HttpServer.create(new InetSocketAddress(port), 2);
        server.createContext("/system", new SystemHandler());
        server.createContext("/presets/siteFaultFrequency", new SiteFaultFrequenciesHandler());
        server.createContext("/presets/siteTachometer", new SiteTachometersHandler());
        server.createContext("/presets/globalFaultFrequency", new GlobalFaultFrequenciesHandler());
        server.createContext("/presets/globalTachometer", new GlobalTachometersHandler());
        server.createContext("/presets/globalApAlSets", new GlobalApAlSetsHandler());
        server.createContext("/presets/siteApAlSets", new SiteApAlSetsHandler());
        server.createContext("/presets/customerApSet", new CustomerApSetHandler());
        server.createContext("/presets/globalDevice", new GlobalDeviceHandler());
        server.createContext("/presets/siteDevice", new SiteDeviceHandler());
        server.createContext("/presets/globalPtLocationNames", new GlobalPtLocationNamesHandler());
        server.createContext("/presets/sitePtLocationNames", new SitePtLocationNamesHandler());
        server.createContext("/presets/siteBearingFavorites", new SiteBearingFavoritesHandler());
        server.createContext("/presets/bearingCatalog", new BearingCatalogHandler());
        server.createContext("/presets/siteFFSetFavorites", new SiteFFSetFavoritesHandler());
        server.createContext("/presets/kafka", new KafkaTachometersHandler());
        server.createContext("/presets/siteAsset", new SiteAssetPresetHandler());
        server.createContext("/presets/globalAsset", new GlobalAssetPresetHandler());
        server.createContext("/presets/globalAssetType", new GlobalAssetTypesHandler());

        server.setExecutor(Executors.newCachedThreadPool());

        //start server
        LOGGER.info("Starting request listener...");
        server.start();
    }
}
