/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.presets.entity.GlobalApAlSets;
import com.uptime.cassandra.presets.entity.GlobalApAlSetsByCustomer;
import com.uptime.services.util.JsonConverterUtil;
import com.uptime.presets.delegate.CreateGlobalApAlSetsDelegate;
import com.uptime.presets.delegate.DeleteGlobalApAlSetsDelegate;
import com.uptime.presets.delegate.ReadGlobalApAlSetsDelegate;
import com.uptime.presets.delegate.UpdateGlobalApAlSetsDelegate;
import com.uptime.presets.utils.JsonUtil;
import com.uptime.presets.utils.Utils;
import com.uptime.presets.vo.GlobalApAlSetsVO;
import java.util.List;
import java.util.ArrayList;
import java.util.UUID;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class GlobalApAlSetsController {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalApAlSetsController.class.getName());
    ReadGlobalApAlSetsDelegate readDelegate;

    public GlobalApAlSetsController() {
        readDelegate = new ReadGlobalApAlSetsDelegate();
    }

    /**
     * Return a json of list of GlobalApAlSets Objects by the given params
     *
     * @param customerAccount, String Object
     * @param apSetId, String Object
     * @param alSetId, String Object
     * @param paramName, String Object
     * @return List of GlobalApAlSets Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByPK(String customerAccount, String apSetId, String alSetId, String paramName) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<GlobalApAlSets> result;
        StringBuilder json;

        if ((result = readDelegate.findByPK(customerAccount, UUID.fromString(apSetId), UUID.fromString(alSetId), paramName)) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"apSetId\":\"").append(apSetId).append("\",")
                        .append("\"alSetId\":\"").append(alSetId).append("\",")
                        .append("\"paramName\":\"").append(paramName).append("\",")
                        .append("\"GlobalApAlSets\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Global ApAlSets not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of List of GlobalApAlSets Objects by the given params
     *
     * @param customerAccount, String Object
     * @param apSetId, String Object
     * @param alSetId, String Object
     * @return List of GlobalApAlSets Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByCustomerApSetAlSet(String customerAccount, String apSetId, String alSetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<GlobalApAlSets> result;
        StringBuilder json;

        if ((result = readDelegate.findByCustomerApSetAlSet(customerAccount, UUID.fromString(apSetId), UUID.fromString(alSetId))) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"apSetId\":\"").append(apSetId).append("\",")
                        .append("\"alSetId\":\"").append(alSetId).append("\",")
                        .append("\"GlobalApAlSets\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Global ApAlSets not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of List of GlobalApAlSets Objects by the given params
     *
     * @param customerAccount, String Object
     * @param apSetIds, String Object
     * @return List of GlobalApAlSets Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByCustomerApSets(String customerAccount, String apSetIds) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<GlobalApAlSets> result;
        StringBuilder json;
        
        List<UUID> apSetIdList = new ArrayList<>();
        String[] apSetIdArr = apSetIds.split(",");
        
        for (String apSetId : apSetIdArr) {
            apSetIdList.add(UUID.fromString(apSetId.trim()));
        }
        
        if ((result = readDelegate.findByCustomerApSets(customerAccount, apSetIdList)) != null) {
            if (result.size() > 0) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"apSetIds\":\"").append(apSetIds).append("\",")
                        .append("\"GlobalApAlSets\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Global ApAlSets not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of List of GlobalApAlSets Objects by the given params
     *
     * @param customerAccount, String Object
     * @param apSetId, String Object
     * @return List of GlobalApAlSets Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByCustomerApSet(String customerAccount, String apSetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<GlobalApAlSets> result;
        StringBuilder json;

        if ((result = readDelegate.findByCustomerApSet(customerAccount, UUID.fromString(apSetId))) != null) {
            if (result.size() > 0) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"apSetId\":\"").append(apSetId).append("\",")
                        .append("\"GlobalApAlSets\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Global ApAlSets not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of List of GlobalApAlSetsByCustomer Objects by the given params
     *
     * @param customerAccount, String Object
     * @param sensorType, String Object
     * @param getAllData
     * @return List of GlobalApAlSetsByCustomer Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByCustomerSensorType(String customerAccount, String sensorType, boolean getAllData) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<GlobalApAlSetsByCustomer> result;
        StringBuilder json;

        if ((result = readDelegate.findByCustomerSensorType(customerAccount, sensorType)) != null) {
            if (result.size() > 0) {
                if (!getAllData) {
                    result = result.stream().filter(Utils.distinctByKeys(GlobalApAlSetsByCustomer::getCustomerAccount, GlobalApAlSetsByCustomer::getSensorType, GlobalApAlSetsByCustomer::getApSetId, GlobalApAlSetsByCustomer::getAlSetId)).collect(Collectors.toList());
                    json = new StringBuilder();
                    json
                            .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                            .append("\"sensorType\":\"").append(sensorType).append("\",")
                            .append("\"GlobalApAlSetsByCustomer\":[");
                    for (int i = 0; i < result.size(); i++) {
                        json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                    }
                    json.append("],\"outcome\":\"GET worked successfully.\"}");
                    return json.toString();
                } else {
                    List<GlobalApAlSets> result1 = new ArrayList();
                    for (GlobalApAlSetsByCustomer globalApAlSetsByCustomer : result) {
                        result1.addAll(readDelegate.findByCustomerApSetAlSet(customerAccount, globalApAlSetsByCustomer.getApSetId(), globalApAlSetsByCustomer.getAlSetId()));
                    }
                    if (!result1.isEmpty()) {
                        json = new StringBuilder();
                        json.append("{\"customerAccount\":\"")
                                .append(customerAccount)
                                .append("\",")
                                .append("\"GlobalApAlSets\":[");
                        for (int i = 0; i < result1.size(); i++) {
                            json.append(JsonConverterUtil.toJSON(result1.get(i))).append(i < result1.size() - 1 ? "," : "");
                        }
                        json.append("],\"outcome\":\"GET worked successfully.\"}");
                        return json.toString();
                    } else {
                        return "{\"outcome\":\"Global ApAlSets not found.\"}";
                    }
                }
            } else {
                return "{\"outcome\":\"Global ApAlSets By Customer not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of List of GlobalApAlSetsByCustomer Objects by the given params
     *
     * @param customerAccount, String Object
     * @param sensorType, String Object
     * @param apSetId
     * @return List of GlobalApAlSetsByCustomer Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByCustomerSensorTypeApSet(String customerAccount, String sensorType, String apSetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<GlobalApAlSetsByCustomer> result;
        StringBuilder json;

        if ((result = readDelegate.findByCustomerSensorTypeApSet(customerAccount, sensorType, UUID.fromString(apSetId))) != null) {
            if (!result.isEmpty()) {
                result = result.stream().filter(Utils.distinctByKeys(GlobalApAlSetsByCustomer::getCustomerAccount, GlobalApAlSetsByCustomer::getSensorType, GlobalApAlSetsByCustomer::getApSetId, GlobalApAlSetsByCustomer::getAlSetId)).collect(Collectors.toList());
                json = new StringBuilder();
                json.append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"sensorType\":\"").append(sensorType).append("\",")
                        .append("\"GlobalApAlSetsByCustomer\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Global ApAlSets By Customer not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Create a new GlobalApAlSets by inserting into global_ap_al_sets and global_ap_al_sets_by_customer tables
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String createGlobalApAlSets(String content) throws IllegalArgumentException {
        CreateGlobalApAlSetsDelegate createDelegate;
        GlobalApAlSetsVO globalApAlSetsVO;

        if ((globalApAlSetsVO = JsonUtil.globalApAlSetsVOParser(content)) != null) {
            if (globalApAlSetsVO != null
                    && globalApAlSetsVO.getCustomerAccount() != null && !globalApAlSetsVO.getCustomerAccount().isEmpty()
                    && globalApAlSetsVO.getApSetName() != null && !globalApAlSetsVO.getApSetName().isEmpty()
                    && globalApAlSetsVO.getAlSetName() != null && !globalApAlSetsVO.getAlSetName().isEmpty()
                    && globalApAlSetsVO.getParamName() != null && !globalApAlSetsVO.getParamName().isEmpty()
                    && globalApAlSetsVO.getSensorType() != null && !globalApAlSetsVO.getSensorType().isEmpty()) {

                // Insert into Cassandra
                try {
                    createDelegate = new CreateGlobalApAlSetsDelegate();
                    return createDelegate.createGlobalApAlSets(globalApAlSetsVO);
                } catch (IllegalArgumentException e) {
                    LOGGER.error(e.getMessage(), e);
                    throw e;
                } catch (Exception e) {
                    return "{\"outcome\":\"Error: Failed to create new Global ApAlSetsVO.\"}";
                }

            } else {
                return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Update global_ap_al_sets and global_ap_al_sets_by_customer tables with the given GlobalApAlSets
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String updateGlobalApAlSets(String content) throws IllegalArgumentException {
        UpdateGlobalApAlSetsDelegate updateDelegate;
        GlobalApAlSetsVO globalApAlSetsVO;

        if ((globalApAlSetsVO = JsonUtil.globalApAlSetsVOParser(content)) != null) {
            if (globalApAlSetsVO != null
                    && globalApAlSetsVO.getCustomerAccount() != null && !globalApAlSetsVO.getCustomerAccount().isEmpty()
                    && globalApAlSetsVO.getApSetId() != null
                    && globalApAlSetsVO.getAlSetId() != null
                    && globalApAlSetsVO.getParamName() != null && !globalApAlSetsVO.getParamName().isEmpty()
                    && globalApAlSetsVO.getSensorType() != null && !globalApAlSetsVO.getSensorType().isEmpty()) {

                // Update in Cassandra
                try {
                    updateDelegate = new UpdateGlobalApAlSetsDelegate();
                    return updateDelegate.updateGlobalApAlSets(globalApAlSetsVO);
                } catch (IllegalArgumentException e) {
                    LOGGER.error(e.getMessage(), e);
                    throw e;
                } catch (Exception e) {
                    return "{\"outcome\":\"Error: Failed to update Global ApAlSets.\"}";
                }

            } else {
                return "{\"outcome\":\"insufficient data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Delete from global_ap_al_sets and global_ap_al_sets_by_customer tables for the given GlobalApAlSets
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String deleteGlobalApAlSets(String content) throws IllegalArgumentException {
        DeleteGlobalApAlSetsDelegate deleteDelegate;
        GlobalApAlSetsVO globalApAlSetsVO;

        if ((globalApAlSetsVO = JsonUtil.globalApAlSetsVOParser(content)) != null) {
            if (globalApAlSetsVO != null
                    && globalApAlSetsVO.getCustomerAccount() != null && !globalApAlSetsVO.getCustomerAccount().isEmpty()
                    && globalApAlSetsVO.getApSetId() != null
                    && globalApAlSetsVO.getAlSetId() != null
                    && globalApAlSetsVO.getParamName() != null && !globalApAlSetsVO.getParamName().isEmpty()) {

                // Delete from Cassandra
                try {
                    deleteDelegate = new DeleteGlobalApAlSetsDelegate();
                    return deleteDelegate.deleteGlobalApAlSets(globalApAlSetsVO);
                } catch (IllegalArgumentException e) {
                    LOGGER.error(e.getMessage(), e);
                    throw e;
                } catch (Exception e) {
                    return "{\"outcome\":\"Error: Failed to delete Global ApAlSets.\"}";
                }

            } else {
                return "{\"outcome\":\"Insufficient data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }
}
