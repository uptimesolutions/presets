/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.presets.entity.BearingCatalog;
import com.uptime.presets.delegate.CreateBearingCatalogDelegate;
import com.uptime.services.util.JsonConverterUtil;
import com.uptime.presets.delegate.DeleteBearingCatalogDelegate;
import com.uptime.presets.delegate.ReadBearingCatalogDelegate;
import com.uptime.presets.delegate.UpdateBearingCatalogDelegate;
import com.uptime.presets.utils.JsonUtil;
import com.uptime.presets.vo.BearingCatalogVO;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class BearingCatalogController {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(BearingCatalogController.class.getName());
    ReadBearingCatalogDelegate readDelegate;

    public BearingCatalogController() {
        readDelegate = new ReadBearingCatalogDelegate();
    }

    /**
     * Return a json of List of BearingCatalog Objects by the given params
     *
     * @param vendorId, String Object
     * @param bearingId, String Object
     * @return List of BearingCatalog Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByPK(String vendorId, String bearingId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<BearingCatalog> result;
        StringBuilder json;

        if ((result = readDelegate.findByPK(vendorId, bearingId)) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"vendorId\":\"").append(vendorId).append("\",")
                        .append("\"bearingId\":\"").append(bearingId).append("\",")
                        .append("\"BearingCatalog\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Bearing Catalog not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of List of BearingCatalog Objects by the given params
     *
     * @param vendorId, String Object
     * @return List of BearingCatalog Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByVendorId(String vendorId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<BearingCatalog> result;
        StringBuilder json;

        if ((result = readDelegate.findByVendorId(vendorId)) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"vendorId\":\"").append(vendorId).append("\",")
                        .append("\"BearingCatalog\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Bearing Catalog not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of List of String Objects of distinct vendor ids 
     *
     * @return List of String Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getDistinctVendorIds() throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<String> result;
        StringBuilder json;

        if ((result = readDelegate.findDistinctVendorIds()) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"vendorIds\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Vendor Ids not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Create a new BearingCatalog by inserting into bearing_catalog table
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String createBearingCatalog(String content) throws IllegalArgumentException {
        CreateBearingCatalogDelegate createDelegate;
        BearingCatalogVO bearingCatalogVO;

        if ((bearingCatalogVO = JsonUtil.bearingCatalogVOParser(content)) != null) {
            if (bearingCatalogVO != null
                    && bearingCatalogVO.getBearingId() != null && !bearingCatalogVO.getBearingId().isEmpty()
                    && bearingCatalogVO.getVendorId() != null && !bearingCatalogVO.getVendorId().isEmpty()) {

                // Insert into Cassandra
                try {
                    createDelegate = new CreateBearingCatalogDelegate();
                    return createDelegate.createBearingCatalog(bearingCatalogVO);
                } catch (IllegalArgumentException e) {
                    LOGGER.error(e.getMessage(), e);
                    throw e;
                } catch (Exception e) {
                    return "{\"outcome\":\"Error: Failed to create new Bearing Catalog.\"}";
                }

            } else {
                return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Update bearing_catalog table with the given BearingCatalog values
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String updateBearingCatalog(String content) throws IllegalArgumentException {
        UpdateBearingCatalogDelegate updateDelegate;
        BearingCatalogVO bearingCatalogVO;

        if ((bearingCatalogVO = JsonUtil.bearingCatalogVOParser(content)) != null) {
            if (bearingCatalogVO != null
                    && bearingCatalogVO.getBearingId() != null && !bearingCatalogVO.getBearingId().isEmpty()
                    && bearingCatalogVO.getVendorId() != null && !bearingCatalogVO.getVendorId().isEmpty()) {

                // Update in Cassandra
                try {
                    updateDelegate = new UpdateBearingCatalogDelegate();
                    return updateDelegate.updateBearingCatalog(bearingCatalogVO);
                } catch (IllegalArgumentException e) {
                    LOGGER.error(e.getMessage(), e);
                    throw e;
                } catch (Exception e) {
                    return "{\"outcome\":\"Error: Failed to update Bearing Catalog.\"}";
                }

            } else {
                return "{\"outcome\":\"insufficient data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Delete from bearing_catalog table for the given BearingCatalog
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String deleteBearingCatalog(String content) throws IllegalArgumentException {
        DeleteBearingCatalogDelegate deleteDelegate;

        BearingCatalogVO bearingCatalogVO;

        if ((bearingCatalogVO = JsonUtil.bearingCatalogVOParser(content)) != null) {
            if (bearingCatalogVO != null
                    && bearingCatalogVO.getBearingId() != null && !bearingCatalogVO.getBearingId().isEmpty()
                    && bearingCatalogVO.getVendorId() != null && !bearingCatalogVO.getVendorId().isEmpty()) {

                // Delete from Cassandra
                try {
                    deleteDelegate = new DeleteBearingCatalogDelegate();
                    return deleteDelegate.deleteBearingCatalog(bearingCatalogVO);
                } catch (IllegalArgumentException e) {
                    LOGGER.error(e.getMessage(), e);
                    throw e;
                } catch (Exception e) {
                    return "{\"outcome\":\"Error: Failed to delete Bearing Catalog.\"}";
                }

            } else {
                return "{\"outcome\":\"Insufficient data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }
}
