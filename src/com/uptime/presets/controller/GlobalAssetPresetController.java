/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import static com.uptime.services.ServiceConstants.DEFAULT_UPTIME_ACCOUNT;
import com.uptime.cassandra.presets.entity.GlobalAssetPreset;
import com.uptime.cassandra.presets.entity.GlobalDevicePreset;
import com.uptime.cassandra.presets.entity.GlobalTachometers;
import com.uptime.presets.delegate.CreateGlobalAssetPresetDelegate;
import com.uptime.presets.delegate.DeleteGlobalAssetPresetDelegate;
import com.uptime.presets.delegate.ReadGlobalAssetPresetDelegate;
import com.uptime.presets.delegate.ReadGlobalDevicePresetDelegate;
import com.uptime.presets.delegate.ReadGlobalTachometersDelegate;
import com.uptime.presets.delegate.UpdateGlobalAssetPresetDelegate;
import com.uptime.presets.utils.JsonUtil;
import com.uptime.presets.vo.AssetPresetVO;
import com.uptime.services.util.JsonConverterUtil;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author venky
 */
public class GlobalAssetPresetController {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalAssetPresetController.class.getName());
    private final ReadGlobalAssetPresetDelegate readDelegate;
    private final ReadGlobalDevicePresetDelegate readGlobalDevicePresetDelegate;
    private final ReadGlobalTachometersDelegate readGlobalTachometersDelegate;
    public GlobalAssetPresetController() {
        readDelegate = new ReadGlobalAssetPresetDelegate();
        readGlobalDevicePresetDelegate = new ReadGlobalDevicePresetDelegate();
        readGlobalTachometersDelegate = new ReadGlobalTachometersDelegate();
        
    }

    /**
     * Return a json of List of GlobalAssetPreset Objects by the given params
     *
     * @param customerAccount, String Object
     * @param assetTypeName, String Object
     * @param assetPresetId, String Object
     * @param pointLocationName, String Object
     *
     * @return List of GlobalAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByPK(String customerAccount, String assetTypeName, String assetPresetId, String pointLocationName) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<GlobalAssetPreset> result;
        StringBuilder json;

        if ((result = readDelegate.findByPK(customerAccount, assetTypeName, UUID.fromString(assetPresetId), pointLocationName)) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"assetTypeName\":\"").append(assetTypeName).append("\",")
                        .append("\"assetPresetId\":\"").append(assetPresetId).append("\",")
                        .append("\"pointLocationName\":\"").append(pointLocationName).append("\",")
                        .append("\"GlobalAssetPreset\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Global Asset Preset not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of List of GlobalAssetPreset Objects by the given params
     *
     * @param customerAccount, String Object
     * @param assetTypeName, String Object
     * @param assetPresetId, String Object
     * @return List of GlobalAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByCustomerAssetTypePresetId(String customerAccount, String assetTypeName, String assetPresetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<GlobalAssetPreset> result;
        StringBuilder json;

        if ((result = readDelegate.findByCustomerAssetTypePresetId(customerAccount, assetTypeName, UUID.fromString(assetPresetId))) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"assetTypeName\":\"").append(assetTypeName).append("\",")
                        .append("\"assetPresetId\":\"").append(assetPresetId).append("\",")
                        .append("\"GlobalAssetPreset\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Global Asset Preset not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of List of GlobalAssetPreset Objects by the given params
     *
     * @param customerAccount, String Object
     * @param assetTypeName, String Object
     * @return List of GlobalAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByCustomerAssetType(String customerAccount, String assetTypeName) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<GlobalAssetPreset> result;
        StringBuilder json;

        if ((result = readDelegate.findByCustomerAssetType(customerAccount, assetTypeName)) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"assetTypeName\":\"").append(assetTypeName).append("\",")
                        .append("\"GlobalAssetPreset\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Global Asset Preset not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }


    /**
     * Return a json of List of GlobalAssetPreset Objects by the given params
     *
     * @param customerAccount, String Object
     * @return List of GlobalAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByCustomer(String customerAccount) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<GlobalAssetPreset> result;
        GlobalAssetPreset globalAssetPreset;
        AssetPresetVO assetPresetVO;
        List<GlobalTachometers> tachometersByCustomer;
        List<GlobalDevicePreset> devicePresetByCustomer;
        StringBuilder json;

        if (null != (result = readDelegate.findByCustomer(customerAccount))) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"GlobalAssetPreset\":[");
                for (int i = 0; i < result.size(); i++) {

                    globalAssetPreset = result.get(i);
                    assetPresetVO = new AssetPresetVO(globalAssetPreset.getCustomerAccount(), null, globalAssetPreset.getAssetTypeName(), globalAssetPreset.getAssetPresetId(), globalAssetPreset.getPointLocationName(), globalAssetPreset.getAssetPresetName(),
                            globalAssetPreset.getDevicePresetId(), globalAssetPreset.getDevicePresetType(), globalAssetPreset.getSampleInterval(),
                            globalAssetPreset.getFfSetIds(), globalAssetPreset.getRollDiameter(), globalAssetPreset.getRollDiameterUnits(),
                            globalAssetPreset.getSpeedRatio(), globalAssetPreset.getTachId(), globalAssetPreset.getDescription(), null, null);
                    if (globalAssetPreset.getCustomerAccount() != null && globalAssetPreset.getTachId() != null && globalAssetPreset.getAssetPresetId() != null) {

                        tachometersByCustomer = readGlobalTachometersDelegate.findByPK(globalAssetPreset.getCustomerAccount(), globalAssetPreset.getTachId());
                        if (tachometersByCustomer != null && !tachometersByCustomer.isEmpty()) {
                            assetPresetVO.setTachName(tachometersByCustomer.get(0).getTachName());
                        } else {
                            tachometersByCustomer = readGlobalTachometersDelegate.findByPK(DEFAULT_UPTIME_ACCOUNT, globalAssetPreset.getTachId());
                            if (tachometersByCustomer != null && !tachometersByCustomer.isEmpty()) {
                                assetPresetVO.setTachName(tachometersByCustomer.get(0).getTachName());
                            }
                        }

                        devicePresetByCustomer = readGlobalDevicePresetDelegate.findByCustomerDeviceTypePreset(globalAssetPreset.getCustomerAccount(), globalAssetPreset.getDevicePresetType(), globalAssetPreset.getDevicePresetId());
                        if (devicePresetByCustomer != null && !devicePresetByCustomer.isEmpty()) {
                            assetPresetVO.setDevicePresetName(devicePresetByCustomer.get(0).getName());
                        } else {
                            devicePresetByCustomer = readGlobalDevicePresetDelegate.findByCustomerDeviceTypePreset(DEFAULT_UPTIME_ACCOUNT, globalAssetPreset.getDevicePresetType(), globalAssetPreset.getDevicePresetId());
                            if (devicePresetByCustomer != null && !devicePresetByCustomer.isEmpty()) {
                                assetPresetVO.setDevicePresetName(devicePresetByCustomer.get(0).getName());
                            }
                        }

                    }
                    json.append(JsonConverterUtil.toJSON(assetPresetVO)).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Global Asset Preset not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Create a new GlobalDevicePreset by inserting into global_device_preset
     * table
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String createGlobalAssetPreset(String content) throws IllegalArgumentException {
        CreateGlobalAssetPresetDelegate createDelegate;
        List<AssetPresetVO> assetPresetVOList;

        if ((assetPresetVOList = JsonUtil.assetPresetVOListParser(content)) != null && !assetPresetVOList.isEmpty()) {
            for (AssetPresetVO assetPresetVO : assetPresetVOList) {
                if (assetPresetVO == null
                        || assetPresetVO.getCustomerAccount() == null || assetPresetVO.getCustomerAccount().isEmpty()
                        || assetPresetVO.getAssetTypeName() == null || assetPresetVO.getAssetTypeName().isEmpty()
                        || assetPresetVO.getPointLocationName() == null || assetPresetVO.getPointLocationName().isEmpty()) {
                    return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
                }
            }

            // Insert into Cassandra
            try {
                createDelegate = new CreateGlobalAssetPresetDelegate();
                return createDelegate.createGlobalAssetPreset(assetPresetVOList);

            } catch (IllegalArgumentException e) {
                LOGGER.error(e.getMessage(), e);
                throw e;
            } catch (Exception e) {
                return "{\"outcome\":\"Error: Failed to create new Global Asset Preset.\"}";
            }

        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Update global_device_preset table with the given GlobalDevicePreset
     *
     * @param content, String Object
     * @return String Object
     */
    public String updateGlobalAssetPreset(String content) throws IllegalArgumentException {
        UpdateGlobalAssetPresetDelegate updateDelegate;
        List<AssetPresetVO> assetPresetVOList;

        if ((assetPresetVOList = JsonUtil.assetPresetVOListParser(content)) != null && !assetPresetVOList.isEmpty()) {
            for (AssetPresetVO assetPresetVO : assetPresetVOList) {
                if (assetPresetVO == null
                        || assetPresetVO.getCustomerAccount() == null || assetPresetVO.getCustomerAccount().isEmpty()
                        || assetPresetVO.getAssetTypeName() == null || assetPresetVO.getAssetTypeName().isEmpty()
                        || assetPresetVO.getAssetPresetId() == null
                        || assetPresetVO.getPointLocationName() == null || assetPresetVO.getPointLocationName().isEmpty()) {
                    return "{\"outcome\":\"insufficient data given in json\"}";
                }
            }

            // Update in Cassandra
            try {
                updateDelegate = new UpdateGlobalAssetPresetDelegate();
                return updateDelegate.updateGlobalAssetPreset(assetPresetVOList);
            } catch (IllegalArgumentException e) {
                LOGGER.error(e.getMessage(), e);
                throw e;
            } catch (Exception e) {
                return "{\"outcome\":\"Error: Failed to update Global Asset Preset.\"}";
            }

        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Delete rows from global_device_preset table for the given
     * GlobalDevicePreset
     *
     * @param content, String Object
     * @return String Object
     */
    public String deleteGlobalAssetPreset(String content) throws IllegalArgumentException {
        DeleteGlobalAssetPresetDelegate deleteDelegate;
        List<AssetPresetVO> assetPresetVOList;

        if ((assetPresetVOList = JsonUtil.assetPresetVOListParser(content)) != null && !assetPresetVOList.isEmpty()) {
            for (AssetPresetVO assetPresetVO : assetPresetVOList) {
                if (assetPresetVO == null
                        || assetPresetVO.getCustomerAccount() == null || assetPresetVO.getCustomerAccount().isEmpty()
                        || assetPresetVO.getAssetTypeName() == null || assetPresetVO.getAssetTypeName().isEmpty()
                        || assetPresetVO.getAssetPresetId() == null
                        || assetPresetVO.getPointLocationName() == null || assetPresetVO.getPointLocationName().isEmpty()) {
                    return "{\"outcome\":\"insufficient data given in json\"}";
                }
            }

            // Delete from Cassandra
            try {
                deleteDelegate = new DeleteGlobalAssetPresetDelegate();
                return deleteDelegate.deleteGlobalAssetPreset(assetPresetVOList);
            } catch (IllegalArgumentException e) {
                LOGGER.error(e.getMessage(), e);
                throw e;
            } catch (Exception e) {
                return "{\"outcome\":\"Error: Failed to delete Global Asset Preset.\"}";
            }

        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

}
