/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.presets.entity.GlobalApAlSetsByCustomer;
import com.uptime.cassandra.presets.entity.SiteApAlSetsByCustomer;
import com.uptime.cassandra.presets.entity.SiteDevicePreset;
import com.uptime.services.util.JsonConverterUtil;
import com.uptime.presets.delegate.CreateSiteDevicePresetDelegate;
import com.uptime.presets.delegate.DeleteSiteDevicePresetDelegate;
import com.uptime.presets.delegate.ReadGlobalApAlSetsDelegate;
import com.uptime.presets.delegate.ReadSiteApAlSetsDelegate;
import com.uptime.presets.delegate.ReadSiteDevicePresetDelegate;
import com.uptime.presets.delegate.UpdateSiteDevicePresetDelegate;
import com.uptime.presets.utils.JsonUtil;
import com.uptime.presets.vo.DevicePresetVO;
import com.uptime.services.ServiceConstants;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class SiteDevicePresetController {
    private static final Logger LOGGER = LoggerFactory.getLogger(SiteDevicePresetController.class.getName());
    ReadSiteDevicePresetDelegate readDelegate;
    ReadGlobalApAlSetsDelegate readGlobalApAlSetsDelegate;
    ReadSiteApAlSetsDelegate readSiteApAlSetsDelegate;

    public SiteDevicePresetController() {
        readDelegate = new ReadSiteDevicePresetDelegate();
        readGlobalApAlSetsDelegate = new ReadGlobalApAlSetsDelegate();
        readSiteApAlSetsDelegate = new ReadSiteApAlSetsDelegate();
    }

    /**
     * Return a json of a List of SiteDevicePreset Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @param deviceType, String Object
     * @param presetId, String Object
     * @param channelType, String Object
     * @param channelNumber, byte
     * @return List of SiteDevicePreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByPK(String customerAccount, String siteId, String deviceType, String presetId, String channelType, byte channelNumber) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<SiteDevicePreset> result;
        StringBuilder json;

        if ((result = readDelegate.findByPK(customerAccount, UUID.fromString(siteId), deviceType, UUID.fromString(presetId), channelType, channelNumber)) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"siteId\":\"").append(siteId).append("\",")
                        .append("\"deviceType\":\"").append(deviceType).append("\",")
                        .append("\"presetId\":\"").append(presetId).append("\",")
                        .append("\"channelType\":\"").append(channelType).append("\",")
                        .append("\"channelNumber\":\"").append(channelNumber).append("\",")
                        .append("\"SiteDevicePreset\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Site Device Preset not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of a List of SiteDevicePreset Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @param deviceType, String Object
     * @param presetId, String Object
     * @return List of SiteDevicePreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByCustomerSiteDeviceTypePreset(String customerAccount, String siteId, String deviceType, String presetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<SiteDevicePreset> result;
        StringBuilder json;

        if ((result = readDelegate.findByCustomerSiteDeviceTypePreset(customerAccount, UUID.fromString(siteId), deviceType, UUID.fromString(presetId))) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"siteId\":\"").append(siteId).append("\",")
                        .append("\"deviceType\":\"").append(deviceType).append("\",")
                        .append("\"presetId\":\"").append(presetId).append("\",")
                        .append("\"SiteDevicePreset\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Site Device Preset not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of a List of SiteDevicePreset Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @param deviceType, String Object
     * @return List of SiteDevicePreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByCustomerSiteDeviceType(String customerAccount, String siteId, String deviceType) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<SiteApAlSetsByCustomer> siteApAlSetsByCustomer;
        List<GlobalApAlSetsByCustomer> apAlSetsByCustomer;
        List<SiteDevicePreset> result;
        SiteDevicePreset siteDevicePreset;
        DevicePresetVO devicePresetVO;
        StringBuilder json;

        if ((result = readDelegate.findByCustomerSiteDeviceType(customerAccount, UUID.fromString(siteId), deviceType)) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"deviceType\":\"").append(deviceType).append("\",")
                        .append("\"siteId\":\"").append(siteId).append("\",")
                        .append("\"SiteDevicePreset\":[");
                for (int i = 0; i < result.size(); i++) {
                    siteDevicePreset = result.get(i);
                    devicePresetVO = new DevicePresetVO(siteDevicePreset.getCustomerAccount(), siteDevicePreset.getSiteId(),
                            siteDevicePreset.getDeviceType(), siteDevicePreset.getPresetId(), siteDevicePreset.getChannelType(),
                            siteDevicePreset.getChannelNumber(), siteDevicePreset.getApSetId(), siteDevicePreset.getAlSetId(),
                            siteDevicePreset.isAlarmEnabled(), siteDevicePreset.isAutoAcknowledge(),
                            siteDevicePreset.isDisabled(), siteDevicePreset.getName(), siteDevicePreset.getSampleInterval(),
                            siteDevicePreset.getSensorOffset(), siteDevicePreset.getSensorSensitivity(), siteDevicePreset.getSensorType(),
                            siteDevicePreset.getSensorUnits(), null, null, siteDevicePreset.getSensorSubType(), siteDevicePreset.getSensorLocalOrientation());
                    if (siteDevicePreset.getApSetId() != null && siteDevicePreset.getAlSetId() != null) {
                        if ((siteApAlSetsByCustomer = readSiteApAlSetsDelegate.findByPK(siteDevicePreset.getCustomerAccount(), siteDevicePreset.getSiteId(), siteDevicePreset.getSensorType(), siteDevicePreset.getApSetId(), siteDevicePreset.getAlSetId())) != null && siteApAlSetsByCustomer.size() > 0) {
                            devicePresetVO.setApSetName(siteApAlSetsByCustomer.get(0).getApSetName());
                            devicePresetVO.setAlSetName(siteApAlSetsByCustomer.get(0).getAlSetName());
                        } else if ((apAlSetsByCustomer = readGlobalApAlSetsDelegate.findByPK(siteDevicePreset.getCustomerAccount(), siteDevicePreset.getSensorType(), siteDevicePreset.getApSetId(), siteDevicePreset.getAlSetId())) != null && apAlSetsByCustomer.size() > 0) {
                            devicePresetVO.setApSetName(apAlSetsByCustomer.get(0).getApSetName());
                            devicePresetVO.setAlSetName(apAlSetsByCustomer.get(0).getAlSetName());
                        } else if ((apAlSetsByCustomer = readGlobalApAlSetsDelegate.findByPK(ServiceConstants.DEFAULT_UPTIME_ACCOUNT, siteDevicePreset.getSensorType(), siteDevicePreset.getApSetId(), siteDevicePreset.getAlSetId())) != null && apAlSetsByCustomer.size() > 0) {
                            devicePresetVO.setApSetName(apAlSetsByCustomer.get(0).getApSetName());
                            devicePresetVO.setAlSetName(apAlSetsByCustomer.get(0).getAlSetName());
                        }
                    }
                    json.append(JsonConverterUtil.toJSON(devicePresetVO)).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Site Device Preset not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of a List of SiteDevicePreset Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @return List of SiteDevicePreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByCustomerSite(String customerAccount, String siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<SiteApAlSetsByCustomer> siteApAlSetsByCustomer;
        List<GlobalApAlSetsByCustomer> apAlSetsByCustomer;
        List<SiteDevicePreset> result;
        SiteDevicePreset siteDevicePreset;
        DevicePresetVO devicePresetVO;
        StringBuilder json;

        if ((result = readDelegate.findByCustomerSite(customerAccount, UUID.fromString(siteId))) != null) {
            if (result.size() > 0) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"siteId\":\"").append(siteId).append("\",")
                        .append("\"SiteDevicePreset\":[");

                for (int i = 0; i < result.size(); i++) {
                    siteDevicePreset = result.get(i);
                    devicePresetVO = new DevicePresetVO(siteDevicePreset.getCustomerAccount(), siteDevicePreset.getSiteId(),
                            siteDevicePreset.getDeviceType(), siteDevicePreset.getPresetId(), siteDevicePreset.getChannelType(),
                            siteDevicePreset.getChannelNumber(), siteDevicePreset.getApSetId(), siteDevicePreset.getAlSetId(),
                            siteDevicePreset.isAlarmEnabled(), siteDevicePreset.isAutoAcknowledge(),
                            siteDevicePreset.isDisabled(), siteDevicePreset.getName(), siteDevicePreset.getSampleInterval(),
                            siteDevicePreset.getSensorOffset(), siteDevicePreset.getSensorSensitivity(), siteDevicePreset.getSensorType(),
                            siteDevicePreset.getSensorUnits(), null, null, siteDevicePreset.getSensorSubType(), siteDevicePreset.getSensorLocalOrientation());
                    if (siteDevicePreset.getApSetId() != null && siteDevicePreset.getAlSetId() != null) {
                        if ((siteApAlSetsByCustomer = readSiteApAlSetsDelegate.findByPK(siteDevicePreset.getCustomerAccount(), siteDevicePreset.getSiteId(), siteDevicePreset.getSensorType(), siteDevicePreset.getApSetId(), siteDevicePreset.getAlSetId())) != null && siteApAlSetsByCustomer.size() > 0) {
                            devicePresetVO.setApSetName(siteApAlSetsByCustomer.get(0).getApSetName());
                            devicePresetVO.setAlSetName(siteApAlSetsByCustomer.get(0).getAlSetName());
                        } else if ((apAlSetsByCustomer = readGlobalApAlSetsDelegate.findByPK(siteDevicePreset.getCustomerAccount(), siteDevicePreset.getSensorType(), siteDevicePreset.getApSetId(), siteDevicePreset.getAlSetId())) != null && apAlSetsByCustomer.size() > 0) {
                            devicePresetVO.setApSetName(apAlSetsByCustomer.get(0).getApSetName());
                            devicePresetVO.setAlSetName(apAlSetsByCustomer.get(0).getAlSetName());
                        } else if ((apAlSetsByCustomer = readGlobalApAlSetsDelegate.findByPK(ServiceConstants.DEFAULT_UPTIME_ACCOUNT, siteDevicePreset.getSensorType(), siteDevicePreset.getApSetId(), siteDevicePreset.getAlSetId())) != null && apAlSetsByCustomer.size() > 0) {
                            devicePresetVO.setApSetName(apAlSetsByCustomer.get(0).getApSetName());
                            devicePresetVO.setAlSetName(apAlSetsByCustomer.get(0).getAlSetName());
                        }
                    }
                    json.append(JsonConverterUtil.toJSON(devicePresetVO)).append(i < result.size() - 1 ? "," : "");
                }

                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Site Device Preset not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Create a new SiteDevicePreset by inserting into site_device_preset table
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String createSiteDevicePreset(String content) throws IllegalArgumentException {
        CreateSiteDevicePresetDelegate createDelegate;
        List<DevicePresetVO> devicePresetVOList;

        if ((devicePresetVOList = JsonUtil.devicePresetVOListParser(content)) != null && !devicePresetVOList.isEmpty()) {
            for (DevicePresetVO devicePresetVO : devicePresetVOList) {
                if (devicePresetVO == null
                        || devicePresetVO.getCustomerAccount() == null || devicePresetVO.getCustomerAccount().isEmpty()
                        || devicePresetVO.getSiteId() == null
                        || devicePresetVO.getDeviceType() == null || devicePresetVO.getDeviceType().isEmpty()
                        || devicePresetVO.getChannelType() == null || devicePresetVO.getChannelType().isEmpty()) {
                    return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
                }
            }

            // Insert into Cassandra
            try {
                createDelegate = new CreateSiteDevicePresetDelegate();
                return createDelegate.createSiteDevicePreset(devicePresetVOList);

            } catch (IllegalArgumentException e) {
                LOGGER.error(e.getMessage(), e);
                throw e;
            } catch (Exception e) {
                return "{\"outcome\":\"Error: Failed to create new Site Device Preset.\"}";
            }

        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Update site_device_preset table with the given SiteDevicePreset
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String updateSiteDevicePreset(String content) throws IllegalArgumentException {
        UpdateSiteDevicePresetDelegate updateDelegate;
        List<DevicePresetVO> devicePresetVOList;

        if (content != null && (devicePresetVOList = JsonUtil.devicePresetVOListParser(content)) != null && !devicePresetVOList.isEmpty()) {
            for (DevicePresetVO devicePresetVO : devicePresetVOList) {
                if (devicePresetVO == null
                        || devicePresetVO.getCustomerAccount() == null || devicePresetVO.getCustomerAccount().isEmpty()
                        || devicePresetVO.getSiteId() == null
                        || devicePresetVO.getDeviceType() == null || devicePresetVO.getDeviceType().isEmpty()
                        || devicePresetVO.getPresetId() == null
                        || devicePresetVO.getChannelType() == null || devicePresetVO.getChannelType().isEmpty()) {
                    return "{\"outcome\":\"insufficient data given in json\"}";
                }
            }

            // Update in Cassandra
            try {
                updateDelegate = new UpdateSiteDevicePresetDelegate();
                return updateDelegate.updateSiteDevicePreset(devicePresetVOList);
            } catch (IllegalArgumentException e) {
                LOGGER.error(e.getMessage(), e);
                throw e;
            } catch (Exception e) {
                return "{\"outcome\":\"Error: Failed to update Site Device Preset.\"}";
            }

        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Delete from site_device_preset table for the given SiteDevicePreset
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String deleteSiteDevicePreset(String content) throws IllegalArgumentException {
        DeleteSiteDevicePresetDelegate deleteDelegate;
        List<DevicePresetVO> devicePresetVOList;

        if (content != null && (devicePresetVOList = JsonUtil.devicePresetVOListParser(content)) != null && !devicePresetVOList.isEmpty()) {
            for (DevicePresetVO devicePresetVO : devicePresetVOList) {
                if (devicePresetVO == null
                        || devicePresetVO.getCustomerAccount() == null || devicePresetVO.getCustomerAccount().isEmpty()
                        || devicePresetVO.getSiteId() == null
                        || devicePresetVO.getDeviceType() == null || devicePresetVO.getDeviceType().isEmpty()
                        || devicePresetVO.getPresetId() == null
                        || devicePresetVO.getChannelType() == null || devicePresetVO.getChannelType().isEmpty()) {
                    return "{\"outcome\":\"insufficient data given in json\"}";
                }
            }

            // Delete from Cassandra
            try {
                deleteDelegate = new DeleteSiteDevicePresetDelegate();
                return deleteDelegate.deleteSiteDevicePreset(devicePresetVOList);
            } catch (IllegalArgumentException e) {
                LOGGER.error(e.getMessage(), e);
                throw e;
            } catch (Exception e) {
                return "{\"outcome\":\"Error: Failed to delete Site Device Preset.\"}";
            }

        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }
}
