/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.presets.entity.SiteApAlSets;
import com.uptime.cassandra.presets.entity.SiteApAlSetsByCustomer;
import com.uptime.services.util.JsonConverterUtil;
import com.uptime.presets.delegate.CreateSiteApAlSetsDelegate;
import com.uptime.presets.delegate.DeleteSiteApAlSetsDelegate;
import com.uptime.presets.delegate.ReadSiteApAlSetsDelegate;
import com.uptime.presets.delegate.UpdateSiteApAlSetsDelegate;
import com.uptime.presets.utils.JsonUtil;
import com.uptime.presets.utils.Utils;
import com.uptime.presets.vo.SiteApAlSetsVO;
import java.util.List;
import java.util.ArrayList;
import java.util.UUID;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class SiteApAlSetsController {
    private static final Logger LOGGER = LoggerFactory.getLogger(SiteApAlSetsController.class.getName());
    ReadSiteApAlSetsDelegate readDelegate;

    public SiteApAlSetsController() {
        readDelegate = new ReadSiteApAlSetsDelegate();
    }

    /**
     * Return a json of List of SiteApAlSets Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @param apSetId, String Object
     * @param alSetId, String Object
     * @param paramName, String Object
     * @return List of SiteApAlSets Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByPK(String customerAccount, String siteId, String apSetId, String alSetId, String paramName) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<SiteApAlSets> result;
        StringBuilder json;

        if ((result = readDelegate.findByPK(customerAccount, UUID.fromString(siteId), UUID.fromString(apSetId), UUID.fromString(alSetId), paramName)) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"siteId\":\"").append(siteId).append("\",")
                        .append("\"apSetId\":\"").append(apSetId).append("\",")
                        .append("\"alSetId\":\"").append(alSetId).append("\",")
                        .append("\"paramName\":\"").append(paramName).append("\",")
                        .append("\"SiteApAlSets\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Site ApAlSets not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of List of SiteApAlSets Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @param apSetId, String Object
     * @param alSetId, String Object
     * @return List of SiteApAlSets Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByCustomerSiteApSetAlSet(String customerAccount, String siteId, String apSetId, String alSetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<SiteApAlSets> result;
        StringBuilder json;

        if ((result = readDelegate.findByCustomerSiteApSetAlSet(customerAccount, UUID.fromString(siteId), UUID.fromString(apSetId), UUID.fromString(alSetId))) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"siteId\":\"").append(siteId).append("\",")
                        .append("\"apSetId\":\"").append(apSetId).append("\",")
                        .append("\"alSetId\":\"").append(alSetId).append("\",")
                        .append("\"SiteApAlSets\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Site ApAlSets not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of List of SiteApAlSets Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @param apSetId, String Object
     * @return List of SiteApAlSets Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByCustomerSiteApSet(String customerAccount, String siteId, String apSetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<SiteApAlSets> result;
        StringBuilder json;

        if ((result = readDelegate.findByCustomerSiteApSet(customerAccount, UUID.fromString(siteId), UUID.fromString(apSetId))) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"siteId\":\"").append(siteId).append("\",")
                        .append("\"apSetId\":\"").append(apSetId).append("\",")
                        .append("\"SiteApAlSets\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Site ApAlSets not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of List of SiteApAlSets Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @param apSetIds, String Object
     * @return List of SiteApAlSets Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByCustomerSiteApSets(String customerAccount, String siteId, String apSetIds) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<SiteApAlSets> result;
        StringBuilder json;
        List<UUID> apSetIdList = new ArrayList<>();
        String[] apSetIdArr = apSetIds.split(",");
        
        for (String apSetId : apSetIdArr) {
            apSetIdList.add(UUID.fromString(apSetId.trim()));
        }
        if ((result = readDelegate.findByCustomerSiteApSets(customerAccount, UUID.fromString(siteId), apSetIdList)) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"siteId\":\"").append(siteId).append("\",")
                        .append("\"apSetIds\":\"").append(apSetIds).append("\",")
                        .append("\"SiteApAlSets\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Site ApAlSets not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of List of SiteApAlSetsByCustomer Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @param sensorType, String Object
     * @param getAllData
     * @return List of SiteApAlSetsByCustomer Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByCustomerSiteSensorType(String customerAccount, String siteId, String sensorType, boolean getAllData) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<SiteApAlSetsByCustomer> result;
        StringBuilder json;
        if ((result = readDelegate.findByCustomerSiteSensorType(customerAccount, UUID.fromString(siteId), sensorType)) != null) {

            if (!result.isEmpty()) {
                if (!getAllData) {
                    result = result.stream().filter(Utils.distinctByKeys(SiteApAlSetsByCustomer::getCustomerAccount, SiteApAlSetsByCustomer::getSiteId, SiteApAlSetsByCustomer::getSensorType, SiteApAlSetsByCustomer::getApSetId, SiteApAlSetsByCustomer::getAlSetId)).collect(Collectors.toList());
                    json = new StringBuilder();
                    json
                            .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                            .append("\"siteId\":\"").append(siteId).append("\",")
                            .append("\"sensorType\":\"").append(sensorType).append("\",")
                            .append("\"SiteApAlSetsByCustomer\":[");
                    for (int i = 0; i < result.size(); i++) {
                        json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                    }
                    json.append("],\"outcome\":\"GET worked successfully.\"}");
                    return json.toString();
                } else {
                    List<SiteApAlSets> result1 = new ArrayList();
                    for (SiteApAlSetsByCustomer siteApAlSetsByCustomer : result) {
                        result1.addAll(readDelegate.findByCustomerSiteApSetAlSet(customerAccount, UUID.fromString(siteId), siteApAlSetsByCustomer.getApSetId(), siteApAlSetsByCustomer.getAlSetId()));
                    }
                    if (!result1.isEmpty()) {
                        json = new StringBuilder();
                        json
                                .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                                .append("\"siteId\":\"").append(siteId).append("\",")
                                .append("\"SiteApAlSets\":[");
                        for (int i = 0; i < result1.size(); i++) {
                            json.append(JsonConverterUtil.toJSON(result1.get(i))).append(i < result1.size() - 1 ? "," : "");
                        }
                        json.append("],\"outcome\":\"GET worked successfully.\"}");
                        return json.toString();
                    } else {
                        return "{\"outcome\":\"Site ApAlSets not found.\"}";
                    }
                }
            } else {
                return "{\"outcome\":\"Site ApAlSets By Customer not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Create a new SiteApAlSets by inserting into site_ap_al_sets table
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String createSiteApAlSets(String content) throws IllegalArgumentException {
        CreateSiteApAlSetsDelegate createDelegate;
        SiteApAlSetsVO siteApAlSetsVO;

        if ((siteApAlSetsVO = JsonUtil.siteApAlSetsVOParser(content)) != null) {
            if (siteApAlSetsVO != null
                    && siteApAlSetsVO.getCustomerAccount() != null && !siteApAlSetsVO.getCustomerAccount().isEmpty()
                    && siteApAlSetsVO.getSiteId() != null
                    && siteApAlSetsVO.getApSetName() != null && !siteApAlSetsVO.getApSetName().isEmpty()
                    && siteApAlSetsVO.getAlSetName() != null && !siteApAlSetsVO.getAlSetName().isEmpty()
                    && siteApAlSetsVO.getParamName() != null && !siteApAlSetsVO.getParamName().isEmpty()
                    && siteApAlSetsVO.getSensorType() != null && !siteApAlSetsVO.getSensorType().isEmpty()) {

                // Insert into Cassandra
                try {
                    createDelegate = new CreateSiteApAlSetsDelegate();
                    return createDelegate.createSiteApAlSets(siteApAlSetsVO);
                } catch (IllegalArgumentException e) {
                    LOGGER.error(e.getMessage(), e);
                    throw e;
                } catch (Exception e) {
                    return "{\"outcome\":\"Error: Failed to create new Site ApAlSetsVO.\"}";
                }

            } else {
                return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Update site_ap_al_sets table with the given SiteApAlSets
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String updateSiteApAlSets(String content) throws IllegalArgumentException {
        UpdateSiteApAlSetsDelegate updateDelegate;
        SiteApAlSetsVO siteApAlSetsVO;

        if ((siteApAlSetsVO = JsonUtil.siteApAlSetsVOParser(content)) != null) {
            if (siteApAlSetsVO != null
                    && siteApAlSetsVO.getCustomerAccount() != null && !siteApAlSetsVO.getCustomerAccount().isEmpty()
                    && siteApAlSetsVO.getSiteId() != null
                    && siteApAlSetsVO.getApSetId() != null
                    && siteApAlSetsVO.getAlSetId() != null
                    && siteApAlSetsVO.getParamName() != null && !siteApAlSetsVO.getParamName().isEmpty()
                    && siteApAlSetsVO.getSensorType() != null && !siteApAlSetsVO.getSensorType().isEmpty()) {

                // Update in Cassandra
                try {
                    updateDelegate = new UpdateSiteApAlSetsDelegate();
                    return updateDelegate.updateSiteApAlSets(siteApAlSetsVO);
                } catch (IllegalArgumentException e) {
                    LOGGER.error(e.getMessage(), e);
                    throw e;
                } catch (Exception e) {
                    return "{\"outcome\":\"Error: Failed to update Site ApAlSets.\"}";
                }

            } else {
                return "{\"outcome\":\"insufficient data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Delete rows from site_ap_al_sets table for the given SiteApAlSets
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String deleteSiteApAlSets(String content) throws IllegalArgumentException {
        DeleteSiteApAlSetsDelegate deleteDelegate;
        SiteApAlSetsVO siteApAlSetsVO;

        if ((siteApAlSetsVO = JsonUtil.siteApAlSetsVOParser(content)) != null) {
            if (siteApAlSetsVO != null
                    && siteApAlSetsVO.getCustomerAccount() != null && !siteApAlSetsVO.getCustomerAccount().isEmpty()
                    && siteApAlSetsVO.getSiteId() != null
                    && siteApAlSetsVO.getApSetId() != null
                    && siteApAlSetsVO.getAlSetId() != null
                    && siteApAlSetsVO.getParamName() != null && !siteApAlSetsVO.getParamName().isEmpty()) {

                // Delete from Cassandra
                try {
                    deleteDelegate = new DeleteSiteApAlSetsDelegate();
                    return deleteDelegate.deleteSiteApAlSets(siteApAlSetsVO);
                } catch (IllegalArgumentException e) {
                    LOGGER.error(e.getMessage(), e);
                    throw e;
                } catch (Exception e) {
                    return "{\"outcome\":\"Error: Failed to delete Site ApAlSets.\"}";
                }

            } else {
                return "{\"outcome\":\"Insufficient data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }
}
