/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.presets.entity.GlobalTachometers;
import com.uptime.cassandra.presets.entity.SiteTachometers;
import com.uptime.presets.delegate.ReadGlobalTachometersDelegate;
import com.uptime.presets.delegate.ReadSiteTachometersDelegate;
import com.uptime.services.util.JsonConverterUtil;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author kpati
 */
public class KafkaTachometersController {
    ReadSiteTachometersDelegate readSiteDelegate;
    ReadGlobalTachometersDelegate readGlobalDelegate;

    public KafkaTachometersController() {
        readSiteDelegate = new ReadSiteTachometersDelegate();
        readGlobalDelegate = new ReadGlobalTachometersDelegate();
    }

    /**
     * Return a json of a List of SiteTachometers or GlobalTachometers Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @param tachId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByPK(String customerAccount, String siteId, String tachId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        String response;

        if ((response = getSiteTachometerByPK(customerAccount, siteId, tachId)) == null && (response = getGlobalTachometerByPK(customerAccount, tachId)) == null) {
            response = "";
        }
        return response;
    }

    /**
     * Helper class for the method getByPK. Return a json of a List of SiteTachometers Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @param tachId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    private String getSiteTachometerByPK(String customerAccount, String siteId, String tachId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<SiteTachometers> result;
        StringBuilder json;

        if ((result = readSiteDelegate.findByPK(customerAccount, UUID.fromString(siteId), UUID.fromString(tachId))) != null && !result.isEmpty()) {
            json = new StringBuilder();
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            return json.toString();
        }
        return null;
    }

    /**
     * Helper class for the method getByPK. Return a json of a List of GlobalTachometers Objects by the given params
     *
     * @param customerAccount, String Object
     * @param tachId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    private String getGlobalTachometerByPK(String customerAccount, String tachId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<GlobalTachometers> result;
        StringBuilder json;

        if ((result = readGlobalDelegate.findByPK(customerAccount, UUID.fromString(tachId))) != null && !result.isEmpty()) {
            json = new StringBuilder();
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            return json.toString();
        }
        return null;
    }
}
