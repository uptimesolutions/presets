/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.presets.entity.GlobalPtLocationNames;
import com.uptime.services.util.JsonConverterUtil;
import com.uptime.presets.delegate.CreateGlobalPtLocationNamesDelegate;
import com.uptime.presets.delegate.DeleteGlobalPtLocationNamesDelegate;
import com.uptime.presets.delegate.ReadGlobalPtLocationNamesDelegate;
import com.uptime.presets.delegate.UpdateGlobalPtLocationNamesDelegate;
import com.uptime.presets.utils.JsonUtil;
import com.uptime.presets.vo.PtLocationNamesVO;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class GlobalPtLocationNamesController {
    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalPtLocationNamesController.class.getName());
    ReadGlobalPtLocationNamesDelegate readDelegate;

    public GlobalPtLocationNamesController() {
        readDelegate = new ReadGlobalPtLocationNamesDelegate();
    }

    /**
     * Return a json of a List of GlobalPtLocationNames Objects by the given params
     *
     * @param customerAccount, String Object
     * @param pointLocationName, String Object
     * @return List of GlobalPtLocationNames Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByPK(String customerAccount, String pointLocationName) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<GlobalPtLocationNames> result;
        StringBuilder json;

        if ((result = readDelegate.findByPK(customerAccount, pointLocationName)) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"pointLocationName\":\"").append(pointLocationName).append("\",")
                        .append("\"GlobalPtLocationNames\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"GlobalPtLocationNames not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of a List of GlobalPtLocationNames Objects by the given params
     *
     * @param customerAccount, String Object
     * @return List of GlobalPtLocationNames Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByCustomer(String customerAccount) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<GlobalPtLocationNames> result;
        StringBuilder json;

        if ((result = readDelegate.findByCustomer(customerAccount)) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"GlobalPtLocationNames\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"GlobalPtLocationNames not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Create a new GlobalPtLocationNames by inserting into global_pt_location_names table
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String createGlobalPtLocationNames(String content) throws IllegalArgumentException {
        CreateGlobalPtLocationNamesDelegate createDelegate;
        PtLocationNamesVO ptLocationNamesVO;

        if ((ptLocationNamesVO = JsonUtil.ptLocationNamesVOParser(content)) != null) {
            if (ptLocationNamesVO.getCustomerAccount() != null && !ptLocationNamesVO.getCustomerAccount().isEmpty()
                    && ptLocationNamesVO.getPointLocationName() != null && !ptLocationNamesVO.getPointLocationName().isEmpty()) {

                // Insert into Cassandra
                try {
                    createDelegate = new CreateGlobalPtLocationNamesDelegate();
                    return createDelegate.createGlobalPtLocationNames(ptLocationNamesVO);
                } catch (IllegalArgumentException e) {
                    LOGGER.error(e.getMessage(), e);
                    throw e;
                } catch (Exception e) {
                    return "{\"outcome\":\"Error: Failed to create new GlobalPtLocationNames.\"}";
                }

            } else {
                return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Update global_pt_location_names table with the given GlobalPtLocationNames
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String updateGlobalPtLocationNames(String content) throws IllegalArgumentException {
        UpdateGlobalPtLocationNamesDelegate updateDelegate;
        PtLocationNamesVO ptLocationNamesVO;

        if ((ptLocationNamesVO = JsonUtil.ptLocationNamesVOParser(content)) != null) {
            if (ptLocationNamesVO != null
                    && ptLocationNamesVO.getCustomerAccount() != null && !ptLocationNamesVO.getCustomerAccount().isEmpty()
                    && ptLocationNamesVO.getPointLocationName() != null && !ptLocationNamesVO.getPointLocationName().isEmpty()) {

                // Update in Cassandra
                try {
                    updateDelegate = new UpdateGlobalPtLocationNamesDelegate();
                    return updateDelegate.updateGlobalPtLocationNames(ptLocationNamesVO);
                } catch (IllegalArgumentException e) {
                    LOGGER.error(e.getMessage(), e);
                    throw e;
                } catch (Exception e) {
                    return "{\"outcome\":\"Error: Failed to update GlobalPtLocationNames.\"}";
                }

            } else {
                return "{\"outcome\":\"insufficient data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Delete the record from global_pt_location_names table for the given GlobalPtLocationNames
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String deleteGlobalPtLocationNames(String content) throws IllegalArgumentException {
        DeleteGlobalPtLocationNamesDelegate deleteDelegate;
        PtLocationNamesVO ptLocationNamesVO;

        if ((ptLocationNamesVO = JsonUtil.ptLocationNamesVOParser(content)) != null) {
            if (ptLocationNamesVO != null
                    && ptLocationNamesVO.getCustomerAccount() != null && !ptLocationNamesVO.getCustomerAccount().isEmpty()
                    && ptLocationNamesVO.getPointLocationName() != null && !ptLocationNamesVO.getPointLocationName().isEmpty()) {

                // Delete from Cassandra
                try {
                    deleteDelegate = new DeleteGlobalPtLocationNamesDelegate();
                    return deleteDelegate.deleteGlobalPtLocationNames(ptLocationNamesVO);
                } catch (IllegalArgumentException e) {
                    LOGGER.error(e.getMessage(), e);
                    throw e;
                } catch (Exception e) {
                    return "{\"outcome\":\"Error: Failed to delete GlobalPtLocationNames.\"}";
                }

            } else {
                return "{\"outcome\":\"Insufficient data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }
}
