/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.presets.entity.CustomerApSet;
import com.uptime.services.util.JsonConverterUtil;
import com.uptime.presets.delegate.CreateCustomerApSetDelegate;
import com.uptime.presets.delegate.DeleteCustomerApSetDelegate;
import com.uptime.presets.delegate.ReadCustomerApSetDelegate;
import com.uptime.presets.delegate.UpdateCustomerApSetDelegate;
import com.uptime.presets.utils.JsonUtil;
import com.uptime.presets.vo.CustomerApSetVO;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class CustomerApSetController {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerApSetController.class.getName());
    ReadCustomerApSetDelegate readDelegate;

    public CustomerApSetController() {
        readDelegate = new ReadCustomerApSetDelegate();
    }

    /**
     * Return a json of List of CustomerApSet Objects by the given params
     *
     * @param customerAccount, String Object
     * @param apSetId, String Object
     * @return List of CustomerApSet Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByPK(String customerAccount, String apSetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<CustomerApSet> result;
        StringBuilder json;

        if ((result = readDelegate.findByPK(customerAccount, UUID.fromString(apSetId))) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"apSetId\":\"").append(apSetId).append("\",")
                        .append("\"CustomerApSet\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Customer ApSet not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of List of CustomerApSet Objects by the given params
     *
     * @param customerAccount, String Object
     * @return List of CustomerApSet Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByCustomer(String customerAccount) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<CustomerApSet> result;
        StringBuilder json;

        if ((result = readDelegate.findByCustomer(customerAccount)) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"CustomerApSet\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Customer ApSet not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Create a new CustomerApSet by inserting into customer_ap_set table
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String createCustomerApSet(String content) throws IllegalArgumentException {
        CreateCustomerApSetDelegate createDelegate;
        CustomerApSetVO customerApSetVO;

        if ((customerApSetVO = JsonUtil.customerApSetVOParser(content)) != null) {
            if (customerApSetVO != null
                    && customerApSetVO.getCustomerAccount() != null && !customerApSetVO.getCustomerAccount().isEmpty()
                    && customerApSetVO.getApSetName() != null && !customerApSetVO.getApSetName().isEmpty()) {

                // Insert into Cassandra
                try {
                    createDelegate = new CreateCustomerApSetDelegate();
                    return createDelegate.createCustomerApSet(customerApSetVO);

                } catch (IllegalArgumentException e) {
                    LOGGER.error(e.getMessage(), e);
                    throw e;
                } catch (Exception e) {
                    return "{\"outcome\":\"Error: Failed to create new Customer ApSet.\"}";
                }

            } else {
                return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Update customer_ap_set table with the given CustomerApSet values
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String updateCustomerApSet(String content) throws IllegalArgumentException {
        UpdateCustomerApSetDelegate updateDelegate;
        CustomerApSetVO customerApSetVO;

        if ((customerApSetVO = JsonUtil.customerApSetVOParser(content)) != null) {
            if (customerApSetVO != null
                    && customerApSetVO.getCustomerAccount() != null && !customerApSetVO.getCustomerAccount().isEmpty()
                    && customerApSetVO.getApSetId() != null) {

                // Update in Cassandra
                try {
                    updateDelegate = new UpdateCustomerApSetDelegate();
                    return updateDelegate.updateCustomerApSet(customerApSetVO);
                } catch (IllegalArgumentException e) {
                    LOGGER.error(e.getMessage(), e);
                    throw e;
                } catch (Exception e) {
                    return "{\"outcome\":\"Error: Failed to update Customer ApSet.\"}";
                }

            } else {
                return "{\"outcome\":\"insufficient data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Delete from customer_ap_set table for the given CustomerApSet
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String deleteCustomerApSet(String content) throws IllegalArgumentException {
        DeleteCustomerApSetDelegate deleteDelegate;
        CustomerApSetVO customerApSetVO;

        if ((customerApSetVO = JsonUtil.customerApSetVOParser(content)) != null) {
            if (customerApSetVO != null
                    && customerApSetVO.getCustomerAccount() != null && !customerApSetVO.getCustomerAccount().isEmpty()
                    && customerApSetVO.getApSetId() != null) {

                // Delete from Cassandra
                try {
                    deleteDelegate = new DeleteCustomerApSetDelegate();
                    return deleteDelegate.deleteCustomerApSet(customerApSetVO);
                } catch (IllegalArgumentException e) {
                    LOGGER.error(e.getMessage(), e);
                    throw e;
                } catch (Exception e) {
                    return "{\"outcome\":\"Error: Failed to delete Customer ApSet.\"}";
                }

            } else {
                return "{\"outcome\":\"Insufficient data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }
}
