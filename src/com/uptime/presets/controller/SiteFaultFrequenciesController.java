/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.presets.entity.SiteFaultFrequencies;
import com.uptime.services.util.JsonConverterUtil;
import com.uptime.presets.delegate.CreateSiteFaultFrequenciesDelegate;
import com.uptime.presets.delegate.DeleteSiteFaultFrequenciesDelegate;
import com.uptime.presets.delegate.ReadSiteFaultFrequenciesDelegate;
import com.uptime.presets.delegate.UpdateSiteFaultFrequenciesDelegate;
import com.uptime.presets.utils.JsonUtil;
import com.uptime.presets.vo.SiteFaultFrequenciesVO;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class SiteFaultFrequenciesController {
    private static final Logger LOGGER = LoggerFactory.getLogger(SiteFaultFrequenciesController.class.getName());
    ReadSiteFaultFrequenciesDelegate readDelegate;

    public SiteFaultFrequenciesController() {
        readDelegate = new ReadSiteFaultFrequenciesDelegate();
    }

    /**
     * Return a json of List of SiteFaultFrequencies Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @param ffSetId, String Object
     * @param ffId, String Object
     * @return List of SiteFaultFrequencies Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByPK(String customerAccount, String siteId, String ffSetId, String ffId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<SiteFaultFrequencies> result;
        StringBuilder json;

        if ((result = readDelegate.findByPK(customerAccount, UUID.fromString(siteId), UUID.fromString(ffSetId), UUID.fromString(ffId))) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"siteId\":\"").append(siteId).append("\",")
                        .append("\"ffSetId\":\"").append(ffSetId).append("\",")
                        .append("\"ffId\":\"").append(ffId).append("\",")
                        .append("\"SiteFaultFrequencies\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Site Fault Frequencies not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of List of SiteFaultFrequencies Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @param ffSetId, String Object
     * @return List of SiteFaultFrequencies Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String findByCustomerSiteSetId(String customerAccount, String siteId, String ffSetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<SiteFaultFrequencies> result;
        StringBuilder json;

        if ((result = readDelegate.findByCustomerSiteSetName(customerAccount, UUID.fromString(siteId), UUID.fromString(ffSetId))) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"siteId\":\"").append(siteId).append("\",")
                        .append("\"ffSetId\":\"").append(ffSetId).append("\",")
                        .append("\"SiteFaultFrequencies\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Site Fault Frequencies not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of List of SiteFaultFrequencies Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @return List of SiteFaultFrequencies Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String findByCustomerSiteId(String customerAccount, String siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<SiteFaultFrequencies> result;
        StringBuilder json;

        if ((result = readDelegate.findByCustomerSite(customerAccount, UUID.fromString(siteId))) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"siteId\":\"").append(siteId).append("\",")
                        .append("\"SiteFaultFrequencies\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Site Fault Frequencies not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Create a new SiteFaultFrequencies by inserting into multiple tables
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String createSiteFaultFrequencies(String content) throws IllegalArgumentException {
        CreateSiteFaultFrequenciesDelegate createDelegate;
        List<SiteFaultFrequenciesVO> faultFrequenciesVOList;

        if (content != null && (faultFrequenciesVOList = JsonUtil.siteFaultFrequenciesParser(content)) != null) {
            for (SiteFaultFrequenciesVO faultFrequenciesVO : faultFrequenciesVOList) {
                if (faultFrequenciesVO == null
                        || faultFrequenciesVO.getCustomerAccount() == null || faultFrequenciesVO.getCustomerAccount().isEmpty()
                        || faultFrequenciesVO.getSiteId() == null
                        || faultFrequenciesVO.getFfSetName() == null || faultFrequenciesVO.getFfSetName().isEmpty()
                        || faultFrequenciesVO.getFfName() == null || faultFrequenciesVO.getFfName().isEmpty()) {
                    return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
                }
            }

            // Insert into Cassandra
            try {
                createDelegate = new CreateSiteFaultFrequenciesDelegate();
                return createDelegate.createFaultFrequencies(faultFrequenciesVOList);
            } catch (IllegalArgumentException e) {
                LOGGER.error(e.getMessage(), e);
                throw e;
            } catch (Exception e) {
                return "{\"outcome\":\"Error: Failed to create new Site Fault FrequenciesVO.\"}";
            }

        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Update tables dealing with the given SiteFaultFrequencies
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String updateSiteFaultFrequencies(String content) throws IllegalArgumentException {
        UpdateSiteFaultFrequenciesDelegate updateDelegate;
        List<SiteFaultFrequenciesVO> faultFrequenciesVOList;

        if (content != null && (faultFrequenciesVOList = JsonUtil.siteFaultFrequenciesParser(content)) != null) {
            for (SiteFaultFrequenciesVO faultFrequenciesVO : faultFrequenciesVOList) {
                if (faultFrequenciesVO == null
                        || faultFrequenciesVO.getCustomerAccount() == null || faultFrequenciesVO.getCustomerAccount().isEmpty()
                        || faultFrequenciesVO.getSiteId() == null || faultFrequenciesVO.getFfSetId() == null
                        || faultFrequenciesVO.getFfId() == null) {
                    return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
                }
            }

            // Update in Cassandra
            try {
                updateDelegate = new UpdateSiteFaultFrequenciesDelegate();
                return updateDelegate.updateFaultFrequencies(faultFrequenciesVOList);
            } catch (IllegalArgumentException e) {
                LOGGER.error(e.getMessage(), e);
                throw e;
            } catch (Exception e) {
                return "{\"outcome\":\"Error: Failed to update Site Fault FrequenciesVO.\"}";
            }

        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Delete tables dealing with the given SiteFaultFrequencies
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String deleteSiteFaultFrequencies(String content) throws IllegalArgumentException {
        DeleteSiteFaultFrequenciesDelegate deleteDelegate;
        List<SiteFaultFrequenciesVO> faultFrequenciesVOList;

        if ((faultFrequenciesVOList = JsonUtil.siteFaultFrequenciesParser(content)) != null) {
            for (SiteFaultFrequenciesVO faultFrequenciesVO : faultFrequenciesVOList) {
                if (faultFrequenciesVO == null
                        || faultFrequenciesVO.getCustomerAccount() == null || faultFrequenciesVO.getCustomerAccount().isEmpty()
                        || faultFrequenciesVO.getSiteId() == null || faultFrequenciesVO.getFfSetId() == null
                        || faultFrequenciesVO.getFfId() == null) {
                    return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
                }
            }

            // Delete from Cassandra
            try {
                deleteDelegate = new DeleteSiteFaultFrequenciesDelegate();
                return deleteDelegate.deleteFaultFrequencies(faultFrequenciesVOList);

            } catch (IllegalArgumentException e) {
                LOGGER.error(e.getMessage(), e);
                throw e;
            } catch (Exception e) {
                return "{\"outcome\":\"Error: Failed to delete Site Fault FrequenciesVO.\"}";
            }

        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }
}
