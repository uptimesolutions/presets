/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.presets.entity.GlobalFaultFrequencies;
import com.uptime.services.util.JsonConverterUtil;
import com.uptime.presets.delegate.CreateGlobalFaultFrequenciesDelegate;
import com.uptime.presets.delegate.DeleteGlobalFaultFrequenciesDelegate;
import com.uptime.presets.delegate.ReadGlobalFaultFrequenciesDelegate;
import com.uptime.presets.delegate.UpdateGlobalFaultFrequenciesDelegate;
import com.uptime.presets.utils.JsonUtil;
import com.uptime.presets.vo.GlobalFaultFrequenciesVO;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class GlobalFaultFrequenciesController {
    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalFaultFrequenciesController.class.getName());
    ReadGlobalFaultFrequenciesDelegate readDelegate;

    public GlobalFaultFrequenciesController() {
        readDelegate = new ReadGlobalFaultFrequenciesDelegate();
    }

    /**
     * Return a json of List of GlobalFaultFrequencies Objects by the given params
     *
     * @param customerAccount, String Object
     * @param ffSetId, String Object
     * @param ffId, String Object
     * @return List of GlobalFaultFrequencies Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByPK(String customerAccount, String ffSetId, String ffId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<GlobalFaultFrequencies> result;
        StringBuilder json;

        if ((result = readDelegate.findByPK(customerAccount, UUID.fromString(ffSetId), UUID.fromString(ffId))) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"ffSetId\":\"").append(ffSetId).append("\",")
                        .append("\"ffId\":\"").append(ffId).append("\",")
                        .append("\"GlobalFaultFrequencies\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Global Fault Frequencies not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of List of GlobalFaultFrequencies Objects by the given params
     *
     * @param customerAccount, String Object
     * @param ffSetId, String Object
     * @return List of GlobalFaultFrequencies Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByCustomerSetId(String customerAccount, String ffSetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<GlobalFaultFrequencies> result;
        StringBuilder json;

        if ((result = readDelegate.findByCustomerSetId(customerAccount, UUID.fromString(ffSetId))) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"ffSetId\":\"").append(ffSetId).append("\",")
                        .append("\"GlobalFaultFrequencies\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Global Fault Frequencies not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of List of GlobalFaultFrequencies Objects by the given params
     *
     * @param customerAccount, String Object
     * @return List of GlobalFaultFrequencies Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByCustomer(String customerAccount) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<GlobalFaultFrequencies> result;
        StringBuilder json;

        if ((result = readDelegate.findByCustomer(customerAccount)) != null) {
            if (result.size() > 0) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"GlobalFaultFrequencies\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Global Fault Frequencies not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Create new GlobalFaultFrequencies by inserting into global_fault_frequencies table
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String createGlobalFaultFrequencies(String content) throws IllegalArgumentException {
        CreateGlobalFaultFrequenciesDelegate createDelegate;
        List<GlobalFaultFrequenciesVO> faultFrequenciesVOList;

        if (content != null && (faultFrequenciesVOList = JsonUtil.globalFaultFrequenciesParser(content)) != null) {
            for (GlobalFaultFrequenciesVO faultFrequenciesVO : faultFrequenciesVOList) {
                if (faultFrequenciesVO == null
                        || faultFrequenciesVO.getCustomerAccount() == null || faultFrequenciesVO.getCustomerAccount().isEmpty()
                        || faultFrequenciesVO.getFfSetName() == null || faultFrequenciesVO.getFfSetName().isEmpty()
                        || faultFrequenciesVO.getFfName() == null || faultFrequenciesVO.getFfName().isEmpty()) {
                    return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
                }
            }

            // Insert into Cassandra
            try {
                createDelegate = new CreateGlobalFaultFrequenciesDelegate();
                return createDelegate.createGlobalFaultFrequencies(faultFrequenciesVOList);

            } catch (IllegalArgumentException e) {
                LOGGER.error(e.getMessage(), e);
                throw e;
            } catch (Exception e) {
                return "{\"outcome\":\"Error: Failed to create new Global Fault FrequenciesVO.\"}";
            }

        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Update global_fault_frequencies table with the given GlobalFaultFrequencies
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String updateGlobalFaultFrequencies(String content) throws IllegalArgumentException {
        UpdateGlobalFaultFrequenciesDelegate updateDelegate;
        List<GlobalFaultFrequenciesVO> faultFrequenciesVOList;

        if (content != null && (faultFrequenciesVOList = JsonUtil.globalFaultFrequenciesParser(content)) != null) {
            for (GlobalFaultFrequenciesVO faultFrequenciesVO : faultFrequenciesVOList) {
                if (faultFrequenciesVO == null
                        || faultFrequenciesVO.getCustomerAccount() == null || faultFrequenciesVO.getCustomerAccount().isEmpty()
                        || faultFrequenciesVO.getFfSetId() == null || faultFrequenciesVO.getFfId() == null) {
                    return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
                }
            }

            // Update in Cassandra
            try {
                updateDelegate = new UpdateGlobalFaultFrequenciesDelegate();
                return updateDelegate.updateGlobalFaultFrequencies(faultFrequenciesVOList);
            } catch (IllegalArgumentException e) {
                LOGGER.error(e.getMessage(), e);
                throw e;
            } catch (Exception e) {
                return "{\"outcome\":\"Error: Failed to update Global Fault FrequenciesVO.\"}";
            }

        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Delete from global_fault_frequencies table for the given GlobalFaultFrequencies
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String deleteGlobalFaultFrequencies(String content) throws IllegalArgumentException {
        DeleteGlobalFaultFrequenciesDelegate deleteDelegate;
        List<GlobalFaultFrequenciesVO> faultFrequenciesVOList;

        if ((faultFrequenciesVOList = JsonUtil.globalFaultFrequenciesParser(content)) != null) {
            for (GlobalFaultFrequenciesVO faultFrequenciesVO : faultFrequenciesVOList) {
                if (faultFrequenciesVO == null
                        || faultFrequenciesVO.getCustomerAccount() == null || faultFrequenciesVO.getCustomerAccount().isEmpty()
                        || faultFrequenciesVO.getFfSetId() == null || faultFrequenciesVO.getFfId() == null) {
                    return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
                }
            }

            // Delete from Cassandra
            try {
                deleteDelegate = new DeleteGlobalFaultFrequenciesDelegate();
                return deleteDelegate.deleteGlobalFaultFrequencies(faultFrequenciesVOList);
            } catch (IllegalArgumentException e) {
                LOGGER.error(e.getMessage(), e);
                throw e;
            } catch (Exception e) {
                return "{\"outcome\":\"Error: Failed to delete Global Fault FrequenciesVO.\"}";
            }

        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }
}
