/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.presets.entity.GlobalTachometers;
import com.uptime.services.util.JsonConverterUtil;
import com.uptime.presets.delegate.CreateGlobalTachometersDelegate;
import com.uptime.presets.delegate.DeleteGlobalTachometersDelegate;
import com.uptime.presets.delegate.ReadGlobalTachometersDelegate;
import com.uptime.presets.delegate.UpdateGlobalTachometersDelegate;
import com.uptime.presets.utils.JsonUtil;
import com.uptime.presets.vo.TachometersVO;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class GlobalTachometersController {
    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalTachometersController.class.getName());
    ReadGlobalTachometersDelegate readDelegate;

    public GlobalTachometersController() {
        readDelegate = new ReadGlobalTachometersDelegate();
    }

    /**
     * Return a json of a List of Tachometers Objects from global_tachometers table by the given params
     *
     * @param customerAccount, String Object
     * @param tachId, String Object
     * @return List of Tachometers Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByPK(String customerAccount, String tachId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<GlobalTachometers> result;
        StringBuilder json;

        if ((result = readDelegate.findByPK(customerAccount, UUID.fromString(tachId))) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"tachId\":\"").append(tachId).append("\",")
                        .append("\"GlobalTachometers\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Global Tachometers not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of a List of Tachometers Objects from global_tachometers table by the given params
     *
     * @param customerAccount, String Object
     * @return List of Tachometers Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByCustomer(String customerAccount) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<GlobalTachometers> result;
        StringBuilder json;

        if ((result = readDelegate.findByCustomer(customerAccount)) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"GlobalTachometers\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Global Tachometers not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Create a new Tachometers by inserting into global_tachometers table
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String createGlobalTachometers(String content) throws IllegalArgumentException {
        CreateGlobalTachometersDelegate createDelegate;
        TachometersVO tachometersVO;

        if ((tachometersVO = JsonUtil.globalTachometersParser(content)) != null) {
            if (tachometersVO != null
                    && tachometersVO.getCustomerAccount() != null && !tachometersVO.getCustomerAccount().isEmpty()
                    && tachometersVO.getTachType() != null && !tachometersVO.getTachType().isEmpty()
                    && tachometersVO.getTachName() != null && !tachometersVO.getTachName().isEmpty()) {

                // Insert into Cassandra
                try {
                    createDelegate = new CreateGlobalTachometersDelegate();
                    return createDelegate.CreateGlobalTachometers(tachometersVO);

                } catch (IllegalArgumentException e) {
                    LOGGER.error(e.getMessage(), e);
                    throw e;
                } catch (Exception e) {
                    return "{\"outcome\":\"Error: Failed to create new Global Tachometers.\"}";
                }

            } else {
                return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Update global_tachometers table with the given Tachometers
     *
     * @param content, String Object
     * @return String Object
     */
    public String updateGlobalTachometers(String content) throws IllegalArgumentException {
        UpdateGlobalTachometersDelegate updateDelegate;
        TachometersVO tachometersVO;

        if ((tachometersVO = JsonUtil.globalTachometersParser(content)) != null) {
            if (tachometersVO != null
                    && tachometersVO.getCustomerAccount() != null && !tachometersVO.getCustomerAccount().isEmpty()
                    && tachometersVO.getTachId() != null) {

                // Update in Cassandra
                try {
                    updateDelegate = new UpdateGlobalTachometersDelegate();
                    return updateDelegate.updateGlobalTachometers(tachometersVO);
                } catch (IllegalArgumentException e) {
                    LOGGER.error(e.getMessage(), e);
                    throw e;
                } catch (Exception e) {
                    return "{\"outcome\":\"Error: Failed to update Global TachometersVO.\"}";
                }

            } else {
                return "{\"outcome\":\"insufficient data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Delete from global_tachometers for the given Tachometers
     *
     * @param content, String Object
     * @return String Object
     */
    public String deleteGlobalTachometers(String content) throws IllegalArgumentException {
        DeleteGlobalTachometersDelegate deleteDelegate;
        TachometersVO tachometersVO;

        if ((tachometersVO = JsonUtil.globalTachometersParser(content)) != null) {
            if (tachometersVO != null
                    && tachometersVO.getCustomerAccount() != null && !tachometersVO.getCustomerAccount().isEmpty()
                    && tachometersVO.getTachId() != null) {

                // Delete from Cassandra
                try {
                    deleteDelegate = new DeleteGlobalTachometersDelegate();
                    return deleteDelegate.deleteGlobalTachometers(tachometersVO);
                } catch (IllegalArgumentException e) {
                    LOGGER.error(e.getMessage(), e);
                    throw e;
                } catch (Exception e) {
                    return "{\"outcome\":\"Error: Failed to delete Global Tachometers.\"}";
                }

            } else {
                return "{\"outcome\":\"Insufficient data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }
}
