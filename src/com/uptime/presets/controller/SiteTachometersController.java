/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.presets.entity.SiteTachometers;
import com.uptime.services.util.JsonConverterUtil;
import com.uptime.presets.delegate.CreateSiteTachometersDelegate;
import com.uptime.presets.delegate.DeleteSiteTachometersDelegate;
import com.uptime.presets.delegate.ReadSiteTachometersDelegate;
import com.uptime.presets.delegate.UpdateSiteTachometersDelegate;
import com.uptime.presets.utils.JsonUtil;
import com.uptime.presets.vo.TachometersVO;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class SiteTachometersController {
    private static final Logger LOGGER = LoggerFactory.getLogger(SiteTachometersController.class.getName());
    ReadSiteTachometersDelegate readDelegate;

    public SiteTachometersController() {
        readDelegate = new ReadSiteTachometersDelegate();
    }

    /**
     * Return a json of a List of SiteTachometers Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @param tachId, String Object
     * @return List of SiteTachometers Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByPK(String customerAccount, String siteId, String tachId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<SiteTachometers> result;
        StringBuilder json;

        if ((result = readDelegate.findByPK(customerAccount, UUID.fromString(siteId), UUID.fromString(tachId))) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                    .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"tachId\":\"").append(tachId).append("\",")
                    .append("\"SiteTachometers\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Site Tachometers not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of a List of SiteTachometers Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @return List of SiteTachometers Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByCustomerSite(String customerAccount, String siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<SiteTachometers> result;
        StringBuilder json;

        if ((result = readDelegate.findByCustomerSite(customerAccount, UUID.fromString(siteId))) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                    .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"SiteTachometers\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Site Tachometers not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Create new SiteTachometers by inserting into site_tachometers table
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String createSiteTachometers(String content) throws IllegalArgumentException {
        CreateSiteTachometersDelegate createDelegate;
        TachometersVO tachometersVO;

        if ((tachometersVO = JsonUtil.siteTachometersParser(content)) != null) {
            if (tachometersVO != null
                    && tachometersVO.getCustomerAccount() != null && !tachometersVO.getCustomerAccount().isEmpty()
                    && tachometersVO.getSiteId() != null
                    && tachometersVO.getTachType() != null && !tachometersVO.getTachType().isEmpty()
                    && tachometersVO.getTachName() != null && !tachometersVO.getTachName().isEmpty()) {

                // Insert into Cassandra
                try {
                    createDelegate = new CreateSiteTachometersDelegate();
                    return createDelegate.CreateSiteTachometers(tachometersVO);

                } catch (IllegalArgumentException e) {
                    LOGGER.error(e.getMessage(), e);
                    throw e;
                } catch (Exception e) {
                    return "{\"outcome\":\"Error: Failed to create new Site Tachometers.\"}";
                }

            } else {
                return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Update site_tachometers table with the given SiteTachometers
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String updateSiteTachometers(String content) throws IllegalArgumentException {
        UpdateSiteTachometersDelegate updateDelegate;
        TachometersVO tachometersVO;

        if ((tachometersVO = JsonUtil.siteTachometersParser(content)) != null) {
            if (tachometersVO != null
                    && tachometersVO.getCustomerAccount() != null && !tachometersVO.getCustomerAccount().isEmpty()
                    && tachometersVO.getSiteId() != null
                    && tachometersVO.getTachId() != null) {

                // Update in Cassandra
                try {
                    updateDelegate = new UpdateSiteTachometersDelegate();
                    return updateDelegate.updateSiteTachometers(tachometersVO);
                } catch (IllegalArgumentException e) {
                    LOGGER.error(e.getMessage(), e);
                    throw e;
                } catch (Exception e) {
                    return "{\"outcome\":\"Error: Failed to update Site TachometersVO.\"}";
                }

            } else {
                return "{\"outcome\":\"insufficient data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Delete from site_tachometers table for the given SiteTachometers
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String deleteSiteTachometers(String content) throws IllegalArgumentException {
        DeleteSiteTachometersDelegate deleteDelegate;
        TachometersVO tachometersVO;

        if ((tachometersVO = JsonUtil.siteTachometersParser(content)) != null) {
            if (tachometersVO != null
                    && tachometersVO.getCustomerAccount() != null && !tachometersVO.getCustomerAccount().isEmpty()
                    && tachometersVO.getSiteId() != null
                    && tachometersVO.getTachId() != null) {

                // Delete from Cassandra
                try {
                    deleteDelegate = new DeleteSiteTachometersDelegate();
                    return deleteDelegate.deleteSiteTachometers(tachometersVO);
                } catch (IllegalArgumentException e) {
                    LOGGER.error(e.getMessage(), e);
                    throw e;
                } catch (Exception e) {
                    return "{\"outcome\":\"Error: Failed to delete Site Tachometers.\"}";
                }

            } else {
                return "{\"outcome\":\"Insufficient data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }
}
