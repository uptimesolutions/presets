/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.presets.entity.SiteBearingFavorites;
import com.uptime.services.util.JsonConverterUtil;
import com.uptime.presets.delegate.CreateSiteBearingFavoritesDelegate;
import com.uptime.presets.delegate.DeleteSiteBearingFavoritesDelegate;
import com.uptime.presets.delegate.ReadSiteBearingFavoritesDelegate;
import com.uptime.presets.delegate.UpdateSiteBearingFavoritesDelegate;
import com.uptime.presets.utils.JsonUtil;
import com.uptime.presets.vo.SiteBearingFavoritesVO;
import com.uptime.presets.vo.SiteFavoritesEncapVO;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class SiteBearingFavoritesController {
    private static final Logger LOGGER = LoggerFactory.getLogger(SiteBearingFavoritesController.class.getName());
    ReadSiteBearingFavoritesDelegate readDelegate;

    public SiteBearingFavoritesController() {
        readDelegate = new ReadSiteBearingFavoritesDelegate();
    }

    /**
     * Return a json of List of SiteBearingFavorites Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @param vendorId, String Object
     * @param bearingId, String Object
     * @return List of SiteBearingFavorites Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByPK(String customerAccount, String siteId, String vendorId, String bearingId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<SiteBearingFavorites> result;
        StringBuilder json;

        if ((result = readDelegate.findByPK(customerAccount, UUID.fromString(siteId), vendorId, bearingId)) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"siteId\":\"").append(siteId).append("\",")
                        .append("\"vendorId\":\"").append(vendorId).append("\",")
                        .append("\"bearingId\":\"").append(bearingId).append("\",")
                        .append("\"SiteBearingFavorites\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"SiteBearingFavorites not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of List of SiteBearingFavorites Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @param vendorId, String Object
     * @return List of SiteBearingFavorites Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByCustomerSiteVendor(String customerAccount, String siteId, String vendorId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<SiteBearingFavorites> result;
        StringBuilder json;

        if ((result = readDelegate.findByCustomerSiteVendor(customerAccount, UUID.fromString(siteId), vendorId)) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"siteId\":\"").append(siteId).append("\",")
                        .append("\"vendorId\":\"").append(vendorId).append("\",")
                        .append("\"SiteBearingFavorites\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"SiteBearingFavorites not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of List of SiteBearingFavorites Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @return List of SiteBearingFavorites Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByCustomerSite(String customerAccount, String siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<SiteBearingFavorites> result;
        StringBuilder json;

        if ((result = readDelegate.findByCustomerSite(customerAccount, UUID.fromString(siteId))) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"siteId\":\"").append(siteId).append("\",")
                        .append("\"SiteBearingFavorites\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"SiteBearingFavorites not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Create a new SiteBearingFavorites by inserting into site_bearing_favorites table
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String createSiteBearingFavorites(String content) throws IllegalArgumentException {
        CreateSiteBearingFavoritesDelegate createDelegate;
        List<SiteBearingFavoritesVO> siteBearingFavoritesVOList;

        if ((siteBearingFavoritesVOList = JsonUtil.siteBearingFavoritesVOListParser(content)) != null && !siteBearingFavoritesVOList.isEmpty()) {
            for (SiteBearingFavoritesVO siteBearingFavoritesVO : siteBearingFavoritesVOList) {
                if (siteBearingFavoritesVO == null
                        || siteBearingFavoritesVO.getCustomerAcct() == null || siteBearingFavoritesVO.getCustomerAcct().isEmpty()
                        || siteBearingFavoritesVO.getSiteId() == null
                        || siteBearingFavoritesVO.getVendorId() == null || siteBearingFavoritesVO.getVendorId().isEmpty()
                        || siteBearingFavoritesVO.getBearingId() == null || siteBearingFavoritesVO.getBearingId().isEmpty()) {
                    return "{\"outcome\":\"insufficient data given in json\"}";
                }
            }
            // Insert into Cassandra
            try {
                createDelegate = new CreateSiteBearingFavoritesDelegate();
                return createDelegate.createSiteBearingFavorites(siteBearingFavoritesVOList);
            } catch (IllegalArgumentException e) {
                LOGGER.error(e.getMessage(), e);
                throw e;
            } catch (Exception e) {
                return "{\"outcome\":\"Error: Failed to create new SiteBearingFavorites.\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Update site_bearing_favorites table with the given SiteBearingFavorites
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String updateSiteBearingFavorites(String content) throws IllegalArgumentException {
        UpdateSiteBearingFavoritesDelegate updateDelegate;
        SiteFavoritesEncapVO siteFavoritesEncapVO;

        if ((siteFavoritesEncapVO = JsonUtil.siteFavoritesEncapVOParser(content)) != null
                && ((siteFavoritesEncapVO.getCreateSiteBearingFavoritesVOList() != null && !siteFavoritesEncapVO.getCreateSiteBearingFavoritesVOList().isEmpty())
                || (siteFavoritesEncapVO.getDeleteSiteBearingFavoritesVOList() != null && !siteFavoritesEncapVO.getDeleteSiteBearingFavoritesVOList().isEmpty()))) {
            if (siteFavoritesEncapVO.getCreateSiteBearingFavoritesVOList() != null && !siteFavoritesEncapVO.getCreateSiteBearingFavoritesVOList().isEmpty()) {
                for (SiteBearingFavoritesVO siteBearingFavoritesVO : siteFavoritesEncapVO.getCreateSiteBearingFavoritesVOList()) {
                    if (siteBearingFavoritesVO == null
                            || siteBearingFavoritesVO.getCustomerAcct() == null || siteBearingFavoritesVO.getCustomerAcct().isEmpty()
                            || siteBearingFavoritesVO.getSiteId() == null
                            || siteBearingFavoritesVO.getVendorId() == null || siteBearingFavoritesVO.getVendorId().isEmpty()
                            || siteBearingFavoritesVO.getBearingId() == null || siteBearingFavoritesVO.getBearingId().isEmpty()) {
                        return "{\"outcome\":\"insufficient data given in json\"}";
                    }
                }
            }

            if (siteFavoritesEncapVO.getDeleteSiteBearingFavoritesVOList() != null && !siteFavoritesEncapVO.getDeleteSiteBearingFavoritesVOList().isEmpty()) {
                for (SiteBearingFavoritesVO siteBearingFavoritesVO : siteFavoritesEncapVO.getDeleteSiteBearingFavoritesVOList()) {
                    if (siteBearingFavoritesVO == null
                            || siteBearingFavoritesVO.getCustomerAcct() == null || siteBearingFavoritesVO.getCustomerAcct().isEmpty()
                            || siteBearingFavoritesVO.getSiteId() == null
                            || siteBearingFavoritesVO.getVendorId() == null || siteBearingFavoritesVO.getVendorId().isEmpty()
                            || siteBearingFavoritesVO.getBearingId() == null || siteBearingFavoritesVO.getBearingId().isEmpty()) {
                        return "{\"outcome\":\"insufficient data given in json\"}";
                    }
                }
            }

            // Update in Cassandra
            try {
                updateDelegate = new UpdateSiteBearingFavoritesDelegate();
                return updateDelegate.updateSiteBearingFavorites(siteFavoritesEncapVO);
            } catch (IllegalArgumentException e) {
                LOGGER.error(e.getMessage(), e);
                throw e;
            } catch (Exception e) {
                return "{\"outcome\":\"Error: Failed to update SiteBearingFavorites.\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Delete rows from site_bearing_favorites table for the given SiteBearingFavorites
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String deleteSiteBearingFavorites(String content) throws IllegalArgumentException {
        DeleteSiteBearingFavoritesDelegate deleteDelegate;
        SiteBearingFavoritesVO siteBearingFavoritesVO;

        if ((siteBearingFavoritesVO = JsonUtil.siteBearingFavoritesVOParser(content)) != null) {
            if (siteBearingFavoritesVO != null
                    && siteBearingFavoritesVO.getCustomerAcct() != null && !siteBearingFavoritesVO.getCustomerAcct().isEmpty()
                    && siteBearingFavoritesVO.getSiteId() != null
                    && siteBearingFavoritesVO.getVendorId() != null && !siteBearingFavoritesVO.getVendorId().isEmpty()
                    && siteBearingFavoritesVO.getBearingId() != null && !siteBearingFavoritesVO.getBearingId().isEmpty()) {

                // Delete from Cassandra
                try {
                    deleteDelegate = new DeleteSiteBearingFavoritesDelegate();
                    return deleteDelegate.deleteSiteBearingFavorites(siteBearingFavoritesVO);
                } catch (IllegalArgumentException e) {
                    LOGGER.error(e.getMessage(), e);
                    throw e;
                } catch (Exception e) {
                    return "{\"outcome\":\"Error: Failed to delete SiteBearingFavorites.\"}";
                }

            } else {
                return "{\"outcome\":\"Insufficient data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }
}
