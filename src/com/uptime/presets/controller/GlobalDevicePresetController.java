/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.presets.entity.GlobalApAlSetsByCustomer;
import static com.uptime.services.ServiceConstants.DEFAULT_UPTIME_ACCOUNT;
import com.uptime.cassandra.presets.entity.GlobalDevicePreset;
import com.uptime.services.util.JsonConverterUtil;
import com.uptime.presets.delegate.CreateGlobalDevicePresetDelegate;
import com.uptime.presets.delegate.DeleteGlobalDevicePresetDelegate;
import com.uptime.presets.delegate.ReadGlobalApAlSetsDelegate;
import com.uptime.presets.delegate.ReadGlobalDevicePresetDelegate;
import com.uptime.presets.delegate.UpdateGlobalDevicePresetDelegate;
import com.uptime.presets.utils.JsonUtil;
import com.uptime.presets.vo.DevicePresetVO;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class GlobalDevicePresetController {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalDevicePresetController.class.getName());
    ReadGlobalDevicePresetDelegate readDelegate;
    ReadGlobalApAlSetsDelegate readGlobalApAlSetsDelegate;

    public GlobalDevicePresetController() {
        readDelegate = new ReadGlobalDevicePresetDelegate();
        readGlobalApAlSetsDelegate = new ReadGlobalApAlSetsDelegate();
    }

    /**
     * Return a json of List of GlobalDevicePreset Objects by the given params
     *
     * @param customerAccount, String Object
     * @param deviceType, String Object
     * @param presetId, String Object
     * @param channelType, String Object
     * @param channelNumber, byte
     * @return List of GlobalDevicePreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByPK(String customerAccount, String deviceType, String presetId, String channelType, byte channelNumber) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<GlobalDevicePreset> result;
        StringBuilder json;

        if ((result = readDelegate.findByPK(customerAccount, deviceType, UUID.fromString(presetId), channelType, channelNumber)) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"deviceType\":\"").append(deviceType).append("\",")
                        .append("\"presetId\":\"").append(presetId).append("\",")
                        .append("\"channelType\":\"").append(channelType).append("\",")
                        .append("\"channelNumber\":\"").append(channelNumber).append("\",")
                        .append("\"GlobalDevicePreset\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Global Device Preset not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of List of GlobalDevicePreset Objects by the given params
     *
     * @param customerAccount, String Object
     * @param deviceType, String Object
     * @param presetId, String Object
     * @return List of GlobalDevicePreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByCustomerDeviceTypePreset(String customerAccount, String deviceType, String presetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<GlobalDevicePreset> result;
        StringBuilder json;

        if ((result = readDelegate.findByCustomerDeviceTypePreset(customerAccount, deviceType, UUID.fromString(presetId))) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"deviceType\":\"").append(deviceType).append("\",")
                        .append("\"presetId\":\"").append(presetId).append("\",")
                        .append("\"GlobalDevicePreset\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Global Device Preset not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of List of GlobalDevicePreset Objects by the given params
     *
     * @param customerAccount, String Object
     * @param deviceType, String Object
     * @return List of GlobalDevicePreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByCustomerDeviceType(String customerAccount, String deviceType) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        GlobalDevicePreset globalDevicePreset;
        List<GlobalDevicePreset> result;
        DevicePresetVO devicePresetVO;
        List<GlobalApAlSetsByCustomer> apAlSetsByCustomer;
        StringBuilder json;

        if ((result = readDelegate.findByCustomerDeviceType(customerAccount, deviceType)) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"deviceType\":\"").append(deviceType).append("\",")
                        .append("\"GlobalDevicePreset\":[");
                for (int i = 0; i < result.size(); i++) {
                    globalDevicePreset = result.get(i);
                    devicePresetVO = new DevicePresetVO(globalDevicePreset.getCustomerAccount(), null,
                            globalDevicePreset.getDeviceType(), globalDevicePreset.getPresetId(), globalDevicePreset.getChannelType(),
                            globalDevicePreset.getChannelNumber(), globalDevicePreset.getApSetId(), globalDevicePreset.getAlSetId(),
                            globalDevicePreset.isAlarmEnabled(), globalDevicePreset.isAutoAcknowledge(),
                            globalDevicePreset.isDisabled(), globalDevicePreset.getName(), globalDevicePreset.getSampleInterval(),
                            globalDevicePreset.getSensorOffset(), globalDevicePreset.getSensorSensitivity(), globalDevicePreset.getSensorType(),
                            globalDevicePreset.getSensorUnits(), null, null, globalDevicePreset.getSensorSubType(), globalDevicePreset.getSensorLocalOrientation());
                    if (globalDevicePreset.getApSetId() != null && globalDevicePreset.getAlSetId() != null) {
                        if ((apAlSetsByCustomer = readGlobalApAlSetsDelegate.findByPK(globalDevicePreset.getCustomerAccount(), globalDevicePreset.getSensorType(), globalDevicePreset.getApSetId(), globalDevicePreset.getAlSetId())) != null && apAlSetsByCustomer.size() > 0) {
                            devicePresetVO.setApSetName(apAlSetsByCustomer.get(0).getApSetName());
                            devicePresetVO.setAlSetName(apAlSetsByCustomer.get(0).getAlSetName());
                        } else if ((apAlSetsByCustomer = readGlobalApAlSetsDelegate.findByPK(DEFAULT_UPTIME_ACCOUNT, globalDevicePreset.getSensorType(), globalDevicePreset.getApSetId(), globalDevicePreset.getAlSetId())) != null && apAlSetsByCustomer.size() > 0) {
                            devicePresetVO.setApSetName(apAlSetsByCustomer.get(0).getApSetName());
                            devicePresetVO.setAlSetName(apAlSetsByCustomer.get(0).getAlSetName());
                        }
                    }
                    json.append(JsonConverterUtil.toJSON(devicePresetVO)).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Global Device Preset not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of List of GlobalDevicePreset Objects by the given params
     *
     * @param customerAccount, String Object
     * @return List of GlobalDevicePreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByCustomer(String customerAccount) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<GlobalApAlSetsByCustomer> apAlSetsByCustomer;
        GlobalDevicePreset globalDevicePreset;
        List<GlobalDevicePreset> result;
        DevicePresetVO devicePresetVO;
        StringBuilder json;

        if ((result = readDelegate.findByCustomer(customerAccount)) != null) {
            if (result.size() > 0) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"GlobalDevicePreset\":[");

                for (int i = 0; i < result.size(); i++) {
                    globalDevicePreset = result.get(i);
                    devicePresetVO = new DevicePresetVO(globalDevicePreset.getCustomerAccount(), null,
                            globalDevicePreset.getDeviceType(), globalDevicePreset.getPresetId(), globalDevicePreset.getChannelType(),
                            globalDevicePreset.getChannelNumber(), globalDevicePreset.getApSetId(), globalDevicePreset.getAlSetId(),
                            globalDevicePreset.isAlarmEnabled(), globalDevicePreset.isAutoAcknowledge(),
                            globalDevicePreset.isDisabled(), globalDevicePreset.getName(), globalDevicePreset.getSampleInterval(),
                            globalDevicePreset.getSensorOffset(), globalDevicePreset.getSensorSensitivity(), globalDevicePreset.getSensorType(),
                            globalDevicePreset.getSensorUnits(), null, null, globalDevicePreset.getSensorSubType(), globalDevicePreset.getSensorLocalOrientation());
                    if (globalDevicePreset.getApSetId() != null && globalDevicePreset.getAlSetId() != null) {
                        if ((apAlSetsByCustomer = readGlobalApAlSetsDelegate.findByPK(globalDevicePreset.getCustomerAccount(), globalDevicePreset.getSensorType(), globalDevicePreset.getApSetId(), globalDevicePreset.getAlSetId())) != null && apAlSetsByCustomer.size() > 0) {
                            devicePresetVO.setApSetName(apAlSetsByCustomer.get(0).getApSetName());
                            devicePresetVO.setAlSetName(apAlSetsByCustomer.get(0).getAlSetName());
                        } else if ((apAlSetsByCustomer = readGlobalApAlSetsDelegate.findByPK(DEFAULT_UPTIME_ACCOUNT, globalDevicePreset.getSensorType(), globalDevicePreset.getApSetId(), globalDevicePreset.getAlSetId())) != null && apAlSetsByCustomer.size() > 0) {
                            devicePresetVO.setApSetName(apAlSetsByCustomer.get(0).getApSetName());
                            devicePresetVO.setAlSetName(apAlSetsByCustomer.get(0).getAlSetName());
                        }
                    }
                    json.append(JsonConverterUtil.toJSON(devicePresetVO)).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Global Device Preset not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Create a new GlobalDevicePreset by inserting into global_device_preset table
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String createGlobalDevicePreset(String content) throws IllegalArgumentException {
        CreateGlobalDevicePresetDelegate createDelegate;
        List<DevicePresetVO> devicePresetVOList;

        if ((devicePresetVOList = JsonUtil.devicePresetVOListParser(content)) != null && !devicePresetVOList.isEmpty()) {
            for (DevicePresetVO devicePresetVO : devicePresetVOList) {
                if (devicePresetVO == null
                        || devicePresetVO.getCustomerAccount() == null || devicePresetVO.getCustomerAccount().isEmpty()
                        || devicePresetVO.getDeviceType() == null || devicePresetVO.getDeviceType().isEmpty()
                        || devicePresetVO.getChannelType() == null || devicePresetVO.getChannelType().isEmpty()) {
                    return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
                }
            }

            // Insert into Cassandra
            try {
                createDelegate = new CreateGlobalDevicePresetDelegate();
                return createDelegate.createGlobalDevicePreset(devicePresetVOList);

            } catch (IllegalArgumentException e) {
                LOGGER.error(e.getMessage(), e);
                throw e;
            } catch (Exception e) {
                return "{\"outcome\":\"Error: Failed to create new Global Device Preset.\"}";
            }

        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Update global_device_preset table with the given GlobalDevicePreset
     *
     * @param content, String Object
     * @return String Object
     */
    public String updateGlobalDevicePreset(String content) throws IllegalArgumentException {
        UpdateGlobalDevicePresetDelegate updateDelegate;
        List<DevicePresetVO> devicePresetVOList;

        if ((devicePresetVOList = JsonUtil.devicePresetVOListParser(content)) != null && !devicePresetVOList.isEmpty()) {
            for (DevicePresetVO devicePresetVO : devicePresetVOList) {
                if (devicePresetVO == null
                        || devicePresetVO.getCustomerAccount() == null || devicePresetVO.getCustomerAccount().isEmpty()
                        || devicePresetVO.getDeviceType() == null || devicePresetVO.getDeviceType().isEmpty()
                        || devicePresetVO.getPresetId() == null
                        || devicePresetVO.getChannelType() == null || devicePresetVO.getChannelType().isEmpty()) {
                    return "{\"outcome\":\"insufficient data given in json\"}";
                }
            }

            // Update in Cassandra
            try {
                updateDelegate = new UpdateGlobalDevicePresetDelegate();
                return updateDelegate.updateGlobalDevicePreset(devicePresetVOList);
            } catch (IllegalArgumentException e) {
                LOGGER.error(e.getMessage(), e);
                throw e;
            } catch (Exception e) {
                return "{\"outcome\":\"Error: Failed to update Global Device Preset.\"}";
            }

        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Delete rows from global_device_preset table for the given GlobalDevicePreset
     *
     * @param content, String Object
     * @return String Object
     */
    public String deleteGlobalDevicePreset(String content) throws IllegalArgumentException {
        DeleteGlobalDevicePresetDelegate deleteDelegate;
        List<DevicePresetVO> devicePresetVOList;

        if ((devicePresetVOList = JsonUtil.devicePresetVOListParser(content)) != null && !devicePresetVOList.isEmpty()) {
            for (DevicePresetVO devicePresetVO : devicePresetVOList) {
                if (devicePresetVO == null
                        || devicePresetVO.getCustomerAccount() == null || devicePresetVO.getCustomerAccount().isEmpty()
                        || devicePresetVO.getDeviceType() == null || devicePresetVO.getDeviceType().isEmpty()
                        || devicePresetVO.getPresetId() == null
                        || devicePresetVO.getChannelType() == null || devicePresetVO.getChannelType().isEmpty()) {
                    return "{\"outcome\":\"insufficient data given in json\"}";
                }
            }

            // Delete from Cassandra
            try {
                deleteDelegate = new DeleteGlobalDevicePresetDelegate();
                return deleteDelegate.deleteGlobalDevicePreset(devicePresetVOList);
            } catch (IllegalArgumentException e) {
                LOGGER.error(e.getMessage(), e);
                throw e;
            } catch (Exception e) {
                return "{\"outcome\":\"Error: Failed to delete Global Device Preset.\"}";
            }

        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }
}
