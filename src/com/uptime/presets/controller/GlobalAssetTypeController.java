/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.presets.entity.GlobalAssetTypes;
import com.uptime.presets.delegate.ReadGlobalAssetTypeDelegate;
import com.uptime.services.util.JsonConverterUtil;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class GlobalAssetTypeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalAssetTypeController.class.getName());
    ReadGlobalAssetTypeDelegate readDelegate;

    public GlobalAssetTypeController() {
        readDelegate = new ReadGlobalAssetTypeDelegate();

    }

    /**
     * Return a json of List of GlobalAssetTypes Objects by the given params
     *
     * @param customerAccount, String Object
     * @return List of GlobalAssetTypes Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByCustomer(String customerAccount) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<GlobalAssetTypes> result;
        StringBuilder json;

        if ((result = readDelegate.findByCustomer(customerAccount)) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"GlobalAssetTypes\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Global Asset Types not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of List of GlobalAssetTypes Objects by the given params
     *
     * @param customerAccount, String Object
     * @param assetTypeName, String Object
     * @return List of GlobalAssetTypes Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByPK(String customerAccount, String assetTypeName) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<GlobalAssetTypes> result;
        StringBuilder json;

        if ((result = readDelegate.findByPK(customerAccount, assetTypeName)) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"assetTypeName\":\"").append(assetTypeName).append("\",")
                        .append("\"GlobalAssetTypes\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Global Asset Types not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

}
