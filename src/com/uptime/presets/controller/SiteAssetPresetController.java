/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import static com.uptime.services.ServiceConstants.DEFAULT_UPTIME_ACCOUNT;
import com.uptime.cassandra.presets.entity.GlobalDevicePreset;
import com.uptime.cassandra.presets.entity.GlobalTachometers;
import com.uptime.cassandra.presets.entity.SiteAssetPreset;
import com.uptime.cassandra.presets.entity.SiteDevicePreset;
import com.uptime.cassandra.presets.entity.SiteTachometers;
import com.uptime.presets.delegate.CreateSiteAssetPresetDelegate;
import com.uptime.presets.delegate.DeleteSiteAssetPresetDelegate;
import com.uptime.presets.delegate.ReadGlobalDevicePresetDelegate;
import com.uptime.presets.delegate.ReadGlobalTachometersDelegate;
import com.uptime.presets.delegate.ReadSiteAssetPresetDelegate;
import com.uptime.presets.delegate.ReadSiteDevicePresetDelegate;
import com.uptime.presets.delegate.ReadSiteTachometersDelegate;
import com.uptime.presets.delegate.UpdateSiteAssetPresetDelegate;
import com.uptime.presets.utils.JsonUtil;
import com.uptime.presets.vo.AssetPresetVO;
import com.uptime.services.util.JsonConverterUtil;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kpati
 */
public class SiteAssetPresetController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SiteAssetPresetController.class.getName());
    private final ReadSiteAssetPresetDelegate readDelegate;
    private final ReadSiteTachometersDelegate readSiteTachometersDelegate;
    private final ReadSiteDevicePresetDelegate readSiteDevicePresetDelegate;

    private final ReadGlobalDevicePresetDelegate readGlobalDevicePresetDelegate;
    private final ReadGlobalTachometersDelegate readGlobalTachometersDelegate;
    public SiteAssetPresetController() {
        readDelegate = new ReadSiteAssetPresetDelegate();
        readSiteTachometersDelegate = new ReadSiteTachometersDelegate();
        readSiteDevicePresetDelegate = new ReadSiteDevicePresetDelegate();

        readGlobalDevicePresetDelegate = new ReadGlobalDevicePresetDelegate();
        readGlobalTachometersDelegate = new ReadGlobalTachometersDelegate();
    }

    /**
     * Return a json of a List of SiteAssetPreset Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @param assetTypeName, String Object
     * @param assetPresetId, String Object
     * @param pointLocationName, String Object
     * @return List of SiteAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByPK(String customerAccount, String siteId, String assetTypeName, String assetPresetId, String pointLocationName) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<SiteAssetPreset> result;
        StringBuilder json;

        if ((result = readDelegate.findByPK(customerAccount, UUID.fromString(siteId), assetTypeName, UUID.fromString(assetPresetId), pointLocationName)) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"siteId\":\"").append(siteId).append("\",")
                        .append("\"assetTypeName\":\"").append(assetTypeName).append("\",")
                        .append("\"assetPresetId\":\"").append(assetPresetId).append("\",")
                        .append("\"pointLocationName\":\"").append(pointLocationName).append("\",")
                        .append("\"SiteAssetPreset\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Site Asset Preset not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of a List of SiteAssetPreset Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @param assetTypeName, String Object
     * @param assetPresetId, String Object
     * @return List of SiteAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByCustomerSiteAssetTypePresetId(String customerAccount, String siteId, String assetTypeName, String assetPresetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<SiteAssetPreset> result;
        StringBuilder json;

        if ((result = readDelegate.findByCustomerSiteAssetTypePresetId(customerAccount, UUID.fromString(siteId), assetTypeName, UUID.fromString(assetPresetId))) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"siteId\":\"").append(siteId).append("\",")
                        .append("\"assetTypeName\":\"").append(assetTypeName).append("\",")
                        .append("\"assetPresetId\":\"").append(assetPresetId).append("\",")
                        .append("\"SiteAssetPreset\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Site Asset Preset not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of a List of SiteAssetPreset Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @param assetTypeName, String Object
     * @return List of SiteAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByCustomerSiteAssetType(String customerAccount, String siteId, String assetTypeName) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<SiteAssetPreset> result;
        StringBuilder json;

        if ((result = readDelegate.findByCustomerSiteAssetType(customerAccount, UUID.fromString(siteId), assetTypeName)) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"siteId\":\"").append(siteId).append("\",")
                        .append("\"assetTypeName\":\"").append(assetTypeName).append("\",")
                        .append("\"SiteAssetPreset\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Site Asset Preset not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of a List of SiteAssetPreset Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @return List of SiteAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByCustomerSite(String customerAccount, String siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<SiteAssetPreset> result = readDelegate.findByCustomerSite(customerAccount, UUID.fromString(siteId));

        if (result == null) {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }

        if (result.isEmpty()) {
            return "{\"outcome\":\"Site Asset Preset not found.\"}";
        }

        StringBuilder json = new StringBuilder();
        json.append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                .append("\"siteId\":\"").append(siteId).append("\",")
                .append("\"SiteAssetPreset\":[");

        for (int i = 0; i < result.size(); i++) {
            SiteAssetPreset siteAssetPreset = result.get(i);
            AssetPresetVO assetPresetVO = new AssetPresetVO(
                    siteAssetPreset.getCustomerAccount(), siteAssetPreset.getSiteId(), siteAssetPreset.getAssetTypeName(),
                    siteAssetPreset.getAssetPresetId(), siteAssetPreset.getPointLocationName(),
                    siteAssetPreset.getAssetPresetName(), siteAssetPreset.getDevicePresetId(),
                    siteAssetPreset.getDevicePresetType(), siteAssetPreset.getSampleInterval(),
                    siteAssetPreset.getFfSetIds(), siteAssetPreset.getRollDiameter(),
                    siteAssetPreset.getRollDiameterUnits(), siteAssetPreset.getSpeedRatio(),
                    siteAssetPreset.getTachId(), siteAssetPreset.getDescription(), null, null
            );

            if (siteAssetPreset.getCustomerAccount() != null && siteAssetPreset.getTachId() != null
                    && siteAssetPreset.getAssetPresetId() != null && siteAssetPreset.getSiteId() != null) {

                // Handle Site Tachometers
                List<SiteTachometers> siteTachometersByCustomer = readSiteTachometersDelegate.findByPK(
                        siteAssetPreset.getCustomerAccount(), siteAssetPreset.getSiteId(), siteAssetPreset.getTachId());
                if (siteTachometersByCustomer != null && !siteTachometersByCustomer.isEmpty()) {
                    assetPresetVO.setTachName(siteTachometersByCustomer.get(0).getTachName());
                } else {
                    siteTachometersByCustomer = readSiteTachometersDelegate.findByPK(
                            DEFAULT_UPTIME_ACCOUNT, siteAssetPreset.getSiteId(), siteAssetPreset.getTachId());
                    if (siteTachometersByCustomer != null && !siteTachometersByCustomer.isEmpty()) {
                        assetPresetVO.setTachName(siteTachometersByCustomer.get(0).getTachName());
                    } else {
                        List<GlobalTachometers> globalTachometersByCustomer = readGlobalTachometersDelegate.findByPK(
                                siteAssetPreset.getCustomerAccount(), siteAssetPreset.getTachId());
                        if (globalTachometersByCustomer != null && !globalTachometersByCustomer.isEmpty()) {
                            assetPresetVO.setTachName(globalTachometersByCustomer.get(0).getTachName());
                        } else {
                            globalTachometersByCustomer = readGlobalTachometersDelegate.findByPK(
                                    DEFAULT_UPTIME_ACCOUNT, siteAssetPreset.getTachId());
                            if (globalTachometersByCustomer != null && !globalTachometersByCustomer.isEmpty()) {
                                assetPresetVO.setTachName(globalTachometersByCustomer.get(0).getTachName());
                            }
                        }
                    }
                }

                // Handle Site Device Presets
                List<SiteDevicePreset> siteDevicePresetByCustomer = readSiteDevicePresetDelegate.findByCustomerSiteDeviceTypePreset(
                        siteAssetPreset.getCustomerAccount(), siteAssetPreset.getSiteId(), siteAssetPreset.getDevicePresetType(), siteAssetPreset.getDevicePresetId());
                if (siteDevicePresetByCustomer != null && !siteDevicePresetByCustomer.isEmpty()) {
                    assetPresetVO.setDevicePresetName(siteDevicePresetByCustomer.get(0).getName());
                } else {
                    siteDevicePresetByCustomer = readSiteDevicePresetDelegate.findByCustomerSiteDeviceTypePreset(
                            DEFAULT_UPTIME_ACCOUNT, siteAssetPreset.getSiteId(), siteAssetPreset.getDevicePresetType(), siteAssetPreset.getDevicePresetId());
                    if (siteDevicePresetByCustomer != null && !siteDevicePresetByCustomer.isEmpty()) {
                        assetPresetVO.setDevicePresetName(siteDevicePresetByCustomer.get(0).getName());
                    } else {
                        List<GlobalDevicePreset> globalDevicePresetByCustomer = readGlobalDevicePresetDelegate.findByCustomerDeviceTypePreset(
                                siteAssetPreset.getCustomerAccount(), siteAssetPreset.getDevicePresetType(), siteAssetPreset.getDevicePresetId());
                        if (globalDevicePresetByCustomer != null && !globalDevicePresetByCustomer.isEmpty()) {
                            assetPresetVO.setDevicePresetName(globalDevicePresetByCustomer.get(0).getName());
                        } else {
                            globalDevicePresetByCustomer = readGlobalDevicePresetDelegate.findByCustomerDeviceTypePreset(
                                    DEFAULT_UPTIME_ACCOUNT, siteAssetPreset.getDevicePresetType(), siteAssetPreset.getDevicePresetId());
                            if (globalDevicePresetByCustomer != null && !globalDevicePresetByCustomer.isEmpty()) {
                                assetPresetVO.setDevicePresetName(globalDevicePresetByCustomer.get(0).getName());
                            }
                        }
                    }
                }
            }

            json.append(JsonConverterUtil.toJSON(assetPresetVO)).append(i < result.size() - 1 ? "," : "");
        }

        json.append("],\"outcome\":\"GET worked successfully.\"}");
        return json.toString();
    }


    /**
     * Create a new SiteAssetPreset by inserting into site_asset_preset table
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String createSiteAssetPreset(String content) throws IllegalArgumentException {
        CreateSiteAssetPresetDelegate createDelegate;
        List<AssetPresetVO> assetPresetVOList;

        if ((assetPresetVOList = JsonUtil.assetPresetVOListParser(content)) != null && !assetPresetVOList.isEmpty()) {
            for (AssetPresetVO assetPresetVO : assetPresetVOList) {
                if (assetPresetVO == null
                        || assetPresetVO.getCustomerAccount() == null || assetPresetVO.getCustomerAccount().isEmpty()
                        || assetPresetVO.getSiteId() == null
                        || assetPresetVO.getAssetTypeName() == null || assetPresetVO.getAssetTypeName().isEmpty()
                        || assetPresetVO.getPointLocationName() == null || assetPresetVO.getPointLocationName().isEmpty()) {
                    return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
                }
            }

            // Insert into Cassandra
            try {
                createDelegate = new CreateSiteAssetPresetDelegate();
                return createDelegate.createSiteAssetPreset(assetPresetVOList);

            } catch (IllegalArgumentException e) {
                LOGGER.error(e.getMessage(), e);
                throw e;
            } catch (Exception e) {
                return "{\"outcome\":\"Error: Failed to create new Site Asset Preset.\"}";
            }

        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Update site_asset_preset table with the given SiteAssetPreset
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String updateSiteAssetPreset(String content) throws IllegalArgumentException {
        UpdateSiteAssetPresetDelegate updateDelegate;
        List<AssetPresetVO> assetPresetVOList;

        if (content != null && (assetPresetVOList = JsonUtil.assetPresetVOListParser(content)) != null && !assetPresetVOList.isEmpty()) {
            for (AssetPresetVO assetPresetVO : assetPresetVOList) {
                if (assetPresetVO == null
                        || assetPresetVO.getCustomerAccount() == null || assetPresetVO.getCustomerAccount().isEmpty()
                        || assetPresetVO.getSiteId() == null
                        || assetPresetVO.getAssetTypeName() == null || assetPresetVO.getAssetTypeName().isEmpty()
                        || assetPresetVO.getAssetPresetId() == null
                        || assetPresetVO.getPointLocationName() == null || assetPresetVO.getPointLocationName().isEmpty()) {
                    return "{\"outcome\":\"insufficient data given in json\"}";
                }
            }

            // Update in Cassandra
            try {
                updateDelegate = new UpdateSiteAssetPresetDelegate();
                return updateDelegate.updateSiteAssetPreset(assetPresetVOList);
            } catch (IllegalArgumentException e) {
                LOGGER.error(e.getMessage(), e);
                throw e;
            } catch (Exception e) {
                return "{\"outcome\":\"Error: Failed to update Site Device Preset.\"}";
            }

        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Delete from site_asset_preset table for the given SiteAssetPreset
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String deleteSiteAssetPreset(String content) throws IllegalArgumentException {
        DeleteSiteAssetPresetDelegate deleteDelegate;
        List<AssetPresetVO> assetPresetVOList;

        if (content != null && (assetPresetVOList = JsonUtil.assetPresetVOListParser(content)) != null && !assetPresetVOList.isEmpty()) {
            for (AssetPresetVO assetPresetVO : assetPresetVOList) {
                if (assetPresetVO == null
                        || assetPresetVO.getCustomerAccount() == null || assetPresetVO.getCustomerAccount().isEmpty()
                        || assetPresetVO.getSiteId() == null
                        || assetPresetVO.getAssetTypeName() == null || assetPresetVO.getAssetTypeName().isEmpty()
                        || assetPresetVO.getAssetPresetId() == null
                        || assetPresetVO.getPointLocationName() == null || assetPresetVO.getPointLocationName().isEmpty()) {
                    return "{\"outcome\":\"insufficient data given in json\"}";
                }
            }

            // Delete from Cassandra
            try {
                deleteDelegate = new DeleteSiteAssetPresetDelegate();
                return deleteDelegate.deleteSiteAssetPreset(assetPresetVOList);
            } catch (IllegalArgumentException e) {
                LOGGER.error(e.getMessage(), e);
                throw e;
            } catch (Exception e) {
                return "{\"outcome\":\"Error: Failed to delete Site Asset Preset.\"}";
            }

        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

//    public static void main(String arg[]) {
//        System.out.println("Helooooooooooooo");
//        AssetPresetVO entity = new AssetPresetVO();
//        List<AssetPresetVO> list = new ArrayList();
//
//        entity.setCustomerAccount("77777");
//        entity.setSiteId(UUID.fromString("8b086e15-371f-4a8d-bedb-c1b67b49be3e"));
//        entity.setAssetPresetId(UUID.randomUUID());
//        entity.setAssetPresetName("Test Asset Preset Name");
//        entity.setAssetTypeName("Test Asset preset type name");
//        entity.setDescription("Test Desc");
//        entity.setDevicePresetId(UUID.randomUUID());
//        entity.setFfSetIds("Test ffsetId");
//        entity.setPointLocationName("Test Pl Name");
//        entity.setRollDiameter(0);
//        entity.setRollDiameterUnits("Test Roll Diam");
//        entity.setSampleInterval(0);
//        entity.setSampleInterval(10);
//        entity.setSpeedRatio(10);
//        entity.setDevicePresetType("MistLx");
//        entity.setSpeedRatio(0);
//        entity.setTachId(UUID.randomUUID());
//
//        list.add(entity);
//        
//        String payLoadJason = "[{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"assetTypeName\":\"Fan\",\"assetPresetId\":\"0f24adeb-36f1-4185-96d9-47877d90b265\",\"sampleInterval\":11,"
//                + "\"devicePresetType\": \"MistLx\","
//                + "\"description\":\"Test Desc\",\"pointLocationName\":\"Test-123\",\"assetPresetName\":\"Test Asset 1\","
//                + "\"devicePresetId\":\"919f7ae1-408a-48b0-9308-26ca567977c0\","
//                + "\"ffSetIds\":\"{\\\"ff_sets\\\":[{\\\"ff_set_name\\\":\\\"FF Set Manual 2\\\",\\\"source\\\":\\\"Site FF Set\\\", \\\"units\\\":\\\"Orders\\\",\\\"frequencies\\\":[{\\\"Freq1\\\":86.26}]}]}\",\"rollDiameter\":0.0,\"rollDiameterUnits\":\"Test RollDiameter Units\",\"speedRatio\":1.0,\"tachId\":\"f9461272-1afe-4b92-9f75-5ba1ec948e9f\"}]";
//        SiteAssetPresetController controller = new SiteAssetPresetController();
//        CreateSiteAssetPresetDelegate createDelegate = new CreateSiteAssetPresetDelegate();
//        DeleteSiteAssetPresetDelegate deleteDelegate = new DeleteSiteAssetPresetDelegate();
//        UpdateSiteAssetPresetDelegate updateDelegate = new UpdateSiteAssetPresetDelegate();
//        try {
//            //String msg = updateDelegate.updateSiteAssetPreset(list);
//            //String msg = controller.getByCustomerSiteAssetType(entity.getCustomerAccount(), entity.getSiteId().toString(), entity.getAssetTypeName());
//            String msg= controller.createSiteAssetPreset(payLoadJason);
//            System.out.println("msg-->" + msg);
//        } catch (Exception exp) {
//
//        }
//
//    }
}
