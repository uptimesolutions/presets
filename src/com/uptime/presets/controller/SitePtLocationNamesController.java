/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.presets.entity.SitePtLocationNames;
import com.uptime.services.util.JsonConverterUtil;
import com.uptime.presets.delegate.CreateSitePtLocationNamesDelegate;
import com.uptime.presets.delegate.DeleteSitePtLocationNamesDelegate;
import com.uptime.presets.delegate.ReadSitePtLocationNamesDelegate;
import com.uptime.presets.delegate.UpdateSitePtLocationNamesDelegate;
import com.uptime.presets.utils.JsonUtil;
import com.uptime.presets.vo.PtLocationNamesVO;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class SitePtLocationNamesController {
    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalApAlSetsController.class.getName());
    ReadSitePtLocationNamesDelegate readDelegate;

    public SitePtLocationNamesController() {
        readDelegate = new ReadSitePtLocationNamesDelegate();
    }

    /**
     * Return a json of a List of SitePtLocationNames Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @param pointLocationName, String Object
     * @return List of SitePtLocationNames Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByPK(String customerAccount, String siteId, String pointLocationName) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<SitePtLocationNames> result;
        StringBuilder json;

        if ((result = readDelegate.findByPK(customerAccount, UUID.fromString(siteId), pointLocationName)) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"siteId\":\"").append(siteId).append("\",")
                        .append("\"pointLocationName\":\"").append(pointLocationName).append("\",")
                        .append("\"SitePtLocationNames\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"SitePtLocationNames not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of a List of SitePtLocationNames Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @return List of SitePtLocationNames Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByCustomerSite(String customerAccount, String siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<SitePtLocationNames> result;
        StringBuilder json;

        if ((result = readDelegate.findByCustomerSite(customerAccount, UUID.fromString(siteId))) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"siteId\":\"").append(siteId).append("\",")
                        .append("\"SitePtLocationNames\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"SitePtLocationNames not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Create a new SitePtLocationNames by inserting into site_pt_location_names table
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String createSitePtLocationNames(String content) throws IllegalArgumentException {
        CreateSitePtLocationNamesDelegate createDelegate;
        PtLocationNamesVO ptLocationNamesVO;

        if ((ptLocationNamesVO = JsonUtil.ptLocationNamesVOParser(content)) != null) {
            if (ptLocationNamesVO.getCustomerAccount() != null && !ptLocationNamesVO.getCustomerAccount().isEmpty()
                    && ptLocationNamesVO.getSiteId() != null
                    && ptLocationNamesVO.getPointLocationName() != null && !ptLocationNamesVO.getPointLocationName().isEmpty()) {

                // Insert into Cassandra
                try {
                    createDelegate = new CreateSitePtLocationNamesDelegate();
                    return createDelegate.createSitePtLocationNames(ptLocationNamesVO);

                } catch (IllegalArgumentException e) {
                    LOGGER.error(e.getMessage(), e);
                    throw e;
                } catch (Exception e) {
                    return "{\"outcome\":\"Error: Failed to create new SitePtLocationNames.\"}";
                }

            } else {
                return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Update site_pt_location_names table with the given SitePtLocationNames
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String updateSitePtLocationNames(String content) throws IllegalArgumentException {
        UpdateSitePtLocationNamesDelegate updateDelegate;
        PtLocationNamesVO ptLocationNamesVO;

        if ((ptLocationNamesVO = JsonUtil.ptLocationNamesVOParser(content)) != null) {
            if (ptLocationNamesVO != null
                    && ptLocationNamesVO.getCustomerAccount() != null && !ptLocationNamesVO.getCustomerAccount().isEmpty()
                    && ptLocationNamesVO.getSiteId() != null
                    && ptLocationNamesVO.getPointLocationName() != null && !ptLocationNamesVO.getPointLocationName().isEmpty()) {

                // Update in Cassandra
                try {
                    updateDelegate = new UpdateSitePtLocationNamesDelegate();
                    return updateDelegate.updateSitePtLocationNames(ptLocationNamesVO);
                } catch (IllegalArgumentException e) {
                    LOGGER.error(e.getMessage(), e);
                    throw e;
                } catch (Exception e) {
                    return "{\"outcome\":\"Error: Failed to update SitePtLocationNames.\"}";
                }

            } else {
                return "{\"outcome\":\"insufficient data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Delete the record from site_pt_location_names table for the given SitePtLocationNames
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String deleteSitePtLocationNames(String content) throws IllegalArgumentException {
        DeleteSitePtLocationNamesDelegate deleteDelegate;
        PtLocationNamesVO ptLocationNamesVO;

        if ((ptLocationNamesVO = JsonUtil.ptLocationNamesVOParser(content)) != null) {
            if (ptLocationNamesVO != null
                    && ptLocationNamesVO.getCustomerAccount() != null && !ptLocationNamesVO.getCustomerAccount().isEmpty()
                    && ptLocationNamesVO.getSiteId() != null
                    && ptLocationNamesVO.getPointLocationName() != null && !ptLocationNamesVO.getPointLocationName().isEmpty()) {

                // Delete from Cassandra
                try {
                    deleteDelegate = new DeleteSitePtLocationNamesDelegate();
                    return deleteDelegate.deleteSitePtLocationNames(ptLocationNamesVO);
                } catch (IllegalArgumentException e) {
                    LOGGER.error(e.getMessage(), e);
                    throw e;
                } catch (Exception e) {
                    return "{\"outcome\":\"Error: Failed to delete SitePtLocationNames.\"}";
                }

            } else {
                return "{\"outcome\":\"Insufficient data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }
}
