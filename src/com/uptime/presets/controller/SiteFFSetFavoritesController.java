/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.presets.entity.SiteBearingFavorites;
import com.uptime.cassandra.presets.entity.SiteFFSetFavorites;
import com.uptime.services.util.JsonConverterUtil;
import com.uptime.presets.delegate.CreateSiteFFSetFavoritesDelegate;
import com.uptime.presets.delegate.DeleteSiteFFSetFavoritesDelegate;
import com.uptime.presets.delegate.ReadSiteBearingFavoritesDelegate;
import com.uptime.presets.delegate.ReadSiteFFSetFavoritesDelegate;
import com.uptime.presets.delegate.UpdateSiteFFSetFavoritesDelegate;
import com.uptime.presets.utils.JsonUtil;
import com.uptime.presets.vo.SiteFFSetFavoritesVO;
import com.uptime.presets.vo.SiteFavoritesEncapVO;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class SiteFFSetFavoritesController {
    private static final Logger LOGGER = LoggerFactory.getLogger(SiteFFSetFavoritesController.class.getName());
    ReadSiteFFSetFavoritesDelegate readDelegate;
    
    public SiteFFSetFavoritesController() {
        readDelegate = new ReadSiteFFSetFavoritesDelegate();
    }

    /**
     * Return a json of List of SiteFFSetFavorites Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @param ffSetId, String Object
     * @param ffId, String Object
     * @return List of SiteFFSetFavorites Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByPK(String customerAccount, String siteId, String ffSetId, String ffId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<SiteFFSetFavorites> result;
        StringBuilder json;

        if ((result = readDelegate.findByPK(customerAccount, UUID.fromString(siteId), UUID.fromString(ffSetId), UUID.fromString(ffId))) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"siteId\":\"").append(siteId).append("\",")
                        .append("\"ffSetId\":\"").append(ffSetId).append("\",")
                        .append("\"ffId\":\"").append(ffId).append("\",")
                        .append("\"SiteFFSetFavorites\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Site FF Set Favorites not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of List of SiteFFSetFavorites and SiteBearingFavorites Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @return List of SiteFFSetFavorites and SiteBearingFavorites Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getFavoritesByCustomerSiteId(String customerAccount, String siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<SiteFFSetFavorites> result;
        List<SiteBearingFavorites> resultBearing;
        StringBuilder json;
        boolean found;
        ReadSiteBearingFavoritesDelegate readBearingsDelegate = new ReadSiteBearingFavoritesDelegate();
        
        json = new StringBuilder();
        json
                .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                .append("\"siteId\":\"").append(siteId).append("\",");
        found = false;
        if ((result = readDelegate.findByCustomerSite(customerAccount, UUID.fromString(siteId))) != null && !result.isEmpty()) {
            json.append("\"SiteFFSetFavorites\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            found = true;
        } 
        
        if ((resultBearing = readBearingsDelegate.findByCustomerSite(customerAccount, UUID.fromString(siteId))) != null && !resultBearing.isEmpty()) {
            if (!found)
                 json.append("\"SiteFFSetFavorites\":[");
            else
                json.append(",");
            
            for (int i = 0; i < resultBearing.size(); i++) {
                SiteFFSetFavorites siteFFSetFavorites = new SiteFFSetFavorites();
                siteFFSetFavorites.setCustomerAccount(resultBearing.get(i).getCustomerAcct());
                siteFFSetFavorites.setFfSetName(resultBearing.get(i).getVendorId() + " - " + resultBearing.get(i).getBearingId());
                //siteFFSetFavorites.setFfSetDesc();
                siteFFSetFavorites.setSiteId(resultBearing.get(i).getSiteId());
                siteFFSetFavorites.setFfName("FTF");
                siteFFSetFavorites.setFfValue(resultBearing.get(i).getFtf());
                siteFFSetFavorites.setFfUnit("Orders");
                json.append(JsonConverterUtil.toJSON(siteFFSetFavorites)).append(",");
                
                siteFFSetFavorites = new SiteFFSetFavorites();
                siteFFSetFavorites.setCustomerAccount(resultBearing.get(i).getCustomerAcct());
                siteFFSetFavorites.setFfSetName(resultBearing.get(i).getVendorId() + " - " + resultBearing.get(i).getBearingId());
                //siteFFSetFavorites.setFfSetDesc();
                siteFFSetFavorites.setSiteId(resultBearing.get(i).getSiteId());
                siteFFSetFavorites.setFfName("BSF");
                siteFFSetFavorites.setFfValue(resultBearing.get(i).getBsf());
                siteFFSetFavorites.setFfUnit("Orders");
                json.append(JsonConverterUtil.toJSON(siteFFSetFavorites)).append(",");
                
                siteFFSetFavorites = new SiteFFSetFavorites();
                siteFFSetFavorites.setCustomerAccount(resultBearing.get(i).getCustomerAcct());
                siteFFSetFavorites.setFfSetName(resultBearing.get(i).getVendorId() + " - " + resultBearing.get(i).getBearingId());
                //siteFFSetFavorites.setFfSetDesc();
                siteFFSetFavorites.setSiteId(resultBearing.get(i).getSiteId());
                siteFFSetFavorites.setFfName("BPFI");
                siteFFSetFavorites.setFfValue(resultBearing.get(i).getBpfi());
                siteFFSetFavorites.setFfUnit("Orders");
                json.append(JsonConverterUtil.toJSON(siteFFSetFavorites)).append(",");
                
                siteFFSetFavorites = new SiteFFSetFavorites();
                siteFFSetFavorites.setCustomerAccount(resultBearing.get(i).getCustomerAcct());
                siteFFSetFavorites.setFfSetName(resultBearing.get(i).getVendorId() + " - " + resultBearing.get(i).getBearingId());
                //siteFFSetFavorites.setFfSetDesc();
                siteFFSetFavorites.setSiteId(resultBearing.get(i).getSiteId());
                siteFFSetFavorites.setFfName("BPFO");
                siteFFSetFavorites.setFfValue(resultBearing.get(i).getBpfo());
                siteFFSetFavorites.setFfUnit("Orders");
                json.append(JsonConverterUtil.toJSON(siteFFSetFavorites)).append(i < resultBearing.size() - 1 ? "," : "");

            } 
            found = true;
        }         
        if (found) {
            json.append("],");
            json.append("\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of List of SiteFFSetFavorites Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @return List of SiteFFSetFavorites Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String findByCustomerSiteId(String customerAccount, String siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<SiteFFSetFavorites> result;
        StringBuilder json;

        if ((result = readDelegate.findByCustomerSite(customerAccount, UUID.fromString(siteId))) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                        .append("\"siteId\":\"").append(siteId).append("\",")
                        .append("\"SiteFFSetFavorites\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Site FF Set Favorites not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Create a new SiteFFSetFavorites by inserting into multiple tables
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String createSiteFFSetFavorites(String content) throws IllegalArgumentException {
        CreateSiteFFSetFavoritesDelegate createDelegate;
        List<SiteFFSetFavoritesVO> faultFrequenciesVOList;

        if (content != null && (faultFrequenciesVOList = JsonUtil.siteFFSetFavoritesVOParser(content)) != null) {
            for (SiteFFSetFavoritesVO faultFrequenciesVO : faultFrequenciesVOList) {
                if (faultFrequenciesVO == null
                        || faultFrequenciesVO.getCustomerAccount() == null || faultFrequenciesVO.getCustomerAccount().isEmpty()
                        || faultFrequenciesVO.getSiteId() == null
                        || faultFrequenciesVO.getFfSetName() == null || faultFrequenciesVO.getFfSetName().isEmpty()
                        || faultFrequenciesVO.getFfName() == null || faultFrequenciesVO.getFfName().isEmpty()) {
                    return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
                }
            }

            // Insert into Cassandra
            try {
                createDelegate = new CreateSiteFFSetFavoritesDelegate();
                return createDelegate.createSiteFFSetFavorites(faultFrequenciesVOList);
            } catch (IllegalArgumentException e) {
                LOGGER.error(e.getMessage(), e);
                throw e;
            } catch (Exception e) {
                return "{\"outcome\":\"Error: Failed to create new Site FF Set FavoritesVO.\"}";
            }

        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Update tables dealing with the given SiteFFSetFavorites
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String updateSiteFFSetFavorites(String content) throws IllegalArgumentException {
        UpdateSiteFFSetFavoritesDelegate updateDelegate;
        SiteFavoritesEncapVO siteFavoritesEncapVO;
        
        if ((siteFavoritesEncapVO = JsonUtil.siteFFFavoritesEncapVOParser(content)) != null  &&
                ((siteFavoritesEncapVO.getCreateSiteFFSetFavoritesVOList() != null && !siteFavoritesEncapVO.getCreateSiteFFSetFavoritesVOList().isEmpty()) ||
                    (siteFavoritesEncapVO.getDeleteSiteFFSetFavoritesVOList() != null && !siteFavoritesEncapVO.getDeleteSiteFFSetFavoritesVOList().isEmpty()))) {
            if (siteFavoritesEncapVO.getCreateSiteFFSetFavoritesVOList() != null && !siteFavoritesEncapVO.getCreateSiteFFSetFavoritesVOList().isEmpty()) {
                for (SiteFFSetFavoritesVO siteFFSetFavoritesVO : siteFavoritesEncapVO.getCreateSiteFFSetFavoritesVOList()) {    
                    if (siteFFSetFavoritesVO == null
                            || siteFFSetFavoritesVO.getCustomerAccount() == null || siteFFSetFavoritesVO.getCustomerAccount().isEmpty()
                            || siteFFSetFavoritesVO.getSiteId() == null
                            || siteFFSetFavoritesVO.getFfSetId() == null 
                            || siteFFSetFavoritesVO.getFfId() == null) {
                        return "{\"outcome\":\"insufficient data given in json\"}";
                    }
                }
            }  
            
            if (siteFavoritesEncapVO.getDeleteSiteFFSetFavoritesVOList() != null && !siteFavoritesEncapVO.getDeleteSiteFFSetFavoritesVOList().isEmpty()) {
                for (SiteFFSetFavoritesVO siteFFSetFavoritesVO : siteFavoritesEncapVO.getDeleteSiteFFSetFavoritesVOList()) {    
                    if (siteFFSetFavoritesVO == null
                            || siteFFSetFavoritesVO.getCustomerAccount() == null || siteFFSetFavoritesVO.getCustomerAccount().isEmpty()
                            || siteFFSetFavoritesVO.getSiteId() == null
                            || siteFFSetFavoritesVO.getFfSetId() == null 
                            || siteFFSetFavoritesVO.getFfId() == null) {
                        return "{\"outcome\":\"insufficient data given in json\"}";
                    }
                }
            }

            // Update in Cassandra
            try {
                updateDelegate = new UpdateSiteFFSetFavoritesDelegate();
                return updateDelegate.updateSiteFFSetFavoritesVO(siteFavoritesEncapVO);
            } catch (IllegalArgumentException e) {
                LOGGER.error(e.getMessage(), e);
                throw e;
            } catch (Exception e) {
                return "{\"outcome\":\"Error: Failed to update Site FF Set FavoritesVO.\"}";
            }

        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Delete tables dealing with the given SiteFFSetFavorites
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String deleteSiteFFSetFavorites(String content) throws IllegalArgumentException {
        DeleteSiteFFSetFavoritesDelegate deleteDelegate;
        List<SiteFFSetFavoritesVO> siteFFSetFavoritesVOList;

        if ((siteFFSetFavoritesVOList = JsonUtil.siteFFSetFavoritesVOParser(content)) != null) {
            for (SiteFFSetFavoritesVO siteFFSetFavoritesVO : siteFFSetFavoritesVOList) {
                if (siteFFSetFavoritesVO == null
                        || siteFFSetFavoritesVO.getCustomerAccount() == null || siteFFSetFavoritesVO.getCustomerAccount().isEmpty()
                        || siteFFSetFavoritesVO.getSiteId() == null || siteFFSetFavoritesVO.getFfSetId() == null
                        || siteFFSetFavoritesVO.getFfId() == null) {
                    return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
                }
            }

            // Delete from Cassandra
            try {
                deleteDelegate = new DeleteSiteFFSetFavoritesDelegate();
                return deleteDelegate.deleteSiteFFSetFavorites(siteFFSetFavoritesVOList);

            } catch (IllegalArgumentException e) {
                LOGGER.error(e.getMessage(), e);
                throw e;
            } catch (Exception e) {
                return "{\"outcome\":\"Error: Failed to delete Site FF Set FavoritesVO.\"}";
            }

        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Delete tables dealing with the given SiteFFSetFavorites
     *
     * @param customerAccount
     * @param siteId
     * @param ffSetId
     * @return String Object
     */
    public String deleteSiteFFSetFavorites(String customerAccount, String siteId, String ffSetId) {
        DeleteSiteFFSetFavoritesDelegate deleteDelegate;
        
        try {
            deleteDelegate = new DeleteSiteFFSetFavoritesDelegate();
            return deleteDelegate.deleteSiteFFSetFavorites(customerAccount, UUID.fromString(siteId), UUID.fromString(ffSetId));
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return "{\"outcome\":\"Error: Failed to delete Site FF Set FavoritesVO.\"}";
        }
    }

    /**
     * Delete tables dealing with the given SiteFFSetFavorites
     *
     * @param customerAccount
     * @param siteId
     * @return String Object
     */
    public String deleteSiteFFSetFavorites(String customerAccount, String siteId) {
        DeleteSiteFFSetFavoritesDelegate deleteDelegate;
        
        try {
            deleteDelegate = new DeleteSiteFFSetFavoritesDelegate();
            return deleteDelegate.deleteSiteFFSetFavorites(customerAccount, UUID.fromString(siteId));
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return "{\"outcome\":\"Error: Failed to delete Site FF Set FavoritesVO.\"}";
        }
    }
}
