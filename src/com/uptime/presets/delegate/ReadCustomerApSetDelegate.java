/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.delegate;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.entity.CustomerApSet;
import java.util.List;
import com.uptime.cassandra.presets.dao.CustomerApSetDAO;
import java.util.UUID;

/**
 *
 * @author Joseph
 */
public class ReadCustomerApSetDelegate {
    private final CustomerApSetDAO customerApSetDAO;

    public ReadCustomerApSetDelegate() {
        customerApSetDAO = PresetsMapperImpl.getInstance().customerApSetDAO();
    }

    /**
     * Return a List of CustomerApSet Objects from customer_ap_set table by the given params
     *
     * @param customerAccount, String Object
     * @param apSetId, UUID Object
     * @return List of CustomerApSet Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<CustomerApSet> findByPK(String customerAccount, UUID apSetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return customerApSetDAO.findByPK(customerAccount, apSetId);
    }

    /**
     * Return a List of CustomerApSet Objects from customer_ap_set table by the given params
     *
     * @param customerAccount, String Object
     * @return List of CustomerApSet Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<CustomerApSet> findByCustomer(String customerAccount) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return customerApSetDAO.findByCustomer(customerAccount);
    }
}
