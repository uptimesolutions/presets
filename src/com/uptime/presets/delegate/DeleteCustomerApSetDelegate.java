/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.presets.vo.CustomerApSetVO;
import java.util.List;
import com.uptime.cassandra.presets.dao.CustomerApSetDAO;
import com.uptime.cassandra.presets.dao.GlobalApAlSetsDAO;
import com.uptime.cassandra.presets.dao.SiteApAlSetsDAO;
import com.uptime.cassandra.presets.entity.CustomerApSet;
import com.uptime.cassandra.presets.entity.GlobalApAlSets;
import com.uptime.cassandra.presets.entity.SiteApAlSets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class DeleteCustomerApSetDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteCustomerApSetDelegate.class.getName());
    private final CustomerApSetDAO customerApSetDAO;
    private final GlobalApAlSetsDAO globalApAlSetsDAO;
    private final SiteApAlSetsDAO siteApAlSetsDAO;

    /**
     * Constructor
     */
    public DeleteCustomerApSetDelegate() {
        customerApSetDAO = PresetsMapperImpl.getInstance().customerApSetDAO();
        globalApAlSetsDAO = PresetsMapperImpl.getInstance().globalApAlSetsDAO();
        siteApAlSetsDAO = PresetsMapperImpl.getInstance().siteApAlSetsDAO();
    }

    /**
     * Delete rows from customer_ap_set table for the given values
     *
     * @param customerApSetVO, CustomerApSetVO Object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String deleteCustomerApSet(CustomerApSetVO customerApSetVO) throws IllegalArgumentException {
        List<CustomerApSet> customerApSetList;
        List<SiteApAlSets> siteApAlSetsList = null;
        List<GlobalApAlSets> globalApAlSetsList = null;
        CustomerApSet customerApSet = null;

        // Find the entities based on the given values
        try {
            if ((customerApSetList = customerApSetDAO.findByPK(customerApSetVO.getCustomerAccount(), customerApSetVO.getApSetId())) != null && !customerApSetList.isEmpty()) {
                customerApSet = customerApSetList.get(0);
                if (customerApSetVO.getSiteId() != null) {
                    siteApAlSetsList = siteApAlSetsDAO.findByCustomerSiteApSet(customerApSetVO.getCustomerAccount(), customerApSetVO.getSiteId(), customerApSetVO.getApSetId());
                } else {
                    globalApAlSetsList = globalApAlSetsDAO.findByCustomerApSet(customerApSetVO.getCustomerAccount(), customerApSetVO.getApSetId());
                }
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find Customer ApSet to delete.\"}";
        }

        // Delete the entities from Cassandra
        try {
            if (customerApSet != null) {
                customerApSetDAO.delete(customerApSet);
                if (siteApAlSetsList != null) {
                    siteApAlSetsDAO.delete(customerApSetVO.getCustomerAccount(), customerApSetVO.getSiteId(), customerApSetVO.getApSetId());
                }
                if (globalApAlSetsList != null) {
                    globalApAlSetsDAO.delete(customerApSetVO.getCustomerAccount(), customerApSetVO.getApSetId());
                }
                return "{\"outcome\":\"Deleted Customer ApSet successfully.\"}";
            } else {
                return "{\"outcome\":\"No Customer ApSet found to delete.\"}";
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to delete Customer ApSet.\"}";
    }
}
