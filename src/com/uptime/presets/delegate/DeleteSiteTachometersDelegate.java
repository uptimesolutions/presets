/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.dao.SiteTachometersDAO;
import com.uptime.cassandra.presets.entity.SiteTachometers;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.presets.vo.TachometersVO;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class DeleteSiteTachometersDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteSiteTachometersDelegate.class.getName());
    private final SiteTachometersDAO siteTachometersDAO;

    /**
     * Constructor
     */
    public DeleteSiteTachometersDelegate() {
        siteTachometersDAO = PresetsMapperImpl.getInstance().siteTachometersDAO();
    }

    /**
     * Delete rows from site_tachometers table for the given values
     *
     * @param siteTachometersVO, TachometersVO Object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String deleteSiteTachometers(TachometersVO siteTachometersVO) throws IllegalArgumentException {
        List<SiteTachometers> siteTachometersList;
        SiteTachometers siteTachometers = null;

        // Find the entities based on the given values
        try {
            if ((siteTachometersList = siteTachometersDAO.findByPK(siteTachometersVO.getCustomerAccount(), siteTachometersVO.getSiteId(), siteTachometersVO.getTachId())) != null && !siteTachometersList.isEmpty()) {
                siteTachometers = siteTachometersList.get(0);
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find Site Tachometers to delete.\"}";
        }

        // Delete the entities from Cassandra
        try {
            if (siteTachometers != null) {
                siteTachometersDAO.delete(siteTachometers);
                return "{\"outcome\":\"Deleted Site Tachometers successfully.\"}";
            } else {
                return "{\"outcome\":\"No Site Tachometers found to delete.\"}";
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to delete Site Tachometers.\"}";
    }
}
