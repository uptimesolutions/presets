/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.entity.SiteFaultFrequencies;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.presets.utils.DelegateUtil;
import com.uptime.presets.vo.SiteFaultFrequenciesVO;
import java.util.List;
import com.uptime.cassandra.presets.dao.SiteFaultFrequenciesDAO;
import com.uptime.services.util.ServiceUtil;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class UpdateSiteFaultFrequenciesDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateSiteFaultFrequenciesDelegate.class.getName());
    private final SiteFaultFrequenciesDAO siteFaultFrequenciesDAO;

    public UpdateSiteFaultFrequenciesDelegate() {
        siteFaultFrequenciesDAO = PresetsMapperImpl.getInstance().siteFaultFrequenciesDAO();
    }

    /**
     * Update rows in site_fault_frequencies table based on the given object
     *
     * @param siteFaultFrequenciesVOList, List of SiteFaultFrequenciesVO object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String updateFaultFrequencies(List<SiteFaultFrequenciesVO> siteFaultFrequenciesVOList) throws IllegalArgumentException {
        List<SiteFaultFrequencies> siteFaultFrequenciesList;
        String errorMsg;

        // Insert updated entities into Cassandra 
        if (siteFaultFrequenciesVOList != null) {
            siteFaultFrequenciesList = new ArrayList();

            try {
                siteFaultFrequenciesVOList.forEach(siteFaultFrequenciesVO -> siteFaultFrequenciesList.add(DelegateUtil.getSiteFaultFrequencies(siteFaultFrequenciesVO)));
                
                if ((errorMsg = ServiceUtil.validateObjects(siteFaultFrequenciesList, false)) == null) {
                    siteFaultFrequenciesDAO.update(siteFaultFrequenciesList);
                    return "{\"outcome\":\"Updated Site Fault Frequencies successfully.\"}";
                } else {
                    return errorMsg;
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
        }
        return "{\"outcome\":\"Error: Failed to update Site Fault Frequencies.\"}";
    }
}
