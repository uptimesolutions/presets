/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.GlobalApAlSetsByCustomerDAO;
import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.presets.vo.GlobalApAlSetsVO;
import java.util.List;
import com.uptime.cassandra.presets.dao.GlobalApAlSetsDAO;
import com.uptime.cassandra.presets.entity.GlobalApAlSets;
import com.uptime.cassandra.presets.entity.GlobalApAlSetsByCustomer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class DeleteGlobalApAlSetsDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteGlobalApAlSetsDelegate.class.getName());
    private final GlobalApAlSetsDAO globalApAlSetsDAO;
    private final GlobalApAlSetsByCustomerDAO globalApAlSetsByCustomerDAO;

    /**
     * Constructor
     */
    public DeleteGlobalApAlSetsDelegate() {
        globalApAlSetsDAO = PresetsMapperImpl.getInstance().globalApAlSetsDAO();
        globalApAlSetsByCustomerDAO = PresetsMapperImpl.getInstance().globalApAlSetsByCustomerDAO();
    }

    /**
     * Delete rows from global_ap_al_sets and global_ap_al_sets_by_customer
     * tables for the given values
     *
     * @param globalApAlSetsVO, GlobalApAlSetsVO Object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String deleteGlobalApAlSets(GlobalApAlSetsVO globalApAlSetsVO) throws IllegalArgumentException {
        List<GlobalApAlSetsByCustomer> globalApAlSetsByCustomerList;
        GlobalApAlSetsByCustomer globalApAlSetsByCustomer = null;
        
        List<GlobalApAlSets> globalApAlSetsList;
        GlobalApAlSets globalApAlSets = null;

        // Find the entities based on the given values
        try {
            if ((globalApAlSetsList = globalApAlSetsDAO.findByPK(globalApAlSetsVO.getCustomerAccount(), globalApAlSetsVO.getApSetId(), globalApAlSetsVO.getAlSetId(), globalApAlSetsVO.getParamName())) != null && !globalApAlSetsList.isEmpty()) {
                globalApAlSets = globalApAlSetsList.get(0);
            }
            if (globalApAlSetsVO.getSensorType() != null && !globalApAlSetsVO.getSensorType().trim().isEmpty()) {
                if ((globalApAlSetsByCustomerList = globalApAlSetsByCustomerDAO.findByPK(globalApAlSetsVO.getCustomerAccount(), globalApAlSetsVO.getSensorType(), globalApAlSetsVO.getApSetId(), globalApAlSetsVO.getAlSetId())) != null && !globalApAlSetsByCustomerList.isEmpty()) {
                    globalApAlSetsByCustomer = globalApAlSetsByCustomerList.get(0);
                }
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find Global ApAlSets to delete.\"}";
        }

        // Delete the entities from Cassandra
        try {
            if (globalApAlSets != null) {
                globalApAlSetsDAO.delete(globalApAlSets);
                if (globalApAlSetsByCustomer != null && ((globalApAlSetsList = globalApAlSetsDAO.findByCustomerApSetAlSet(globalApAlSetsVO.getCustomerAccount(), globalApAlSetsVO.getApSetId(), globalApAlSetsVO.getAlSetId())) == null || globalApAlSetsList.isEmpty())) {
                    globalApAlSetsByCustomerDAO.delete(globalApAlSetsByCustomer);
                }
                return "{\"outcome\":\"Deleted Global ApAlSets successfully.\"}";
            } else {
                return "{\"outcome\":\"No Global ApAlSets found to delete.\"}";
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to delete Global ApAlSets.\"}";
    }
}
