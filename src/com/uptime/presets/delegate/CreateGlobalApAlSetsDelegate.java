/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.GlobalApAlSetsByCustomerDAO;
import com.uptime.cassandra.presets.dao.GlobalApAlSetsDAO;
import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.entity.GlobalApAlSets;
import com.uptime.cassandra.presets.entity.GlobalApAlSetsByCustomer;
import com.uptime.presets.utils.DelegateUtil;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.presets.vo.GlobalApAlSetsVO;
import com.uptime.services.util.ServiceUtil;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class CreateGlobalApAlSetsDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateGlobalApAlSetsDelegate.class.getName());
    private final GlobalApAlSetsDAO globalApAlSetsDAO;
    private final GlobalApAlSetsByCustomerDAO globalApAlSetsByCustomerDAO;

    public CreateGlobalApAlSetsDelegate() {
        globalApAlSetsDAO = PresetsMapperImpl.getInstance().globalApAlSetsDAO();
        globalApAlSetsByCustomerDAO = PresetsMapperImpl.getInstance().globalApAlSetsByCustomerDAO();
    }

    /**
     * Insert new rows into global_ap_al_sets and global_ap_al_sets_by_customer tables based on the given object
     *
     * @param globalApAlSetsVO
     * @return String object
     * @throws IllegalArgumentException
     */
    public String createGlobalApAlSets(GlobalApAlSetsVO globalApAlSetsVO) throws IllegalArgumentException {
        GlobalApAlSets globalApAlSets;
        GlobalApAlSetsByCustomer globalApAlSetsByCustomer;
        String errorMsg;

        if (globalApAlSetsVO != null) {
            if (globalApAlSetsVO.getApSetId() == null) {
                globalApAlSetsVO.setApSetId(UUID.randomUUID());
            }
            if (globalApAlSetsVO.getAlSetId() == null) {
                globalApAlSetsVO.setAlSetId(UUID.randomUUID());
            }

            // Insert the entities into Cassandra
            try {
                globalApAlSets = DelegateUtil.getGlobalApAlSets(globalApAlSetsVO);
                globalApAlSetsByCustomer = DelegateUtil.getGlobalApAlSetsByCustomer(globalApAlSetsVO);
                
                if (((errorMsg = ServiceUtil.validateObjectData(globalApAlSets)) == null)
                        && ((errorMsg = ServiceUtil.validateObjectData(globalApAlSets)) == null)) {
                    globalApAlSetsDAO.create(globalApAlSets);
                    globalApAlSetsByCustomerDAO.create(globalApAlSetsByCustomer);
                    return "{\"outcome\":\"New Global ApAlSets created successfully.\"}";
                } else {
                    return errorMsg;
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
        }
        return "{\"outcome\":\"Error: Failed to create new Global ApAlSets.\"}";
    }
}
