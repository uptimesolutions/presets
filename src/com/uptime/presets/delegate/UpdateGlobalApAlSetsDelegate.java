/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.GlobalApAlSetsByCustomerDAO;
import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.presets.utils.DelegateUtil;
import com.uptime.presets.vo.GlobalApAlSetsVO;
import com.uptime.cassandra.presets.dao.GlobalApAlSetsDAO;
import com.uptime.cassandra.presets.entity.GlobalApAlSets;
import com.uptime.cassandra.presets.entity.GlobalApAlSetsByCustomer;
import com.uptime.services.util.ServiceUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class UpdateGlobalApAlSetsDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateGlobalApAlSetsDelegate.class.getName());
    private final GlobalApAlSetsDAO globalApAlSetsDAO;
    private final GlobalApAlSetsByCustomerDAO globalApAlSetsByCustomerDAO;

    public UpdateGlobalApAlSetsDelegate() {
        globalApAlSetsDAO = PresetsMapperImpl.getInstance().globalApAlSetsDAO();
        globalApAlSetsByCustomerDAO = PresetsMapperImpl.getInstance().globalApAlSetsByCustomerDAO();
    }

    /**
     * Update Rows in global_ap_al_sets and global_ap_al_sets_by_customer tables based on the given object
     *
     * @param globalApAlSetsVO, GlobalApAlSetsVO object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String updateGlobalApAlSets(GlobalApAlSetsVO globalApAlSetsVO) throws IllegalArgumentException {
        GlobalApAlSetsByCustomer globalApAlSetsByCustomer;
        GlobalApAlSets globalApAlSets;
        String errorMsg;

        // Insert updated entities into Cassandra
        try {
            globalApAlSets = DelegateUtil.getGlobalApAlSets(globalApAlSetsVO);
            globalApAlSetsByCustomer = DelegateUtil.getGlobalApAlSetsByCustomer(globalApAlSetsVO);
            
            if (((errorMsg = ServiceUtil.validateObjectData(globalApAlSets)) == null)
                    && (globalApAlSetsByCustomer == null || (errorMsg = ServiceUtil.validateObjectData(globalApAlSetsByCustomer)) == null)) {
                globalApAlSetsDAO.update(globalApAlSets);
                if (globalApAlSetsByCustomer != null) {
                    globalApAlSetsByCustomerDAO.update(globalApAlSetsByCustomer);
                }
                return "{\"outcome\":\"Updated Global ApAlSets successfully.\"}";
            } else {
                return errorMsg;
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to update Global ApAlSets.\"}";
    }
}
