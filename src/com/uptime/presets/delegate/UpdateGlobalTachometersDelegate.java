/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.dao.GlobalTachometersDAO;
import com.uptime.cassandra.presets.entity.GlobalTachometers;
import static com.uptime.presets.PresetsService.sendEvent;
import static com.uptime.presets.PresetsService.sendKafkaMessage;
import com.uptime.presets.utils.DelegateUtil;
import com.uptime.presets.vo.TachometersVO;
import com.uptime.services.util.ServiceUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class UpdateGlobalTachometersDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateGlobalTachometersDelegate.class.getName());
    private final GlobalTachometersDAO globalTachometersDAO;

    public UpdateGlobalTachometersDelegate() {
        globalTachometersDAO = PresetsMapperImpl.getInstance().globalTachometersDAO();
    }

    /**
     * Update Rows in global_tachometers table based on the given object
     *
     * @param globalTachometersVO, TachometersVO object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String updateGlobalTachometers(TachometersVO globalTachometersVO) throws IllegalArgumentException {
        GlobalTachometers globalTachometers;
        String errorMsg;

        // Insert updated entities into Cassandra 
        try {
            globalTachometers = DelegateUtil.getGlobalTachometers(globalTachometersVO);
            
            if ((errorMsg = ServiceUtil.validateObjectData(globalTachometers)) == null) {
                globalTachometersDAO.update(globalTachometers);
                sendKafkaMessage(globalTachometersVO);
                return "{\"outcome\":\"Updated Global Tachometers successfully.\"}";
            } else {
                return errorMsg;
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to update Global Tachometers.\"}";
    }
}
