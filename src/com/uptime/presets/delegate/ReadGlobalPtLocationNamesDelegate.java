/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.delegate;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import java.util.List;
import com.uptime.cassandra.presets.dao.GlobalPtLocationNamesDAO;
import com.uptime.cassandra.presets.entity.GlobalPtLocationNames;

/**
 *
 * @author madhavi
 */
public class ReadGlobalPtLocationNamesDelegate {
    private final GlobalPtLocationNamesDAO globalPtLocationNamesDAO;

    public ReadGlobalPtLocationNamesDelegate() {
        globalPtLocationNamesDAO = PresetsMapperImpl.getInstance().globalPtLocationNamesDAO();
    }

    /**
     * Return a List of GlobalPtLocationNames Objects from global_pt_location_names table by the given params
     *
     * @param customerAccount, String Object
     * @param pointLocationName, String Object
     * @return List of GlobalPtLocationNames Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<GlobalPtLocationNames> findByPK(String customerAccount, String pointLocationName) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return globalPtLocationNamesDAO.findByPK(customerAccount, pointLocationName);
    }

    /**
     * Return a List of GlobalPtLocationNames Objects from global_pt_location_names table by the given params
     *
     * @param customerAccount, String Object
     * @return List of GlobalPtLocationNames Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<GlobalPtLocationNames> findByCustomer(String customerAccount) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return globalPtLocationNamesDAO.findByCustomer(customerAccount);
    }
}
