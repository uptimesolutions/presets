/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.dao.CustomerApSetDAO;
import com.uptime.cassandra.presets.entity.CustomerApSet;
import com.uptime.presets.utils.DelegateUtil;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.presets.vo.CustomerApSetVO;
import com.uptime.services.util.ServiceUtil;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class CreateCustomerApSetDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateCustomerApSetDelegate.class.getName());
    private final CustomerApSetDAO customerApSetDAO;

    public CreateCustomerApSetDelegate() {
        customerApSetDAO = PresetsMapperImpl.getInstance().customerApSetDAO();
    }

    /**
     * Insert new row into customer_ap_set table based on the given object
     *
     * @param customerApSetVO
     * @return String object
     * @throws IllegalArgumentException
     */
    public String createCustomerApSet(CustomerApSetVO customerApSetVO) throws IllegalArgumentException {
        CustomerApSet customerApSet;
        String errorMsg;
        
        if (customerApSetVO != null) {
            if (customerApSetVO.getApSetId() == null) {
                customerApSetVO.setApSetId(UUID.randomUUID());
            }

            // Insert the entities into Cassandra
            try {
                customerApSet = DelegateUtil.getCustomerApSet(customerApSetVO);
                
                if ((errorMsg = ServiceUtil.validateObjectData(customerApSet)) == null) {
                    customerApSetDAO.create(customerApSet);
                    return "{\"outcome\":\"New Customer ApSet created successfully.\"}";
                }
                else {
                    return errorMsg;
                }
            } catch (IllegalArgumentException e) {
                throw e;           
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());           
            }
        }
        return "{\"outcome\":\"Error: Failed to create new Customer ApSet.\"}";
    }
}
