/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.GlobalAssetPresetDAO;
import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.entity.GlobalAssetPreset;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.presets.utils.DelegateUtil;
import com.uptime.presets.vo.AssetPresetVO;
import com.uptime.services.util.ServiceUtil;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author venky
 */
public class UpdateGlobalAssetPresetDelegate {
    
    
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateGlobalAssetPresetDelegate.class.getName());
    private final GlobalAssetPresetDAO globalAssetPresetDAO;

    public UpdateGlobalAssetPresetDelegate() {
        globalAssetPresetDAO = PresetsMapperImpl.getInstance().globalAssetPresetDAO();
    }

    /**
     * Update Rows in global_asset_preset table based on the given object
     *
     * @param assetPresetVOList, List of GlobalAssetPresetVO objects
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String updateGlobalAssetPreset(List<AssetPresetVO> assetPresetVOList) throws IllegalArgumentException, Exception {

        List<GlobalAssetPreset> dbGlobalAssetPresetList, globalAssetPresetUpdList = new ArrayList<>(), globalAssetPresetDelList = new ArrayList<>();
        String errorMsg;
        
        // Find the GlobalAssetPreset objects present in C* but were not sent in the JSON
        try {
            if ((dbGlobalAssetPresetList = globalAssetPresetDAO.findByCustomerAssetTypePresetId(assetPresetVOList.get(0).getCustomerAccount(), assetPresetVOList.get(0).getAssetTypeName(), assetPresetVOList.get(0).getAssetPresetId())) != null && !dbGlobalAssetPresetList.isEmpty()) {
                for (GlobalAssetPreset dbSiteAssetPreset : dbGlobalAssetPresetList ) {
                    boolean found = false;
                    for (AssetPresetVO assetPreset : assetPresetVOList ) {
                        if (assetPreset.getPointLocationName().equals(dbSiteAssetPreset.getPointLocationName())) {
                            found = true;
                        }
                    }
                    if (!found)
                        globalAssetPresetDelList.add(dbSiteAssetPreset);
                }
            }

        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find Global Asset Preset to update.\"}";
        }
        try {
            for (AssetPresetVO assetPreset : assetPresetVOList ) {
                globalAssetPresetUpdList.add(DelegateUtil.getGlobalAssetPreset(assetPreset));
            }
            if ((errorMsg = ServiceUtil.validateObjects(globalAssetPresetUpdList, false)) == null) {
                globalAssetPresetDAO.update(globalAssetPresetUpdList);
                
                //delete the removed poinntlocation names from C*
                if (!globalAssetPresetDelList.isEmpty())
                    globalAssetPresetDAO.delete(globalAssetPresetDelList);
                return "{\"outcome\":\"Updated GlobalAssetPreset successfully.\"}";
            } else {
                return errorMsg;
            }

        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }

        return "{\"outcome\":\"Global Asset Preset not found to update.\"}";
    }
    
}
