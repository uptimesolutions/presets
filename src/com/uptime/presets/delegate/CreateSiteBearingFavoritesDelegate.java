/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.dao.SiteBearingFavoritesDAO;
import com.uptime.presets.utils.DelegateUtil;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.cassandra.presets.entity.SiteBearingFavorites;
import com.uptime.presets.vo.SiteBearingFavoritesVO;
import com.uptime.services.util.ServiceUtil;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class CreateSiteBearingFavoritesDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateSiteBearingFavoritesDelegate.class.getName());
    private final SiteBearingFavoritesDAO siteBearingFavoritesDAO;
   
    public CreateSiteBearingFavoritesDelegate() {
        siteBearingFavoritesDAO = PresetsMapperImpl.getInstance().siteBearingFavoritesDAO();
    }

    /**
     * Insert new rows into site_bearing_favorites table based on the given object
     *
     * @param siteBearingFavoritesVOList, List of SiteBearingFavoritesVO objects
     * @return String object
     * @throws IllegalArgumentException
     */
    public String createSiteBearingFavorites(List<SiteBearingFavoritesVO> siteBearingFavoritesVOList) throws IllegalArgumentException {
        List<SiteBearingFavorites> siteBearingFavoritesList;
        String errorMsg;
        
        siteBearingFavoritesList = new ArrayList();
        if (siteBearingFavoritesVOList != null) {

            // Insert the entities into Cassandra
            try {
                siteBearingFavoritesVOList.forEach(siteBearingFavoritesVO -> siteBearingFavoritesList.add(DelegateUtil.getSiteBearingFavorites(siteBearingFavoritesVO)));
                
                if (((errorMsg = ServiceUtil.validateObjects(siteBearingFavoritesList, false)) == null)) {
                    siteBearingFavoritesDAO.create(siteBearingFavoritesList);
                    return "{\"outcome\":\"New Site Bearing Favorites created successfully.\"}";
                }
                else {
                    return errorMsg;
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
        }
        return "{\"outcome\":\"Error: Failed to create new Site Bearing Favorites.\"}";
    }
}
