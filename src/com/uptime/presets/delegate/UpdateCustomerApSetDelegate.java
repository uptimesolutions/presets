/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.presets.utils.DelegateUtil;
import com.uptime.presets.vo.CustomerApSetVO;
import com.uptime.cassandra.presets.dao.CustomerApSetDAO;
import com.uptime.cassandra.presets.entity.CustomerApSet;
import com.uptime.services.util.ServiceUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class UpdateCustomerApSetDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateCustomerApSetDelegate.class.getName());
    private final CustomerApSetDAO customerApSetDAO;

    public UpdateCustomerApSetDelegate() {
        customerApSetDAO = PresetsMapperImpl.getInstance().customerApSetDAO();
    }

    /**
     * Update Rows in customer_ap_set table based on the given object
     *
     * @param customerApSetVO, CustomerApSetVO object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String updateCustomerApSet(CustomerApSetVO customerApSetVO) throws IllegalArgumentException {
        CustomerApSet customerApSet;
        String errorMsg;

        // Insert updated entities into Cassandra
        try {
            customerApSet = DelegateUtil.getCustomerApSet(customerApSetVO);
            
            if ((errorMsg = ServiceUtil.validateObjectData(customerApSet)) == null) {
                customerApSetDAO.update(customerApSet);
                return "{\"outcome\":\"Updated Customer ApSet successfully.\"}";
            } else {
                return errorMsg;
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to update Customer ApSet.\"}";
    }
}
