/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.dao.SiteAssetPresetDAO;
import com.uptime.cassandra.presets.entity.SiteAssetPreset;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.presets.vo.AssetPresetVO;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kpati
 */
public class DeleteSiteAssetPresetDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteSiteAssetPresetDelegate.class.getName());
    private final SiteAssetPresetDAO siteAssetPresetDAO;
    
    public DeleteSiteAssetPresetDelegate(){
        siteAssetPresetDAO = PresetsMapperImpl.getInstance().siteAssetPresetDAO();
    }
    
    /**
     * Delete rows from site_asset_preset table for the given values
     *
     * @param assetPresetVOList, List of SiteAssetPresetVO Objects
     * @return String object
     * @throws IllegalArgumentException
     */
    public String deleteSiteAssetPreset(List<AssetPresetVO> assetPresetVOList) throws IllegalArgumentException {
        List<SiteAssetPreset> siteAssetPresetList;
        List<SiteAssetPreset> siteAssetPresetDelList = new ArrayList<>();

        // Find the entities based on the given values
        try {
            for (AssetPresetVO assetPresetVO : assetPresetVOList) {
                if ((siteAssetPresetList = siteAssetPresetDAO.findByPK(assetPresetVO.getCustomerAccount(), assetPresetVO.getSiteId(), assetPresetVO.getAssetTypeName(), assetPresetVO.getAssetPresetId(), assetPresetVO.getPointLocationName())) != null && !siteAssetPresetList.isEmpty()) {
                    siteAssetPresetDelList.add(siteAssetPresetList.get(0));
                }
            }

        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find Site Asset Preset to delete.\"}";
        }

        // Delete the entities from Cassandra
        try {
            if (!siteAssetPresetDelList.isEmpty() && siteAssetPresetDelList.size() == siteAssetPresetDelList.size()) {
                siteAssetPresetDAO.delete(siteAssetPresetDelList);
                return "{\"outcome\":\"Deleted Site Asset Preset successfully.\"}";
            } else {
                return "{\"outcome\":\"Site Asset Preset not found to delete.\"}";
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        
        return "{\"outcome\":\"Error: Failed to delete Site Asset Preset.\"}";
    }
}
