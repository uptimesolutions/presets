/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.delegate;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.dao.SiteFFSetFavoritesDAO;
import com.uptime.cassandra.presets.entity.SiteFFSetFavorites;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author Joseph
 */
public class ReadSiteFFSetFavoritesDelegate {
    private final SiteFFSetFavoritesDAO siteFFSetFavoritesDAO;

    public ReadSiteFFSetFavoritesDelegate() {
        siteFFSetFavoritesDAO = PresetsMapperImpl.getInstance().siteFFSetFavoritesDAO();
    }

    /**
     * Return a List of SiteFFSetFavorites Objects from site_ff_set_favorites table by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param ffSetId, UUID Object
     * @param ffId, UUID Object
     * @return List of SiteFFSetFavorites Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteFFSetFavorites> findByPK(String customerAccount, UUID siteId, UUID ffSetId, UUID ffId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteFFSetFavoritesDAO.findByPK(customerAccount, siteId, ffSetId, ffId);
    }

    /**
     * Return a List of SiteFFSetFavorites Objects from site_ff_set_favorites table by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @return List of SiteFFSetFavorites Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteFFSetFavorites> findByCustomerSite(String customerAccount, UUID siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteFFSetFavoritesDAO.findByCustomerSite(customerAccount, siteId);
    }

}
