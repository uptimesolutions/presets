/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.dao.SiteBearingFavoritesDAO;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.cassandra.presets.entity.SiteBearingFavorites;
import com.uptime.presets.vo.SiteBearingFavoritesVO;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class DeleteSiteBearingFavoritesDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteSiteBearingFavoritesDelegate.class.getName());
    private final SiteBearingFavoritesDAO siteBearingFavoritesDAO;

    public DeleteSiteBearingFavoritesDelegate() {
        siteBearingFavoritesDAO = PresetsMapperImpl.getInstance().siteBearingFavoritesDAO();
    }

    /**
     * Delete rows from site_bearing_favorites table based on the given object
     *
     * @param siteBearingFavoritesVO, SiteBearingFavoritesVO object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String deleteSiteBearingFavorites(SiteBearingFavoritesVO siteBearingFavoritesVO) throws IllegalArgumentException {
        List<SiteBearingFavorites> siteBearingFavoritesList;
        SiteBearingFavorites siteBearingFavorites = null;

        // Find the entities based on the given values
        try {
            if ((siteBearingFavoritesList = siteBearingFavoritesDAO.findByPK(siteBearingFavoritesVO.getCustomerAcct(), siteBearingFavoritesVO.getSiteId(), siteBearingFavoritesVO.getVendorId(), siteBearingFavoritesVO.getBearingId())) != null && !siteBearingFavoritesList.isEmpty()) {
                siteBearingFavorites = siteBearingFavoritesList.get(0);
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find SiteBearingFavorites to delete.\"}";
        }

        // Delete the entities from Cassandra
        try {
            if (siteBearingFavorites != null) {
                siteBearingFavoritesDAO.delete(siteBearingFavorites);
                return "{\"outcome\":\"Deleted SiteBearingFavorites successfully.\"}";
            } else {
                return "{\"outcome\":\"No SiteBearingFavorites found to delete.\"}";
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to delete SiteBearingFavorites.\"}";
    }
}
