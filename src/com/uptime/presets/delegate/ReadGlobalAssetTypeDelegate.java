/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.delegate;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.presets.dao.GlobalAssetTypesDAO;
import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.entity.GlobalAssetTypes;
import java.util.List;


/**
 *
 * @author madhavi
 */
public class ReadGlobalAssetTypeDelegate {
    
    private final GlobalAssetTypesDAO globalAssetTypesDAO;
    
    public ReadGlobalAssetTypeDelegate() {
        globalAssetTypesDAO = PresetsMapperImpl.getInstance().globalAssetTypesDAO();
    }
     
    /**
     * Gets the GlobalAssetTypes data by customer from global_asset_types table by the given params
     *
     * @param customer
     * @return List of GlobalAssetTypes Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    public List<GlobalAssetTypes>  findByCustomer(String customer)throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return globalAssetTypesDAO.findByCustomer(customer);
    }
      
    /**
     * Gets the GlobalAssetTypes data by customer and assetTypeName from global_asset_types table by the given params
     *
     * @param customer, String Object
     * @param assetTypeName, String Object
     * @return List of GlobalAssetTypes Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
     
    public List<GlobalAssetTypes>  findByPK(String customer, String assetTypeName)throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return globalAssetTypesDAO.findByPK(customer, assetTypeName);
    }
     
}
