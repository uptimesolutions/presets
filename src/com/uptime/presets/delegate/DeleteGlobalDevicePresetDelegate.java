/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.presets.vo.DevicePresetVO;
import java.util.List;
import com.uptime.cassandra.presets.dao.GlobalDevicePresetDAO;
import com.uptime.cassandra.presets.entity.GlobalDevicePreset;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class DeleteGlobalDevicePresetDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteGlobalDevicePresetDelegate.class.getName());
    private final GlobalDevicePresetDAO globalDevicePresetDAO;

    /**
     * Constructor
     */
    public DeleteGlobalDevicePresetDelegate() {
        globalDevicePresetDAO = PresetsMapperImpl.getInstance().globalDevicePresetDAO();
    }

    /**
     * Delete rows from global_device_preset table with containing the given values
     *
     * @param devicePresetVOList, List of DevicePresetVO objects
     * @return String object
     * @throws IllegalArgumentException
     */
    public String deleteGlobalDevicePreset(List<DevicePresetVO> devicePresetVOList) throws IllegalArgumentException {
        List<GlobalDevicePreset> globalDevicePresetList;
        List<GlobalDevicePreset> globalDevicePresetDelList = new ArrayList<>();

        // Find the entities based on the given values
        try {
            for (DevicePresetVO devicePresetVO : devicePresetVOList) {
                if ((globalDevicePresetList = globalDevicePresetDAO.findByPK(devicePresetVO.getCustomerAccount(), devicePresetVO.getDeviceType(), devicePresetVO.getPresetId(), devicePresetVO.getChannelType(), devicePresetVO.getChannelNumber())) != null && !globalDevicePresetList.isEmpty()) {
                    globalDevicePresetDelList.add(globalDevicePresetList.get(0));
                }
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find Global Device Preset to delete.\"}";
        }

        // Delete the entities from Cassandra
        try {
            if (!globalDevicePresetDelList.isEmpty() && globalDevicePresetDelList.size() == devicePresetVOList.size()) {
                globalDevicePresetDAO.delete(globalDevicePresetDelList);
                return "{\"outcome\":\"Deleted Global Device Preset successfully.\"}";
            } else {
                return "{\"outcome\":\"No Global Device Preset found to delete.\"}";
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        
        return "{\"outcome\":\"Error: Failed to delete Global Device Preset.\"}";
    }
}
