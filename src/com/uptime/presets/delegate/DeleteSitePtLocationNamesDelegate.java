/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import static com.uptime.presets.PresetsService.sendEvent;
import java.util.List;
import com.uptime.cassandra.presets.dao.SitePtLocationNamesDAO;
import com.uptime.cassandra.presets.entity.SitePtLocationNames;
import com.uptime.presets.vo.PtLocationNamesVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class DeleteSitePtLocationNamesDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteSitePtLocationNamesDelegate.class.getName());
    private final SitePtLocationNamesDAO sitePtLocationNamesDAO;

    /**
     * Constructor
     */
    public DeleteSitePtLocationNamesDelegate() {
        sitePtLocationNamesDAO = PresetsMapperImpl.getInstance().sitePtLocationNamesDAO();
    }

    /**
     * Delete rows from site_pt_location_names table for the given values
     *
     * @param ptLocationNamesVO, PtLocationNamesVO Object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String deleteSitePtLocationNames(PtLocationNamesVO ptLocationNamesVO) throws IllegalArgumentException {
        List<SitePtLocationNames> sitePtLocationNamesList;
        SitePtLocationNames sitePtLocationNames = null;

        // Find the entities based on the given values
        try {
            if ((sitePtLocationNamesList = sitePtLocationNamesDAO.findByPK(ptLocationNamesVO.getCustomerAccount(), ptLocationNamesVO.getSiteId(), ptLocationNamesVO.getPointLocationName())) != null && !sitePtLocationNamesList.isEmpty()) {
                sitePtLocationNames = sitePtLocationNamesList.get(0);
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find SitePtLocationNames to delete.\"}";
        }

        // Delete the entities from Cassandra
        try {
            if (sitePtLocationNames != null) {
                sitePtLocationNamesDAO.delete(sitePtLocationNames);
                return "{\"outcome\":\"Deleted SitePtLocationNames successfully.\"}";
            } else {
                return "{\"outcome\":\"No SitePtLocationNames found to delete.\"}";
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to delete SitePtLocationNames.\"}";
    }
}
