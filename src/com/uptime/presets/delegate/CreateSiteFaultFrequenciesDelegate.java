/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.entity.SiteFaultFrequencies;
import com.uptime.presets.vo.SiteFaultFrequenciesVO;
import com.uptime.presets.utils.DelegateUtil;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.cassandra.presets.dao.SiteFaultFrequenciesDAO;
import com.uptime.services.util.ServiceUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class CreateSiteFaultFrequenciesDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateSiteFaultFrequenciesDelegate.class.getName());
    private final SiteFaultFrequenciesDAO siteFaultFrequenciesDAO;

    public CreateSiteFaultFrequenciesDelegate() {
        siteFaultFrequenciesDAO = PresetsMapperImpl.getInstance().siteFaultFrequenciesDAO();
    }

    /**
     * Insert new rows into site_fault_frequencies table based on the given objects
     *
     * @param siteFaultFrequenciesVOList, list of SiteFaultFrequenciesVO objects
     * @return String object
     * @throws IllegalArgumentException
     */
    public String createFaultFrequencies(List<SiteFaultFrequenciesVO> siteFaultFrequenciesVOList) throws IllegalArgumentException {
        List<SiteFaultFrequencies> siteFaultFrequenciesList;
        UUID setId;
        String errorMsg;

        if (siteFaultFrequenciesVOList != null) {
            siteFaultFrequenciesList = new ArrayList();
            setId = UUID.randomUUID();

            siteFaultFrequenciesVOList.stream().forEachOrdered(siteFaultFrequenciesVO -> {
                if (siteFaultFrequenciesVO != null) {
                    if (siteFaultFrequenciesVO.getFfSetId() == null) {
                        siteFaultFrequenciesVO.setFfSetId(setId);
                    }
                    if (siteFaultFrequenciesVO.getFfId() == null) {
                        siteFaultFrequenciesVO.setFfId(UUID.randomUUID());
                    }
                    siteFaultFrequenciesList.add(DelegateUtil.getSiteFaultFrequencies(siteFaultFrequenciesVO));
                }
            });

            // Insert the entities into Cassandra
            try {
                if ((errorMsg = ServiceUtil.validateObjects(siteFaultFrequenciesList, false)) == null) {
                    siteFaultFrequenciesDAO.create(siteFaultFrequenciesList);
                    return "{\"outcome\":\"New Site Fault Frequencies created successfully.\"}";
                }
                else {
                    return errorMsg;
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
        }
        return "{\"outcome\":\"Error: Failed to create new Site Fault Frequencies.\"}";
    }
}
