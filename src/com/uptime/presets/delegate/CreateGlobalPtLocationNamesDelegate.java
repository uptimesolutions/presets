/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.dao.GlobalPtLocationNamesDAO;
import com.uptime.cassandra.presets.entity.GlobalPtLocationNames;
import com.uptime.presets.utils.DelegateUtil;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.presets.vo.PtLocationNamesVO;
import com.uptime.services.util.ServiceUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class CreateGlobalPtLocationNamesDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateGlobalPtLocationNamesDelegate.class.getName());
    private final GlobalPtLocationNamesDAO globalPtLocationNamesDAO;

    public CreateGlobalPtLocationNamesDelegate() {
        globalPtLocationNamesDAO = PresetsMapperImpl.getInstance().globalPtLocationNamesDAO();
    }

    /**
     * Insert new Rows into global_pt_location_names table based on the given object
     *
     * @param ptLocationNamesVO
     * @return String object
     * @throws IllegalArgumentException
     */
    public String createGlobalPtLocationNames(PtLocationNamesVO ptLocationNamesVO) throws IllegalArgumentException {
        GlobalPtLocationNames globalPtLocationNames;
        String errorMsg;


        // Insert the entities into Cassandra
        try {
            globalPtLocationNames = DelegateUtil.getGlobalPtLocationNames(ptLocationNamesVO);
            
            if ((errorMsg = ServiceUtil.validateObjectData(globalPtLocationNames)) == null) {
                globalPtLocationNamesDAO.create(globalPtLocationNames);
                return "{\"outcome\":\"New GlobalPtLocationNames created successfully.\"}";
            } else {
                return errorMsg;
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to create new GlobalPtLocationNames.\"}";
    }
}
