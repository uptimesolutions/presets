/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.dao.SiteApAlSetsByCustomerDAO;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.presets.utils.DelegateUtil;
import com.uptime.presets.vo.SiteApAlSetsVO;
import com.uptime.cassandra.presets.dao.SiteApAlSetsDAO;
import com.uptime.cassandra.presets.entity.SiteApAlSets;
import com.uptime.cassandra.presets.entity.SiteApAlSetsByCustomer;
import com.uptime.services.util.ServiceUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class UpdateSiteApAlSetsDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateSiteApAlSetsDelegate.class.getName());
    private final SiteApAlSetsDAO siteApAlSetsDAO;
    private final SiteApAlSetsByCustomerDAO siteApAlSetsByCustomerDAO;

    public UpdateSiteApAlSetsDelegate() {
        siteApAlSetsDAO = PresetsMapperImpl.getInstance().siteApAlSetsDAO();
        siteApAlSetsByCustomerDAO = PresetsMapperImpl.getInstance().siteApAlSetsByCustomerDAO();
    }

    /**
     * Update rows in site_ap_al_sets table based on the given object
     *
     * @param siteApAlSetsVO, SiteApAlSetsVO object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String updateSiteApAlSets(SiteApAlSetsVO siteApAlSetsVO) throws IllegalArgumentException {
        SiteApAlSets siteApAlSets;
        SiteApAlSetsByCustomer siteApAlSetsByCustomer;
        String errorMsg;

        // Insert updated entities into Cassandra 
        try {
            siteApAlSets = DelegateUtil.getSiteApAlSets(siteApAlSetsVO);
            siteApAlSetsByCustomer = DelegateUtil.getSiteApAlSetsByCustomer(siteApAlSetsVO);
            
            if (((errorMsg = ServiceUtil.validateObjectData(siteApAlSets)) == null)
                    && (errorMsg = ServiceUtil.validateObjectData(siteApAlSetsByCustomer)) == null) {
                siteApAlSetsDAO.update(siteApAlSets);
                siteApAlSetsByCustomerDAO.update(siteApAlSetsByCustomer);
                return "{\"outcome\":\"Updated Site ApAlSets successfully.\"}";
            } else {
                return errorMsg;
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to update Site ApAlSets.\"}";

    }
}
