/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.entity.GlobalFaultFrequencies;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.presets.vo.GlobalFaultFrequenciesVO;
import java.util.List;
import com.uptime.cassandra.presets.dao.GlobalFaultFrequenciesDAO;
import com.uptime.presets.utils.DelegateUtil;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class DeleteGlobalFaultFrequenciesDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteGlobalFaultFrequenciesDelegate.class.getName());
    private final GlobalFaultFrequenciesDAO globalFaultFrequenciesDAO;

    /**
     * Constructor
     */
    public DeleteGlobalFaultFrequenciesDelegate() {
        globalFaultFrequenciesDAO = PresetsMapperImpl.getInstance().globalFaultFrequenciesDAO();
    }

    /**
     * Delete rows from global_fault_frequencies table for the given values
     *
     * @param globalFaultFrequenciesVOList, List of GlobalFaultFrequenciesVO Object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String deleteGlobalFaultFrequencies(List<GlobalFaultFrequenciesVO> globalFaultFrequenciesVOList) throws IllegalArgumentException {
        List<GlobalFaultFrequencies> globalFaultFrequenciesList;
        GlobalFaultFrequencies globalFaultFrequencies = null;
        
        // Find the entities based on the given values
        try {
            if ((globalFaultFrequenciesList = globalFaultFrequenciesDAO.findByPK(globalFaultFrequenciesVOList.get(0).getCustomerAccount(), globalFaultFrequenciesVOList.get(0).getFfSetId(), globalFaultFrequenciesVOList.get(0).getFfId())) != null && !globalFaultFrequenciesList.isEmpty()) {
                globalFaultFrequencies = globalFaultFrequenciesList.get(0);
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find Global Fault Frequencies to delete.\"}";
        }

        // Delete the entities from Cassandra
        try {
            if (globalFaultFrequencies != null) {
                globalFaultFrequenciesList = new ArrayList();
                for (GlobalFaultFrequenciesVO gffVo : globalFaultFrequenciesVOList) {
                    globalFaultFrequenciesList.add(DelegateUtil.getGlobalFaultFrequencies(gffVo));
                }
                globalFaultFrequenciesDAO.delete(globalFaultFrequenciesList);
                return "{\"outcome\":\"Deleted Global Fault Frequencies successfully.\"}";
            } else {
                return "{\"outcome\":\"No Global Fault Frequencies found to delete.\"}";
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to delete Global Fault Frequencies.\"}";
    }
}
