/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.entity.SiteFFSetFavorites;
import com.uptime.presets.vo.SiteFFSetFavoritesVO;
import com.uptime.presets.utils.DelegateUtil;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.cassandra.presets.dao.SiteFFSetFavoritesDAO;
import com.uptime.services.util.ServiceUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class CreateSiteFFSetFavoritesDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateSiteFFSetFavoritesDelegate.class.getName());
    private final SiteFFSetFavoritesDAO siteFFSetFavoritesDAO;

    public CreateSiteFFSetFavoritesDelegate() {
        siteFFSetFavoritesDAO = PresetsMapperImpl.getInstance().siteFFSetFavoritesDAO();
    }

    /**
     * Insert new rows into site_ff_set_favorites table based on the given objects
     *
     * @param siteFFSetFavoritesVOList, list of SiteFFSetFavoritesVO objects
     * @return String object
     * @throws IllegalArgumentException
     */
    public String createSiteFFSetFavorites(List<SiteFFSetFavoritesVO> siteFFSetFavoritesVOList) throws IllegalArgumentException {
        List<SiteFFSetFavorites> siteFFSetFavoritesList;
        UUID setId;
        String errorMsg;

        if (siteFFSetFavoritesVOList != null) {
            siteFFSetFavoritesList = new ArrayList();
            setId = UUID.randomUUID();

            siteFFSetFavoritesVOList.stream().forEachOrdered(siteFFSetFavoritesVO -> {
                if (siteFFSetFavoritesVO != null) {
                    if (siteFFSetFavoritesVO.getFfSetId() == null) {
                        siteFFSetFavoritesVO.setFfSetId(setId);
                    }
                    if (siteFFSetFavoritesVO.getFfId() == null) {
                        siteFFSetFavoritesVO.setFfId(UUID.randomUUID());
                    }
                    siteFFSetFavoritesList.add(DelegateUtil.getsiteFFSetFavorites(siteFFSetFavoritesVO));
                }
            });

            // Insert the entities into Cassandra
            try {
                if ((errorMsg = ServiceUtil.validateObjects(siteFFSetFavoritesList, false)) == null) {
                    siteFFSetFavoritesDAO.create(siteFFSetFavoritesList);
                    return "{\"outcome\":\"New Site FF Set Favorites created successfully.\"}";
                }
                else {
                    return errorMsg;
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
        }
        return "{\"outcome\":\"Error: Failed to create new Site FF Set Favorites.\"}";
    }
}
