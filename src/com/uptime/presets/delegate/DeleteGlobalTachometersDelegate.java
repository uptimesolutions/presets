/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.dao.GlobalTachometersDAO;
import com.uptime.cassandra.presets.entity.GlobalTachometers;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.presets.vo.TachometersVO;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class DeleteGlobalTachometersDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteGlobalTachometersDelegate.class.getName());
    private final GlobalTachometersDAO globalTachometersDAO;

    /**
     * Constructor
     */
    public DeleteGlobalTachometersDelegate() {
        globalTachometersDAO = PresetsMapperImpl.getInstance().globalTachometersDAO();
    }

    /**
     * Delete rows from global_tachometers table for the given values
     *
     * @param globalTachometersVO, TachometersVO Object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String deleteGlobalTachometers(TachometersVO globalTachometersVO) throws IllegalArgumentException {
        List<GlobalTachometers> globalTachometersList;
        GlobalTachometers globalTachometers = null;

        // Find the entities based on the given values
        try {
            if ((globalTachometersList = globalTachometersDAO.findByPK(globalTachometersVO.getCustomerAccount(), globalTachometersVO.getTachId())) != null && !globalTachometersList.isEmpty()) {
                globalTachometers = globalTachometersList.get(0);
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find Global Tachometers to delete.\"}";
        }

        // Delete the entities from Cassandra
        try {
            if (globalTachometers != null) {
                globalTachometersDAO.delete(globalTachometers);
                return "{\"outcome\":\"Deleted Global Tachometers successfully.\"}";
            } else {
                return "{\"outcome\":\"No Global Tachometers found to delete.\"}";
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to delete Global Tachometers.\"}";
    }
}
