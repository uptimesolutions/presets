/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.delegate;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.dao.SiteTachometersDAO;
import com.uptime.cassandra.presets.entity.SiteTachometers;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author Joseph
 */
public class ReadSiteTachometersDelegate {
    private final SiteTachometersDAO siteTachometersDAO;

    public ReadSiteTachometersDelegate() {
        siteTachometersDAO = PresetsMapperImpl.getInstance().siteTachometersDAO();
    }

    /**
     * Return a List of SiteTachometers Objects from site_tachometers table by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param tachId, UUID Object
     * @return List of SiteTachometers Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteTachometers> findByPK(String customerAccount, UUID siteId, UUID tachId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteTachometersDAO.findByPK(customerAccount, siteId, tachId);
    }

    /**
     * Return a List of SiteTachometers Objects from site_tachometers table by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @return List of SiteTachometers Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteTachometers> findByCustomerSite(String customerAccount, UUID siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteTachometersDAO.findByCustomerSite(customerAccount, siteId);
    }

}
