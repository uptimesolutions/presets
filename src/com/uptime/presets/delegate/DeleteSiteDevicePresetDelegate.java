/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.presets.vo.DevicePresetVO;
import java.util.List;
import com.uptime.cassandra.presets.dao.SiteDevicePresetDAO;
import com.uptime.cassandra.presets.entity.SiteDevicePreset;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class DeleteSiteDevicePresetDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteSiteDevicePresetDelegate.class.getName());
    private final SiteDevicePresetDAO siteDevicePresetDAO;

    /**
     * Constructor
     */
    public DeleteSiteDevicePresetDelegate() {
        siteDevicePresetDAO = PresetsMapperImpl.getInstance().siteDevicePresetDAO();
    }

    /**
     * Delete rows from site_device_preset table for the given values
     *
     * @param devicePresetVOList, List of SiteDevicePresetVO Objects
     * @return String object
     * @throws IllegalArgumentException
     */
    public String deleteSiteDevicePreset(List<DevicePresetVO> devicePresetVOList) throws IllegalArgumentException {
        List<SiteDevicePreset> siteDevicePresetList;
        List<SiteDevicePreset> siteDevicePresetDelList = new ArrayList<>();

        // Find the entities based on the given values
        try {
            for (DevicePresetVO devicePresetVO : devicePresetVOList) {
                if ((siteDevicePresetList = siteDevicePresetDAO.findByPK(devicePresetVO.getCustomerAccount(), devicePresetVO.getSiteId(), devicePresetVO.getDeviceType(), devicePresetVO.getPresetId(), devicePresetVO.getChannelType(), devicePresetVO.getChannelNumber())) != null && !siteDevicePresetList.isEmpty()) {
                    siteDevicePresetDelList.add(siteDevicePresetList.get(0));
                }
            }

        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find Site Device Preset to delete.\"}";
        }

        // Delete the entities from Cassandra
        try {
            if (!siteDevicePresetDelList.isEmpty() && siteDevicePresetDelList.size() == devicePresetVOList.size()) {
                siteDevicePresetDAO.delete(siteDevicePresetDelList);
                return "{\"outcome\":\"Deleted Site Device Preset successfully.\"}";
            } else {
                return "{\"outcome\":\"Site Device Preset not found to delete.\"}";
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        
        return "{\"outcome\":\"Error: Failed to delete Site Device Preset.\"}";
    }
}
