/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.presets.utils.DelegateUtil;
import com.uptime.presets.vo.DevicePresetVO;
import java.util.List;
import com.uptime.cassandra.presets.dao.SiteDevicePresetDAO;
import com.uptime.cassandra.presets.entity.SiteDevicePreset;
import com.uptime.services.util.ServiceUtil;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class UpdateSiteDevicePresetDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateSiteDevicePresetDelegate.class.getName());
    private final SiteDevicePresetDAO siteDevicePresetDAO;

    public UpdateSiteDevicePresetDelegate() {
        siteDevicePresetDAO = PresetsMapperImpl.getInstance().siteDevicePresetDAO();
    }

    /**
     * Update rows in site_device_preset table based on the given object
     *
     * @param devicePresetVOList, List of DevicePresetVO objects
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String updateSiteDevicePreset(List<DevicePresetVO> devicePresetVOList) throws IllegalArgumentException, Exception {
        List<SiteDevicePreset> deleteSiteDevicePresetList = new ArrayList();
        List<SiteDevicePreset> originalSiteDevicePresetList = new ArrayList();
        List<SiteDevicePreset> newSiteDevicePresetList = new ArrayList();
        DevicePresetVO dpvo;
        String errorMsg;

        try {
            originalSiteDevicePresetList = siteDevicePresetDAO.findByCustomerSiteDeviceTypePreset(devicePresetVOList.get(0).getCustomerAccount(), devicePresetVOList.get(0).getSiteId(), devicePresetVOList.get(0).getDeviceType(), devicePresetVOList.get(0).getPresetId());
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find Site Device Preset to update.\"}";
        }

        //check and add to deleteSiteDevicePresetList
        for (SiteDevicePreset siteDevicePreset : originalSiteDevicePresetList) {
            dpvo = devicePresetVOList.stream().filter(d -> d.getChannelType().equalsIgnoreCase(siteDevicePreset.getChannelType()) && d.getChannelNumber() == siteDevicePreset.getChannelNumber()).findAny().orElse(null);
            if (dpvo == null) {
                deleteSiteDevicePresetList.add(siteDevicePreset);
            }
        }

        if (!deleteSiteDevicePresetList.isEmpty()) {
            siteDevicePresetDAO.delete(deleteSiteDevicePresetList);
        }

        // Insert updated entities into Cassandra if original entities are found
        if (!originalSiteDevicePresetList.isEmpty()) {
            devicePresetVOList.forEach(devicePresetVO -> newSiteDevicePresetList.add(DelegateUtil.getSiteDevicePreset(devicePresetVO)));

            try {
                if ((errorMsg = ServiceUtil.validateObjects(newSiteDevicePresetList, false)) == null) {
                    siteDevicePresetDAO.update(newSiteDevicePresetList);
                    return "{\"outcome\":\"Updated Site Device Preset successfully.\"}";
                } else {
                    return errorMsg;
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
            return "{\"outcome\":\"Error: Failed to update Site Device Preset.\"}";
        }

        return "{\"outcome\":\"Site Device Preset not found to update.\"}";
    }
}
