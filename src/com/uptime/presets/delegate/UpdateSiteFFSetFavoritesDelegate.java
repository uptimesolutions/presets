/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.dao.SiteFFSetFavoritesDAO;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.presets.utils.DelegateUtil;
import com.uptime.presets.vo.SiteFFSetFavoritesVO;
import java.util.List;
import com.uptime.cassandra.presets.entity.SiteFFSetFavorites;
import com.uptime.presets.vo.SiteFavoritesEncapVO;
import com.uptime.services.util.ServiceUtil;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class UpdateSiteFFSetFavoritesDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateSiteFFSetFavoritesDelegate.class.getName());
    private final SiteFFSetFavoritesDAO siteFFSetFavoritesDAO;

    public UpdateSiteFFSetFavoritesDelegate() {
        siteFFSetFavoritesDAO = PresetsMapperImpl.getInstance().siteFFSetFavoritesDAO();
    }

    /**
     * Update rows in site_ff_set_favorites table based on the given object
     *
     * @param siteFavoritesEncapVO, SiteFavoritesEncapVO object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String updateSiteFFSetFavoritesVO(SiteFavoritesEncapVO siteFavoritesEncapVO) throws IllegalArgumentException {
        List<SiteFFSetFavorites> insertSiteFFSetFavoritesList;
        List<SiteFFSetFavorites> deleteSiteFFSetFavoritesList;
        String errorMsg;

        // Insert updated entities into Cassandra
        if (siteFavoritesEncapVO != null) {
            insertSiteFFSetFavoritesList = new ArrayList();
            deleteSiteFFSetFavoritesList = new ArrayList();

            if (siteFavoritesEncapVO.getCreateSiteFFSetFavoritesVOList() != null && !siteFavoritesEncapVO.getCreateSiteFFSetFavoritesVOList().isEmpty()) {
                siteFavoritesEncapVO.getCreateSiteFFSetFavoritesVOList().forEach(siteFFSetFavoritesVO -> insertSiteFFSetFavoritesList.add(DelegateUtil.getsiteFFSetFavorites(siteFFSetFavoritesVO)));
            }
            if (siteFavoritesEncapVO.getDeleteSiteFFSetFavoritesVOList() != null && !siteFavoritesEncapVO.getDeleteSiteFFSetFavoritesVOList().isEmpty()) {
                siteFavoritesEncapVO.getDeleteSiteFFSetFavoritesVOList().forEach(siteFFSetFavoritesVO -> deleteSiteFFSetFavoritesList.add(DelegateUtil.getsiteFFSetFavorites(siteFFSetFavoritesVO)));
            }

            try {
                if (((errorMsg = ServiceUtil.validateObjects(insertSiteFFSetFavoritesList, true)) == null)
                        && ((errorMsg = ServiceUtil.validateObjects(deleteSiteFFSetFavoritesList, true)) == null)) {
                    if (!insertSiteFFSetFavoritesList.isEmpty())
                        siteFFSetFavoritesDAO.create(insertSiteFFSetFavoritesList);
                    if (!deleteSiteFFSetFavoritesList.isEmpty()) 
                        siteFFSetFavoritesDAO.delete(deleteSiteFFSetFavoritesList);
                    return "{\"outcome\":\"Updated Site FF Set Favorites successfully.\"}";
                } else {
                    return errorMsg;
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
        }
        return "{\"outcome\":\"Error: Failed to update Site FF Set Favorites.\"}";
    }
}
