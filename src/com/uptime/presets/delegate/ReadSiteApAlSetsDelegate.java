/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.delegate;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.dao.SiteApAlSetsByCustomerDAO;
import com.uptime.cassandra.presets.entity.SiteApAlSets;
import java.util.List;
import com.uptime.cassandra.presets.dao.SiteApAlSetsDAO;
import com.uptime.cassandra.presets.entity.SiteApAlSetsByCustomer;
import java.util.UUID;

/**
 *
 * @author Joseph
 */
public class ReadSiteApAlSetsDelegate {
    private final SiteApAlSetsDAO siteApAlSetsDAO;
    private final SiteApAlSetsByCustomerDAO siteApAlSetsByCustomerDAO;

    public ReadSiteApAlSetsDelegate() {
        siteApAlSetsDAO = PresetsMapperImpl.getInstance().siteApAlSetsDAO();
        siteApAlSetsByCustomerDAO = PresetsMapperImpl.getInstance().siteApAlSetsByCustomerDAO();
    }

    /**
     * Return a List of SiteApAlSets Objects from site_ap_al_sets table by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param apSetId, UUID Object
     * @param alSetId, UUID Object
     * @param paramName, String Object
     * @return List of SiteApAlSets Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteApAlSets> findByPK(String customerAccount, UUID siteId, UUID apSetId, UUID alSetId, String paramName) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteApAlSetsDAO.findByPK(customerAccount, siteId, apSetId, alSetId, paramName);
    }

    /**
     * Return a List of SiteApAlSets Objects from site_ap_al_sets table by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param apSetId, UUID Object
     * @param alSetId, UUID Object
     * @return List of SiteApAlSets Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteApAlSets> findByCustomerSiteApSetAlSet(String customerAccount, UUID siteId, UUID apSetId, UUID alSetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteApAlSetsDAO.findByCustomerSiteApSetAlSet(customerAccount, siteId, apSetId, alSetId);
    }

    /**
     * Return a List of SiteApAlSets Objects from site_ap_al_sets table by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param apSetId, UUID Object
     * @return List of SiteApAlSets Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteApAlSets> findByCustomerSiteApSet(String customerAccount, UUID siteId, UUID apSetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteApAlSetsDAO.findByCustomerSiteApSet(customerAccount, siteId, apSetId);
    }

    /**
     * Return a List of SiteApAlSets Objects from site_ap_al_sets table by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param apSetIdList, List of UUID Object
     * @return List of SiteApAlSets Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteApAlSets> findByCustomerSiteApSets(String customerAccount, UUID siteId, List<UUID> apSetIdList) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteApAlSetsDAO.findByCustomerSiteApSets(customerAccount, siteId, apSetIdList);
    }

    /**
     * Return a List of SiteApAlSetsByCustomer Objects from site_ap_al_sets table by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param sensorType, String Object
     * @param apSetId, UUID Object
     * @param alSetId
     * @return List of SiteApAlSetsByCustomer Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteApAlSetsByCustomer> findByPK(String customerAccount, UUID siteId, String sensorType, UUID apSetId, UUID alSetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteApAlSetsByCustomerDAO.findByPK(customerAccount, siteId, sensorType, apSetId, alSetId);
    }

    /**
     * Return a List of SiteApAlSetsByCustomer Objects from site_ap_al_sets table by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param sensorType, String Object
     * @param apSetId, UUID Object
     * @return List of SiteApAlSetsByCustomer Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteApAlSetsByCustomer> findByCustomerSiteSensorTypeApSet(String customerAccount, UUID siteId, String sensorType, UUID apSetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteApAlSetsByCustomerDAO.findByCustomerSensorTypeApSetId(customerAccount, siteId, sensorType, apSetId);
    }

    /**
     * Return a List of SiteApAlSetsByCustomer Objects from site_ap_al_sets table by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param sensorType, String Object
     * @return List of SiteApAlSetsByCustomer Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteApAlSetsByCustomer> findByCustomerSiteSensorType(String customerAccount, UUID siteId, String sensorType) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteApAlSetsByCustomerDAO.findByCustomerSensorType(customerAccount, siteId, sensorType);
    }

    /**
     * Return a List of SiteApAlSetsByCustomer Objects from site_ap_al_sets table by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @return List of SiteApAlSetsByCustomer Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteApAlSetsByCustomer> findByCustomerSite(String customerAccount, UUID siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteApAlSetsByCustomerDAO.findByCustomerSiteId(customerAccount, siteId);
    }
}
