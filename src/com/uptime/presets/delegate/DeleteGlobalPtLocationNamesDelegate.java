/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import static com.uptime.presets.PresetsService.sendEvent;
import java.util.List;
import com.uptime.cassandra.presets.dao.GlobalPtLocationNamesDAO;
import com.uptime.cassandra.presets.entity.GlobalPtLocationNames;
import com.uptime.presets.vo.PtLocationNamesVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class DeleteGlobalPtLocationNamesDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteGlobalPtLocationNamesDelegate.class.getName());
    private final GlobalPtLocationNamesDAO globalPtLocationNamesDAO;

    /**
     * Constructor
     */
    public DeleteGlobalPtLocationNamesDelegate() {
        globalPtLocationNamesDAO = PresetsMapperImpl.getInstance().globalPtLocationNamesDAO();
    }

    /**
     * Delete rows from global_pt_location_names table for the given values
     *
     * @param ptLocationNamesVO, PtLocationNamesVO Object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String deleteGlobalPtLocationNames(PtLocationNamesVO ptLocationNamesVO) throws IllegalArgumentException {
        List<GlobalPtLocationNames> globalPtLocationNamesList;
        GlobalPtLocationNames globalPtLocationNames = null;

        // Find the entities based on the given values
        try {
            if ((globalPtLocationNamesList = globalPtLocationNamesDAO.findByPK(ptLocationNamesVO.getCustomerAccount(), ptLocationNamesVO.getPointLocationName())) != null && !globalPtLocationNamesList.isEmpty()) {
                globalPtLocationNames = globalPtLocationNamesList.get(0);
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find GlobalPtLocationNames to delete.\"}";
        }

        // Delete the entities from Cassandra
        try {
            if (globalPtLocationNames != null) {
                globalPtLocationNamesDAO.delete(globalPtLocationNames);
                return "{\"outcome\":\"Deleted GlobalPtLocationNames successfully.\"}";
            } else {
                return "{\"outcome\":\"No GlobalPtLocationNames found to delete.\"}";
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to delete GlobalPtLocationNames.\"}";
    }
}
