/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.dao.SiteAssetPresetDAO;
import com.uptime.cassandra.presets.entity.SiteAssetPreset;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.presets.utils.DelegateUtil;
import com.uptime.presets.vo.AssetPresetVO;
import com.uptime.services.util.ServiceUtil;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kpati
 */
public class UpdateSiteAssetPresetDelegate {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateSiteAssetPresetDelegate.class.getName());
    private final SiteAssetPresetDAO siteAssetPresetDAO;

    public UpdateSiteAssetPresetDelegate() {
        siteAssetPresetDAO = PresetsMapperImpl.getInstance().siteAssetPresetDAO();
    }

    /**
     * Update rows in site_asset_preset table based on the given object
     *
     * @param assetPresetVOList, List of AssetPresetVO objects
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String updateSiteAssetPreset(List<AssetPresetVO> assetPresetVOList) throws IllegalArgumentException, Exception {

        String errorMsg;
        List<SiteAssetPreset> dbSiteAssetPresetList, siteAssetPresetUpdList = new ArrayList<>(), siteAssetPresetDelList = new ArrayList<>();
        
        // Find the SiteAssetPreset objects present in C* but were not sent in the JSON
        try {
            if ((dbSiteAssetPresetList = siteAssetPresetDAO.findByCustomerSiteAssetTypePresetId(assetPresetVOList.get(0).getCustomerAccount(), assetPresetVOList.get(0).getSiteId(), assetPresetVOList.get(0).getAssetTypeName(), assetPresetVOList.get(0).getAssetPresetId())) != null && !dbSiteAssetPresetList.isEmpty()) {
                for (SiteAssetPreset dbSiteAssetPreset : dbSiteAssetPresetList ) {
                    boolean found = false;
                    for (AssetPresetVO assetPreset : assetPresetVOList ) {
                        if (assetPreset.getPointLocationName().equals(dbSiteAssetPreset.getPointLocationName())) {
                            found = true;
                        }
                    }
                    if (!found)
                        siteAssetPresetDelList.add(dbSiteAssetPreset);
                }
            }

        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find Site Asset Preset to update.\"}";
        }
                
        // Insert updated entities into Cassandra 
        try {
            for (AssetPresetVO assetPreset : assetPresetVOList ) {
                siteAssetPresetUpdList.add(DelegateUtil.getSiteAssetPreset(assetPreset));
            }
            if ((errorMsg = ServiceUtil.validateObjects(siteAssetPresetUpdList, false)) == null) {
                siteAssetPresetDAO.update(siteAssetPresetUpdList);
                
                //delete the removed poinntlocation names from C*
                if (!siteAssetPresetDelList.isEmpty())
                    siteAssetPresetDAO.delete(siteAssetPresetDelList);
                return "{\"outcome\":\"Updated SiteAssetPreset successfully.\"}";
            } else {
                return errorMsg;
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }

        return "{\"outcome\":\"Site Asset Preset not found to update.\"}";
    }

}
