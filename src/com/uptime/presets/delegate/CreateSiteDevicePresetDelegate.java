/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.dao.SiteDevicePresetDAO;
import com.uptime.presets.utils.DelegateUtil;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.cassandra.presets.entity.SiteDevicePreset;
import com.uptime.presets.vo.DevicePresetVO;
import com.uptime.services.util.ServiceUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class CreateSiteDevicePresetDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateSiteDevicePresetDelegate.class.getName());
    private final SiteDevicePresetDAO siteDevicePresetDAO;

    public CreateSiteDevicePresetDelegate() {
        siteDevicePresetDAO = PresetsMapperImpl.getInstance().siteDevicePresetDAO();
    }

    /**
     * Insert new rows into site_device_preset table based on the given object
     *
     * @param devicePresetVOList, List of devicePresetVOList Objects
     * @return String object
     * @throws IllegalArgumentException
     */
    public String createSiteDevicePreset(List<DevicePresetVO> devicePresetVOList) throws IllegalArgumentException {
        List<SiteDevicePreset> siteDevicePresetList;
        String errorMsg;
        
        siteDevicePresetList = new ArrayList();
        if (devicePresetVOList != null) {
            devicePresetVOList.stream().forEachOrdered(devicePresetVO -> {
                if (devicePresetVO != null) {
                    if (devicePresetVO.getPresetId() == null) {
                        devicePresetVO.setPresetId(UUID.randomUUID());
                    }
                    siteDevicePresetList.add(DelegateUtil.getSiteDevicePreset(devicePresetVO));
                }
            });
        
            // Insert the entities into Cassandra
            try {
                if ((errorMsg = ServiceUtil.validateObjects(siteDevicePresetList, false)) == null) {
                    siteDevicePresetDAO.create(siteDevicePresetList);
                    return "{\"outcome\":\"New Site Device Preset created successfully.\"}";
                }
                else {
                    return errorMsg;
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
        }
        return "{\"outcome\":\"Error: Failed to create new Site Device Preset.\"}";
    }
}
