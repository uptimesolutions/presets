/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.entity.SiteFaultFrequencies;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.presets.vo.SiteFaultFrequenciesVO;
import java.util.List;
import com.uptime.cassandra.presets.dao.SiteFaultFrequenciesDAO;
import com.uptime.presets.utils.DelegateUtil;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class DeleteSiteFaultFrequenciesDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteSiteFaultFrequenciesDelegate.class.getName());
    private final SiteFaultFrequenciesDAO siteFaultFrequenciesDAO;

    /**
     * Constructor
     */
    public DeleteSiteFaultFrequenciesDelegate() {
        siteFaultFrequenciesDAO = PresetsMapperImpl.getInstance().siteFaultFrequenciesDAO();
    }

    /**
     * Delete rows from site_fault_frequencies table for the given values
     *
     * @param siteFaultFrequenciesVOList, List of SiteFaultFrequenciesVO Object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String deleteFaultFrequencies(List<SiteFaultFrequenciesVO> siteFaultFrequenciesVOList) throws IllegalArgumentException {
        List<SiteFaultFrequencies> siteFaultFrequenciesList;
        SiteFaultFrequencies siteFaultFrequencies = null;

        // Find the entities based on the given values
        try {
            if ((siteFaultFrequenciesList = siteFaultFrequenciesDAO.findByPK(siteFaultFrequenciesVOList.get(0).getCustomerAccount(), siteFaultFrequenciesVOList.get(0).getSiteId(), siteFaultFrequenciesVOList.get(0).getFfSetId(), siteFaultFrequenciesVOList.get(0).getFfId())) != null && !siteFaultFrequenciesList.isEmpty()) {
                siteFaultFrequencies = siteFaultFrequenciesList.get(0);
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find Site Fault Frequencies to delete.\"}";
        }

        // Delete the entities from Cassandra
        try {
            if (siteFaultFrequencies != null) {
                siteFaultFrequenciesList = new ArrayList();
                for (SiteFaultFrequenciesVO siteFaultFrequenciesVO : siteFaultFrequenciesVOList) {
                    siteFaultFrequenciesList.add(DelegateUtil.getSiteFaultFrequencies(siteFaultFrequenciesVO));
                }
                siteFaultFrequenciesDAO.delete(siteFaultFrequenciesList);
                return "{\"outcome\":\"Deleted Site Fault Frequencies successfully.\"}";
            } else {
                return "{\"outcome\":\"No Site Fault Frequencies found to delete.\"}";
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to delete Site Fault Frequencies.\"}";
    }
}
