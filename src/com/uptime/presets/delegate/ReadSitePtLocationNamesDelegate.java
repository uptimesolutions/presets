/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.delegate;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import java.util.List;
import com.uptime.cassandra.presets.dao.SitePtLocationNamesDAO;
import com.uptime.cassandra.presets.entity.SitePtLocationNames;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
public class ReadSitePtLocationNamesDelegate {
    private final SitePtLocationNamesDAO sitePtLocationNamesDAO;

    public ReadSitePtLocationNamesDelegate() {
        sitePtLocationNamesDAO = PresetsMapperImpl.getInstance().sitePtLocationNamesDAO();
    }

    /**
     * Return a List of SitePtLocationNames Objects from site_pt_location_names table by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param pointLocationName, String Object
     * @return List of SitePtLocationNames Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SitePtLocationNames> findByPK(String customerAccount, UUID siteId, String pointLocationName) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return sitePtLocationNamesDAO.findByPK(customerAccount, siteId, pointLocationName);
    }

    /**
     * Return a List of SitePtLocationNames Objects from site_pt_location_names table by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @return List of SitePtLocationNames Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SitePtLocationNames> findByCustomerSite(String customerAccount, UUID siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return sitePtLocationNamesDAO.findByCustomerSite(customerAccount, siteId);
    }
}
