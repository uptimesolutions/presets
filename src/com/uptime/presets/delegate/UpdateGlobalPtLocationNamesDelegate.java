/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.presets.utils.DelegateUtil;
import com.uptime.presets.vo.PtLocationNamesVO;
import com.uptime.cassandra.presets.dao.GlobalPtLocationNamesDAO;
import com.uptime.cassandra.presets.entity.GlobalPtLocationNames;
import com.uptime.services.util.ServiceUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class UpdateGlobalPtLocationNamesDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateGlobalPtLocationNamesDelegate.class.getName());
    private final GlobalPtLocationNamesDAO globalPtLocationNamesDAO;

    public UpdateGlobalPtLocationNamesDelegate() {
        globalPtLocationNamesDAO = PresetsMapperImpl.getInstance().globalPtLocationNamesDAO();
    }

    /**
     * Update Rows in global_pt_location_names table based on the given object
     *
     * @param ptLocationNamesVO, PtLocationNamesVO object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String updateGlobalPtLocationNames(PtLocationNamesVO ptLocationNamesVO) throws IllegalArgumentException {
        GlobalPtLocationNames globalPtLocationNames;
        String errorMsg;

        // Insert updated entities into Cassandra 
        try {
            globalPtLocationNames = DelegateUtil.getGlobalPtLocationNames(ptLocationNamesVO);
            
            if ((errorMsg = ServiceUtil.validateObjectData(globalPtLocationNames)) == null) {
                globalPtLocationNamesDAO.update(globalPtLocationNames);
                return "{\"outcome\":\"Updated GlobalPtLocationNames successfully.\"}";
            } else {
                return errorMsg;
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to update GlobalPtLocationNames.\"}";
    }
}
