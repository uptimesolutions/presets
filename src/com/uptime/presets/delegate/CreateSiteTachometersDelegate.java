/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.dao.SiteTachometersDAO;
import com.uptime.cassandra.presets.entity.SiteTachometers;
import static com.uptime.presets.PresetsService.sendEvent;
import static com.uptime.presets.PresetsService.sendKafkaMessage;
import com.uptime.presets.utils.DelegateUtil;
import com.uptime.presets.vo.TachometersVO;
import com.uptime.services.util.ServiceUtil;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class CreateSiteTachometersDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateSiteTachometersDelegate.class.getName());
    private final SiteTachometersDAO siteTachometersDAO;
    
    public CreateSiteTachometersDelegate() {
        siteTachometersDAO = PresetsMapperImpl.getInstance().siteTachometersDAO();
    }

    /**
     * Insert new rows into site_tachometers table based on the given object
     *
     * @param siteTachometersVO, TachometersVO object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String CreateSiteTachometers(TachometersVO siteTachometersVO) throws IllegalArgumentException {
        SiteTachometers siteTachometers;
        String errorMsg;
        
        if (siteTachometersVO != null) {
            if (siteTachometersVO.getTachId() == null) {
                siteTachometersVO.setTachId(UUID.randomUUID());
            }

            // Insert the entities into Cassandra
            try {
                siteTachometers = DelegateUtil.getSiteTachometers(siteTachometersVO);

                if ((errorMsg = ServiceUtil.validateObjectData(siteTachometers)) == null) {
                    siteTachometersDAO.create(siteTachometers);
                    sendKafkaMessage(siteTachometersVO);
                    return "{\"outcome\":\"New SiteTachometers created successfully.\"}";
                }
                else {
                    return errorMsg;
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
        }
        return "{\"outcome\":\"Error: Failed to create new SiteTachometers.\"}";
    }
}
