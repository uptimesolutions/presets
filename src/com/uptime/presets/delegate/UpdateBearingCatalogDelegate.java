/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.BearingCatalogDAO;
import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.presets.utils.DelegateUtil;
import com.uptime.cassandra.presets.entity.BearingCatalog;
import com.uptime.presets.vo.BearingCatalogVO;
import com.uptime.services.util.ServiceUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class UpdateBearingCatalogDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateBearingCatalogDelegate.class.getName());
    private final BearingCatalogDAO bearingCatalogDAO;

    public UpdateBearingCatalogDelegate() {
        bearingCatalogDAO = PresetsMapperImpl.getInstance().bearingCatalogDAO();
    }

    /**
     * Update Rows in bearing_catalog table based on the given object
     *
     * @param bearingCatalogVO, BearingCatalogVO object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String updateBearingCatalog(BearingCatalogVO bearingCatalogVO) throws IllegalArgumentException {
        BearingCatalog bearingCatalog;
        String errorMsg;

        // Insert updated entities into Cassandra
        try {
            bearingCatalog = DelegateUtil.getbearingCatalog(bearingCatalogVO);
            
            if ((errorMsg = ServiceUtil.validateObjectData(bearingCatalog)) == null) {
                bearingCatalogDAO.update(bearingCatalog);
                return "{\"outcome\":\"Updated Bearing Catalog successfully.\"}";
            } else {
                return errorMsg;
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to update Bearing Catalog.\"}";
    }
}
