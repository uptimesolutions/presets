/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.delegate;

import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.entity.GlobalFaultFrequencies;
import com.uptime.presets.vo.GlobalFaultFrequenciesVO;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.cassandra.presets.dao.GlobalFaultFrequenciesDAO;
import com.uptime.presets.utils.DelegateUtil;
import com.uptime.services.util.ServiceUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class CreateGlobalFaultFrequenciesDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateGlobalFaultFrequenciesDelegate.class.getName());
    private final GlobalFaultFrequenciesDAO globalFaultFrequenciesDAO;

    public CreateGlobalFaultFrequenciesDelegate() {
        globalFaultFrequenciesDAO = PresetsMapperImpl.getInstance().globalFaultFrequenciesDAO();
    }

    /**
     * Insert new Rows into Cassandra based on the given object
     *
     * @param globalFaultFrequenciesVOList
     * @return String object
     * @throws IllegalArgumentException
     */
    public String createGlobalFaultFrequencies(List<GlobalFaultFrequenciesVO> globalFaultFrequenciesVOList) throws IllegalArgumentException {
        List<GlobalFaultFrequencies> globalFaultFrequenciesList;
        UUID setId;
        String errorMsg;

        if (globalFaultFrequenciesVOList != null) { 
            setId = UUID.randomUUID();
            globalFaultFrequenciesList = new ArrayList();

            globalFaultFrequenciesVOList.stream().forEachOrdered(globalFaultFrequenciesVO -> {
                if (globalFaultFrequenciesVO != null) {
                    if (globalFaultFrequenciesVO.getFfSetId() == null) {
                        globalFaultFrequenciesVO.setFfSetId(setId);
                    }
                    if (globalFaultFrequenciesVO.getFfId() == null) {
                        globalFaultFrequenciesVO.setFfId(UUID.randomUUID());
                    }
                    globalFaultFrequenciesList.add(DelegateUtil.getGlobalFaultFrequencies(globalFaultFrequenciesVO));
                }
            });

            // Insert the entities into Cassandra
            try {
                if ((errorMsg = ServiceUtil.validateObjects(globalFaultFrequenciesList, false)) == null) {
                    globalFaultFrequenciesDAO.create(globalFaultFrequenciesList);
                    return "{\"outcome\":\"New Global Fault Frequencies created successfully.\"}";
                } else {
                    return errorMsg;
                }
            } catch (IllegalArgumentException e) {
                    throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
        }
        return "{\"outcome\":\"Error: Failed to create new Global Fault Frequencies.\"}";
    }
}
