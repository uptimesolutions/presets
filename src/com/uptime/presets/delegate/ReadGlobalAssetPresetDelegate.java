/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.delegate;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.presets.dao.GlobalAssetPresetDAO;
import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.entity.GlobalAssetPreset;
import java.util.List;
import java.util.UUID;


/**
 *
 * @author venka
 */
public class ReadGlobalAssetPresetDelegate {
    
    private final GlobalAssetPresetDAO globalAssetPresetDAO;
    
    public ReadGlobalAssetPresetDelegate() {
        globalAssetPresetDAO = PresetsMapperImpl.getInstance().globalAssetPresetDAO();
    }
     
    /**
     * Gets the GlobalAssetPreset data by customer and assetTypeName from global_asset_preset table by the given params
     *
     * @param customer
     * @param assetTypeName
     * @return List of GlobalAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
     
    public List<GlobalAssetPreset>  findByCustomerAssetType(String customer, String assetTypeName)throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return globalAssetPresetDAO.findByCustomerAssetType(customer, assetTypeName);
    }
     
     /**
     * Gets the GlobalAssetPreset data by customer , assetTypeName and assetPresetId from global_asset_preset table by the given params
     *
     * @param customer
     * @param assetTypeName
     * @param assetPresetId
     * @return List of GlobalAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    public List<GlobalAssetPreset>  findByCustomerAssetTypePresetId(String customer, String assetTypeName, UUID assetPresetId)throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return globalAssetPresetDAO.findByCustomerAssetTypePresetId(customer, assetTypeName,assetPresetId);
    }
      
    
    
    /**
     * Gets the GlobalAssetPreset data by customer , assetTypeName, assetPresetId and pointLocationName from global_asset_preset table by the given params
     *
     * @param customer
     * @param assetTypeName
     * @param assetPresetId
     * @param pointLocationName
     * @return List of GlobalAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    public List<GlobalAssetPreset>  findByPK(String customer, String assetTypeName, UUID assetPresetId, String pointLocationName)throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return globalAssetPresetDAO.findByPK(customer, assetTypeName,assetPresetId,pointLocationName);
    }
    
    
    
    /**
     * Gets the GlobalAssetPreset data by customer  from global_asset_preset table by the given params
     *
     * @param customer
     * @return List of GlobalAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
     
    public List<GlobalAssetPreset>  findByCustomer(String customer)throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return globalAssetPresetDAO.findByCustomer(customer);
    }

}
