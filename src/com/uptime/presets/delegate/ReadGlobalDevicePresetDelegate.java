/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.delegate;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.entity.GlobalDevicePreset;
import java.util.List;
import com.uptime.cassandra.presets.dao.GlobalDevicePresetDAO;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
public class ReadGlobalDevicePresetDelegate {
    private final GlobalDevicePresetDAO globalDevicePresetDAO;

    public ReadGlobalDevicePresetDelegate() {
        globalDevicePresetDAO = PresetsMapperImpl.getInstance().globalDevicePresetDAO();
    }

    /**
     * Return a List of GlobalDevicePreset Objects from global_device_preset table by the given params
     *
     * @param customerAccount, String Object
     * @param deviceType, String Object
     * @param presetId, UUID Object
     * @param channelType, String Object
     * @param channelNumber, byte
     * @return List of GlobalDevicePreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<GlobalDevicePreset> findByPK(String customerAccount, String deviceType, UUID presetId, String channelType, byte channelNumber) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return globalDevicePresetDAO.findByPK(customerAccount, deviceType, presetId, channelType, channelNumber);
    }

    /**
     * Return a List of GlobalDevicePreset Objects from global_device_preset table by the given params
     *
     * @param customerAccount, String Object
     * @param deviceType, String Object
     * @param presetId, UUID Object
     * @return List of GlobalDevicePreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<GlobalDevicePreset> findByCustomerDeviceTypePreset(String customerAccount, String deviceType, UUID presetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return globalDevicePresetDAO.findByCustomerDeviceTypePreset(customerAccount, deviceType, presetId);
    }

    /**
     * Return a List of GlobalDevicePreset Objects from global_device_preset table by the given params
     *
     * @param customerAccount, String Object
     * @param deviceType, String Object
     * @return List of GlobalDevicePreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<GlobalDevicePreset> findByCustomerDeviceType(String customerAccount, String deviceType) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return globalDevicePresetDAO.findByCustomerDeviceType(customerAccount, deviceType);
    }

    /**
     * Return a List of GlobalDevicePreset Objects from global_device_preset table by the given params
     *
     * @param customerAccount, String Object
     * @return List of GlobalDevicePreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<GlobalDevicePreset> findByCustomer(String customerAccount) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return globalDevicePresetDAO.findByCustomer(customerAccount);
    }
}
