/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.delegate;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import java.util.List;
import com.uptime.cassandra.presets.dao.SiteBearingFavoritesDAO;
import com.uptime.cassandra.presets.entity.SiteBearingFavorites;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
public class ReadSiteBearingFavoritesDelegate {
    private final SiteBearingFavoritesDAO siteBearingFavoritesDAO;

    public ReadSiteBearingFavoritesDelegate() {
        siteBearingFavoritesDAO = PresetsMapperImpl.getInstance().siteBearingFavoritesDAO();
    }

    /**
     * Return a List of SiteBearingFavorites Objects from site_bearing_favorites table by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param vendorId, String Object
     * @param bearingId, String Object
     * @return List of SiteBearingFavorites Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteBearingFavorites> findByPK(String customerAccount, UUID siteId, String vendorId, String bearingId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteBearingFavoritesDAO.findByPK(customerAccount, siteId, vendorId, bearingId);
    }

    /**
     * Return a List of SiteBearingFavorites Objects from site_bearing_favorites table by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param vendorId, String Object
     * @return List of SiteBearingFavorites Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteBearingFavorites> findByCustomerSiteVendor(String customerAccount, UUID siteId, String vendorId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteBearingFavoritesDAO.findByCustomerSiteVendor(customerAccount, siteId, vendorId);
    }

    /**
     * Return a List of SiteBearingFavorites Objects from site_bearing_favorites table by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @return List of SiteBearingFavorites Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteBearingFavorites> findByCustomerSite(String customerAccount, UUID siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteBearingFavoritesDAO.findByCustomerSite(customerAccount, siteId);
    }
}
