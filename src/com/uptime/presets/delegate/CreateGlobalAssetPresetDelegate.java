/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.GlobalAssetPresetDAO;

import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.entity.GlobalAssetPreset;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.presets.utils.DelegateUtil;
import com.uptime.presets.vo.AssetPresetVO;
import com.uptime.services.util.ServiceUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author venka
 */
public class CreateGlobalAssetPresetDelegate {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateGlobalAssetPresetDelegate.class.getName());
    private final GlobalAssetPresetDAO globalAssetPresetDAO;

    public CreateGlobalAssetPresetDelegate() {
        globalAssetPresetDAO = PresetsMapperImpl.getInstance().globalAssetPresetDAO();
    }
    
    /**
     * Insert new rows into global_asset_preset table based on the given object
     *
     * @param assetPresetVOList, List of GlobalAssetPresetVO objects
     * @return String object
     * @throws IllegalArgumentException
     */
    
    public String createGlobalAssetPreset(List<AssetPresetVO> assetPresetVOList) throws IllegalArgumentException {
        List<GlobalAssetPreset> globalAssetPresetList;
        String errorMsg;

        if (assetPresetVOList != null) {
            UUID assetPresetId=UUID.randomUUID();
            globalAssetPresetList = new ArrayList();
            assetPresetVOList.stream().forEachOrdered(assetPresetVO -> {
                if (assetPresetVO != null && assetPresetVO.getAssetPresetId() == null) {
                    assetPresetVO.setAssetPresetId(assetPresetId);
                }
                globalAssetPresetList.add(DelegateUtil.getGlobalAssetPreset(assetPresetVO));
               
            });

            // Insert the entities into Cassandra
            try {
                if ((errorMsg = ServiceUtil.validateObjects(globalAssetPresetList, false)) == null) {
                    globalAssetPresetDAO.create(globalAssetPresetList);
                    return "{\"outcome\":\"New Global Asset Preset created successfully.\"}";
                } else {
                    return errorMsg;
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
        }
        return "{\"outcome\":\"Error: Failed to create new Global Asset Preset.\"}";
    }
    
}
