/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.presets.utils.DelegateUtil;
import com.uptime.presets.vo.PtLocationNamesVO;
import com.uptime.cassandra.presets.dao.SitePtLocationNamesDAO;
import com.uptime.cassandra.presets.entity.SitePtLocationNames;
import com.uptime.services.util.ServiceUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class UpdateSitePtLocationNamesDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateSitePtLocationNamesDelegate.class.getName());
    private final SitePtLocationNamesDAO sitePtLocationNamesDAO;

    public UpdateSitePtLocationNamesDelegate() {
        sitePtLocationNamesDAO = PresetsMapperImpl.getInstance().sitePtLocationNamesDAO();
    }

    /**
     * Update rows in site_pt_location_names table based on the given object
     *
     * @param ptLocationNamesVO, PtLocationNamesVO object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String updateSitePtLocationNames(PtLocationNamesVO ptLocationNamesVO) throws IllegalArgumentException {
        SitePtLocationNames sitePtLocationNames;
        String errorMsg;

        // Insert updated entities into Cassandra 
        try {
            sitePtLocationNames = DelegateUtil.getSitePtLocationNames(ptLocationNamesVO);
            
            if ((errorMsg = ServiceUtil.validateObjectData(sitePtLocationNames)) == null) {
                sitePtLocationNamesDAO.update(sitePtLocationNames);
                return "{\"outcome\":\"Updated SitePtLocationNames successfully.\"}";
            } else {
                return errorMsg;
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to update SitePtLocationNames.\"}";
    }
}
