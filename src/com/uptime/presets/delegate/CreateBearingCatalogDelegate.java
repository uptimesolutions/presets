/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.BearingCatalogDAO;
import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.entity.BearingCatalog;
import com.uptime.presets.utils.DelegateUtil;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.presets.vo.BearingCatalogVO;
import com.uptime.services.util.ServiceUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class CreateBearingCatalogDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateBearingCatalogDelegate.class.getName());
    private final BearingCatalogDAO bearingCatalogDAO;

    public CreateBearingCatalogDelegate() {
        bearingCatalogDAO = PresetsMapperImpl.getInstance().bearingCatalogDAO();
    }

    /**
     * Insert new row into bearing_catalog table based on the given object
     *
     * @param bearingCatalogVO
     * @return String object
     * @throws IllegalArgumentException
     */
    public String createBearingCatalog(BearingCatalogVO bearingCatalogVO) throws IllegalArgumentException {
        BearingCatalog bearingCatalog;
        String errorMsg;

        // Insert the entities into Cassandra
        try {
            bearingCatalog = DelegateUtil.getbearingCatalog(bearingCatalogVO);
            
            if ((errorMsg = ServiceUtil.validateObjectData(bearingCatalog)) == null) {
                bearingCatalogDAO.create(bearingCatalog);
                return "{\"outcome\":\"New Bearing Catalog created successfully.\"}";
            } else {
                return errorMsg;
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to create new Bearing Catalog.\"}";
    }
}
