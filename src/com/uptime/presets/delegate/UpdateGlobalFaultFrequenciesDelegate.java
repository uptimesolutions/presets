/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.entity.GlobalFaultFrequencies;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.presets.utils.DelegateUtil;
import com.uptime.presets.vo.GlobalFaultFrequenciesVO;
import java.util.List;
import com.uptime.cassandra.presets.dao.GlobalFaultFrequenciesDAO;
import com.uptime.services.util.ServiceUtil;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class UpdateGlobalFaultFrequenciesDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateGlobalFaultFrequenciesDelegate.class.getName());
    private final GlobalFaultFrequenciesDAO globalFaultFrequenciesDAO;

    public UpdateGlobalFaultFrequenciesDelegate() {
        globalFaultFrequenciesDAO = PresetsMapperImpl.getInstance().globalFaultFrequenciesDAO();
    }

    /**
     * Update Rows in global_fault_frequencies table based on the given object
     *
     * @param globalFaultFrequenciesVOList, list of GlobalFaultFrequenciesVO object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String updateGlobalFaultFrequencies(List<GlobalFaultFrequenciesVO> globalFaultFrequenciesVOList) throws IllegalArgumentException {
        List<GlobalFaultFrequencies> globalFaultFrequenciesList;
        String errorMsg;

        // Insert updated entities into Cassandra 
        if (globalFaultFrequenciesVOList != null) {
            globalFaultFrequenciesList = new ArrayList();

            try {
                globalFaultFrequenciesVOList.forEach(vo -> globalFaultFrequenciesList.add(DelegateUtil.getGlobalFaultFrequencies(vo)));

                if ((errorMsg = ServiceUtil.validateObjects(globalFaultFrequenciesList, false)) == null) {
                    globalFaultFrequenciesDAO.update(globalFaultFrequenciesList);
                    return "{\"outcome\":\"Updated Global Fault Frequencies successfully.\"}";
                } else {
                    return errorMsg;
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
        }
        return "{\"outcome\":\"Error: Failed to update Global Fault Frequencies.\"}";
    }
}
