/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.delegate;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.entity.SiteFaultFrequencies;
import java.util.List;
import com.uptime.cassandra.presets.dao.SiteFaultFrequenciesDAO;
import java.util.UUID;

/**
 *
 * @author Joseph
 */
public class ReadSiteFaultFrequenciesDelegate {
    private final SiteFaultFrequenciesDAO siteFaultFrequenciesDAO;

    public ReadSiteFaultFrequenciesDelegate() {
        siteFaultFrequenciesDAO = PresetsMapperImpl.getInstance().siteFaultFrequenciesDAO();
    }

    /**
     * Return a List of SiteFaultFrequencies Objects from site_fault_frequencies table by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param ffSetId, UUID Object
     * @param ffId, UUID Object
     * @return List of SiteFaultFrequencies Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteFaultFrequencies> findByPK(String customerAccount, UUID siteId, UUID ffSetId, UUID ffId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteFaultFrequenciesDAO.findByPK(customerAccount, siteId, ffSetId, ffId);
    }

    /**
     * Return a List of SiteFaultFrequencies Objects from site_fault_frequencies table by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @return List of SiteFaultFrequencies Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteFaultFrequencies> findByCustomerSite(String customerAccount, UUID siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteFaultFrequenciesDAO.findByCustomerSite(customerAccount, siteId);
    }

    /**
     * Return a List of SiteFaultFrequencies Objects from site_fault_frequencies table by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param ffSetId, UUID Object
     * @return List of SiteFaultFrequencies Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteFaultFrequencies> findByCustomerSiteSetName(String customerAccount, UUID siteId, UUID ffSetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteFaultFrequenciesDAO.findByCustomerSiteSet(customerAccount, siteId, ffSetId);
    }
}
