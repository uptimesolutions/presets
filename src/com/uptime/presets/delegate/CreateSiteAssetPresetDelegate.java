/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.dao.SiteAssetPresetDAO;
import com.uptime.cassandra.presets.entity.SiteAssetPreset;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.presets.utils.DelegateUtil;
import com.uptime.presets.vo.AssetPresetVO;
import com.uptime.services.util.ServiceUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kpati
 */
public class CreateSiteAssetPresetDelegate {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateSiteAssetPresetDelegate.class.getName());
    private final SiteAssetPresetDAO siteAssetPresetDAO;
    public CreateSiteAssetPresetDelegate(){
        siteAssetPresetDAO = PresetsMapperImpl.getInstance().siteAssetPresetDAO();
    }
    
    /**
     * Insert new rows into site_asset_preset table based on the given object
     *
     * @param assetPresetVOList, List of AssetPresetVOList Objects
     * @return String object
     * @throws IllegalArgumentException
     */
    
     public String createSiteAssetPreset(List<AssetPresetVO> assetPresetVOList) throws IllegalArgumentException {
        
        List<SiteAssetPreset> siteAssetPresetList = new ArrayList<>();
        String errorMsg;

        if (assetPresetVOList != null) {
            UUID assetPresetId = UUID.randomUUID();
            assetPresetVOList.stream()
                .filter(Objects::nonNull)
                .forEachOrdered(assetPresetVO -> {
                    if (assetPresetVO.getAssetPresetId() == null)
                        assetPresetVO.setAssetPresetId(assetPresetId);
                    siteAssetPresetList.add(DelegateUtil.getSiteAssetPreset(assetPresetVO));
                });

            // Insert the entities into Cassandra
            try {
                errorMsg = ServiceUtil.validateObjects(siteAssetPresetList, false);
                if (errorMsg == null) {
                    siteAssetPresetDAO.create(siteAssetPresetList);
                    return "{\"outcome\":\"New Site Asset Preset created successfully.\"}";
                } else {
                    return String.format("{\"error\":\"%s\"}", errorMsg);
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error("An unexpected error occurred while creating the Site Asset Preset.", e);
                sendEvent(e.getStackTrace());
                return "{\"error\":\"An unexpected error occurred while creating the Site Asset Preset.\"}";
            }
        } else {
            return "{\"error\":\"AssetPresetVOList is null.\"}";
        }
    }
    
}
