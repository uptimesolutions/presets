/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.delegate;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.dao.GlobalTachometersDAO;
import com.uptime.cassandra.presets.entity.GlobalTachometers;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author Joseph
 */
public class ReadGlobalTachometersDelegate {
    private final GlobalTachometersDAO globalTachometersDAO;

    public ReadGlobalTachometersDelegate() {
        globalTachometersDAO = PresetsMapperImpl.getInstance().globalTachometersDAO();
    }

    /**
     * Return a List of Tachometers Objects from global_tachometers table by the given params
     *
     * @param customerAccount, String Object
     * @param tachId, UUID Object
     * @return List of Tachometers Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<GlobalTachometers> findByPK(String customerAccount, UUID tachId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return globalTachometersDAO.findByPK(customerAccount, tachId);
    }

    /**
     * Return a List of Tachometers Objects from global_tachometers table by the given params
     *
     * @param customerAccount, String Object
     * @return List of Tachometers Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<GlobalTachometers> findByCustomer(String customerAccount) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return globalTachometersDAO.findByCustomer(customerAccount);
    }

}
