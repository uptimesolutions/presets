/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.dao.SiteBearingFavoritesDAO;
import com.uptime.presets.utils.DelegateUtil;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.cassandra.presets.entity.SiteBearingFavorites;
import com.uptime.presets.vo.SiteBearingFavoritesVO;
import com.uptime.presets.vo.SiteFavoritesEncapVO;
import com.uptime.services.util.ServiceUtil;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class UpdateSiteBearingFavoritesDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateSiteBearingFavoritesDelegate.class.getName());
    private final SiteBearingFavoritesDAO siteBearingFavoritesDAO;

    public UpdateSiteBearingFavoritesDelegate() {
        siteBearingFavoritesDAO = PresetsMapperImpl.getInstance().siteBearingFavoritesDAO();
    }

    /**
     * Update rows in the site_bearing_favorites table based on the given object
     *
     * @param siteFavoritesEncapVO, SiteFavoritesEncapVO object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String updateSiteBearingFavorites(SiteFavoritesEncapVO siteFavoritesEncapVO) throws IllegalArgumentException {
        List<SiteBearingFavorites> insertSiteBearingFavoritesList;
        List<SiteBearingFavorites> deleteSiteBearingFavoritesList;
        String errorMsg;

        if (siteFavoritesEncapVO != null) {
            insertSiteBearingFavoritesList = new ArrayList();
            deleteSiteBearingFavoritesList = new ArrayList();
            
            if (siteFavoritesEncapVO.getCreateSiteBearingFavoritesVOList() != null && !siteFavoritesEncapVO.getCreateSiteBearingFavoritesVOList().isEmpty()) {
                siteFavoritesEncapVO.getCreateSiteBearingFavoritesVOList().forEach(siteBearingFavoritesVO -> insertSiteBearingFavoritesList.add(DelegateUtil.getSiteBearingFavorites(siteBearingFavoritesVO)));
            }

            if (siteFavoritesEncapVO.getDeleteSiteBearingFavoritesVOList() != null && !siteFavoritesEncapVO.getDeleteSiteBearingFavoritesVOList().isEmpty()) {
                siteFavoritesEncapVO.getDeleteSiteBearingFavoritesVOList().forEach(siteBearingFavoritesVO -> deleteSiteBearingFavoritesList.add(DelegateUtil.getSiteBearingFavorites(siteBearingFavoritesVO)));
            }

            // Insert updated entities into Cassandra
            try {
                if (((errorMsg = ServiceUtil.validateObjects(insertSiteBearingFavoritesList, true)) == null)
                        && ((errorMsg = ServiceUtil.validateObjects(deleteSiteBearingFavoritesList, true)) == null)) {
                    if (!insertSiteBearingFavoritesList.isEmpty()) {
                        siteBearingFavoritesDAO.create(insertSiteBearingFavoritesList);
                    }
                    if (!deleteSiteBearingFavoritesList.isEmpty()) {
                        siteBearingFavoritesDAO.delete(deleteSiteBearingFavoritesList);
                    }
                    return "{\"outcome\":\"Updated Site Bearing Favorites successfully.\"}";
                } else {
                    return errorMsg;
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
        }
        return "{\"outcome\":\"Error: Failed to update Site Bearing Favorites.\"}";
    }
}
