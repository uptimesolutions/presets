/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.delegate;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.presets.dao.GlobalApAlSetsByCustomerDAO;
import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.entity.GlobalApAlSets;
import java.util.List;
import com.uptime.cassandra.presets.dao.GlobalApAlSetsDAO;
import com.uptime.cassandra.presets.entity.GlobalApAlSetsByCustomer;
import java.util.UUID;

/**
 *
 * @author Joseph
 */
public class ReadGlobalApAlSetsDelegate {
    private final GlobalApAlSetsDAO globalApAlSetsDAO;
    private final GlobalApAlSetsByCustomerDAO globalApAlSetsByCustomerDAO;

    public ReadGlobalApAlSetsDelegate() {
        globalApAlSetsDAO = PresetsMapperImpl.getInstance().globalApAlSetsDAO();
        globalApAlSetsByCustomerDAO = PresetsMapperImpl.getInstance().globalApAlSetsByCustomerDAO();
    }

    /**
     * Return a List of GlobalApAlSets Objects from global_ap_al_sets table by the given params
     *
     * @param customerAccount, String Object
     * @param apSetId, UUID Object
     * @param alSetId, UUID Object
     * @param paramName, String Object
     * @return List of GlobalApAlSets Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<GlobalApAlSets> findByPK(String customerAccount, UUID apSetId, UUID alSetId, String paramName) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return globalApAlSetsDAO.findByPK(customerAccount, apSetId, alSetId, paramName);
    }

    /**
     * Return a List of GlobalApAlSets Objects from global_ap_al_sets table by the given params
     *
     * @param customerAccount, String Object
     * @param apSetId, UUID Object
     * @param alSetId, UUID Object
     * @return List of GlobalApAlSets Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<GlobalApAlSets> findByCustomerApSetAlSet(String customerAccount, UUID apSetId, UUID alSetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return globalApAlSetsDAO.findByCustomerApSetAlSet(customerAccount, apSetId, alSetId);
    }

    /**
     * Return a List of GlobalApAlSets Objects from global_ap_al_sets table by the given params
     *
     * @param customerAccount, String Object
     * @param apSetIdList, List of UUID Object
     * @return List of GlobalApAlSets Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<GlobalApAlSets> findByCustomerApSets(String customerAccount, List<UUID> apSetIdList) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return globalApAlSetsDAO.findByCustomerApSets(customerAccount, apSetIdList);
    }

    /**
     * Return a List of GlobalApAlSets Objects from global_ap_al_sets table by the given params
     *
     * @param customerAccount, String Object
     * @param apSetId, UUID Object
     * @return List of GlobalApAlSets Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<GlobalApAlSets> findByCustomerApSet(String customerAccount, UUID apSetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return globalApAlSetsDAO.findByCustomerApSet(customerAccount, apSetId);
    }

    /**
     * Return a List of GlobalApAlSetsByCustomer Objects from global_ap_al_sets_by_customer table by the given params
     *
     * @param customerAccount, String Object
     * @param sensorType, String Object
     * @param apSetId, UUID Object
     * @param alSetId, UUID Object
     * @return List of GlobalApAlSetsByCustomer Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<GlobalApAlSetsByCustomer> findByPK(String customerAccount, String sensorType, UUID apSetId, UUID alSetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return globalApAlSetsByCustomerDAO.findByPK(customerAccount, sensorType, apSetId, alSetId);
    }

    /**
     * Return a List of GlobalApAlSetsByCustomer Objects from global_ap_al_sets_by_customer table by the given params
     *
     * @param customerAccount, String Object
     * @param sensorType, String Object
     * @param apSetId, UUID Object
     * @return List of GlobalApAlSetsByCustomer Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<GlobalApAlSetsByCustomer> findByCustomerSensorTypeApSet(String customerAccount, String sensorType, UUID apSetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return globalApAlSetsByCustomerDAO.findByCustomerSensorTypeApSet(customerAccount, sensorType, apSetId);
    }

    /**
     * Return a List of GlobalApAlSetsByCustomer from global_ap_al_sets_by_customer table Objects by the given params
     *
     * @param customerAccount, String Object
     * @param sensorType, String Object
     * @return List of GlobalApAlSetsByCustomer Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<GlobalApAlSetsByCustomer> findByCustomerSensorType(String customerAccount, String sensorType) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return globalApAlSetsByCustomerDAO.findByCustomerSensorType(customerAccount, sensorType);
    }

    /**
     * Return a List of GlobalApAlSetsByCustomer from global_ap_al_sets_by_customer table Objects by the given params
     *
     * @param customerAccount, String Object
     * @return List of GlobalApAlSetsByCustomer Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<GlobalApAlSetsByCustomer> findByCustomer(String customerAccount) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return globalApAlSetsByCustomerDAO.findByCustomer(customerAccount);
    }
}
