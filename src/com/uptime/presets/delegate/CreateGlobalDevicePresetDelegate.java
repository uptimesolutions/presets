/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.GlobalDevicePresetDAO;
import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.presets.utils.DelegateUtil;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.cassandra.presets.entity.GlobalDevicePreset;
import com.uptime.presets.vo.DevicePresetVO;
import com.uptime.services.util.ServiceUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class CreateGlobalDevicePresetDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateGlobalDevicePresetDelegate.class.getName());
    private final GlobalDevicePresetDAO globalDevicePresetDAO;

    public CreateGlobalDevicePresetDelegate() {
        globalDevicePresetDAO = PresetsMapperImpl.getInstance().globalDevicePresetDAO();
    }

    /**
     * Insert new rows into global_device_preset table based on the given object
     *
     * @param devicePresetVOList, List of DevicePresetVO objects
     * @return String object
     * @throws IllegalArgumentException
     */
    public String createGlobalDevicePreset(List<DevicePresetVO> devicePresetVOList) throws IllegalArgumentException {
        List<GlobalDevicePreset> globalDevicePresetList;
        String errorMsg;

        if (devicePresetVOList != null) {
            globalDevicePresetList = new ArrayList();
            devicePresetVOList.stream().forEachOrdered(devicePresetVO -> {
                if (devicePresetVO != null) {
                    if (devicePresetVO.getPresetId() == null) {
                        devicePresetVO.setPresetId(UUID.randomUUID());
                    }
                }
                globalDevicePresetList.add(DelegateUtil.getGlobalDevicePreset(devicePresetVO));
            });

            // Insert the entities into Cassandra
            try {
                if ((errorMsg = ServiceUtil.validateObjects(globalDevicePresetList, false)) == null) {
                    globalDevicePresetDAO.create(globalDevicePresetList);
                    return "{\"outcome\":\"New Global Device Preset created successfully.\"}";
                } else {
                    return errorMsg;
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
        }
        return "{\"outcome\":\"Error: Failed to create new Global Device Preset.\"}";
    }
}
