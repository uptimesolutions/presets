/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.delegate;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.entity.GlobalFaultFrequencies;
import java.util.List;
import com.uptime.cassandra.presets.dao.GlobalFaultFrequenciesDAO;
import java.util.UUID;

/**
 *
 * @author Joseph
 */
public class ReadGlobalFaultFrequenciesDelegate {
    private final GlobalFaultFrequenciesDAO globalFaultFrequenciesDAO;

    public ReadGlobalFaultFrequenciesDelegate() {
        globalFaultFrequenciesDAO = PresetsMapperImpl.getInstance().globalFaultFrequenciesDAO();
    }

    /**
     * Return a List of GlobalFaultFrequencies Objects from the global_fault_frequencies table by the given params
     *
     * @param customerAccount, String Object
     * @param ffSetId, UUID Object
     * @param ffId, UUID Object
     * @return List of GlobalFaultFrequencies Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<GlobalFaultFrequencies> findByPK(String customerAccount, UUID ffSetId, UUID ffId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return globalFaultFrequenciesDAO.findByPK(customerAccount, ffSetId, ffId);
    }

    /**
     * Return a List of GlobalFaultFrequencies Objects from the global_fault_frequencies table by the given params
     *
     * @param customerAccount, String Object
     * @return List of GlobalFaultFrequencies Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<GlobalFaultFrequencies> findByCustomer(String customerAccount) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return globalFaultFrequenciesDAO.findByCustomer(customerAccount);
    }

    /**
     * Return a List of GlobalFaultFrequencies Objects from the global_fault_frequencies table by the given params
     *
     * @param customerAccount, String Object
     * @param setId, UUID Object
     * @return List of GlobalFaultFrequencies Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<GlobalFaultFrequencies> findByCustomerSetId(String customerAccount, UUID setId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return globalFaultFrequenciesDAO.findByCustomerSetId(customerAccount, setId);
    }
}
