/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.GlobalAssetPresetDAO;

import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.entity.GlobalAssetPreset;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.presets.vo.AssetPresetVO;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author venka
 */
public class DeleteGlobalAssetPresetDelegate {

    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteGlobalAssetPresetDelegate.class.getName());
    private final GlobalAssetPresetDAO globalAssetPresetDAO;

    /**
     * Constructor
     */
    public DeleteGlobalAssetPresetDelegate() {
        globalAssetPresetDAO = PresetsMapperImpl.getInstance().globalAssetPresetDAO();
    }

    /**
     * Delete rows from global_asset_preset table with containing the given
     * values
     *
     * @param assetPresetVOList, List of DevicePresetVO objects
     * @return String object
     * @throws IllegalArgumentException
     */
    public String deleteGlobalAssetPreset(List<AssetPresetVO> assetPresetVOList) throws IllegalArgumentException {
        List<GlobalAssetPreset> globalAssetPresetList;
        List<GlobalAssetPreset> globalAssetPresetDelList = new ArrayList<>();

        // Find the entities based on the given values
        try {
            for (AssetPresetVO assetPresetVO : assetPresetVOList) {
                if ((globalAssetPresetList = globalAssetPresetDAO.findByPK(assetPresetVO.getCustomerAccount(), assetPresetVO.getAssetTypeName(), assetPresetVO.getAssetPresetId(), assetPresetVO.getPointLocationName())) != null && !globalAssetPresetList.isEmpty()) {
                    globalAssetPresetDelList.add(globalAssetPresetList.get(0));
                }
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find Global Asset Preset to delete.\"}";
        }

        // Delete the entities from Cassandra
        try {
            if (!globalAssetPresetDelList.isEmpty() && globalAssetPresetDelList.size() == assetPresetVOList.size()) {
                globalAssetPresetDAO.delete(globalAssetPresetDelList);
                return "{\"outcome\":\"Deleted Global Asset Preset successfully.\"}";
            } else {
                return "{\"outcome\":\"No Global Asset Preset found to delete.\"}";
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }

        return "{\"outcome\":\"Error: Failed to delete Global Asset Preset.\"}";
    }

}
