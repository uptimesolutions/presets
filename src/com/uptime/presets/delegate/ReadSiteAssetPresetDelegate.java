/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.delegate;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.dao.SiteAssetPresetDAO;
import com.uptime.cassandra.presets.entity.SiteAssetPreset;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author kpati
 */
public class ReadSiteAssetPresetDelegate {
    
    private final SiteAssetPresetDAO siteAssetPresetDAO;

    public ReadSiteAssetPresetDelegate() {
        siteAssetPresetDAO = PresetsMapperImpl.getInstance().siteAssetPresetDAO();
    }
    
    /**
     * Return a List of SiteAssetPreset Objects from site_asset_preset table by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param assetTypeName, String Object
     * @param assetPresetId, String Object
     * @param pointLocationName, byte
     * @return List of SiteAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteAssetPreset> findByPK(String customerAccount, UUID siteId, String assetTypeName, UUID assetPresetId, String pointLocationName) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteAssetPresetDAO.findByPK(customerAccount, siteId, assetTypeName, assetPresetId, pointLocationName);
    }


    /**
     * Return a List of SiteAssetPreset Objects from site_asset_preset table by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param assetTypeName, String Object
     * @param assetPresetId, UUID Object
     * @return List of SiteAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteAssetPreset> findByCustomerSiteAssetTypePresetId(String customerAccount, UUID siteId, String assetTypeName, UUID assetPresetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteAssetPresetDAO.findByCustomerSiteAssetTypePresetId(customerAccount, siteId, assetTypeName, assetPresetId);
    }
    
    /**
     * Return a List of SiteAssetPreset Objects from site_asset_preset table by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param assetTypeName, String Object
     * @return List of SiteAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteAssetPreset> findByCustomerSiteAssetType(String customerAccount, UUID siteId, String assetTypeName) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteAssetPresetDAO.findByCustomerSiteAssetType(customerAccount, siteId, assetTypeName);
    }
    
    /**
     * Return a List of SiteAssetPreset Objects from site_asset_preset table by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @return List of SiteAssetPreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteAssetPreset> findByCustomerSite(String customerAccount, UUID siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteAssetPresetDAO.findByCustomerSite(customerAccount, siteId);
    }
    
}
