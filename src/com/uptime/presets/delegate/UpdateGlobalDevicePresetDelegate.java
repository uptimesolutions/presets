/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.presets.utils.DelegateUtil;
import com.uptime.presets.vo.DevicePresetVO;
import java.util.List;
import com.uptime.cassandra.presets.dao.GlobalDevicePresetDAO;
import com.uptime.cassandra.presets.entity.GlobalDevicePreset;
import com.uptime.services.util.ServiceUtil;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class UpdateGlobalDevicePresetDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateGlobalDevicePresetDelegate.class.getName());
    private final GlobalDevicePresetDAO globalDevicePresetDAO;

    public UpdateGlobalDevicePresetDelegate() {
        globalDevicePresetDAO = PresetsMapperImpl.getInstance().globalDevicePresetDAO();
    }

    /**
     * Update Rows in global_device_preset table based on the given object
     *
     * @param devicePresetVOList, List of DevicePresetVO objects
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String updateGlobalDevicePreset(List<DevicePresetVO> devicePresetVOList) throws IllegalArgumentException, Exception {
        List<GlobalDevicePreset> deleteGlobalDevicePresetList = new ArrayList();
        List<GlobalDevicePreset> originalGlobalDevicePresetList = new ArrayList();
        List<GlobalDevicePreset> newGlobalDevicePresetList = new ArrayList();
        List<GlobalDevicePreset> globalDevicePresetList;
        String errorMsg;

        try {
            for (DevicePresetVO devicePresetVO : devicePresetVOList) {
                if ((globalDevicePresetList = globalDevicePresetDAO.findByPK(devicePresetVO.getCustomerAccount(), devicePresetVO.getDeviceType(), devicePresetVO.getPresetId(), devicePresetVO.getChannelType(), devicePresetVO.getChannelNumber())) != null && !globalDevicePresetList.isEmpty()) {
                    originalGlobalDevicePresetList.add(globalDevicePresetList.get(0));
                }
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find Global Device Preset to update.\"}";
        }

        //check and add to deleteSiteDevicePresetList
        for (GlobalDevicePreset devicePreset : originalGlobalDevicePresetList) {
            DevicePresetVO dpvo = devicePresetVOList.stream().filter(d -> d.getChannelType().equalsIgnoreCase(devicePreset.getChannelType()) && d.getChannelNumber() == devicePreset.getChannelNumber()).findAny().orElse(null);
            if (dpvo == null) {
                deleteGlobalDevicePresetList.add(devicePreset);
            }
        }
        if (!originalGlobalDevicePresetList.isEmpty() && originalGlobalDevicePresetList.size() > 0) {
            if (!deleteGlobalDevicePresetList.isEmpty()) {
                globalDevicePresetDAO.delete(deleteGlobalDevicePresetList);
            }
            // Insert updated entities into Cassandra if original entities are found
            devicePresetVOList.forEach(devicePresetVO -> newGlobalDevicePresetList.add(DelegateUtil.getGlobalDevicePreset(devicePresetVO)));

            try {
                if ((errorMsg = ServiceUtil.validateObjects(newGlobalDevicePresetList, false)) == null) {
                    globalDevicePresetDAO.create(newGlobalDevicePresetList);
                    return "{\"outcome\":\"Updated Global Device Preset successfully.\"}";
                } else {
                    return errorMsg;
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
        }

        return "{\"outcome\":\"Global Device Preset not found to update.\"}";
    }
}
