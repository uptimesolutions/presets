/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.BearingCatalogDAO;
import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import static com.uptime.presets.PresetsService.sendEvent;
import java.util.List;
import com.uptime.cassandra.presets.entity.BearingCatalog;
import com.uptime.presets.vo.BearingCatalogVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class DeleteBearingCatalogDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteBearingCatalogDelegate.class.getName());
    private final BearingCatalogDAO bearingCatalogDAO;

    /**
     * Constructor
     */
    public DeleteBearingCatalogDelegate() {
        bearingCatalogDAO = PresetsMapperImpl.getInstance().bearingCatalogDAO();
    }

    /**
     * Delete rows from bearing_catalog table for the given values
     *
     * @param bearingCatalogVO, BearingCatalogVO Object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String deleteBearingCatalog(BearingCatalogVO bearingCatalogVO) throws IllegalArgumentException {
        List<BearingCatalog> bearingCatalogList;
        BearingCatalog bearingCatalog = null;

        // Find the entities based on the given values
        try {
            if ((bearingCatalogList = bearingCatalogDAO.findByPK(bearingCatalogVO.getVendorId(), bearingCatalogVO.getBearingId())) != null && !bearingCatalogList.isEmpty()) {
                bearingCatalog = bearingCatalogList.get(0);
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find Customer ApSet to delete.\"}";
        }

        // Delete the entities from Cassandra
        try {
            if (bearingCatalog != null) {
                bearingCatalogDAO.delete(bearingCatalog);

                return "{\"outcome\":\"Deleted Bearing Catalog successfully.\"}";
            } else {
                return "{\"outcome\":\"No Bearing Catalog found to delete.\"}";
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to delete Bearing Catalog.\"}";
    }
}
