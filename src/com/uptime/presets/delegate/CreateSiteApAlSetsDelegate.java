/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.dao.SiteApAlSetsByCustomerDAO;
import com.uptime.cassandra.presets.dao.SiteApAlSetsDAO;
import com.uptime.cassandra.presets.entity.SiteApAlSets;
import com.uptime.presets.utils.DelegateUtil;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.cassandra.presets.entity.SiteApAlSetsByCustomer;
import com.uptime.presets.vo.SiteApAlSetsVO;
import com.uptime.services.util.ServiceUtil;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class CreateSiteApAlSetsDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateSiteApAlSetsDelegate.class.getName());
    private final SiteApAlSetsDAO siteApAlSetsDAO;
    private final SiteApAlSetsByCustomerDAO siteApAlSetsByCustomerDAO;

    public CreateSiteApAlSetsDelegate() {
        siteApAlSetsDAO = PresetsMapperImpl.getInstance().siteApAlSetsDAO();
        siteApAlSetsByCustomerDAO= PresetsMapperImpl.getInstance().siteApAlSetsByCustomerDAO();
    }

    /**
     * Insert new rows into site_ap_al_sets table based on the given object
     *
     * @param siteApAlSetsVO, SiteApAlSetsVO object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String createSiteApAlSets(SiteApAlSetsVO siteApAlSetsVO) throws IllegalArgumentException {
        SiteApAlSets siteApAlSets;
        SiteApAlSetsByCustomer siteApAlSetsByCustomer;
        String errorMsg;
        
        if (siteApAlSetsVO != null) {
            if (siteApAlSetsVO.getApSetId() == null) {
                siteApAlSetsVO.setApSetId(UUID.randomUUID());
            }
            if (siteApAlSetsVO.getAlSetId() == null) {
                siteApAlSetsVO.setAlSetId(UUID.randomUUID());
            }

            // Insert the entities into Cassandra
            try {
                siteApAlSets = DelegateUtil.getSiteApAlSets(siteApAlSetsVO);
                siteApAlSetsByCustomer = DelegateUtil.getSiteApAlSetsByCustomer(siteApAlSetsVO);
                
                if (((errorMsg = ServiceUtil.validateObjectData(siteApAlSets)) == null) &&
                        ((errorMsg = ServiceUtil.validateObjectData(siteApAlSetsByCustomer)) == null)) {
                    siteApAlSetsDAO.create(siteApAlSets);
                    siteApAlSetsByCustomerDAO.create(siteApAlSetsByCustomer);
                    return "{\"outcome\":\"New Site ApAlSets created successfully.\"}";
                }
                else {
                    return errorMsg;
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
        }
        return "{\"outcome\":\"Error: Failed to create new Site ApAlSets.\"}";
    }
}
