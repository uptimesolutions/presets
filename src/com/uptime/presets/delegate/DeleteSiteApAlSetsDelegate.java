/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.dao.SiteApAlSetsByCustomerDAO;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.presets.vo.SiteApAlSetsVO;
import java.util.List;
import com.uptime.cassandra.presets.dao.SiteApAlSetsDAO;
import com.uptime.cassandra.presets.entity.SiteApAlSets;
import com.uptime.cassandra.presets.entity.SiteApAlSetsByCustomer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class DeleteSiteApAlSetsDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteSiteApAlSetsDelegate.class.getName());
    private final SiteApAlSetsDAO siteApAlSetsDAO;
    private final SiteApAlSetsByCustomerDAO siteApAlSetsByCustomerDAO;

    /**
     * Constructor
     */
    public DeleteSiteApAlSetsDelegate() {
        siteApAlSetsDAO = PresetsMapperImpl.getInstance().siteApAlSetsDAO();
        siteApAlSetsByCustomerDAO = PresetsMapperImpl.getInstance().siteApAlSetsByCustomerDAO();
    }

    /**
     * Delete rows from site_ap_al_sets table for the given values
     *
     * @param siteApAlSetsVO, SiteApAlSetsVO Object
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String deleteSiteApAlSets(SiteApAlSetsVO siteApAlSetsVO) throws IllegalArgumentException, Exception {
        List<SiteApAlSets> siteApAlSetsList;
        SiteApAlSets siteApAlSets = null;

        List<SiteApAlSetsByCustomer> siteApAlSetsByCustomerList;
        SiteApAlSetsByCustomer siteApAlSetsByCustomer = null;
        
        // Find the entities based on the given values
        try {
            if ((siteApAlSetsList = siteApAlSetsDAO.findByPK(siteApAlSetsVO.getCustomerAccount(), siteApAlSetsVO.getSiteId(), siteApAlSetsVO.getApSetId(), siteApAlSetsVO.getAlSetId(), siteApAlSetsVO.getParamName())) != null && !siteApAlSetsList.isEmpty()) {
                siteApAlSets = siteApAlSetsList.get(0);
            }
            if ((siteApAlSetsByCustomerList = siteApAlSetsByCustomerDAO.findByPK(siteApAlSetsVO.getCustomerAccount(), siteApAlSetsVO.getSiteId(), siteApAlSetsVO.getSensorType(), siteApAlSetsVO.getApSetId(), siteApAlSetsVO.getAlSetId())) != null && !siteApAlSetsByCustomerList.isEmpty()) {
                siteApAlSetsByCustomer = siteApAlSetsByCustomerList.get(0);
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find Site ApAlSets to delete.\"}";
        }

        // Delete the entities from Cassandra
        try {
            if (siteApAlSets != null) {
                siteApAlSetsDAO.delete(siteApAlSets);
                if (siteApAlSetsByCustomer != null && ((siteApAlSetsList = siteApAlSetsDAO.findByCustomerSiteApSetAlSet(siteApAlSetsVO.getCustomerAccount(), siteApAlSetsVO.getSiteId(), siteApAlSetsVO.getApSetId(), siteApAlSetsVO.getAlSetId())) == null || siteApAlSetsList.isEmpty())) {
                    siteApAlSetsByCustomerDAO.delete(siteApAlSetsByCustomer);
                }
                return "{\"outcome\":\"Deleted Site ApAlSets successfully.\"}";
            } else {
                return "{\"outcome\":\"No Site ApAlSets found to delete.\"}";
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to delete Site ApAlSets.\"}";
    }
}
