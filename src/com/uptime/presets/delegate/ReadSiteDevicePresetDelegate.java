/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.delegate;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.entity.SiteDevicePreset;
import java.util.List;
import com.uptime.cassandra.presets.dao.SiteDevicePresetDAO;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
public class ReadSiteDevicePresetDelegate {
    private final SiteDevicePresetDAO siteDevicePresetDAO;

    public ReadSiteDevicePresetDelegate() {
        siteDevicePresetDAO = PresetsMapperImpl.getInstance().siteDevicePresetDAO();
    }

    /**
     * Return a List of SiteDevicePreset Objects from site_device_preset table by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param deviceType, String Object
     * @param presetId, UUID Object
     * @param channelType, String Object
     * @param channelNumber, byte
     * @return List of SiteDevicePreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteDevicePreset> findByPK(String customerAccount, UUID siteId, String deviceType, UUID presetId, String channelType, byte channelNumber) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteDevicePresetDAO.findByPK(customerAccount, siteId, deviceType, presetId, channelType, channelNumber);
    }

    /**
     * Return a List of SiteDevicePreset Objects from site_device_preset table by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param deviceType, String Object
     * @param presetId, UUID Object
     * @return List of SiteDevicePreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteDevicePreset> findByCustomerSiteDeviceTypePreset(String customerAccount, UUID siteId, String deviceType, UUID presetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteDevicePresetDAO.findByCustomerSiteDeviceTypePreset(customerAccount, siteId, deviceType, presetId);
    }

    /**
     * Return a List of SiteDevicePreset Objects from site_device_preset table by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param deviceType, String Object
     * @return List of SiteDevicePreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteDevicePreset> findByCustomerSiteDeviceType(String customerAccount, UUID siteId, String deviceType) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteDevicePresetDAO.findByCustomerSiteDeviceType(customerAccount, siteId, deviceType);
    }

    /**
     * Return a List of SiteDevicePreset Objects from site_device_preset table by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @return List of SiteDevicePreset Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteDevicePreset> findByCustomerSite(String customerAccount, UUID siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteDevicePresetDAO.findByCustomerSite(customerAccount, siteId);
    }
}
