/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.dao.GlobalTachometersDAO;
import com.uptime.cassandra.presets.entity.GlobalTachometers;
import static com.uptime.presets.PresetsService.sendEvent;
import static com.uptime.presets.PresetsService.sendKafkaMessage;
import com.uptime.presets.utils.DelegateUtil;
import com.uptime.presets.vo.TachometersVO;
import com.uptime.services.util.ServiceUtil;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class CreateGlobalTachometersDelegate {
     private static final Logger LOGGER = LoggerFactory.getLogger(CreateGlobalTachometersDelegate.class.getName());
    private final GlobalTachometersDAO globalTachometersDAO;

    public CreateGlobalTachometersDelegate() {
        globalTachometersDAO = PresetsMapperImpl.getInstance().globalTachometersDAO();
    }

    /**
     * Insert new Rows into global_tachometers table based on the given object
     *
     * @param globalTachometersVO, TachometersVO object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String CreateGlobalTachometers(TachometersVO globalTachometersVO) throws IllegalArgumentException {
        GlobalTachometers globalTachometers;
        String errorMsg;
        
        if (globalTachometersVO != null) {
            if (globalTachometersVO.getTachId() == null) {
                globalTachometersVO.setTachId(UUID.randomUUID());
            }

            // Insert the entities into Cassandra
            try {
                globalTachometers = DelegateUtil.getGlobalTachometers(globalTachometersVO);
                
                if ((errorMsg = ServiceUtil.validateObjectData(globalTachometers)) == null) {
                    globalTachometersDAO.create(globalTachometers);
                    //Send message to Kafka
                    sendKafkaMessage(globalTachometersVO);
                    return "{\"outcome\":\"New GlobalTachometers created successfully.\"}";
                }
                else {
                    return errorMsg;
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
        }
        return "{\"outcome\":\"Error: Failed to create new GlobalTachometers.\"}";
    }
}
