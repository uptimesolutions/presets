/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.delegate;

import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.entity.SiteFFSetFavorites;
import static com.uptime.presets.PresetsService.sendEvent;
import com.uptime.presets.vo.SiteFFSetFavoritesVO;
import java.util.List;
import com.uptime.cassandra.presets.dao.SiteFFSetFavoritesDAO;
import com.uptime.presets.utils.DelegateUtil;
import java.util.ArrayList;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class DeleteSiteFFSetFavoritesDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteSiteFFSetFavoritesDelegate.class.getName());
    private final SiteFFSetFavoritesDAO siteFFSetFavoritesDAO;

    /**
     * Constructor
     */
    public DeleteSiteFFSetFavoritesDelegate() {
        siteFFSetFavoritesDAO = PresetsMapperImpl.getInstance().siteFFSetFavoritesDAO();
    }

    /**
     * Delete rows from site_ff_set_favorites table for the given values
     *
     * @param siteFFSetFavoritesVOList, List of SiteFFSetFavoritesVO Object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String deleteSiteFFSetFavorites(List<SiteFFSetFavoritesVO> siteFFSetFavoritesVOList) throws IllegalArgumentException {
        List<SiteFFSetFavorites> siteFFSetFavoritesList;
        SiteFFSetFavorites siteFaultFrequencies = null;

        // Find the entities based on the given values
        try {
            if ((siteFFSetFavoritesList = siteFFSetFavoritesDAO.findByPK(siteFFSetFavoritesVOList.get(0).getCustomerAccount(), siteFFSetFavoritesVOList.get(0).getSiteId(), siteFFSetFavoritesVOList.get(0).getFfSetId(), siteFFSetFavoritesVOList.get(0).getFfId())) != null && !siteFFSetFavoritesList.isEmpty()) {
                siteFaultFrequencies = siteFFSetFavoritesList.get(0);
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find Site FF Set Favorites to delete.\"}";
        }

        // Delete the entities from Cassandra
        try {
            if (siteFaultFrequencies != null) {
                siteFFSetFavoritesList = new ArrayList();
                for (SiteFFSetFavoritesVO siteFFSetFavoritesVO : siteFFSetFavoritesVOList) {
                    siteFFSetFavoritesList.add(DelegateUtil.getsiteFFSetFavorites(siteFFSetFavoritesVO));
                }
                siteFFSetFavoritesDAO.delete(siteFFSetFavoritesList);
                return "{\"outcome\":\"Deleted Site FF Set Favorites successfully.\"}";
            } else {
                return "{\"outcome\":\"No Site FF Set Favorites found to delete.\"}";
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to delete Site FF Set Favorites.\"}";
    }

    /**
     * Delete rows from site_ff_set_favorites table for the given values
     *
     * @param customerAccount
     * @param siteId
     * @param ffSetId
     * @return String object
     */
    public String deleteSiteFFSetFavorites(String customerAccount, UUID siteId, UUID ffSetId) {
        // Delete the entities from Cassandra
        try {
            siteFFSetFavoritesDAO.delete(customerAccount, siteId, ffSetId);
            return "{\"outcome\":\"Deleted Site FF Set Favorites successfully.\"}";
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to delete Site FF Set Favorites.\"}";
    }

    /**
     * Delete rows from site_ff_set_favorites table for the given values
     *
     * @param customerAccount
     * @param siteId
     * @return String object
     */
    public String deleteSiteFFSetFavorites(String customerAccount, UUID siteId) {
        // Delete the entities from Cassandra
        try {
            siteFFSetFavoritesDAO.delete(customerAccount, siteId);
            return "{\"outcome\":\"Deleted Site FF Set Favorites successfully.\"}";
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to delete Site FF Set Favorites.\"}";
    }

}
