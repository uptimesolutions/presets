/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.utils;

import com.uptime.cassandra.presets.entity.BearingCatalog;
import com.uptime.cassandra.presets.entity.CustomerApSet;
import com.uptime.cassandra.presets.entity.GlobalApAlSets;
import com.uptime.cassandra.presets.entity.GlobalApAlSetsByCustomer;
import com.uptime.cassandra.presets.entity.GlobalAssetPreset;
import com.uptime.cassandra.presets.entity.GlobalDevicePreset;
import com.uptime.cassandra.presets.entity.GlobalFaultFrequencies;
import com.uptime.cassandra.presets.entity.GlobalPtLocationNames;
import com.uptime.cassandra.presets.entity.GlobalTachometers;
import com.uptime.cassandra.presets.entity.SiteApAlSets;
import com.uptime.cassandra.presets.entity.SiteApAlSetsByCustomer;
import com.uptime.cassandra.presets.entity.SiteAssetPreset;
import com.uptime.cassandra.presets.entity.SiteBearingFavorites;
import com.uptime.cassandra.presets.entity.SiteDevicePreset;
import com.uptime.cassandra.presets.entity.SiteFFSetFavorites;
import com.uptime.cassandra.presets.entity.SiteFaultFrequencies;
import com.uptime.cassandra.presets.entity.SitePtLocationNames;
import com.uptime.cassandra.presets.entity.SiteTachometers;
import com.uptime.presets.vo.AssetPresetVO;
import com.uptime.presets.vo.BearingCatalogVO;
import com.uptime.presets.vo.CustomerApSetVO;
import com.uptime.presets.vo.DevicePresetVO;
import com.uptime.presets.vo.GlobalApAlSetsVO;
import com.uptime.presets.vo.GlobalFaultFrequenciesVO;
import com.uptime.presets.vo.PtLocationNamesVO;
import com.uptime.presets.vo.SiteApAlSetsVO;
import com.uptime.presets.vo.SiteBearingFavoritesVO;
import com.uptime.presets.vo.SiteFFSetFavoritesVO;
import com.uptime.presets.vo.SiteFaultFrequenciesVO;
import com.uptime.presets.vo.TachometersVO;

/**
 *
 * @author Joseph
 */
public class DelegateUtil {

    /**
     * Convert SiteFaultFrequenciesVO Object to SiteFaultFrequencies Object
     *
     * @param faultFrequenciesVO
     * @return SiteFaultFrequencies Object
     */
    public static SiteFaultFrequencies getSiteFaultFrequencies(SiteFaultFrequenciesVO faultFrequenciesVO) {
        SiteFaultFrequencies entity = new SiteFaultFrequencies();

        if (faultFrequenciesVO != null) {
            entity.setCustomerAccount(faultFrequenciesVO.getCustomerAccount());
            entity.setSiteId(faultFrequenciesVO.getSiteId());
            entity.setFfName(faultFrequenciesVO.getFfName());
            entity.setFfSetDesc(faultFrequenciesVO.getFfSetDesc());
            entity.setFfSetName(faultFrequenciesVO.getFfSetName());
            entity.setFfUnit(faultFrequenciesVO.getFfUnit());
            entity.setFfValue(faultFrequenciesVO.getFfValue());
            entity.setFfSetId(faultFrequenciesVO.getFfSetId());
            entity.setFfId(faultFrequenciesVO.getFfId());
        }
        return entity;
    }

    /**
     * Convert GlobalFaultFrequenciesVO Object to GlobalFaultFrequencies Object
     *
     * @param faultFrequenciesVO
     * @return GlobalFaultFrequencies Object
     */
    public static GlobalFaultFrequencies getGlobalFaultFrequencies(GlobalFaultFrequenciesVO faultFrequenciesVO) {
        GlobalFaultFrequencies entity = new GlobalFaultFrequencies();

        if (faultFrequenciesVO != null) {
            entity.setCustomerAccount(faultFrequenciesVO.getCustomerAccount());
            entity.setFfName(faultFrequenciesVO.getFfName());
            entity.setFfSetDesc(faultFrequenciesVO.getFfSetDesc());
            entity.setFfSetName(faultFrequenciesVO.getFfSetName());
            entity.setFfUnit(faultFrequenciesVO.getFfUnit());
            entity.setFfValue(faultFrequenciesVO.getFfValue());
            entity.setFfSetId(faultFrequenciesVO.getFfSetId());
            entity.setFfId(faultFrequenciesVO.getFfId());
        }
        return entity;
    }

    /**
     * Convert TachometersVO Object to Tachometers Object
     *
     * @param tachometersVO
     * @return Tachometers Object
     */
    public static SiteTachometers getSiteTachometers(TachometersVO tachometersVO) {
        SiteTachometers entity = new SiteTachometers();

        if (tachometersVO != null) {
            entity.setCustomerAccount(tachometersVO.getCustomerAccount());
            entity.setSiteId(tachometersVO.getSiteId());
            entity.setTachName(tachometersVO.getTachName());
            entity.setTachReferenceSpeed(tachometersVO.getTachReferenceSpeed());
            entity.setTachReferenceUnits(tachometersVO.getTachReferenceUnits());
            entity.setTachType(tachometersVO.getTachType());
            entity.setTachId(tachometersVO.getTachId());
        }
        return entity;
    }

    /**
     * Convert TachometersVO Object to GlobalTachometers Object
     *
     * @param tachometersVO
     * @return GlobalTachometers Object
     */
    public static GlobalTachometers getGlobalTachometers(TachometersVO tachometersVO) {
        GlobalTachometers entity = new GlobalTachometers();

        if (tachometersVO != null) {
            entity.setCustomerAccount(tachometersVO.getCustomerAccount());
            entity.setTachId(tachometersVO.getTachId());
            entity.setTachName(tachometersVO.getTachName());
            entity.setTachReferenceSpeed(tachometersVO.getTachReferenceSpeed());
            entity.setTachReferenceUnits(tachometersVO.getTachReferenceUnits());
            entity.setTachType(tachometersVO.getTachType());
        }
        return entity;
    }

    /**
     * Convert GlobalApAlSetsVO Object to GlobalApAlSets Object
     *
     * @param globalApAlSetsVO
     * @return GlobalApAlSets Object
     */
    public static GlobalApAlSets getGlobalApAlSets(GlobalApAlSetsVO globalApAlSetsVO) {
        GlobalApAlSets entity = new GlobalApAlSets();

        if (globalApAlSetsVO != null) {
            entity.setAlSetName(globalApAlSetsVO.getAlSetName());
            entity.setApSetName(globalApAlSetsVO.getApSetName());
            entity.setCustomerAccount(globalApAlSetsVO.getCustomerAccount());
            entity.setDemod(globalApAlSetsVO.isDemod());
            entity.setDemodHighPass(globalApAlSetsVO.getDemodHighPass());
            entity.setDemodLowPass(globalApAlSetsVO.getDemodLowPass());
            entity.setDwell(globalApAlSetsVO.getDwell());
            entity.setFmax(globalApAlSetsVO.getFmax());
            entity.setFrequencyUnits(globalApAlSetsVO.getFrequencyUnits());
            entity.setHighAlert(globalApAlSetsVO.getHighAlert());
            entity.setHighAlertActive(globalApAlSetsVO.isHighAlertActive());
            entity.setHighFault(globalApAlSetsVO.getHighFault());
            entity.setHighFaultActive(globalApAlSetsVO.isHighFaultActive());
            entity.setLowAlert(globalApAlSetsVO.getLowAlert());
            entity.setLowAlertActive(globalApAlSetsVO.isLowAlertActive());
            entity.setLowFault(globalApAlSetsVO.getLowFault());
            entity.setLowFaultActive(globalApAlSetsVO.isLowFaultActive());
            entity.setMaxFrequency(globalApAlSetsVO.getMaxFrequency());
            entity.setMinFrequency(globalApAlSetsVO.getMinFrequency());
            entity.setParamAmpFactor(globalApAlSetsVO.getParamAmpFactor());
            entity.setParamName(globalApAlSetsVO.getParamName());
            entity.setParamType(globalApAlSetsVO.getParamType());
            entity.setParamUnits(globalApAlSetsVO.getParamUnits());
            entity.setPeriod(globalApAlSetsVO.getPeriod());
            entity.setResolution(globalApAlSetsVO.getResolution());
            entity.setSampleRate(globalApAlSetsVO.getSampleRate());
            entity.setSensorType(globalApAlSetsVO.getSensorType());
            entity.setApSetId(globalApAlSetsVO.getApSetId());
            entity.setAlSetId(globalApAlSetsVO.getAlSetId());
        }
        return entity;
    }

    /**
     * Convert GlobalApAlSetsVO Object to GlobalApAlSetsByCustomer Object
     *
     * @param globalApAlSetsVO
     * @return GlobalApAlSetsByCustomer Object
     */
    public static GlobalApAlSetsByCustomer getGlobalApAlSetsByCustomer(GlobalApAlSetsVO globalApAlSetsVO) {
        GlobalApAlSetsByCustomer entity = new GlobalApAlSetsByCustomer();

        if (globalApAlSetsVO != null) {
            entity.setAlSetName(globalApAlSetsVO.getAlSetName());
            entity.setApSetName(globalApAlSetsVO.getApSetName());
            entity.setCustomerAccount(globalApAlSetsVO.getCustomerAccount());
            entity.setSensorType(globalApAlSetsVO.getSensorType());
            entity.setApSetId(globalApAlSetsVO.getApSetId());
            entity.setAlSetId(globalApAlSetsVO.getAlSetId());
        }
        return entity;
    }

    /**
     * Convert SiteApAlSetsVO Object to SiteApAlSets Object
     *
     * @param siteApAlSetsVO
     * @return SiteApAlSets Object
     */
    public static SiteApAlSets getSiteApAlSets(SiteApAlSetsVO siteApAlSetsVO) {
        SiteApAlSets entity = new SiteApAlSets();

        if (siteApAlSetsVO != null) {
            entity.setAlSetName(siteApAlSetsVO.getAlSetName());
            entity.setApSetName(siteApAlSetsVO.getApSetName());
            entity.setAlSetId(siteApAlSetsVO.getAlSetId());
            entity.setSiteId(siteApAlSetsVO.getSiteId());
            entity.setApSetId(siteApAlSetsVO.getApSetId());
            entity.setCustomerAccount(siteApAlSetsVO.getCustomerAccount());
            entity.setDemod(siteApAlSetsVO.isDemod());
            entity.setDemodHighPass(siteApAlSetsVO.getDemodHighPass());
            entity.setDemodLowPass(siteApAlSetsVO.getDemodLowPass());
            entity.setDwell(siteApAlSetsVO.getDwell());
            entity.setFmax(siteApAlSetsVO.getFmax());
            entity.setFrequencyUnits(siteApAlSetsVO.getFrequencyUnits());
            entity.setHighAlert(siteApAlSetsVO.getHighAlert());
            entity.setHighAlertActive(siteApAlSetsVO.isHighAlertActive());
            entity.setHighFault(siteApAlSetsVO.getHighFault());
            entity.setHighFaultActive(siteApAlSetsVO.isHighFaultActive());
            entity.setLowAlert(siteApAlSetsVO.getLowAlert());
            entity.setLowAlertActive(siteApAlSetsVO.isLowAlertActive());
            entity.setLowFault(siteApAlSetsVO.getLowFault());
            entity.setLowFaultActive(siteApAlSetsVO.isLowFaultActive());
            entity.setMaxFrequency(siteApAlSetsVO.getMaxFrequency());
            entity.setMinFrequency(siteApAlSetsVO.getMinFrequency());
            entity.setParamAmpFactor(siteApAlSetsVO.getParamAmpFactor());
            entity.setParamName(siteApAlSetsVO.getParamName());
            entity.setParamType(siteApAlSetsVO.getParamType());
            entity.setParamUnits(siteApAlSetsVO.getParamUnits());
            entity.setPeriod(siteApAlSetsVO.getPeriod());
            entity.setResolution(siteApAlSetsVO.getResolution());
            entity.setSampleRate(siteApAlSetsVO.getSampleRate());
            entity.setSensorType(siteApAlSetsVO.getSensorType());
        }
        return entity;
    }

    /**
     * Convert SiteApAlSetsVO Object to SiteApAlSetsByCustomer Object
     *
     * @param siteApAlSetsVO
     * @return SiteApAlSetsByCustomer Object
     */
    public static SiteApAlSetsByCustomer getSiteApAlSetsByCustomer(SiteApAlSetsVO siteApAlSetsVO) {
        SiteApAlSetsByCustomer entity = new SiteApAlSetsByCustomer();

        if (siteApAlSetsVO != null) {
            entity.setAlSetName(siteApAlSetsVO.getAlSetName());
            entity.setApSetName(siteApAlSetsVO.getApSetName());
            entity.setAlSetId(siteApAlSetsVO.getAlSetId());
            entity.setSiteId(siteApAlSetsVO.getSiteId());
            entity.setApSetId(siteApAlSetsVO.getApSetId());
            entity.setCustomerAccount(siteApAlSetsVO.getCustomerAccount());
            entity.setSensorType(siteApAlSetsVO.getSensorType());
        }
        return entity;
    }

    /**
     * Convert CustomerApSetVO Object to CustomerApSet Object
     *
     * @param customerApSetVO
     * @return CustomerApSet Object
     */
    public static CustomerApSet getCustomerApSet(CustomerApSetVO customerApSetVO) {
        CustomerApSet entity = new CustomerApSet();

        if (customerApSetVO != null) {
            entity.setCustomerAccount(customerApSetVO.getCustomerAccount());
            entity.setApSetId(customerApSetVO.getApSetId());
            entity.setApSetName(customerApSetVO.getApSetName());
            entity.setFmax(customerApSetVO.getFmax());
            entity.setPeriod(customerApSetVO.getPeriod());
            entity.setResolution(customerApSetVO.getResolution());
            entity.setSampleRate(customerApSetVO.getSampleRate());
            entity.setSensorType(customerApSetVO.getSensorType());
            entity.setSiteId(customerApSetVO.getSiteId());
        }
        return entity;
    }

    /**
     * Convert DevicePresetVO Object to GlobalDevicePreset Object
     *
     * @param devicePresetVO
     * @return GlobalDevicePreset Object
     */
    public static GlobalDevicePreset getGlobalDevicePreset(DevicePresetVO devicePresetVO) {
        GlobalDevicePreset entity = new GlobalDevicePreset();

        if (devicePresetVO != null) {
            entity.setCustomerAccount(devicePresetVO.getCustomerAccount());
            entity.setDeviceType(devicePresetVO.getDeviceType());
            entity.setPresetId(devicePresetVO.getPresetId());
            entity.setChannelType(devicePresetVO.getChannelType());
            entity.setChannelNumber(devicePresetVO.getChannelNumber());
            entity.setApSetId(devicePresetVO.getApSetId());
            entity.setAlSetId(devicePresetVO.getAlSetId());
            entity.setAlarmEnabled(devicePresetVO.isAlarmEnabled());
            entity.setAutoAcknowledge(devicePresetVO.isAutoAcknowledge());
            entity.setDisabled(devicePresetVO.isDisabled());
            entity.setName(devicePresetVO.getName());
            entity.setSampleInterval(devicePresetVO.getSampleInterval());
            entity.setSensorOffset(devicePresetVO.getSensorOffset());
            entity.setSensorSensitivity(devicePresetVO.getSensorSensitivity());
            entity.setSensorType(devicePresetVO.getSensorType());
            entity.setSensorUnits(devicePresetVO.getSensorUnits());
            entity.setSensorSubType(devicePresetVO.getSensorSubType());
            entity.setSensorLocalOrientation(devicePresetVO.getSensorLocalOrientation());
        }
        return entity;
    }

    /**
     * Convert DevicePresetVO Object to SiteDevicePreset Object
     *
     * @param devicePresetVO
     * @return SiteDevicePreset Object
     */
    public static SiteDevicePreset getSiteDevicePreset(DevicePresetVO devicePresetVO) {
        SiteDevicePreset entity = new SiteDevicePreset();

        if (devicePresetVO != null) {
            entity.setCustomerAccount(devicePresetVO.getCustomerAccount());
            entity.setSiteId(devicePresetVO.getSiteId());
            entity.setDeviceType(devicePresetVO.getDeviceType());
            entity.setPresetId(devicePresetVO.getPresetId());
            entity.setChannelType(devicePresetVO.getChannelType());
            entity.setChannelNumber(devicePresetVO.getChannelNumber());
            entity.setApSetId(devicePresetVO.getApSetId());
            entity.setAlSetId(devicePresetVO.getAlSetId());
            entity.setAlarmEnabled(devicePresetVO.isAlarmEnabled());
            entity.setAutoAcknowledge(devicePresetVO.isAutoAcknowledge());
            entity.setDisabled(devicePresetVO.isDisabled());
            entity.setName(devicePresetVO.getName());
            entity.setSampleInterval(devicePresetVO.getSampleInterval());
            entity.setSensorOffset(devicePresetVO.getSensorOffset());
            entity.setSensorSensitivity(devicePresetVO.getSensorSensitivity());
            entity.setSensorType(devicePresetVO.getSensorType());
            entity.setSensorUnits(devicePresetVO.getSensorUnits());
            entity.setSensorSubType(devicePresetVO.getSensorSubType());
            entity.setSensorLocalOrientation(devicePresetVO.getSensorLocalOrientation());
        }
        return entity;
    }

    /**
     * Convert PtLocationNamesVO Object to GlobalPtLocationNames Object
     *
     * @param ptLocationNamesVO
     * @return GlobalPtLocationNames Object
     */
    public static GlobalPtLocationNames getGlobalPtLocationNames(PtLocationNamesVO ptLocationNamesVO) {
        GlobalPtLocationNames entity = new GlobalPtLocationNames();

        if (ptLocationNamesVO != null) {
            entity.setCustomerAccount(ptLocationNamesVO.getCustomerAccount());
            entity.setPointLocationName(ptLocationNamesVO.getPointLocationName());
            entity.setDescription(ptLocationNamesVO.getDescription());
        }
        return entity;
    }

    /**
     * Convert PtLocationNamesVO Object to SitePtLocationNames Object
     *
     * @param ptLocationNamesVO
     * @return SitePtLocationNames Object
     */
    public static SitePtLocationNames getSitePtLocationNames(PtLocationNamesVO ptLocationNamesVO) {
        SitePtLocationNames entity = new SitePtLocationNames();

        if (ptLocationNamesVO != null) {
            entity.setCustomerAccount(ptLocationNamesVO.getCustomerAccount());
            entity.setSiteId(ptLocationNamesVO.getSiteId());
            entity.setPointLocationName(ptLocationNamesVO.getPointLocationName());
            entity.setDescription(ptLocationNamesVO.getDescription());
        }
        return entity;
    }

    /**
     * Convert SiteBearingFavoritesVO Object to SiteBearingFavorites Object
     *
     * @param siteBearingFavoritesVO, SiteBearingFavoritesVO
     * @return SiteBearingFavorites Object
     */
    public static SiteBearingFavorites getSiteBearingFavorites(SiteBearingFavoritesVO siteBearingFavoritesVO) {
        SiteBearingFavorites entity = new SiteBearingFavorites();

        if (siteBearingFavoritesVO != null) {
            entity.setCustomerAcct(siteBearingFavoritesVO.getCustomerAcct());
            entity.setSiteId(siteBearingFavoritesVO.getSiteId());
            entity.setVendorId(siteBearingFavoritesVO.getVendorId());
            entity.setBearingId(siteBearingFavoritesVO.getBearingId());
            entity.setFtf(siteBearingFavoritesVO.getFtf());
            entity.setBsf(siteBearingFavoritesVO.getBsf());
            entity.setBpfi(siteBearingFavoritesVO.getBpfi());
            entity.setBpfo(siteBearingFavoritesVO.getBpfo());
        }
        return entity;
    }

    /**
     * Convert bearingCatalogVO Object to BearingCatalog Object
     *
     * @param bearingCatalogVO
     * @return BearingCatalog Object
     */
    public static BearingCatalog getbearingCatalog(BearingCatalogVO bearingCatalogVO) {
        BearingCatalog bearingCatalog = new BearingCatalog();
        if (bearingCatalogVO != null) {
            bearingCatalog.setBearingId(bearingCatalogVO.getBearingId());
            bearingCatalog.setVendorId(bearingCatalogVO.getVendorId());
            bearingCatalog.setBc(bearingCatalogVO.getBc());
            bearingCatalog.setBpfi(bearingCatalogVO.getBpfi());
            bearingCatalog.setBpfo(bearingCatalogVO.getBpfo());
            bearingCatalog.setBsf(bearingCatalogVO.getBsf());
            bearingCatalog.setFtf(bearingCatalogVO.getFtf());
        }
        return bearingCatalog;
    }

    /**
     * Convert SiteFFSetFavoritesVO Object to SiteFFSetFavorites Object
     *
     * @param siteFFSetFavoritesVO
     * @return SiteFFSetFavorites Object
     */
    public static SiteFFSetFavorites getsiteFFSetFavorites(SiteFFSetFavoritesVO siteFFSetFavoritesVO) {
        SiteFFSetFavorites entity = new SiteFFSetFavorites();

        if (siteFFSetFavoritesVO != null) {
            entity.setCustomerAccount(siteFFSetFavoritesVO.getCustomerAccount());
            entity.setSiteId(siteFFSetFavoritesVO.getSiteId());
            entity.setFfName(siteFFSetFavoritesVO.getFfName());
            entity.setFfSetDesc(siteFFSetFavoritesVO.getFfSetDesc());
            entity.setFfSetName(siteFFSetFavoritesVO.getFfSetName());
            entity.setFfUnit(siteFFSetFavoritesVO.getFfUnit());
            entity.setFfValue(siteFFSetFavoritesVO.getFfValue());
            entity.setFfSetId(siteFFSetFavoritesVO.getFfSetId());
            entity.setFfId(siteFFSetFavoritesVO.getFfId());
            entity.setGlobal(siteFFSetFavoritesVO.isGlobal());
        }
        return entity;
    }
    
    /**
     * Convert AssetPresetVO Object to SiteDevicePreset Object
     *
     * @param assetPresetVO
     * @return SiteAssetPreset Object
     */
    public static SiteAssetPreset getSiteAssetPreset(AssetPresetVO assetPresetVO) {
        SiteAssetPreset entity = new SiteAssetPreset();

        if (assetPresetVO != null) {
            entity.setCustomerAccount(assetPresetVO.getCustomerAccount());
            entity.setSiteId(assetPresetVO.getSiteId());
            entity.setDevicePresetType(assetPresetVO.getDevicePresetType());
            entity.setAssetPresetId(assetPresetVO.getAssetPresetId());
            entity.setAssetPresetName(assetPresetVO.getAssetPresetName());
            entity.setAssetTypeName(assetPresetVO.getAssetTypeName());
            entity.setDescription(assetPresetVO.getDescription());
            entity.setDevicePresetId(assetPresetVO.getDevicePresetId());
            entity.setFfSetIds(assetPresetVO.getFfSetIds());
            entity.setPointLocationName(assetPresetVO.getPointLocationName());
            entity.setRollDiameter(assetPresetVO.getRollDiameter());
            entity.setRollDiameterUnits(assetPresetVO.getRollDiameterUnits());
            entity.setSampleInterval(assetPresetVO.getSampleInterval());
            entity.setSpeedRatio(assetPresetVO.getSpeedRatio());
            entity.setTachId(assetPresetVO.getTachId());
            
        }
        return entity;
    }

     /**
     * Convert DevicePresetVO Object to GlobalAssetPreset Object
     *
     * @param assetPresetVO
     * @return GlobalAssetPreset Object
     */
    public static GlobalAssetPreset getGlobalAssetPreset(AssetPresetVO assetPresetVO) {
        GlobalAssetPreset entity = new GlobalAssetPreset();

        if (assetPresetVO != null) {
            entity.setCustomerAccount(assetPresetVO.getCustomerAccount());
            entity.setAssetTypeName(assetPresetVO.getAssetTypeName());
            entity.setAssetPresetId(assetPresetVO.getAssetPresetId());
            entity.setPointLocationName(assetPresetVO.getPointLocationName());
            entity.setAssetPresetName(assetPresetVO.getAssetPresetName());
            entity.setDevicePresetId(assetPresetVO.getDevicePresetId());
            entity.setDevicePresetType(assetPresetVO.getDevicePresetType());
            entity.setSampleInterval(assetPresetVO.getSampleInterval());
            entity.setFfSetIds(assetPresetVO.getFfSetIds());
            entity.setRollDiameter(assetPresetVO.getRollDiameter());
            entity.setRollDiameterUnits(assetPresetVO.getRollDiameterUnits());
            entity.setSpeedRatio(assetPresetVO.getSpeedRatio());
            entity.setTachId(assetPresetVO.getTachId());
            entity.setDescription(assetPresetVO.getDescription());
             
        }
        return entity;
    }
}
