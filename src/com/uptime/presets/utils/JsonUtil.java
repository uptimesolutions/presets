/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.uptime.presets.vo.AssetPresetVO;
import com.uptime.presets.vo.BearingCatalogVO;
import com.uptime.presets.vo.CustomerApSetVO;
import com.uptime.presets.vo.GlobalApAlSetsVO;
import com.uptime.presets.vo.GlobalFaultFrequenciesVO;
import com.uptime.presets.vo.SiteApAlSetsVO;
import com.uptime.presets.vo.SiteFaultFrequenciesVO;
import com.uptime.presets.vo.TachometersVO;
import com.uptime.presets.vo.DevicePresetVO;
import com.uptime.presets.vo.PtLocationNamesVO;
import com.uptime.presets.vo.SiteBearingFavoritesVO;
import com.uptime.presets.vo.SiteFFSetFavoritesVO;
import com.uptime.presets.vo.SiteFavoritesEncapVO;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class JsonUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(JsonUtil.class.getName());

    /**
     * Parse the given json String Object and return a SiteFaultFrequenciesVO
     * object
     *
     * @param content, String Object
     * @return SiteFaultFrequenciesVO Object
     */
    public static List<SiteFaultFrequenciesVO> siteFaultFrequenciesParser(String content) {
        SiteFaultFrequenciesVO faultFrequenciesVO;
        List<SiteFaultFrequenciesVO> sFFVOList = new ArrayList();
        JsonElement element;
        JsonObject jsonObject;
        JsonArray jsonArray;

        if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && (jsonArray = element.getAsJsonArray()) != null) {
            for (int i = 0; i < jsonArray.size(); i++) {
                jsonObject = jsonArray.get(i).getAsJsonObject();
                faultFrequenciesVO = new SiteFaultFrequenciesVO();

                if (jsonObject.has("customerAccount")) {
                    try {
                        faultFrequenciesVO.setCustomerAccount(jsonObject.get("customerAccount").getAsString());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("siteId")) {
                    try {
                        faultFrequenciesVO.setSiteId(UUID.fromString(jsonObject.get("siteId").getAsString()));
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("siteName")) {
                    try {
                        faultFrequenciesVO.setSiteName(jsonObject.get("siteName").getAsString());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("ffId")) {
                    try {
                        faultFrequenciesVO.setFfId(UUID.fromString(jsonObject.get("ffId").getAsString()));
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("ffName")) {
                    try {
                        faultFrequenciesVO.setFfName(jsonObject.get("ffName").getAsString());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("ffSetDesc")) {
                    try {
                        faultFrequenciesVO.setFfSetDesc(jsonObject.get("ffSetDesc").getAsString());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("ffSetId")) {
                    try {
                        faultFrequenciesVO.setFfSetId(UUID.fromString(jsonObject.get("ffSetId").getAsString()));
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("ffSetName")) {
                    try {
                        faultFrequenciesVO.setFfSetName(jsonObject.get("ffSetName").getAsString());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("ffUnit")) {
                    try {
                        faultFrequenciesVO.setFfUnit(jsonObject.get("ffUnit").getAsString());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("ffValue")) {
                    try {
                        faultFrequenciesVO.setFfValue(jsonObject.get("ffValue").getAsFloat());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                sFFVOList.add(faultFrequenciesVO);
            }

        }
        return sFFVOList;
    }

    /**
     * Parse the given json String Object and return a GlobalFaultFrequenciesVO
     * object
     *
     * @param content, String Object
     * @return List of GlobalFaultFrequenciesVO Object
     */
    public static List<GlobalFaultFrequenciesVO> globalFaultFrequenciesParser(String content) {
        List<GlobalFaultFrequenciesVO> gFFVOList = new ArrayList();
        GlobalFaultFrequenciesVO faultFrequenciesVO;
        JsonElement element;
        JsonObject jsonObject;
        JsonArray jsonArray;

        if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && (jsonArray = element.getAsJsonArray()) != null) {
            for (int i = 0; i < jsonArray.size(); i++) {
                jsonObject = jsonArray.get(i).getAsJsonObject();
                faultFrequenciesVO = new GlobalFaultFrequenciesVO();

                if (jsonObject.has("customerAccount")) {
                    try {
                        faultFrequenciesVO.setCustomerAccount(jsonObject.get("customerAccount").getAsString());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("ffId")) {
                    try {
                        faultFrequenciesVO.setFfId(UUID.fromString(jsonObject.get("ffId").getAsString()));
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("ffName")) {
                    try {
                        faultFrequenciesVO.setFfName(jsonObject.get("ffName").getAsString());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("ffSetDesc")) {
                    try {
                        faultFrequenciesVO.setFfSetDesc(jsonObject.get("ffSetDesc").getAsString());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("ffSetName")) {
                    try {
                        faultFrequenciesVO.setFfSetName(jsonObject.get("ffSetName").getAsString());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("ffSetId")) {
                    try {
                        faultFrequenciesVO.setFfSetId(UUID.fromString(jsonObject.get("ffSetId").getAsString()));
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("ffUnit")) {
                    try {
                        faultFrequenciesVO.setFfUnit(jsonObject.get("ffUnit").getAsString());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("ffValue")) {
                    try {
                        faultFrequenciesVO.setFfValue(jsonObject.get("ffValue").getAsFloat());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                gFFVOList.add(faultFrequenciesVO);
            }
        }
        return gFFVOList;
    }

    /**
     * Parse the given json String Object and return a TachometersVO object
     *
     * @param content, String Object
     * @return TachometersVO Object
     */
    public static TachometersVO siteTachometersParser(String content) {
        TachometersVO tachometersVO = null;
        JsonElement element;
        JsonObject jsonObject;

        if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && (jsonObject = element.getAsJsonObject()) != null) {
            tachometersVO = new TachometersVO();

            if (jsonObject.has("customerAccount")) {
                try {
                    tachometersVO.setCustomerAccount(jsonObject.get("customerAccount").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("siteId")) {
                try {
                    tachometersVO.setSiteId(UUID.fromString(jsonObject.get("siteId").getAsString()));
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("tachId")) {
                try {
                    tachometersVO.setTachId(UUID.fromString(jsonObject.get("tachId").getAsString()));
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("tachName")) {
                try {
                    tachometersVO.setTachName(jsonObject.get("tachName").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("tachReferenceSpeed")) {
                try {
                    tachometersVO.setTachReferenceSpeed(jsonObject.get("tachReferenceSpeed").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("tachReferenceUnits")) {
                try {
                    tachometersVO.setTachReferenceUnits(jsonObject.get("tachReferenceUnits").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("tachType")) {
                try {
                    tachometersVO.setTachType(jsonObject.get("tachType").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }
        }
        return tachometersVO;
    }

    /**
     * Parse the given json String Object and return a TachometersVO object
     *
     * @param content, String Object
     * @return TachometersVO Object
     */
    public static TachometersVO globalTachometersParser(String content) {
        TachometersVO tachometersVO = null;
        JsonElement element;
        JsonObject jsonObject;

        if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && (jsonObject = element.getAsJsonObject()) != null) {
            tachometersVO = new TachometersVO();

            if (jsonObject.has("customerAccount")) {
                try {
                    tachometersVO.setCustomerAccount(jsonObject.get("customerAccount").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("tachId")) {
                try {
                    tachometersVO.setTachId(UUID.fromString(jsonObject.get("tachId").getAsString()));
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("tachName")) {
                try {
                    tachometersVO.setTachName(jsonObject.get("tachName").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("tachReferenceSpeed")) {
                try {
                    tachometersVO.setTachReferenceSpeed(jsonObject.get("tachReferenceSpeed").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("tachReferenceUnits")) {
                try {
                    tachometersVO.setTachReferenceUnits(jsonObject.get("tachReferenceUnits").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("tachType")) {
                try {
                    tachometersVO.setTachType(jsonObject.get("tachType").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }
        }
        return tachometersVO;
    }

    /**
     * Parse the given json String Object and return a GlobalApAlSetsVO object
     *
     * @param content, String Object
     * @return GlobalApAlSetsVO Object
     */
    public static GlobalApAlSetsVO globalApAlSetsVOParser(String content) {
        GlobalApAlSetsVO globalApAlSetsVO = null;
        JsonElement element;
        JsonObject jsonObject;

        if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && (jsonObject = element.getAsJsonObject()) != null) {
            globalApAlSetsVO = new GlobalApAlSetsVO();

            if (jsonObject.has("customerAccount")) {
                try {
                    globalApAlSetsVO.setCustomerAccount(jsonObject.get("customerAccount").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("apSetId")) {
                try {
                    globalApAlSetsVO.setApSetId(UUID.fromString(jsonObject.get("apSetId").getAsString()));
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("apSetName")) {
                try {
                    globalApAlSetsVO.setApSetName(jsonObject.get("apSetName").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("alSetId")) {
                try {
                    globalApAlSetsVO.setAlSetId(UUID.fromString(jsonObject.get("alSetId").getAsString()));
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("alSetName")) {
                try {
                    globalApAlSetsVO.setAlSetName(jsonObject.get("alSetName").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("demod")) {
                try {
                    globalApAlSetsVO.setDemod(jsonObject.get("demod").getAsBoolean());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("demodHighPass")) {
                try {
                    globalApAlSetsVO.setDemodHighPass(jsonObject.get("demodHighPass").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("demodLowPass")) {
                try {
                    globalApAlSetsVO.setDemodLowPass(jsonObject.get("demodLowPass").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("dwell")) {
                try {
                    globalApAlSetsVO.setDwell(jsonObject.get("dwell").getAsInt());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("fmax")) {
                try {
                    globalApAlSetsVO.setFmax(jsonObject.get("fmax").getAsInt());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("frequencyUnits")) {
                try {
                    globalApAlSetsVO.setFrequencyUnits(jsonObject.get("frequencyUnits").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("highAlert")) {
                try {
                    globalApAlSetsVO.setHighAlert(jsonObject.get("highAlert").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("highAlertActive")) {
                try {
                    globalApAlSetsVO.setHighAlertActive(jsonObject.get("highAlertActive").getAsBoolean());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("highFault")) {
                try {
                    globalApAlSetsVO.setHighFault(jsonObject.get("highFault").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("highFaultActive")) {
                try {
                    globalApAlSetsVO.setHighFaultActive(jsonObject.get("highFaultActive").getAsBoolean());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("lowAlert")) {
                try {
                    globalApAlSetsVO.setLowAlert(jsonObject.get("lowAlert").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("lowAlertActive")) {
                try {
                    globalApAlSetsVO.setLowAlertActive(jsonObject.get("lowAlertActive").getAsBoolean());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("lowFault")) {
                try {
                    globalApAlSetsVO.setLowFault(jsonObject.get("lowFault").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("lowFaultActive")) {
                try {
                    globalApAlSetsVO.setLowFaultActive(jsonObject.get("lowFaultActive").getAsBoolean());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("maxFrequency")) {
                try {
                    globalApAlSetsVO.setMaxFrequency(jsonObject.get("maxFrequency").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("minFrequency")) {
                try {
                    globalApAlSetsVO.setMinFrequency(jsonObject.get("minFrequency").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("paramAmpFactor")) {
                try {
                    globalApAlSetsVO.setParamAmpFactor(jsonObject.get("paramAmpFactor").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("paramName")) {
                try {
                    globalApAlSetsVO.setParamName(jsonObject.get("paramName").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("paramType")) {
                try {
                    globalApAlSetsVO.setParamType(jsonObject.get("paramType").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("paramUnits")) {
                try {
                    globalApAlSetsVO.setParamUnits(jsonObject.get("paramUnits").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("period")) {
                try {
                    globalApAlSetsVO.setPeriod(jsonObject.get("period").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("resolution")) {
                try {
                    globalApAlSetsVO.setResolution(jsonObject.get("resolution").getAsInt());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("sampleRate")) {
                try {
                    globalApAlSetsVO.setSampleRate(jsonObject.get("sampleRate").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("sensorType")) {
                try {
                    globalApAlSetsVO.setSensorType(jsonObject.get("sensorType").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }
        }
        return globalApAlSetsVO;
    }

    /**
     * Parse the given json String Object and return a SiteApAlSetsVO object
     *
     * @param content, String Object
     * @return SiteApAlSetsVO Object
     */
    public static SiteApAlSetsVO siteApAlSetsVOParser(String content) {
        SiteApAlSetsVO siteApAlSetsVO = null;
        JsonElement element;
        JsonObject jsonObject;

        if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && (jsonObject = element.getAsJsonObject()) != null) {
            siteApAlSetsVO = new SiteApAlSetsVO();

            if (jsonObject.has("customerAccount")) {
                try {
                    siteApAlSetsVO.setCustomerAccount(jsonObject.get("customerAccount").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("siteName")) {
                try {
                    siteApAlSetsVO.setSiteName(jsonObject.get("siteName").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("siteId")) {
                try {
                    siteApAlSetsVO.setSiteId(UUID.fromString(jsonObject.get("siteId").getAsString()));
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("apSetId")) {
                try {
                    siteApAlSetsVO.setApSetId(UUID.fromString(jsonObject.get("apSetId").getAsString()));
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("alSetId")) {
                try {
                    siteApAlSetsVO.setAlSetId(UUID.fromString(jsonObject.get("alSetId").getAsString()));
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("apSetName")) {
                try {
                    siteApAlSetsVO.setApSetName(jsonObject.get("apSetName").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("alSetName")) {
                try {
                    siteApAlSetsVO.setAlSetName(jsonObject.get("alSetName").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("demod")) {
                try {
                    siteApAlSetsVO.setDemod(jsonObject.get("demod").getAsBoolean());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("demodHighPass")) {
                try {
                    siteApAlSetsVO.setDemodHighPass(jsonObject.get("demodHighPass").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("demodLowPass")) {
                try {
                    siteApAlSetsVO.setDemodLowPass(jsonObject.get("demodLowPass").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("dwell")) {
                try {
                    siteApAlSetsVO.setDwell(jsonObject.get("dwell").getAsInt());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("fmax")) {
                try {
                    siteApAlSetsVO.setFmax(jsonObject.get("fmax").getAsInt());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("frequencyUnits")) {
                try {
                    siteApAlSetsVO.setFrequencyUnits(jsonObject.get("frequencyUnits").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("highAlert")) {
                try {
                    siteApAlSetsVO.setHighAlert(jsonObject.get("highAlert").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("highAlertActive")) {
                try {
                    siteApAlSetsVO.setHighAlertActive(jsonObject.get("highAlertActive").getAsBoolean());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("highFault")) {
                try {
                    siteApAlSetsVO.setHighFault(jsonObject.get("highFault").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("highFaultActive")) {
                try {
                    siteApAlSetsVO.setHighFaultActive(jsonObject.get("highFaultActive").getAsBoolean());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("lowAlert")) {
                try {
                    siteApAlSetsVO.setLowAlert(jsonObject.get("lowAlert").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("lowAlertActive")) {
                try {
                    siteApAlSetsVO.setLowAlertActive(jsonObject.get("lowAlertActive").getAsBoolean());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("lowFault")) {
                try {
                    siteApAlSetsVO.setLowFault(jsonObject.get("lowFault").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("lowFaultActive")) {
                try {
                    siteApAlSetsVO.setLowFaultActive(jsonObject.get("lowFaultActive").getAsBoolean());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("maxFrequency")) {
                try {
                    siteApAlSetsVO.setMaxFrequency(jsonObject.get("maxFrequency").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("minFrequency")) {
                try {
                    siteApAlSetsVO.setMinFrequency(jsonObject.get("minFrequency").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("paramAmpFactor")) {
                try {
                    siteApAlSetsVO.setParamAmpFactor(jsonObject.get("paramAmpFactor").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("paramName")) {
                try {
                    siteApAlSetsVO.setParamName(jsonObject.get("paramName").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("paramType")) {
                try {
                    siteApAlSetsVO.setParamType(jsonObject.get("paramType").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("paramUnits")) {
                try {
                    siteApAlSetsVO.setParamUnits(jsonObject.get("paramUnits").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("period")) {
                try {
                    siteApAlSetsVO.setPeriod(jsonObject.get("period").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("resolution")) {
                try {
                    siteApAlSetsVO.setResolution(jsonObject.get("resolution").getAsInt());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("sampleRate")) {
                try {
                    siteApAlSetsVO.setSampleRate(jsonObject.get("sampleRate").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("sensorType")) {
                try {
                    siteApAlSetsVO.setSensorType(jsonObject.get("sensorType").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }
        }
        return siteApAlSetsVO;
    }

    /**
     * Parse the given json String Object and return a CustomerApSetVO object
     *
     * @param content, String Object
     * @return CustomerApSetVO Object
     */
    public static CustomerApSetVO customerApSetVOParser(String content) {
        CustomerApSetVO customerApSetVO = null;
        JsonElement element;
        JsonObject jsonObject;

        if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && (jsonObject = element.getAsJsonObject()) != null) {
            customerApSetVO = new CustomerApSetVO();

            if (jsonObject.has("customerAccount")) {
                try {
                    customerApSetVO.setCustomerAccount(jsonObject.get("customerAccount").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("siteId")) {
                try {
                    customerApSetVO.setSiteId(UUID.fromString(jsonObject.get("siteId").getAsString()));
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("apSetId")) {
                try {
                    customerApSetVO.setApSetId(UUID.fromString(jsonObject.get("apSetId").getAsString()));
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("apSetName")) {
                try {
                    customerApSetVO.setApSetName(jsonObject.get("apSetName").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("fmax")) {
                try {
                    customerApSetVO.setFmax(jsonObject.get("fmax").getAsInt());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("period")) {
                try {
                    customerApSetVO.setPeriod(jsonObject.get("period").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("resolution")) {
                try {
                    customerApSetVO.setResolution(jsonObject.get("resolution").getAsInt());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("sampleRate")) {
                try {
                    customerApSetVO.setSampleRate(jsonObject.get("sampleRate").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("sensorType")) {
                try {
                    customerApSetVO.setSensorType(jsonObject.get("sensorType").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }
        }
        return customerApSetVO;
    }

    /**
     * Parse the given json String Object and return a GlobalApAlSetsVO object
     *
     * @param content, String Object
     * @return DevicePresetVO Object
     */
    public static DevicePresetVO devicePresetVOParser(String content) {
        DevicePresetVO devicePresetVO = null;
        JsonElement element;
        JsonObject jsonObject;

        if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && (jsonObject = element.getAsJsonObject()) != null) {
            devicePresetVO = new DevicePresetVO();

            if (jsonObject.has("customerAccount")) {
                try {
                    devicePresetVO.setCustomerAccount(jsonObject.get("customerAccount").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("deviceType")) {
                try {
                    devicePresetVO.setDeviceType(jsonObject.get("deviceType").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("presetId")) {
                try {
                    devicePresetVO.setPresetId(UUID.fromString(jsonObject.get("presetId").getAsString()));
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("siteId")) {
                try {
                    devicePresetVO.setSiteId(UUID.fromString(jsonObject.get("siteId").getAsString()));
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("channelType")) {
                try {
                    devicePresetVO.setChannelType(jsonObject.get("channelType").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("channelNumber")) {
                try {
                    devicePresetVO.setChannelNumber(jsonObject.get("channelNumber").getAsByte());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("apSetId")) {
                try {
                    devicePresetVO.setApSetId(UUID.fromString(jsonObject.get("apSetId").getAsString()));
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("alSetId")) {
                try {
                    devicePresetVO.setAlSetId(UUID.fromString(jsonObject.get("alSetId").getAsString()));
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("alarmEnabled")) {
                try {
                    devicePresetVO.setAlarmEnabled(jsonObject.get("alarmEnabled").getAsBoolean());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("autoAcknowledge")) {
                try {
                    devicePresetVO.setAutoAcknowledge(jsonObject.get("autoAcknowledge").getAsBoolean());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("disabled")) {
                try {
                    devicePresetVO.setDisabled(jsonObject.get("disabled").getAsBoolean());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("sampleInterval")) {
                try {
                    devicePresetVO.setSampleInterval(jsonObject.get("sampleInterval").getAsInt());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("sensorOffset")) {
                try {
                    devicePresetVO.setSensorOffset(jsonObject.get("sensorOffset").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("sensorSensitivity")) {
                try {
                    devicePresetVO.setSensorSensitivity(jsonObject.get("sensorSensitivity").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("sensorType")) {
                try {
                    devicePresetVO.setSensorType(jsonObject.get("sensorType").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("sensorUnits")) {
                try {
                    devicePresetVO.setSensorUnits(jsonObject.get("sensorUnits").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }
        }
        return devicePresetVO;
    }

    /**
     * Parse the given json String Object and return a List of DevicePresetVO
     * objects
     *
     * @param content, String Object
     * @return List of DevicePresetVO Object
     */
    public static List<DevicePresetVO> devicePresetVOListParser(String content) {
        List<DevicePresetVO> devicePresetVOList = new ArrayList();
        DevicePresetVO devicePresetVO;
        JsonElement element;
        JsonObject jsonObject;
        JsonArray jsonArray;

        if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && (jsonArray = element.getAsJsonArray()) != null) {
            for (int i = 0; i < jsonArray.size(); i++) {
                jsonObject = jsonArray.get(i).getAsJsonObject();
                devicePresetVO = new DevicePresetVO();

                if (jsonObject.has("customerAccount")) {
                    try {
                        devicePresetVO.setCustomerAccount(jsonObject.get("customerAccount").getAsString());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("deviceType")) {
                    try {
                        devicePresetVO.setDeviceType(jsonObject.get("deviceType").getAsString());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("presetId")) {
                    try {
                        devicePresetVO.setPresetId(UUID.fromString(jsonObject.get("presetId").getAsString()));
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("siteId")) {
                    try {
                        devicePresetVO.setSiteId(UUID.fromString(jsonObject.get("siteId").getAsString()));
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("channelType")) {
                    try {
                        devicePresetVO.setChannelType(jsonObject.get("channelType").getAsString());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("channelNumber")) {
                    try {
                        devicePresetVO.setChannelNumber(jsonObject.get("channelNumber").getAsByte());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("apSetId")) {
                    try {
                        devicePresetVO.setApSetId(UUID.fromString(jsonObject.get("apSetId").getAsString()));
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("alSetId")) {
                    try {
                        devicePresetVO.setAlSetId(UUID.fromString(jsonObject.get("alSetId").getAsString()));
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("alarmEnabled")) {
                    try {
                        devicePresetVO.setAlarmEnabled(jsonObject.get("alarmEnabled").getAsBoolean());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("autoAcknowledge")) {
                    try {
                        devicePresetVO.setAutoAcknowledge(jsonObject.get("autoAcknowledge").getAsBoolean());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("disabled")) {
                    try {
                        devicePresetVO.setDisabled(jsonObject.get("disabled").getAsBoolean());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("sampleInterval")) {
                    try {
                        devicePresetVO.setSampleInterval(jsonObject.get("sampleInterval").getAsInt());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("sensorOffset")) {
                    try {
                        devicePresetVO.setSensorOffset(jsonObject.get("sensorOffset").getAsFloat());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("sensorSensitivity")) {
                    try {
                        devicePresetVO.setSensorSensitivity(jsonObject.get("sensorSensitivity").getAsFloat());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("sensorType")) {
                    try {
                        devicePresetVO.setSensorType(jsonObject.get("sensorType").getAsString());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("sensorUnits")) {
                    try {
                        devicePresetVO.setSensorUnits(jsonObject.get("sensorUnits").getAsString());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }
                
                if (jsonObject.has("sensorSubType")) {
                    try {
                        devicePresetVO.setSensorSubType(jsonObject.get("sensorSubType").getAsString());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }
                
                if (jsonObject.has("sensorLocalOrientation")) {
                    try {
                        devicePresetVO.setSensorLocalOrientation(jsonObject.get("sensorLocalOrientation").getAsString());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("name")) {
                    try {
                        devicePresetVO.setName(jsonObject.get("name").getAsString());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                devicePresetVOList.add(devicePresetVO);
            }
        }
        return devicePresetVOList;
    }

    /**
     * Parse the given json String Object and return a PtLocationNamesVO object
     *
     * @param content, String Object
     * @return PtLocationNamesVO Object
     */
    public static PtLocationNamesVO ptLocationNamesVOParser(String content) {
        PtLocationNamesVO ptLocationNamesVO = null;
        JsonElement element;
        JsonObject jsonObject;

        if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && (jsonObject = element.getAsJsonObject()) != null) {
            ptLocationNamesVO = new PtLocationNamesVO();

            if (jsonObject.has("customerAccount")) {
                try {
                    ptLocationNamesVO.setCustomerAccount(jsonObject.get("customerAccount").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("pointLocationName")) {
                try {
                    ptLocationNamesVO.setPointLocationName(jsonObject.get("pointLocationName").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("siteId")) {
                try {
                    ptLocationNamesVO.setSiteId(UUID.fromString(jsonObject.get("siteId").getAsString()));
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("description")) {
                try {
                    ptLocationNamesVO.setDescription(jsonObject.get("description").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }
        }
        return ptLocationNamesVO;
    }

    /**
     * Parse the given json String Object and return a SiteBearingFavoritesVO
     * object
     *
     * @param content, String Object
     * @return PtLocationNamesVO Object
     */
    public static SiteBearingFavoritesVO siteBearingFavoritesVOParser(String content) {
        SiteBearingFavoritesVO siteBearingFavoritesVO = null;
        JsonElement element;
        JsonObject jsonObject;

        if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && (jsonObject = element.getAsJsonObject()) != null) {
            siteBearingFavoritesVO = new SiteBearingFavoritesVO();

            if (jsonObject.has("customerAcct")) {
                try {
                    siteBearingFavoritesVO.setCustomerAcct(jsonObject.get("customerAcct").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("siteId")) {
                try {
                    siteBearingFavoritesVO.setSiteId(UUID.fromString(jsonObject.get("siteId").getAsString()));
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("vendorId")) {
                try {
                    siteBearingFavoritesVO.setVendorId(jsonObject.get("vendorId").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("bearingId")) {
                try {
                    siteBearingFavoritesVO.setBearingId(jsonObject.get("bearingId").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("ftf")) {
                try {
                    siteBearingFavoritesVO.setFtf(jsonObject.get("ftf").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("bsf")) {
                try {
                    siteBearingFavoritesVO.setBsf(jsonObject.get("bsf").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("bpfo")) {
                try {
                    siteBearingFavoritesVO.setBpfo(jsonObject.get("bpfo").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("bpfi")) {
                try {
                    siteBearingFavoritesVO.setBpfi(jsonObject.get("bpfi").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }
        }
        return siteBearingFavoritesVO;
    }

    /**
     * Parse the given json String Object and return a BearingCatalogVO object
     *
     * @param content, String Object
     * @return DevicePresetVO Object
     */
    public static BearingCatalogVO bearingCatalogVOParser(String content) {
        BearingCatalogVO bearingCatalogVO = null;
        JsonElement element;
        JsonObject jsonObject;

        if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && (jsonObject = element.getAsJsonObject()) != null) {
            bearingCatalogVO = new BearingCatalogVO();

            if (jsonObject.has("vendorId")) {
                try {
                    bearingCatalogVO.setVendorId(jsonObject.get("vendorId").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("bearingId")) {
                try {
                    bearingCatalogVO.setBearingId(jsonObject.get("bearingId").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("bc")) {
                try {
                    bearingCatalogVO.setBc(jsonObject.get("bc").getAsShort());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("bpfi")) {
                try {
                    bearingCatalogVO.setBpfi(jsonObject.get("bpfi").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("bpfo")) {
                try {
                    bearingCatalogVO.setBpfo(jsonObject.get("bpfo").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("bsf")) {
                try {
                    bearingCatalogVO.setBsf(jsonObject.get("bsf").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("ftf")) {
                try {
                    bearingCatalogVO.setFtf(jsonObject.get("ftf").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.error("Exception: {}", ex.getMessage());
                }
            }
        }
        return bearingCatalogVO;
    }

    /**
     * Parse the given json String Object and return a SiteFFSetFavoritesVO
     * object
     *
     * @param content, String Object
     * @return SiteFFSetFavoritesVO Object
     */
    public static List<SiteFFSetFavoritesVO> siteFFSetFavoritesVOParser(String content) {
        List<SiteFFSetFavoritesVO> siteFFSetFavoritesVOs = new ArrayList();
        SiteFFSetFavoritesVO siteFFSetFavoritesVO = null;
        JsonElement element;
        JsonObject jsonObject;
        JsonArray jsonArray;

        if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && (jsonArray = element.getAsJsonArray()) != null) {
            for (int i = 0; i < jsonArray.size(); i++) {
                jsonObject = jsonArray.get(i).getAsJsonObject();
                siteFFSetFavoritesVO = new SiteFFSetFavoritesVO();

                if (jsonObject.has("customerAccount")) {
                    try {
                        siteFFSetFavoritesVO.setCustomerAccount(jsonObject.get("customerAccount").getAsString());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("siteId")) {
                    try {
                        siteFFSetFavoritesVO.setSiteId(UUID.fromString(jsonObject.get("siteId").getAsString()));
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("ffSetId")) {
                    try {
                        siteFFSetFavoritesVO.setFfSetId(UUID.fromString(jsonObject.get("ffSetId").getAsString()));
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("ffId")) {
                    try {
                        siteFFSetFavoritesVO.setFfId(UUID.fromString(jsonObject.get("ffId").getAsString()));
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("ffName")) {
                    try {
                        siteFFSetFavoritesVO.setFfName(jsonObject.get("ffName").getAsString());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("ffSetDesc")) {
                    try {
                        siteFFSetFavoritesVO.setFfSetDesc(jsonObject.get("ffSetDesc").getAsString());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("ffSetName")) {
                    try {
                        siteFFSetFavoritesVO.setFfSetName(jsonObject.get("ffSetName").getAsString());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("ffUnit")) {
                    try {
                        siteFFSetFavoritesVO.setFfUnit(jsonObject.get("ffUnit").getAsString());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("ffValue")) {
                    try {
                        siteFFSetFavoritesVO.setFfValue(jsonObject.get("ffValue").getAsFloat());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }
                if (jsonObject.has("global")) {
                    try {
                        siteFFSetFavoritesVO.setGlobal(jsonObject.get("global").getAsBoolean());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }
                siteFFSetFavoritesVOs.add(siteFFSetFavoritesVO);
            }
        }
        return siteFFSetFavoritesVOs;
    }

    /**
     * Parse the given json String Object and return a SiteFFSetFavoritesVO
     * object
     *
     * @param content, String Object
     * @return List of SiteFFSetFavoritesVO Objects
     */
    public static List<SiteBearingFavoritesVO> siteBearingFavoritesVOListParser(String content) {
        List<SiteBearingFavoritesVO> siteBearingFavoritesVOList = new ArrayList();
        SiteBearingFavoritesVO siteBearingFavoritesVO = null;
        JsonElement element;
        JsonObject jsonObject;
        JsonArray jsonArray;

        if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && (jsonArray = element.getAsJsonArray()) != null) {
            for (int i = 0; i < jsonArray.size(); i++) {
                jsonObject = jsonArray.get(i).getAsJsonObject();
                siteBearingFavoritesVO = new SiteBearingFavoritesVO();

                if (jsonObject.has("customerAcct")) {
                    try {
                        siteBearingFavoritesVO.setCustomerAcct(jsonObject.get("customerAcct").getAsString());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("siteId")) {
                    try {
                        siteBearingFavoritesVO.setSiteId(UUID.fromString(jsonObject.get("siteId").getAsString()));
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("vendorId")) {
                    try {
                        siteBearingFavoritesVO.setVendorId(jsonObject.get("vendorId").getAsString());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("bearingId")) {
                    try {
                        siteBearingFavoritesVO.setBearingId(jsonObject.get("bearingId").getAsString());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("bsf")) {
                    try {
                        siteBearingFavoritesVO.setBsf(jsonObject.get("bsf").getAsFloat());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("bpfo")) {
                    try {
                        siteBearingFavoritesVO.setBpfo(jsonObject.get("bpfo").getAsFloat());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("bpfi")) {
                    try {
                        siteBearingFavoritesVO.setBpfi(jsonObject.get("bpfi").getAsFloat());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("ftf")) {
                    try {
                        siteBearingFavoritesVO.setFtf(jsonObject.get("ftf").getAsFloat());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                siteBearingFavoritesVOList.add(siteBearingFavoritesVO);
            }
        }
        return siteBearingFavoritesVOList;
    }

    /**
     * Parse the given json String Object and return a SiteFavoritesEncapVO
     * object
     *
     * @param content, String Object
     * @return SiteFavoritesEncapVO Object
     */
    public static SiteFavoritesEncapVO siteFavoritesEncapVOParser(String content) {
        SiteFavoritesEncapVO siteFavoritesEncapVO = new SiteFavoritesEncapVO();
        List<SiteBearingFavoritesVO> createSiteBearingFavoritesVOList = new ArrayList();
        List<SiteBearingFavoritesVO> deleteSiteBearingFavoritesVOList = new ArrayList();
        SiteBearingFavoritesVO siteBearingFavoritesVO = null;
        JsonElement element;
        JsonObject mainObject, jsonObject;
        JsonArray jsonArray;

        if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null) {
            mainObject = element.getAsJsonObject();
            if (mainObject.has("createSiteBearingFavoritesVOList")) {
                if ((jsonArray = mainObject.getAsJsonArray("createSiteBearingFavoritesVOList")) != null && !jsonArray.isEmpty()) {
                    for (int i = 0; i < jsonArray.size(); i++) {
                        jsonObject = jsonArray.get(i).getAsJsonObject();
                        siteBearingFavoritesVO = new SiteBearingFavoritesVO();

                        if (jsonObject.has("customerAcct")) {
                            try {
                                siteBearingFavoritesVO.setCustomerAcct(jsonObject.get("customerAcct").getAsString());
                            } catch (Exception ex) {
                                LOGGER.error("Exception: {}", ex.getMessage());
                            }
                        }

                        if (jsonObject.has("siteId")) {
                            try {
                                siteBearingFavoritesVO.setSiteId(UUID.fromString(jsonObject.get("siteId").getAsString()));
                            } catch (Exception ex) {
                                LOGGER.error("Exception: {}", ex.getMessage());
                            }
                        }

                        if (jsonObject.has("vendorId")) {
                            try {
                                siteBearingFavoritesVO.setVendorId(jsonObject.get("vendorId").getAsString());
                            } catch (Exception ex) {
                                LOGGER.error("Exception: {}", ex.getMessage());
                            }
                        }

                        if (jsonObject.has("bearingId")) {
                            try {
                                siteBearingFavoritesVO.setBearingId(jsonObject.get("bearingId").getAsString());
                            } catch (Exception ex) {
                                LOGGER.error("Exception: {}", ex.getMessage());
                            }
                        }

                        if (jsonObject.has("bsf")) {
                            try {
                                siteBearingFavoritesVO.setBsf(jsonObject.get("bsf").getAsFloat());
                            } catch (Exception ex) {
                                LOGGER.error("Exception: {}", ex.getMessage());
                            }
                        }

                        if (jsonObject.has("bpfo")) {
                            try {
                                siteBearingFavoritesVO.setBpfo(jsonObject.get("bpfo").getAsFloat());
                            } catch (Exception ex) {
                                LOGGER.error("Exception: {}", ex.getMessage());
                            }
                        }

                        if (jsonObject.has("bpfi")) {
                            try {
                                siteBearingFavoritesVO.setBpfi(jsonObject.get("bpfi").getAsFloat());
                            } catch (Exception ex) {
                                LOGGER.error("Exception: {}", ex.getMessage());
                            }
                        }

                        if (jsonObject.has("ftf")) {
                            try {
                                siteBearingFavoritesVO.setFtf(jsonObject.get("ftf").getAsFloat());
                            } catch (Exception ex) {
                                LOGGER.error("Exception: {}", ex.getMessage());
                            }
                        }

                        createSiteBearingFavoritesVOList.add(siteBearingFavoritesVO);
                    }
                }
            }
            if (mainObject.has("deleteSiteBearingFavoritesVOList")) {
                if ((jsonArray = mainObject.getAsJsonArray("deleteSiteBearingFavoritesVOList")) != null && !jsonArray.isEmpty()) {
                    for (int i = 0; i < jsonArray.size(); i++) {
                        jsonObject = jsonArray.get(i).getAsJsonObject();
                        siteBearingFavoritesVO = new SiteBearingFavoritesVO();

                        if (jsonObject.has("customerAcct")) {
                            try {
                                siteBearingFavoritesVO.setCustomerAcct(jsonObject.get("customerAcct").getAsString());
                            } catch (Exception ex) {
                                LOGGER.error("Exception: {}", ex.getMessage());
                            }
                        }

                        if (jsonObject.has("siteId")) {
                            try {
                                siteBearingFavoritesVO.setSiteId(UUID.fromString(jsonObject.get("siteId").getAsString()));
                            } catch (Exception ex) {
                                LOGGER.error("Exception: {}", ex.getMessage());
                            }
                        }

                        if (jsonObject.has("vendorId")) {
                            try {
                                siteBearingFavoritesVO.setVendorId(jsonObject.get("vendorId").getAsString());
                            } catch (Exception ex) {
                                LOGGER.error("Exception: {}", ex.getMessage());
                            }
                        }

                        if (jsonObject.has("bearingId")) {
                            try {
                                siteBearingFavoritesVO.setBearingId(jsonObject.get("bearingId").getAsString());
                            } catch (Exception ex) {
                                LOGGER.error("Exception: {}", ex.getMessage());
                            }
                        }

                        if (jsonObject.has("bsf")) {
                            try {
                                siteBearingFavoritesVO.setBsf(jsonObject.get("bsf").getAsFloat());
                            } catch (Exception ex) {
                                LOGGER.error("Exception: {}", ex.getMessage());
                            }
                        }

                        if (jsonObject.has("bpfo")) {
                            try {
                                siteBearingFavoritesVO.setBpfo(jsonObject.get("bpfo").getAsFloat());
                            } catch (Exception ex) {
                                LOGGER.error("Exception: {}", ex.getMessage());
                            }
                        }

                        if (jsonObject.has("bpfi")) {
                            try {
                                siteBearingFavoritesVO.setBpfi(jsonObject.get("bpfi").getAsFloat());
                            } catch (Exception ex) {
                                LOGGER.error("Exception: {}", ex.getMessage());
                            }
                        }

                        if (jsonObject.has("ftf")) {
                            try {
                                siteBearingFavoritesVO.setFtf(jsonObject.get("ftf").getAsFloat());
                            } catch (Exception ex) {
                                LOGGER.error("Exception: {}", ex.getMessage());
                            }
                        }

                        deleteSiteBearingFavoritesVOList.add(siteBearingFavoritesVO);
                    }
                }
            }
            siteFavoritesEncapVO.setCreateSiteBearingFavoritesVOList(createSiteBearingFavoritesVOList);
            siteFavoritesEncapVO.setDeleteSiteBearingFavoritesVOList(deleteSiteBearingFavoritesVOList);
        }
        return siteFavoritesEncapVO;
    }

    /**
     * Parse the given json String Object and return a SiteFavoritesEncapVO
     * object
     *
     * @param content, String Object
     * @return SiteFavoritesEncapVO Object
     */
    public static SiteFavoritesEncapVO siteFFFavoritesEncapVOParser(String content) {
        SiteFavoritesEncapVO siteFavoritesEncapVO = new SiteFavoritesEncapVO();
        List<SiteFFSetFavoritesVO> createSiteFFSetFavoritesVOList = new ArrayList();
        List<SiteFFSetFavoritesVO> deleteSiteFFSetFavoritesVOList = new ArrayList();
        SiteFFSetFavoritesVO siteFFSetFavoritesVO = null;
        JsonElement element;
        JsonObject mainObject, jsonObject;
        JsonArray jsonArray;

        if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null) {
            mainObject = element.getAsJsonObject();
            if (mainObject.has("createSiteFFSetFavoritesVOList")) {
                if ((jsonArray = mainObject.getAsJsonArray("createSiteFFSetFavoritesVOList")) != null && !jsonArray.isEmpty()) {
                    for (int i = 0; i < jsonArray.size(); i++) {
                        jsonObject = jsonArray.get(i).getAsJsonObject();
                        siteFFSetFavoritesVO = new SiteFFSetFavoritesVO();

                        if (jsonObject.has("customerAccount")) {
                            try {
                                siteFFSetFavoritesVO.setCustomerAccount(jsonObject.get("customerAccount").getAsString());
                            } catch (Exception ex) {
                                LOGGER.error("Exception: {}", ex.getMessage());
                            }
                        }

                        if (jsonObject.has("siteId")) {
                            try {
                                siteFFSetFavoritesVO.setSiteId(UUID.fromString(jsonObject.get("siteId").getAsString()));
                            } catch (Exception ex) {
                                LOGGER.error("Exception: {}", ex.getMessage());
                            }
                        }

                        if (jsonObject.has("ffSetId")) {
                            try {
                                siteFFSetFavoritesVO.setFfSetId(UUID.fromString(jsonObject.get("ffSetId").getAsString()));
                            } catch (Exception ex) {
                                LOGGER.error("Exception: {}", ex.getMessage());
                            }
                        }

                        if (jsonObject.has("ffId")) {
                            try {
                                siteFFSetFavoritesVO.setFfId(UUID.fromString(jsonObject.get("ffId").getAsString()));
                            } catch (Exception ex) {
                                LOGGER.error("Exception: {}", ex.getMessage());
                            }
                        }

                        if (jsonObject.has("ffName")) {
                            try {
                                siteFFSetFavoritesVO.setFfName(jsonObject.get("ffName").getAsString());
                            } catch (Exception ex) {
                                LOGGER.error("Exception: {}", ex.getMessage());
                            }
                        }

                        if (jsonObject.has("ffSetDesc")) {
                            try {
                                siteFFSetFavoritesVO.setFfSetDesc(jsonObject.get("ffSetDesc").getAsString());
                            } catch (Exception ex) {
                                LOGGER.error("Exception: {}", ex.getMessage());
                            }
                        }

                        if (jsonObject.has("ffSetName")) {
                            try {
                                siteFFSetFavoritesVO.setFfSetName(jsonObject.get("ffSetName").getAsString());
                            } catch (Exception ex) {
                                LOGGER.error("Exception: {}", ex.getMessage());
                            }
                        }

                        if (jsonObject.has("ffUnit")) {
                            try {
                                siteFFSetFavoritesVO.setFfUnit(jsonObject.get("ffUnit").getAsString());
                            } catch (Exception ex) {
                                LOGGER.error("Exception: {}", ex.getMessage());
                            }
                        }

                        if (jsonObject.has("ffValue")) {
                            try {
                                siteFFSetFavoritesVO.setFfValue(jsonObject.get("ffValue").getAsFloat());
                            } catch (Exception ex) {
                                LOGGER.error("Exception: {}", ex.getMessage());
                            }
                        }

                        if (jsonObject.has("global")) {
                            try {
                                siteFFSetFavoritesVO.setGlobal(jsonObject.get("global").getAsBoolean());
                            } catch (Exception ex) {
                                LOGGER.error("Exception: {}", ex.getMessage());
                            }
                        }
                        createSiteFFSetFavoritesVOList.add(siteFFSetFavoritesVO);
                    }
                }
            }
            if (mainObject.has("deleteSiteFFSetFavoritesVOList")) {
                if ((jsonArray = mainObject.getAsJsonArray("deleteSiteFFSetFavoritesVOList")) != null && !jsonArray.isEmpty()) {
                    for (int i = 0; i < jsonArray.size(); i++) {
                        jsonObject = jsonArray.get(i).getAsJsonObject();
                        siteFFSetFavoritesVO = new SiteFFSetFavoritesVO();

                        if (jsonObject.has("customerAccount")) {
                            try {
                                siteFFSetFavoritesVO.setCustomerAccount(jsonObject.get("customerAccount").getAsString());
                            } catch (Exception ex) {
                                LOGGER.error("Exception: {}", ex.getMessage());
                            }
                        }

                        if (jsonObject.has("siteId")) {
                            try {
                                siteFFSetFavoritesVO.setSiteId(UUID.fromString(jsonObject.get("siteId").getAsString()));
                            } catch (Exception ex) {
                                LOGGER.error("Exception: {}", ex.getMessage());
                            }
                        }

                        if (jsonObject.has("ffSetId")) {
                            try {
                                siteFFSetFavoritesVO.setFfSetId(UUID.fromString(jsonObject.get("ffSetId").getAsString()));
                            } catch (Exception ex) {
                                LOGGER.error("Exception: {}", ex.getMessage());
                            }
                        }

                        if (jsonObject.has("ffId")) {
                            try {
                                siteFFSetFavoritesVO.setFfId(UUID.fromString(jsonObject.get("ffId").getAsString()));
                            } catch (Exception ex) {
                                LOGGER.error("Exception: {}", ex.getMessage());
                            }
                        }

                        if (jsonObject.has("ffName")) {
                            try {
                                siteFFSetFavoritesVO.setFfName(jsonObject.get("ffName").getAsString());
                            } catch (Exception ex) {
                                LOGGER.error("Exception: {}", ex.getMessage());
                            }
                        }

                        if (jsonObject.has("ffSetDesc")) {
                            try {
                                siteFFSetFavoritesVO.setFfSetDesc(jsonObject.get("ffSetDesc").getAsString());
                            } catch (Exception ex) {
                                LOGGER.error("Exception: {}", ex.getMessage());
                            }
                        }

                        if (jsonObject.has("ffSetName")) {
                            try {
                                siteFFSetFavoritesVO.setFfSetName(jsonObject.get("ffSetName").getAsString());
                            } catch (Exception ex) {
                                LOGGER.error("Exception: {}", ex.getMessage());
                            }
                        }

                        if (jsonObject.has("ffUnit")) {
                            try {
                                siteFFSetFavoritesVO.setFfUnit(jsonObject.get("ffUnit").getAsString());
                            } catch (Exception ex) {
                                LOGGER.error("Exception: {}", ex.getMessage());
                            }
                        }

                        if (jsonObject.has("ffValue")) {
                            try {
                                siteFFSetFavoritesVO.setFfValue(jsonObject.get("ffValue").getAsFloat());
                            } catch (Exception ex) {
                                LOGGER.error("Exception: {}", ex.getMessage());
                            }
                        }
                        if (jsonObject.has("global")) {
                            try {
                                siteFFSetFavoritesVO.setGlobal(jsonObject.get("global").getAsBoolean());
                            } catch (Exception ex) {
                                LOGGER.error("Exception: {}", ex.getMessage());
                            }
                        }
                        deleteSiteFFSetFavoritesVOList.add(siteFFSetFavoritesVO);
                    }
                }
            }
            siteFavoritesEncapVO.setCreateSiteFFSetFavoritesVOList(createSiteFFSetFavoritesVOList);
            siteFavoritesEncapVO.setDeleteSiteFFSetFavoritesVOList(deleteSiteFFSetFavoritesVOList);
        }
        return siteFavoritesEncapVO;
    }

    /**
     * Parse the given json String Object and return a List of AssetPresetVO
     * objects
     *
     * @param content, String Object
     * @return List of AssetPresetVO Object
     */
    public static List<AssetPresetVO> assetPresetVOListParser(String content) {
        List<AssetPresetVO> assetPresetVOList = new ArrayList();
        AssetPresetVO assetPresetVO;
        JsonElement element;
        JsonObject jsonObject;
        JsonArray jsonArray;

        if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && (jsonArray = element.getAsJsonArray()) != null) {
            for (int i = 0; i < jsonArray.size(); i++) {
                jsonObject = jsonArray.get(i).getAsJsonObject();
                assetPresetVO = new AssetPresetVO();

                if (jsonObject.has("customerAccount")) {
                    try {
                        assetPresetVO.setCustomerAccount(jsonObject.get("customerAccount").getAsString());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("siteId")) {
                    try {
                        assetPresetVO.setSiteId(UUID.fromString(jsonObject.get("siteId").getAsString()));
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("assetTypeName")) {
                    try {
                        assetPresetVO.setAssetTypeName(jsonObject.get("assetTypeName").getAsString());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("assetPresetId")) {
                    try {
                        assetPresetVO.setAssetPresetId(UUID.fromString(jsonObject.get("assetPresetId").getAsString()));
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("pointLocationName")) {
                    try {
                        assetPresetVO.setPointLocationName(jsonObject.get("pointLocationName").getAsString());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("assetPresetName")) {
                    try {
                        assetPresetVO.setAssetPresetName(jsonObject.get("assetPresetName").getAsString());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }
                
                if (jsonObject.has("devicePresetId")) {
                    try {
                        assetPresetVO.setDevicePresetId(UUID.fromString(jsonObject.get("devicePresetId").getAsString()));
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("devicePresetType")) {
                    try {
                        assetPresetVO.setDevicePresetType(jsonObject.get("devicePresetType").getAsString());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("assetPresetName")) {
                    try {
                        assetPresetVO.setAssetPresetName(jsonObject.get("assetPresetName").getAsString());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("sampleInterval")) {
                    try {
                        assetPresetVO.setSampleInterval(jsonObject.get("sampleInterval").getAsInt());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("ffSetIds")) {
                    try {
                        assetPresetVO.setFfSetIds(jsonObject.get("ffSetIds").getAsString());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("rollDiameter")) {
                    try {
                        assetPresetVO.setRollDiameter(jsonObject.get("rollDiameter").getAsFloat());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("rollDiameterUnits")) {
                    try {
                        assetPresetVO.setRollDiameterUnits(jsonObject.get("rollDiameterUnits").getAsString());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }
                if (jsonObject.has("speedRatio")) {
                    try {
                        assetPresetVO.setSpeedRatio(jsonObject.get("speedRatio").getAsFloat());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("tachId")) {
                    try {
                        assetPresetVO.setTachId(UUID.fromString(jsonObject.get("tachId").getAsString()));
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                if (jsonObject.has("description")) {
                    try {
                        assetPresetVO.setDescription(jsonObject.get("description").getAsString());
                    } catch (Exception ex) {
                        LOGGER.error("Exception: {}", ex.getMessage());
                    }
                }

                assetPresetVOList.add(assetPresetVO);
            }
        }
        return assetPresetVOList;
    }

}
