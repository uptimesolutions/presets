/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.vo;

import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
public class DevicePresetVO {
    private String customerAccount;
    private UUID siteId;
    private String deviceType;
    private UUID presetId;
    private String channelType;
    private byte channelNumber;
    private UUID apSetId;
    private UUID alSetId;
    private boolean alarmEnabled;
    boolean autoAcknowledge;
    boolean disabled;
    String name;
    int sampleInterval;
    float sensorOffset;
    float sensorSensitivity;
    String sensorType;
    String sensorUnits;
    private String apSetName;
    private String alSetName;
    private String sensorSubType;
    private String sensorLocalOrientation;
    
    public DevicePresetVO() {
    }

    public DevicePresetVO(String customerAccount, UUID siteId, String deviceType, UUID presetId, String channelType, byte channelNumber, UUID apSetId, UUID alSetId, boolean alarmEnabled, boolean autoAcknowledge, boolean disabled, String name, int sampleInterval, float sensorOffset, float sensorSensitivity, String sensorType, String sensorUnits, String apSetName, String alSetName, String sensorSubType, String sensorLocalOrientation) {
        this.customerAccount = customerAccount;
        this.siteId = siteId;
        this.deviceType = deviceType;
        this.presetId = presetId;
        this.channelType = channelType;
        this.channelNumber = channelNumber;
        this.apSetId = apSetId;
        this.alSetId = alSetId;
        this.alarmEnabled = alarmEnabled;
        this.autoAcknowledge = autoAcknowledge;
        this.disabled = disabled;
        this.name = name;
        this.sampleInterval = sampleInterval;
        this.sensorOffset = sensorOffset;
        this.sensorSensitivity = sensorSensitivity;
        this.sensorType = sensorType;
        this.sensorUnits = sensorUnits;
        this.apSetName = apSetName;
        this.alSetName = alSetName;
        this.sensorSubType = sensorSubType;
        this.sensorLocalOrientation = sensorLocalOrientation;
    }
    

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public UUID getPresetId() {
        return presetId;
    }

    public void setPresetId(UUID presetId) {
        this.presetId = presetId;
    }

    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    public byte getChannelNumber() {
        return channelNumber;
    }

    public void setChannelNumber(byte channelNumber) {
        this.channelNumber = channelNumber;
    }

    public UUID getApSetId() {
        return apSetId;
    }

    public void setApSetId(UUID apSetId) {
        this.apSetId = apSetId;
    }

    public UUID getAlSetId() {
        return alSetId;
    }

    public void setAlSetId(UUID alSetId) {
        this.alSetId = alSetId;
    }

    public boolean isAlarmEnabled() {
        return alarmEnabled;
    }

    public void setAlarmEnabled(boolean alarmEnabled) {
        this.alarmEnabled = alarmEnabled;
    }

    public boolean isAutoAcknowledge() {
        return autoAcknowledge;
    }

    public void setAutoAcknowledge(boolean autoAcknowledge) {
        this.autoAcknowledge = autoAcknowledge;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSampleInterval() {
        return sampleInterval;
    }

    public void setSampleInterval(int sampleInterval) {
        this.sampleInterval = sampleInterval;
    }

    public float getSensorOffset() {
        return sensorOffset;
    }

    public void setSensorOffset(float sensorOffset) {
        this.sensorOffset = sensorOffset;
    }

    public float getSensorSensitivity() {
        return sensorSensitivity;
    }

    public void setSensorSensitivity(float sensorSensitivity) {
        this.sensorSensitivity = sensorSensitivity;
    }

    public String getSensorType() {
        return sensorType;
    }

    public void setSensorType(String sensorType) {
        this.sensorType = sensorType;
    }

    public String getSensorUnits() {
        return sensorUnits;
    }

    public void setSensorUnits(String sensorUnits) {
        this.sensorUnits = sensorUnits;
    }

    public String getApSetName() {
        return apSetName;
    }

    public void setApSetName(String apSetName) {
        this.apSetName = apSetName;
    }

    public String getAlSetName() {
        return alSetName;
    }

    public void setAlSetName(String alSetName) {
        this.alSetName = alSetName;
    }

    public String getSensorSubType() {
        return sensorSubType;
    }

    public void setSensorSubType(String sensorSubType) {
        this.sensorSubType = sensorSubType;
    }

    public String getSensorLocalOrientation() {
        return sensorLocalOrientation;
    }

    public void setSensorLocalOrientation(String sensorLocalOrientation) {
        this.sensorLocalOrientation = sensorLocalOrientation;
    }
    

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 43 * hash + Objects.hashCode(this.customerAccount);
        hash = 43 * hash + Objects.hashCode(this.siteId);
        hash = 43 * hash + Objects.hashCode(this.deviceType);
        hash = 43 * hash + Objects.hashCode(this.presetId);
        hash = 43 * hash + Objects.hashCode(this.channelType);
        hash = 43 * hash + this.channelNumber;
        hash = 43 * hash + Objects.hashCode(this.apSetId);
        hash = 43 * hash + Objects.hashCode(this.alSetId);
        hash = 43 * hash + (this.alarmEnabled ? 1 : 0);
        hash = 43 * hash + (this.autoAcknowledge ? 1 : 0);
        hash = 43 * hash + (this.disabled ? 1 : 0);
        hash = 43 * hash + Objects.hashCode(this.name);
        hash = 43 * hash + this.sampleInterval;
        hash = 43 * hash + Float.floatToIntBits(this.sensorOffset);
        hash = 43 * hash + Float.floatToIntBits(this.sensorSensitivity);
        hash = 43 * hash + Objects.hashCode(this.sensorType);
        hash = 43 * hash + Objects.hashCode(this.sensorUnits);
        hash = 43 * hash + Objects.hashCode(this.apSetName);
        hash = 43 * hash + Objects.hashCode(this.alSetName);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DevicePresetVO other = (DevicePresetVO) obj;
        if (this.channelNumber != other.channelNumber) {
            return false;
        }
        if (this.alarmEnabled != other.alarmEnabled) {
            return false;
        }
        if (this.autoAcknowledge != other.autoAcknowledge) {
            return false;
        }
        if (this.disabled != other.disabled) {
            return false;
        }
        if (this.sampleInterval != other.sampleInterval) {
            return false;
        }
        if (Float.floatToIntBits(this.sensorOffset) != Float.floatToIntBits(other.sensorOffset)) {
            return false;
        }
        if (Float.floatToIntBits(this.sensorSensitivity) != Float.floatToIntBits(other.sensorSensitivity)) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.deviceType, other.deviceType)) {
            return false;
        }
        if (!Objects.equals(this.channelType, other.channelType)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.sensorType, other.sensorType)) {
            return false;
        }
        if (!Objects.equals(this.sensorUnits, other.sensorUnits)) {
            return false;
        }
        if (!Objects.equals(this.apSetName, other.apSetName)) {
            return false;
        }
        if (!Objects.equals(this.alSetName, other.alSetName)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.presetId, other.presetId)) {
            return false;
        }
        if (!Objects.equals(this.apSetId, other.apSetId)) {
            return false;
        }
        if (!Objects.equals(this.alSetId, other.alSetId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DevicePresetVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", deviceType=" + deviceType + ", presetId=" + presetId + ", channelType=" + channelType + ", channelNumber=" + channelNumber + ", apSetId=" + apSetId + ", alSetId=" + alSetId + ", alarmEnabled=" + alarmEnabled + ", autoAcknowledge=" + autoAcknowledge + ", disabled=" + disabled + ", name=" + name + ", sampleInterval=" + sampleInterval + ", sensorOffset=" + sensorOffset + ", sensorSensitivity=" + sensorSensitivity + ", sensorType=" + sensorType + ", sensorUnits=" + sensorUnits + ", apSetName=" + apSetName + ", alSetName=" + alSetName + ", sensorSubType=" + sensorSubType + ", sensorLocalOrientation=" + sensorLocalOrientation + '}';
    }
}
