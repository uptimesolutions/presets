/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.vo;

import com.uptime.cassandra.presets.entity.SiteFFSetFavorites;
import java.util.List;

/**
 *
 * @author madhavi
 */
public class SiteFavoritesEncapVO {
    
    List<SiteBearingFavoritesVO> createSiteBearingFavoritesVOList;
    List<SiteBearingFavoritesVO> deleteSiteBearingFavoritesVOList;
    
    List<SiteFFSetFavoritesVO> createSiteFFSetFavoritesVOList;
    List<SiteFFSetFavoritesVO> deleteSiteFFSetFavoritesVOList;

    public List<SiteBearingFavoritesVO> getCreateSiteBearingFavoritesVOList() {
        return createSiteBearingFavoritesVOList;
    }

    public void setCreateSiteBearingFavoritesVOList(List<SiteBearingFavoritesVO> createSiteBearingFavoritesVOList) {
        this.createSiteBearingFavoritesVOList = createSiteBearingFavoritesVOList;
    }

    public List<SiteBearingFavoritesVO> getDeleteSiteBearingFavoritesVOList() {
        return deleteSiteBearingFavoritesVOList;
    }

    public void setDeleteSiteBearingFavoritesVOList(List<SiteBearingFavoritesVO> deleteSiteBearingFavoritesVOList) {
        this.deleteSiteBearingFavoritesVOList = deleteSiteBearingFavoritesVOList;
    }

    public List<SiteFFSetFavoritesVO> getCreateSiteFFSetFavoritesVOList() {
        return createSiteFFSetFavoritesVOList;
    }

    public void setCreateSiteFFSetFavoritesVOList(List<SiteFFSetFavoritesVO> createSiteFFSetFavoritesVOList) {
        this.createSiteFFSetFavoritesVOList = createSiteFFSetFavoritesVOList;
    }

    public List<SiteFFSetFavoritesVO> getDeleteSiteFFSetFavoritesVOList() {
        return deleteSiteFFSetFavoritesVOList;
    }

    public void setDeleteSiteFFSetFavoritesVOList(List<SiteFFSetFavoritesVO> deleteSiteFFSetFavoritesVOList) {
        this.deleteSiteFFSetFavoritesVOList = deleteSiteFFSetFavoritesVOList;
    }

}
