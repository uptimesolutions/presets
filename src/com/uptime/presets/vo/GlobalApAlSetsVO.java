/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.vo;

import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author Joseph
 */
public class GlobalApAlSetsVO {
    private String customerAccount;
    private UUID apSetId;
    private UUID alSetId;
    private String apSetName;
    private String alSetName;
    private String paramName;
    private float demodHighPass;
    private float demodLowPass;
    private int dwell;
    private int fmax;
    private String frequencyUnits;
    private float highAlert;
    private boolean highAlertActive;
    private float highFault;
    private boolean highFaultActive;
    private boolean demod;
    private float lowAlert;
    private boolean lowAlertActive;
    private float lowFault;
    private boolean lowFaultActive;
    private float maxFrequency;
    private float minFrequency;
    private String paramAmpFactor;
    private String paramType;
    private String paramUnits;
    private float period;
    private int resolution;
    private float sampleRate;
    private String sensorType;

    public GlobalApAlSetsVO() {
    }

    public GlobalApAlSetsVO(String customerAccount, UUID apSetId, UUID alSetId, String apSetName, String alSetName, String paramName, float demodHighPass, float demodLowPass, int dwell, int fmax, String frequencyUnits, float highAlert, boolean highAlertActive, float highFault, boolean highFaultActive, boolean demod, float lowAlert, boolean lowAlertActive, float lowFault, boolean lowFaultActive, float maxFrequency, float minFrequency, String paramAmpFactor, String paramType, String paramUnits, float period, int resolution, float sampleRate, String sensorType) {
        this.customerAccount = customerAccount;
        this.apSetId = apSetId;
        this.alSetId = alSetId;
        this.apSetName = apSetName;
        this.alSetName = alSetName;
        this.paramName = paramName;
        this.demodHighPass = demodHighPass;
        this.demodLowPass = demodLowPass;
        this.dwell = dwell;
        this.fmax = fmax;
        this.frequencyUnits = frequencyUnits;
        this.highAlert = highAlert;
        this.highAlertActive = highAlertActive;
        this.highFault = highFault;
        this.highFaultActive = highFaultActive;
        this.demod = demod;
        this.lowAlert = lowAlert;
        this.lowAlertActive = lowAlertActive;
        this.lowFault = lowFault;
        this.lowFaultActive = lowFaultActive;
        this.maxFrequency = maxFrequency;
        this.minFrequency = minFrequency;
        this.paramAmpFactor = paramAmpFactor;
        this.paramType = paramType;
        this.paramUnits = paramUnits;
        this.period = period;
        this.resolution = resolution;
        this.sampleRate = sampleRate;
        this.sensorType = sensorType;
    }
    
    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getApSetId() {
        return apSetId;
    }

    public void setApSetId(UUID apSetId) {
        this.apSetId = apSetId;
    }

    public UUID getAlSetId() {
        return alSetId;
    }

    public void setAlSetId(UUID alSetId) {
        this.alSetId = alSetId;
    }

    public String getApSetName() {
        return apSetName;
    }

    public void setApSetName(String apSetName) {
        this.apSetName = apSetName;
    }

    public String getAlSetName() {
        return alSetName;
    }

    public void setAlSetName(String alSetName) {
        this.alSetName = alSetName;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public float getDemodHighPass() {
        return demodHighPass;
    }

    public void setDemodHighPass(float demodHighPass) {
        this.demodHighPass = demodHighPass;
    }

    public float getDemodLowPass() {
        return demodLowPass;
    }

    public void setDemodLowPass(float demodLowPass) {
        this.demodLowPass = demodLowPass;
    }

    public int getDwell() {
        return dwell;
    }

    public void setDwell(int dwell) {
        this.dwell = dwell;
    }

    public int getFmax() {
        return fmax;
    }

    public void setFmax(int fmax) {
        this.fmax = fmax;
    }

    public String getFrequencyUnits() {
        return frequencyUnits;
    }

    public void setFrequencyUnits(String frequencyUnits) {
        this.frequencyUnits = frequencyUnits;
    }

    public float getHighAlert() {
        return highAlert;
    }

    public void setHighAlert(float highAlert) {
        this.highAlert = highAlert;
    }

    public boolean isHighAlertActive() {
        return highAlertActive;
    }

    public void setHighAlertActive(boolean highAlertActive) {
        this.highAlertActive = highAlertActive;
    }

    public float getHighFault() {
        return highFault;
    }

    public void setHighFault(float highFault) {
        this.highFault = highFault;
    }

    public boolean isHighFaultActive() {
        return highFaultActive;
    }

    public void setHighFaultActive(boolean highFaultActive) {
        this.highFaultActive = highFaultActive;
    }

    public boolean isDemod() {
        return demod;
    }

    public void setDemod(boolean demod) {
        this.demod = demod;
    }

    public float getLowAlert() {
        return lowAlert;
    }

    public void setLowAlert(float lowAlert) {
        this.lowAlert = lowAlert;
    }

    public boolean isLowAlertActive() {
        return lowAlertActive;
    }

    public void setLowAlertActive(boolean lowAlertActive) {
        this.lowAlertActive = lowAlertActive;
    }

    public float getLowFault() {
        return lowFault;
    }

    public void setLowFault(float lowFault) {
        this.lowFault = lowFault;
    }

    public boolean isLowFaultActive() {
        return lowFaultActive;
    }

    public void setLowFaultActive(boolean lowFaultActive) {
        this.lowFaultActive = lowFaultActive;
    }

    public float getMaxFrequency() {
        return maxFrequency;
    }

    public void setMaxFrequency(float maxFrequency) {
        this.maxFrequency = maxFrequency;
    }

    public float getMinFrequency() {
        return minFrequency;
    }

    public void setMinFrequency(float minFrequency) {
        this.minFrequency = minFrequency;
    }

    public String getParamAmpFactor() {
        return paramAmpFactor;
    }

    public void setParamAmpFactor(String paramAmpFactor) {
        this.paramAmpFactor = paramAmpFactor;
    }

    public String getParamType() {
        return paramType;
    }

    public void setParamType(String paramType) {
        this.paramType = paramType;
    }

    public String getParamUnits() {
        return paramUnits;
    }

    public void setParamUnits(String paramUnits) {
        this.paramUnits = paramUnits;
    }

    public float getPeriod() {
        return period;
    }

    public void setPeriod(float period) {
        this.period = period;
    }

    public int getResolution() {
        return resolution;
    }

    public void setResolution(int resolution) {
        this.resolution = resolution;
    }

    public float getSampleRate() {
        return sampleRate;
    }

    public void setSampleRate(float sampleRate) {
        this.sampleRate = sampleRate;
    }

    public String getSensorType() {
        return sensorType;
    }

    public void setSensorType(String sensorType) {
        this.sensorType = sensorType;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.customerAccount);
        hash = 29 * hash + Objects.hashCode(this.apSetId);
        hash = 29 * hash + Objects.hashCode(this.alSetId);
        hash = 29 * hash + Objects.hashCode(this.apSetName);
        hash = 29 * hash + Objects.hashCode(this.alSetName);
        hash = 29 * hash + Objects.hashCode(this.paramName);
        hash = 29 * hash + Float.floatToIntBits(this.demodHighPass);
        hash = 29 * hash + Float.floatToIntBits(this.demodLowPass);
        hash = 29 * hash + this.dwell;
        hash = 29 * hash + this.fmax;
        hash = 29 * hash + Objects.hashCode(this.frequencyUnits);
        hash = 29 * hash + Float.floatToIntBits(this.highAlert);
        hash = 29 * hash + (this.highAlertActive ? 1 : 0);
        hash = 29 * hash + Float.floatToIntBits(this.highFault);
        hash = 29 * hash + (this.highFaultActive ? 1 : 0);
        hash = 29 * hash + (this.demod ? 1 : 0);
        hash = 29 * hash + Float.floatToIntBits(this.lowAlert);
        hash = 29 * hash + (this.lowAlertActive ? 1 : 0);
        hash = 29 * hash + Float.floatToIntBits(this.lowFault);
        hash = 29 * hash + (this.lowFaultActive ? 1 : 0);
        hash = 29 * hash + Float.floatToIntBits(this.maxFrequency);
        hash = 29 * hash + Float.floatToIntBits(this.minFrequency);
        hash = 29 * hash + Objects.hashCode(this.paramAmpFactor);
        hash = 29 * hash + Objects.hashCode(this.paramType);
        hash = 29 * hash + Objects.hashCode(this.paramUnits);
        hash = 29 * hash + Float.floatToIntBits(this.period);
        hash = 29 * hash + this.resolution;
        hash = 29 * hash + Float.floatToIntBits(this.sampleRate);
        hash = 29 * hash + Objects.hashCode(this.sensorType);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GlobalApAlSetsVO other = (GlobalApAlSetsVO) obj;
        if (Float.floatToIntBits(this.demodHighPass) != Float.floatToIntBits(other.demodHighPass)) {
            return false;
        }
        if (Float.floatToIntBits(this.demodLowPass) != Float.floatToIntBits(other.demodLowPass)) {
            return false;
        }
        if (this.dwell != other.dwell) {
            return false;
        }
        if (this.fmax != other.fmax) {
            return false;
        }
        if (Float.floatToIntBits(this.highAlert) != Float.floatToIntBits(other.highAlert)) {
            return false;
        }
        if (this.highAlertActive != other.highAlertActive) {
            return false;
        }
        if (Float.floatToIntBits(this.highFault) != Float.floatToIntBits(other.highFault)) {
            return false;
        }
        if (this.highFaultActive != other.highFaultActive) {
            return false;
        }
        if (this.demod != other.demod) {
            return false;
        }
        if (Float.floatToIntBits(this.lowAlert) != Float.floatToIntBits(other.lowAlert)) {
            return false;
        }
        if (this.lowAlertActive != other.lowAlertActive) {
            return false;
        }
        if (Float.floatToIntBits(this.lowFault) != Float.floatToIntBits(other.lowFault)) {
            return false;
        }
        if (this.lowFaultActive != other.lowFaultActive) {
            return false;
        }
        if (Float.floatToIntBits(this.maxFrequency) != Float.floatToIntBits(other.maxFrequency)) {
            return false;
        }
        if (Float.floatToIntBits(this.minFrequency) != Float.floatToIntBits(other.minFrequency)) {
            return false;
        }
        if (Float.floatToIntBits(this.period) != Float.floatToIntBits(other.period)) {
            return false;
        }
        if (this.resolution != other.resolution) {
            return false;
        }
        if (Float.floatToIntBits(this.sampleRate) != Float.floatToIntBits(other.sampleRate)) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.apSetName, other.apSetName)) {
            return false;
        }
        if (!Objects.equals(this.alSetName, other.alSetName)) {
            return false;
        }
        if (!Objects.equals(this.paramName, other.paramName)) {
            return false;
        }
        if (!Objects.equals(this.frequencyUnits, other.frequencyUnits)) {
            return false;
        }
        if (!Objects.equals(this.paramAmpFactor, other.paramAmpFactor)) {
            return false;
        }
        if (!Objects.equals(this.paramType, other.paramType)) {
            return false;
        }
        if (!Objects.equals(this.paramUnits, other.paramUnits)) {
            return false;
        }
        if (!Objects.equals(this.sensorType, other.sensorType)) {
            return false;
        }
        if (!Objects.equals(this.apSetId, other.apSetId)) {
            return false;
        }
        if (!Objects.equals(this.alSetId, other.alSetId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "GlobalApAlSetsVO{" + "customerAccount=" + customerAccount + ", apSetId=" + apSetId + ", alSetId=" + alSetId + ", apSetName=" + apSetName + ", alSetName=" + alSetName + ", paramName=" + paramName + ", demodHighPass=" + demodHighPass + ", demodLowPass=" + demodLowPass + ", dwell=" + dwell + ", fmax=" + fmax + ", frequencyUnits=" + frequencyUnits + ", highAlert=" + highAlert + ", highAlertActive=" + highAlertActive + ", highFault=" + highFault + ", highFaultActive=" + highFaultActive + ", demod=" + demod + ", lowAlert=" + lowAlert + ", lowAlertActive=" + lowAlertActive + ", lowFault=" + lowFault + ", lowFaultActive=" + lowFaultActive + ", maxFrequency=" + maxFrequency + ", minFrequency=" + minFrequency + ", paramAmpFactor=" + paramAmpFactor + ", paramType=" + paramType + ", paramUnits=" + paramUnits + ", period=" + period + ", resolution=" + resolution + ", sampleRate=" + sampleRate + ", sensorType=" + sensorType + '}';
    }
}
