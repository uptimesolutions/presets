/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.vo;

import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author kpati
 */
public class AssetPresetVO {

    private String customerAccount;
    private UUID siteId;
    private String assetTypeName;
    private UUID assetPresetId;
    private String pointLocationName;
    private String assetPresetName;
    private UUID devicePresetId;
    private String devicePresetType;
    private int sampleInterval;
    private String ffSetIds;
    private float rollDiameter;
    private String rollDiameterUnits;
    private float speedRatio;
    private UUID tachId;
    private String description;
    private String devicePresetName;
    private String tachName;

    public AssetPresetVO() {

    }

    public AssetPresetVO(String customerAccount, UUID siteId, String assetTypeName, UUID assetPresetId, String pointLocationName, String assetPresetName,
            UUID devicePresetId, String devicePresetType, int sampleInterval, String ffSetIds, float rollDiameter, String rollDiameterUnits,
            float speedRatio, UUID tachId, String description, String devicePresetName, String tachName) {
        
        this.customerAccount = customerAccount;
        this.siteId = siteId;
        this.assetTypeName = assetTypeName;
        this.assetPresetId = assetPresetId;
        this.pointLocationName = pointLocationName;
        this.assetPresetName = assetPresetName;
        this.devicePresetId = devicePresetId;
        this.devicePresetType = devicePresetType;
        this.sampleInterval = sampleInterval;
        this.ffSetIds = ffSetIds;
        this.rollDiameter = rollDiameter;
        this.rollDiameterUnits = rollDiameterUnits;
        this.speedRatio = speedRatio;
        this.tachId = tachId;
        this.description = description;
        this.devicePresetName = devicePresetName;
        this.tachName = tachName;

    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getAssetTypeName() {
        return assetTypeName;
    }

    public void setAssetTypeName(String assetTypeName) {
        this.assetTypeName = assetTypeName;
    }

    public UUID getAssetPresetId() {
        return assetPresetId;
    }

    public void setAssetPresetId(UUID assetPresetId) {
        this.assetPresetId = assetPresetId;
    }

    public String getPointLocationName() {
        return pointLocationName;
    }

    public void setPointLocationName(String pointLocationName) {
        this.pointLocationName = pointLocationName;
    }

    public String getAssetPresetName() {
        return assetPresetName;
    }

    public void setAssetPresetName(String assetPresetName) {
        this.assetPresetName = assetPresetName;
    }

    public UUID getDevicePresetId() {
        return devicePresetId;
    }

    public void setDevicePresetId(UUID devicePresetId) {
        this.devicePresetId = devicePresetId;
    }

    public int getSampleInterval() {
        return sampleInterval;
    }

    public void setSampleInterval(int sampleInterval) {
        this.sampleInterval = sampleInterval;
    }

    public String getFfSetIds() {
        return ffSetIds;
    }

    public void setFfSetIds(String ffSetIds) {
        this.ffSetIds = ffSetIds;
    }

    public float getRollDiameter() {
        return rollDiameter;
    }

    public void setRollDiameter(float rollDiameter) {
        this.rollDiameter = rollDiameter;
    }

    public String getRollDiameterUnits() {
        return rollDiameterUnits;
    }

    public void setRollDiameterUnits(String rollDiameterUnits) {
        this.rollDiameterUnits = rollDiameterUnits;
    }

    public float getSpeedRatio() {
        return speedRatio;
    }

    public void setSpeedRatio(float speedRatio) {
        this.speedRatio = speedRatio;
    }

    public UUID getTachId() {
        return tachId;
    }

    public void setTachId(UUID tachId) {
        this.tachId = tachId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDevicePresetName() {
        return devicePresetName;
    }

    public void setDevicePresetName(String devicePresetName) {
        this.devicePresetName = devicePresetName;
    }

    public String getTachName() {
        return tachName;
    }

    public void setTachName(String tachName) {
        this.tachName = tachName;
    }

    public String getDevicePresetType() {
        return devicePresetType;
    }

    public void setDevicePresetType(String devicePresetType) {
        this.devicePresetType = devicePresetType;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.customerAccount);
        hash = 37 * hash + Objects.hashCode(this.siteId);
        hash = 37 * hash + Objects.hashCode(this.assetTypeName);
        hash = 37 * hash + Objects.hashCode(this.assetPresetId);
        hash = 37 * hash + Objects.hashCode(this.pointLocationName);
        hash = 37 * hash + Objects.hashCode(this.assetPresetName);
        hash = 37 * hash + Objects.hashCode(this.devicePresetId);
        hash = 37 * hash + this.sampleInterval;
        hash = 37 * hash + Objects.hashCode(this.ffSetIds);
        hash = 37 * hash + Float.floatToIntBits(this.rollDiameter);
        hash = 37 * hash + Objects.hashCode(this.rollDiameterUnits);
        hash = 37 * hash + Float.floatToIntBits(this.speedRatio);
        hash = 37 * hash + Objects.hashCode(this.tachId);
        hash = 37 * hash + Objects.hashCode(this.description);
        hash = 37 * hash + Objects.hashCode(this.devicePresetName);
        hash = 37 * hash + Objects.hashCode(this.tachName);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AssetPresetVO other = (AssetPresetVO) obj;
        if (this.sampleInterval != other.sampleInterval) {
            return false;
        }
        if (Float.floatToIntBits(this.rollDiameter) != Float.floatToIntBits(other.rollDiameter)) {
            return false;
        }
        if (Float.floatToIntBits(this.speedRatio) != Float.floatToIntBits(other.speedRatio)) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.assetTypeName, other.assetTypeName)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationName, other.pointLocationName)) {
            return false;
        }
        if (!Objects.equals(this.assetPresetName, other.assetPresetName)) {
            return false;
        }
        if (!Objects.equals(this.ffSetIds, other.ffSetIds)) {
            return false;
        }
        if (!Objects.equals(this.rollDiameterUnits, other.rollDiameterUnits)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.devicePresetName, other.devicePresetName)) {
            return false;
        }
        if (!Objects.equals(this.tachName, other.tachName)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.assetPresetId, other.assetPresetId)) {
            return false;
        }
        if (!Objects.equals(this.devicePresetId, other.devicePresetId)) {
            return false;
        }
        return Objects.equals(this.tachId, other.tachId);
    }

    @Override
    public String toString() {
        return "AssetPresetVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", assetTypeName=" + assetTypeName + ", assetPresetId=" + assetPresetId + ", pointLocationName=" + pointLocationName + ", assetPresetName=" + assetPresetName + ", devicePresetId=" + devicePresetId + ", sampleInterval=" + sampleInterval + ", ffSetIds=" + ffSetIds + ", rollDiameter=" + rollDiameter + ", rollDiameterUnits=" + rollDiameterUnits + ", speedRatio=" + speedRatio + ", tachId=" + tachId + ", description=" + description + ", devicePresetName=" + devicePresetName + ", tachName=" + tachName + ", devicePresetType=" + devicePresetType + '}';
    }

}
