/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.vo;

import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author madhavi
 */

public class SiteBearingFavoritesVO {

    String customerAcct;
    private UUID siteId;
    private String vendorId;
    private String bearingId;
    private float ftf;
    private float bsf;
    private float bpfo;
    private float bpfi;
    

    public SiteBearingFavoritesVO() {
    }

    public SiteBearingFavoritesVO(SiteBearingFavoritesVO entity) {
        customerAcct = entity.getCustomerAcct();
        siteId = entity.getSiteId();
        vendorId = entity.getVendorId();
        bearingId = entity.getBearingId();
        ftf = entity.getFtf();
        bsf = entity.getBsf();
        bpfo = entity.getBpfo();
        bpfi = entity.getBpfi();
    }

    public String getCustomerAcct() {
        return customerAcct;
    }

    public void setCustomerAcct(String customerAcct) {
        this.customerAcct = customerAcct;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getBearingId() {
        return bearingId;
    }

    public void setBearingId(String bearingId) {
        this.bearingId = bearingId;
    }

    public float getFtf() {
        return ftf;
    }

    public void setFtf(float ftf) {
        this.ftf = ftf;
    }

    public float getBsf() {
        return bsf;
    }

    public void setBsf(float bsf) {
        this.bsf = bsf;
    }

    public float getBpfo() {
        return bpfo;
    }

    public void setBpfo(float bpfo) {
        this.bpfo = bpfo;
    }

    public float getBpfi() {
        return bpfi;
    }

    public void setBpfi(float bpfi) {
        this.bpfi = bpfi;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.customerAcct);
        hash = 17 * hash + Objects.hashCode(this.siteId);
        hash = 17 * hash + Objects.hashCode(this.vendorId);
        hash = 17 * hash + Objects.hashCode(this.bearingId);
        hash = 17 * hash + Float.floatToIntBits(this.ftf);
        hash = 17 * hash + Float.floatToIntBits(this.bsf);
        hash = 17 * hash + Float.floatToIntBits(this.bpfo);
        hash = 17 * hash + Float.floatToIntBits(this.bpfi);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SiteBearingFavoritesVO other = (SiteBearingFavoritesVO) obj;
        if (Float.floatToIntBits(this.ftf) != Float.floatToIntBits(other.ftf)) {
            return false;
        }
        if (Float.floatToIntBits(this.bsf) != Float.floatToIntBits(other.bsf)) {
            return false;
        }
        if (Float.floatToIntBits(this.bpfo) != Float.floatToIntBits(other.bpfo)) {
            return false;
        }
        if (Float.floatToIntBits(this.bpfi) != Float.floatToIntBits(other.bpfi)) {
            return false;
        }
        if (!Objects.equals(this.customerAcct, other.customerAcct)) {
            return false;
        }
        if (!Objects.equals(this.vendorId, other.vendorId)) {
            return false;
        }
        if (!Objects.equals(this.bearingId, other.bearingId)) {
            return false;
        }
        return Objects.equals(this.siteId, other.siteId);
    }

    @Override
    public String toString() {
        return "SiteBearingFavoritesVO{" + "customerAcct=" + customerAcct + ", siteId=" + siteId + ", vendorId=" + vendorId + ", bearingId=" + bearingId + ", ftf=" + ftf + ", bsf=" + bsf + ", bpfo=" + bpfo + ", bpfi=" + bpfi + '}';
    }

}
