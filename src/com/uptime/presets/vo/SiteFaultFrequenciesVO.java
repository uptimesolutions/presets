/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.vo;

import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author Joseph
 */
public class SiteFaultFrequenciesVO {
    private String customerAccount;    
    private UUID siteId;
    private UUID ffSetId;
    private UUID ffId;
    private String siteName;
    private String ffSetName;
    private String ffName;
    private String ffSetDesc;
    private float ffValue;
    private String ffUnit;

    public SiteFaultFrequenciesVO() {
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getFfSetId() {
        return ffSetId;
    }

    public void setFfSetId(UUID ffSetId) {
        this.ffSetId = ffSetId;
    }

    public UUID getFfId() {
        return ffId;
    }

    public void setFfId(UUID ffId) {
        this.ffId = ffId;
    }

    public String getFfSetName() {
        return ffSetName;
    }

    public void setFfSetName(String ffSetName) {
        this.ffSetName = ffSetName;
    }

    public String getFfName() {
        return ffName;
    }

    public void setFfName(String ffName) {
        this.ffName = ffName;
    }

    public String getFfSetDesc() {
        return ffSetDesc;
    }

    public void setFfSetDesc(String ffSetDesc) {
        this.ffSetDesc = ffSetDesc;
    }

    public float getFfValue() {
        return ffValue;
    }

    public void setFfValue(float ffValue) {
        this.ffValue = ffValue;
    }

    public String getFfUnit() {
        return ffUnit;
    }

    public void setFfUnit(String ffUnit) {
        this.ffUnit = ffUnit;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 13 * hash + Objects.hashCode(this.customerAccount);
        hash = 13 * hash + Objects.hashCode(this.siteId);
        hash = 13 * hash + Objects.hashCode(this.ffSetId);
        hash = 13 * hash + Objects.hashCode(this.ffId);
        hash = 13 * hash + Objects.hashCode(this.siteName);
        hash = 13 * hash + Objects.hashCode(this.ffSetName);
        hash = 13 * hash + Objects.hashCode(this.ffName);
        hash = 13 * hash + Objects.hashCode(this.ffSetDesc);
        hash = 13 * hash + Float.floatToIntBits(this.ffValue);
        hash = 13 * hash + Objects.hashCode(this.ffUnit);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SiteFaultFrequenciesVO other = (SiteFaultFrequenciesVO) obj;
        if (Float.floatToIntBits(this.ffValue) != Float.floatToIntBits(other.ffValue)) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.siteName, other.siteName)) {
            return false;
        }
        if (!Objects.equals(this.ffSetName, other.ffSetName)) {
            return false;
        }
        if (!Objects.equals(this.ffName, other.ffName)) {
            return false;
        }
        if (!Objects.equals(this.ffSetDesc, other.ffSetDesc)) {
            return false;
        }
        if (!Objects.equals(this.ffUnit, other.ffUnit)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.ffSetId, other.ffSetId)) {
            return false;
        }
        if (!Objects.equals(this.ffId, other.ffId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SiteFaultFrequenciesVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", ffSetId=" + ffSetId + ", ffId=" + ffId + ", siteName=" + siteName + ", ffSetName=" + ffSetName + ", ffName=" + ffName + ", ffSetDesc=" + ffSetDesc + ", ffValue=" + ffValue + ", ffUnit=" + ffUnit + '}';
    }
}
