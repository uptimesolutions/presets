/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.vo;

import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author Joseph
 */
public class TachometersVO {
    private String customerAccount;
    private UUID tachId;
    private UUID siteId;
    private String tachName;
    private float tachReferenceSpeed;
    private String tachType;
    private String tachReferenceUnits;

    public TachometersVO() {
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getTachId() {
        return tachId;
    }

    public void setTachId(UUID tachId) {
        this.tachId = tachId;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getTachName() {
        return tachName;
    }

    public void setTachName(String tachName) {
        this.tachName = tachName;
    }

    public float getTachReferenceSpeed() {
        return tachReferenceSpeed;
    }

    public void setTachReferenceSpeed(float tachReferenceSpeed) {
        this.tachReferenceSpeed = tachReferenceSpeed;
    }

    public String getTachReferenceUnits() {
        return tachReferenceUnits;
    }

    public void setTachReferenceUnits(String tachReferenceUnits) {
        this.tachReferenceUnits = tachReferenceUnits;
    }

   
    
  
    public String getTachType() {
        return tachType;
    }

    public void setTachType(String tachType) {
        this.tachType = tachType;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + Objects.hashCode(this.customerAccount);
        hash = 31 * hash + Objects.hashCode(this.tachId);
        hash = 31 * hash + Objects.hashCode(this.siteId);
        hash = 31 * hash + Objects.hashCode(this.tachName);
        hash = 31 * hash + Float.floatToIntBits(this.tachReferenceSpeed);
        hash = 31 * hash + Objects.hashCode(this.tachReferenceUnits);
        hash = 31 * hash + Objects.hashCode(this.tachType);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TachometersVO other = (TachometersVO) obj;
        if (Float.floatToIntBits(this.tachReferenceSpeed) != Float.floatToIntBits(other.tachReferenceSpeed)) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.tachName, other.tachName)) {
            return false;
        }
        if (!Objects.equals(this.tachReferenceUnits, other.tachReferenceUnits)) {
            return false;
        }
        if (!Objects.equals(this.tachType, other.tachType)) {
            return false;
        }
        if (!Objects.equals(this.tachId, other.tachId)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TachometersVO{" + "customerAccount=" + customerAccount + ", tachId=" + tachId + ", siteId=" + siteId + ", tachName=" + tachName + ", tachReferenceSpeed=" + tachReferenceSpeed + ", tachType=" + tachType + ", tachReferenceUnits=" + tachReferenceUnits + '}';
    }

}
