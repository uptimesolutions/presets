/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.presets.controller;

import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author Joseph
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CustomerApSetControllerTest {
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//presets.cql", true, true, "worldview_dev1"));    
    private static String content = "";
    private static CustomerApSetController instance;
    private static final String CUSTOMER_ACCOUNT = "\"77777\"";
    private static final String AP_SET_ID = "\"ca610979-890b-4ee8-84e2-0e240933ae21\"";
    private static final String CUSTOMER_ACCOUNT_1 = "77777";
    private static final String AP_SET_ID_1 = "ca610979-890b-4ee8-84e2-0e240933ae21";
    public CustomerApSetControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = new CustomerApSetController();
        content = "{\n"
                + "\"customerAccount\":%s ,\n"
                + "\"apSetName\":\"apSetName Test 1\",\n"
                + "\"apSetId\":%s,\n"
                + "\"fmax\":2,\n"
                + "\"period\":5,\n"
                + "\"resolution\":5,\n"
                + "\"sampleRate\":3,\n"
                + "\"sensorType\":\"sensor 1\",\n"
                + "\"siteId\":\"e24f8863-7e12-44c0-95b5-1ca33b0785c6\"\n"
                + "}";
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getByPK method, of class CustomerApSetController.
     * @throws java.lang.Exception
     */
    @Test
    public void test20_FindByPK() throws Exception {
        System.out.println("findByPK");
        String expResult = "{\"customerAccount\":\"77777\",\"apSetId\":\"ca610979-890b-4ee8-84e2-0e240933ae21\",\"CustomerApSet\":[{\"customerAccount\":\"77777\",\"apSetId\":\"ca610979-890b-4ee8-84e2-0e240933ae21\",\"apSetName\":\"apSetName Test 1\",\"fmax\":2,\"period\":5.0,\"resolution\":5,\"sampleRate\":3.0,\"sensorType\":\"sensor 1\",\"siteId\":\"e24f8863-7e12-44c0-95b5-1ca33b0785c6\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByPK(CUSTOMER_ACCOUNT_1, AP_SET_ID_1);
        assertEquals(expResult, result);
    }
    /**
     * Test of getByPK method, of class CustomerApSetController.
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test21_FindByPK() throws Exception {
        System.out.println("findByPK");
        fail(instance.getByPK(null, AP_SET_ID_1));
    }
    /**
     * Test of getByPK method, of class CustomerApSetController.
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test22_FindByPK() throws Exception {
        System.out.println("findByPK");
        fail(instance.getByPK(CUSTOMER_ACCOUNT_1, null));
    }

    /**
     * Test of getByCustomer method, of class CustomerApSetController.
     * @throws java.lang.Exception
     */
    @Test
    public void test30_FindByCustomer() throws Exception {
        System.out.println("findByCustomer");
        String expResult = "{\"customerAccount\":\"77777\",\"CustomerApSet\":[{\"customerAccount\":\"77777\",\"apSetId\":\"ca610979-890b-4ee8-84e2-0e240933ae21\",\"apSetName\":\"apSetName Test 1\",\"fmax\":2,\"period\":5.0,\"resolution\":5,\"sampleRate\":3.0,\"sensorType\":\"sensor 1\",\"siteId\":\"e24f8863-7e12-44c0-95b5-1ca33b0785c6\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByCustomer(CUSTOMER_ACCOUNT_1);
        System.out.println("result : " + result);
        assertEquals(expResult, result);
    }
    /**
     * Test of getByCustomer method, of class CustomerApSetController.
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test31_FindByCustomer() throws Exception {
        System.out.println("findByCustomer");
        fail(instance.getByCustomer(null));
    }

    /**
     * Test of createCustomerApSet method, of class CustomerApSetController.
     */
    @Test
    public void test10_CreateCustomerApSet() {
        System.out.println("createCustomerApSet");
        String expResult = "{\"outcome\":\"New Customer ApSet created successfully.\"}";
        String result = instance.createCustomerApSet(String.format(content, CUSTOMER_ACCOUNT, AP_SET_ID));
        assertEquals(expResult, result);
    }
    /**
     * Test of createCustomerApSet method, of class CustomerApSetController.
     */
    @Test
    public void test11_CreateCustomerApSet() {
        System.out.println("createCustomerApSet");
        String expResult = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        String result = instance.createCustomerApSet(String.format(content, null, AP_SET_ID));
        assertEquals(expResult, result);
    }

    /**
     * Test of updateCustomerApSet method, of class CustomerApSetController.
     */
    @Test
    public void test40_UpdateCustomerApSet() {
        System.out.println("updateCustomerApSet");
        String expResult = "{\"outcome\":\"Updated Customer ApSet successfully.\"}";
        String result = instance.updateCustomerApSet(String.format(content, CUSTOMER_ACCOUNT, AP_SET_ID));
        assertEquals(expResult, result);
    }
    /**
     * Test of updateCustomerApSet method, of class CustomerApSetController.
     */
    @Test
    public void test41_UpdateCustomerApSet() {
        System.out.println("updateCustomerApSet");
        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.updateCustomerApSet(String.format(content, null, AP_SET_ID));
        assertEquals(expResult, result);
    }
    /**
     * Test of updateCustomerApSet method, of class CustomerApSetController.
     */
    @Test
    public void test42_UpdateCustomerApSet() {
        System.out.println("updateCustomerApSet");
        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.updateCustomerApSet(String.format(content, CUSTOMER_ACCOUNT, null));
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteCustomerApSet method, of class CustomerApSetController.
     */
    @Test
    public void test50_DeleteCustomerApSet() {
        System.out.println("deleteCustomerApSet");        
        String expResult = "{\"outcome\":\"Deleted Customer ApSet successfully.\"}";
        String result = instance.deleteCustomerApSet(String.format(content, CUSTOMER_ACCOUNT, AP_SET_ID));
        assertEquals(expResult, result);
    }
    /**
     * Test of deleteCustomerApSet method, of class CustomerApSetController.
     */
    @Test
    public void test51_DeleteCustomerApSet() {
        System.out.println("deleteCustomerApSet");        
        String expResult = "{\"outcome\":\"Insufficient data given in json\"}";
        String result = instance.deleteCustomerApSet(String.format(content, null, AP_SET_ID));
        assertEquals(expResult, result);
    }
    /**
     * Test of deleteCustomerApSet method, of class CustomerApSetController.
     */
    @Test
    public void test52_DeleteCustomerApSet() {
        System.out.println("deleteCustomerApSet");        
        String expResult = "{\"outcome\":\"Insufficient data given in json\"}";
        String result = instance.deleteCustomerApSet(String.format(content, CUSTOMER_ACCOUNT, null));
        assertEquals(expResult, result);
    }
    
}
