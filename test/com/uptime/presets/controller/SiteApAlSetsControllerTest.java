/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.presets.controller;

import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author Joseph
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SiteApAlSetsControllerTest {

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//presets.cql", true, true, "worldview_dev1"));
    private static SiteApAlSetsController instance;
    private static String content;
    private static final String CUSTOMER_ACCOUNT = "\"77777\"";
    private static final String SITE_ID = "\"6102feaa-98e4-417b-a6b6-939af94e8194\"";
    private static final String AL_SET_ID = "\"bad01d96-c2a1-40fc-87b3-d17a8b64f067\"";
    private static final String AP_SET_ID = "\"ca610979-890b-4ee8-84e2-0e240933ae21\"";
    private static final String AP_SET_ID_2 = "\"bad01d96-c2a1-40fc-87b3-d17a8b64f067\"";
    private static final String AP_SET_NAME = "\"apSetName Test 1\"";
    private static final String AL_SET_NAME = "\"alSetName Test 1\"";
    private static final String SENSOR_TYPE = "\"sensorTest Type 1\"";
    private static final String PARAM_NAME = "\"paramName Test 1\"";
    private static final String CUSTOMER_ACCOUNT_1 = "77777";
    private static final String SITE_ID_1 = "6102feaa-98e4-417b-a6b6-939af94e8194";
    private static final String AL_SET_ID_1 = "bad01d96-c2a1-40fc-87b3-d17a8b64f067";
    private static final String AP_SET_ID_1 = "ca610979-890b-4ee8-84e2-0e240933ae21";
    private static final String AP_SET_ID_2_1 = "bad01d96-c2a1-40fc-87b3-d17a8b64f067";
    private static final String PARAM_NAME_1 = "paramName Test 1";

    public SiteApAlSetsControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        instance = new SiteApAlSetsController();
        content = "{\n"
                + "\"customerAccount\": %s,\n"
                + "\"siteId\":%s,\n"
                + "\"alSetId\":%s,\n"
                + "\"apSetId\":%s,\n"
                + "\"apSetName\":%s, \n"
                + "\"alSetName\":%s,\n"
                + "\"paramName\":%s,\n"
                + "\"demodHighPass\":9,\n"
                + "\"demodLowPass\":2, \n"
                + "\"dwell\":5, \n"
                + "\"fmax\":8,\n"
                + "\"frequencyUnits\":\"frequency Test Unit 1\",\n"
                + "\"highAlert\":6,\n"
                + "\"highAlertActive\":true,\n"
                + "\"highFault\":6,\n"
                + "\"highFaultActive\":true,\n"
                + "\"demod\":true,\n"
                + "\"lowAlert\":2,\n"
                + "\"lowAlertActive\":true,\n"
                + "\"lowFault\":1,\n"
                + "\"lowFaultActive\":true,\n"
                + "\"maxFrequency\":10,\n"
                + "\"minFrequency\":1,\n"
                + "\"paramAmpFactor\":\"paramAmp Test Factor 1\", \n"
                + "\"paramType\":\"param Test Type 1\", \n"
                + "\"paramUnits\":\"param Test Units 1\",\n"
                + "\"period\":5,\n"
                + "\"resolution\":5,\n"
                + "\"sampleRate\":5,\n"
                + "\"sensorType\":%s\n"
                + "}";
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of createSiteApAlSets method, of class SiteApAlSetsController.
     */
    @Test
    public void test10_CreateSiteApAlSets() {
        System.out.println("createSiteApAlSets");
        String expResult = "{\"outcome\":\"New Site ApAlSets created successfully.\"}";
        System.out.println("createSiteApAlSets content -" + String.format(content, CUSTOMER_ACCOUNT, SITE_ID, AL_SET_ID, AP_SET_ID, AP_SET_NAME, AL_SET_NAME, PARAM_NAME, SENSOR_TYPE));
        String result = instance.createSiteApAlSets(String.format(content, CUSTOMER_ACCOUNT, SITE_ID, AL_SET_ID, AP_SET_ID, AP_SET_NAME, AL_SET_NAME, PARAM_NAME, SENSOR_TYPE));
        assertEquals(expResult, result);
    }

    /**
     * Test of createSiteApAlSets method, of class SiteApAlSetsController.
     */
    @Test
    public void test11_CreateSiteApAlSets() {
        System.out.println("createSiteApAlSets");
        String expResult = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        String result = instance.createSiteApAlSets(String.format(content, null, SITE_ID, AL_SET_ID, AP_SET_ID, AP_SET_NAME, AL_SET_NAME, PARAM_NAME, SENSOR_TYPE));
        assertEquals(expResult, result);
    }

    /**
     * Test of createSiteApAlSets method, of class SiteApAlSetsController.
     */
    @Test
    public void test12_CreateSiteApAlSets() {
        System.out.println("createSiteApAlSets");
        String expResult = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        String result = instance.createSiteApAlSets(String.format(content, CUSTOMER_ACCOUNT, null, AL_SET_ID, AP_SET_ID, AP_SET_NAME, AL_SET_NAME, PARAM_NAME, SENSOR_TYPE));
        assertEquals(expResult, result);
    }

    /**
     * Test of createSiteApAlSets method, of class SiteApAlSetsController.
     */
    @Test
    public void test13_CreateSiteApAlSets() {
        System.out.println("createSiteApAlSets");
        String expResult = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        String result = instance.createSiteApAlSets(String.format(content, CUSTOMER_ACCOUNT, SITE_ID, AL_SET_ID, AP_SET_ID, null, AL_SET_NAME, PARAM_NAME, SENSOR_TYPE));
        assertEquals(expResult, result);
    }

    /**
     * Test of createSiteApAlSets method, of class SiteApAlSetsController.
     */
    @Test
    public void test14_CreateSiteApAlSets() {
        System.out.println("createSiteApAlSets");
        String expResult = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        String result = instance.createSiteApAlSets(String.format(content, CUSTOMER_ACCOUNT, SITE_ID, AL_SET_ID, AP_SET_ID, AP_SET_NAME, null, PARAM_NAME, SENSOR_TYPE));
        assertEquals(expResult, result);
    }

    /**
     * Test of createSiteApAlSets method, of class SiteApAlSetsController.
     */
    @Test
    public void test15_CreateSiteApAlSets() {
        System.out.println("createSiteApAlSets");
        String expResult = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        String result = instance.createSiteApAlSets(String.format(content, CUSTOMER_ACCOUNT, SITE_ID, AL_SET_ID, AP_SET_ID, AP_SET_NAME, AL_SET_NAME, null, SENSOR_TYPE));
        assertEquals(expResult, result);
    }

    /**
     * Test of updateSiteApAlSets method, of class SiteApAlSetsController.
     */
    @Test
    public void test50_UpdateSiteApAlSets() {
        System.out.println("updateSiteApAlSets");
        String expResult = "{\"outcome\":\"Updated Site ApAlSets successfully.\"}";
        String result = instance.updateSiteApAlSets(String.format(content, CUSTOMER_ACCOUNT, SITE_ID, AL_SET_ID, AP_SET_ID, AP_SET_NAME, AL_SET_NAME, PARAM_NAME, SENSOR_TYPE));
        assertEquals(expResult, result);
    }

    /**
     * Test of updateSiteApAlSets method, of class SiteApAlSetsController.
     */
    @Test
    public void test51_UpdateSiteApAlSets() {
        System.out.println("updateSiteApAlSets");
        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.updateSiteApAlSets(String.format(content, null, SITE_ID, AL_SET_ID, AP_SET_ID, AP_SET_NAME, AL_SET_NAME, PARAM_NAME, SENSOR_TYPE));
        assertEquals(expResult, result);
    }

    /**
     * Test of updateSiteApAlSets method, of class SiteApAlSetsController.
     */
    @Test
    public void test52_UpdateSiteApAlSets() {
        System.out.println("updateSiteApAlSets");
        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.updateSiteApAlSets(String.format(content, CUSTOMER_ACCOUNT, null, AL_SET_ID, AP_SET_ID, AP_SET_NAME, AL_SET_NAME, PARAM_NAME, SENSOR_TYPE));
        assertEquals(expResult, result);
    }

    /**
     * Test of updateSiteApAlSets method, of class SiteApAlSetsController.
     */
    @Test
    public void test53_UpdateSiteApAlSets() {
        System.out.println("updateSiteApAlSets");
        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.updateSiteApAlSets(String.format(content, CUSTOMER_ACCOUNT, SITE_ID, null, AP_SET_ID, AP_SET_NAME, AL_SET_NAME, PARAM_NAME, SENSOR_TYPE));
        assertEquals(expResult, result);
    }

    /**
     * Test of updateSiteApAlSets method, of class SiteApAlSetsController.
     */
    @Test
    public void test54_UpdateSiteApAlSets() {
        System.out.println("updateSiteApAlSets");
        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.updateSiteApAlSets(String.format(content, CUSTOMER_ACCOUNT, SITE_ID, AL_SET_ID, null, AP_SET_NAME, AL_SET_NAME, PARAM_NAME, SENSOR_TYPE));
        assertEquals(expResult, result);
    }

    /**
     * Test of updateSiteApAlSets method, of class SiteApAlSetsController.
     */
    @Test
    public void test55_UpdateSiteApAlSets() {
        System.out.println("updateSiteApAlSets");
        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.updateSiteApAlSets(String.format(content, CUSTOMER_ACCOUNT, SITE_ID, AL_SET_ID, AP_SET_ID, AP_SET_NAME, AL_SET_NAME, null, SENSOR_TYPE));
        assertEquals(expResult, result);
    }

    /**
     * Test of updateSiteApAlSets method, of class SiteApAlSetsController.
     */
    @Test
    public void test56_UpdateSiteApAlSets() {
        System.out.println("updateSiteApAlSets");
        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.updateSiteApAlSets("{}");
        assertEquals(expResult, result);
    }

    /**
     * Test of getByPK method, of class SiteApAlSetsController.
     * @throws java.lang.Exception
     */
    @Test
    public void test20_FindByPK() throws Exception {
        System.out.println("findByPK");
        String expResult = "{\"customerAccount\":\"77777\",\"siteId\":\"6102feaa-98e4-417b-a6b6-939af94e8194\",\"apSetId\":\"ca610979-890b-4ee8-84e2-0e240933ae21\",\"alSetId\":\"bad01d96-c2a1-40fc-87b3-d17a8b64f067\",\"paramName\":\"paramName Test 1\",\"SiteApAlSets\":[{\"customerAccount\":\"77777\",\"siteId\":\"6102feaa-98e4-417b-a6b6-939af94e8194\",\"apSetId\":\"ca610979-890b-4ee8-84e2-0e240933ae21\",\"alSetId\":\"bad01d96-c2a1-40fc-87b3-d17a8b64f067\",\"paramName\":\"paramName Test 1\",\"apSetName\":\"apSetName Test 1\",\"alSetName\":\"alSetName Test 1\",\"demodHighPass\":9.0,\"demodLowPass\":2.0,\"dwell\":5,\"fmax\":8,\"frequencyUnits\":\"frequency Test Unit 1\",\"highAlert\":6.0,\"highAlertActive\":true,\"highFault\":6.0,\"highFaultActive\":true,\"demod\":true,\"lowAlert\":2.0,\"lowAlertActive\":true,\"lowFault\":1.0,\"lowFaultActive\":true,\"maxFrequency\":10.0,\"minFrequency\":1.0,\"paramAmpFactor\":\"paramAmp Test Factor 1\",\"paramType\":\"param Test Type 1\",\"paramUnits\":\"param Test Units 1\",\"period\":5.0,\"resolution\":5,\"sampleRate\":5.0,\"sensorType\":\"sensorTest Type 1\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByPK(CUSTOMER_ACCOUNT_1, SITE_ID_1, AP_SET_ID_1, AL_SET_ID_1, PARAM_NAME_1);
        System.out.println("findByPK result -" + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByPK method, of class SiteApAlSetsController.
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test21_FindByPK() throws Exception {
        System.out.println("findByPK");
        fail(instance.getByPK(null, SITE_ID_1, AP_SET_ID_1, AL_SET_ID_1, PARAM_NAME_1));
    }

    /**
     * Test of getByPK method, of class SiteApAlSetsController.
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test22_FindByPK() throws Exception {
        System.out.println("findByPK");
        fail(instance.getByPK(CUSTOMER_ACCOUNT_1, null, AP_SET_ID_1, AL_SET_ID_1, PARAM_NAME_1));
    }

    /**
     * Test of getByPK method, of class SiteApAlSetsController.
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test23_FindByPK() throws Exception {
        System.out.println("findByPK");
        fail(instance.getByPK(CUSTOMER_ACCOUNT_1, SITE_ID_1, null, AL_SET_ID_1, PARAM_NAME_1));
    }

    /**
     * Test of getByPK method, of class SiteApAlSetsController.
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test24_FindByPK() throws Exception {
        System.out.println("findByPK");
        fail(instance.getByPK(CUSTOMER_ACCOUNT_1, SITE_ID_1, AP_SET_ID_1, null, PARAM_NAME_1));
    }

    /**
     * Test of getByPK method, of class SiteApAlSetsController.
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test25_FindByPK() throws Exception {
        System.out.println("findByPK");
        fail(instance.getByPK(CUSTOMER_ACCOUNT_1, SITE_ID_1, AP_SET_ID_1, AL_SET_ID_1, null));
    }

    /**
     * Test of getByCustomerSiteApSetAlSet method, of class
     * SiteApAlSetsController.
     * @throws java.lang.Exception
     */
    @Test
    public void test30_FindByCustomerSiteApSetAlSet() throws Exception {
        System.out.println("findByCustomerSiteApSetAlSet");
        //fail(instance.getByCustomerSiteApSetAlSet(customerAccount1, siteId1, apSetId1, alSetId1));
        String expResult = "{\"customerAccount\":\"77777\",\"siteId\":\"6102feaa-98e4-417b-a6b6-939af94e8194\",\"apSetId\":\"ca610979-890b-4ee8-84e2-0e240933ae21\",\"alSetId\":\"bad01d96-c2a1-40fc-87b3-d17a8b64f067\",\"SiteApAlSets\":[{\"customerAccount\":\"77777\",\"siteId\":\"6102feaa-98e4-417b-a6b6-939af94e8194\",\"apSetId\":\"ca610979-890b-4ee8-84e2-0e240933ae21\",\"alSetId\":\"bad01d96-c2a1-40fc-87b3-d17a8b64f067\",\"paramName\":\"paramName Test 1\",\"apSetName\":\"apSetName Test 1\",\"alSetName\":\"alSetName Test 1\",\"demodHighPass\":9.0,\"demodLowPass\":2.0,\"dwell\":5,\"fmax\":8,\"frequencyUnits\":\"frequency Test Unit 1\",\"highAlert\":6.0,\"highAlertActive\":true,\"highFault\":6.0,\"highFaultActive\":true,\"demod\":true,\"lowAlert\":2.0,\"lowAlertActive\":true,\"lowFault\":1.0,\"lowFaultActive\":true,\"maxFrequency\":10.0,\"minFrequency\":1.0,\"paramAmpFactor\":\"paramAmp Test Factor 1\",\"paramType\":\"param Test Type 1\",\"paramUnits\":\"param Test Units 1\",\"period\":5.0,\"resolution\":5,\"sampleRate\":5.0,\"sensorType\":\"sensorTest Type 1\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByCustomerSiteApSetAlSet(CUSTOMER_ACCOUNT_1, SITE_ID_1, AP_SET_ID_1, AL_SET_ID_1);
        System.out.println("findByCustomerSiteApSetAlSet result 30 -" + result);
        assertEquals(expResult, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test31_FindByCustomerSiteApSetAlSet() throws Exception {
        System.out.println("findByCustomerSiteApSetAlSet");
       fail(instance.getByCustomerSiteApSetAlSet(null, SITE_ID_1, AP_SET_ID_1, AL_SET_ID_1));
    }

    @Test(expected = NullPointerException.class)
    public void test32_FindByCustomerSiteApSetAlSet() throws Exception {
        System.out.println("findByCustomerSiteApSetAlSet");
        fail(instance.getByCustomerSiteApSetAlSet(CUSTOMER_ACCOUNT_1, null, AP_SET_ID_1, AL_SET_ID_1));
    }

    @Test(expected = NullPointerException.class)
    public void test33_FindByCustomerSiteApSetAlSet() throws Exception {
        System.out.println("findByCustomerSiteApSetAlSet");
        fail(instance.getByCustomerSiteApSetAlSet(CUSTOMER_ACCOUNT_1, SITE_ID_1, null, AL_SET_ID_1));
    }

    @Test(expected = NullPointerException.class)
    public void test34_FindByCustomerSiteApSetAlSet() throws Exception {
        System.out.println("findByCustomerSiteApSetAlSet 34");
        fail(instance.getByCustomerSiteApSetAlSet(CUSTOMER_ACCOUNT_1, SITE_ID_1, AP_SET_ID_1, null));
    }

    /**
     * Test of getByCustomerSiteApSet method, of class SiteApAlSetsController.
     * @throws java.lang.Exception
     */
    @Test
    public void test40_FindByCustomerSiteApSet() throws Exception {
        System.out.println("findByCustomerSiteApSet 40");
        String expResult = "{\"customerAccount\":\"77777\",\"siteId\":\"6102feaa-98e4-417b-a6b6-939af94e8194\",\"apSetId\":\"ca610979-890b-4ee8-84e2-0e240933ae21\",\"SiteApAlSets\":[{\"customerAccount\":\"77777\",\"siteId\":\"6102feaa-98e4-417b-a6b6-939af94e8194\",\"apSetId\":\"ca610979-890b-4ee8-84e2-0e240933ae21\",\"alSetId\":\"bad01d96-c2a1-40fc-87b3-d17a8b64f067\",\"paramName\":\"paramName Test 1\",\"apSetName\":\"apSetName Test 1\",\"alSetName\":\"alSetName Test 1\",\"demodHighPass\":9.0,\"demodLowPass\":2.0,\"dwell\":5,\"fmax\":8,\"frequencyUnits\":\"frequency Test Unit 1\",\"highAlert\":6.0,\"highAlertActive\":true,\"highFault\":6.0,\"highFaultActive\":true,\"demod\":true,\"lowAlert\":2.0,\"lowAlertActive\":true,\"lowFault\":1.0,\"lowFaultActive\":true,\"maxFrequency\":10.0,\"minFrequency\":1.0,\"paramAmpFactor\":\"paramAmp Test Factor 1\",\"paramType\":\"param Test Type 1\",\"paramUnits\":\"param Test Units 1\",\"period\":5.0,\"resolution\":5,\"sampleRate\":5.0,\"sensorType\":\"sensorTest Type 1\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByCustomerSiteApSet(CUSTOMER_ACCOUNT_1, SITE_ID_1, AP_SET_ID_1);
        System.out.println("getByCustomerSiteApSet result 40 -" + result);
        assertEquals(expResult, result);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void test41_FindByCustomerSiteApSet() throws Exception {
        System.out.println("findByCustomerSiteApSet");
        fail(instance.getByCustomerSiteApSet(null, SITE_ID_1, AP_SET_ID_1));
    }
    
    @Test(expected = NullPointerException.class)
    public void test42_FindByCustomerSiteApSet() throws Exception {
        System.out.println("findByCustomerSiteApSet");
        fail(instance.getByCustomerSiteApSet(CUSTOMER_ACCOUNT_1, null, AP_SET_ID_1));
    }
    
    @Test(expected = NullPointerException.class)
    public void test43_FindByCustomerSiteApSet() throws Exception {
        System.out.println("findByCustomerSiteApSet");
        fail(instance.getByCustomerSiteApSet(CUSTOMER_ACCOUNT_1, SITE_ID_1, null));
    }
    
    /**
     * Test of getByCustomerSiteApSet method, of class SiteApAlSetsController.
     * @throws java.lang.Exception
     */
    @Test
    public void test50_FindByCustomerSiteApSets() throws Exception {
        content = "{\n"
                + "\"customerAccount\": %s,\n"
                + "\"siteId\":%s,\n"
                + "\"alSetId\":%s,\n"
                + "\"apSetId\":%s,\n"
                + "\"apSetName\":%s, \n"
                + "\"alSetName\":%s,\n"
                + "\"paramName\":%s,\n"
                + "\"demodHighPass\":9,\n"
                + "\"demodLowPass\":2, \n"
                + "\"dwell\":5, \n"
                + "\"fmax\":8,\n"
                + "\"frequencyUnits\":\"frequency Test Unit 1\",\n"
                + "\"highAlert\":6,\n"
                + "\"highAlertActive\":true,\n"
                + "\"highFault\":6,\n"
                + "\"highFaultActive\":true,\n"
                + "\"demod\":true,\n"
                + "\"lowAlert\":2,\n"
                + "\"lowAlertActive\":true,\n"
                + "\"lowFault\":1,\n"
                + "\"lowFaultActive\":true,\n"
                + "\"maxFrequency\":10,\n"
                + "\"minFrequency\":1,\n"
                + "\"paramAmpFactor\":\"paramAmp Test Factor 1\", \n"
                + "\"paramType\":\"param Test Type 1\", \n"
                + "\"paramUnits\":\"param Test Units 1\",\n"
                + "\"period\":5,\n"
                + "\"resolution\":5,\n"
                + "\"sampleRate\":5,\n"
                + "\"sensorType\":%s\n"
                + "}";
        System.out.println("createSiteApAlSets content -" + String.format(content, CUSTOMER_ACCOUNT, SITE_ID, AL_SET_ID, AP_SET_ID, AP_SET_NAME, AL_SET_NAME, PARAM_NAME, SENSOR_TYPE));
        String result0 = instance.createSiteApAlSets(String.format(content, CUSTOMER_ACCOUNT, SITE_ID, AL_SET_ID, AP_SET_ID_2, AP_SET_NAME, AL_SET_NAME, PARAM_NAME, SENSOR_TYPE));
        
        System.out.println("createSiteApAlSets 50 result0 = " + result0);
        System.out.println("findByCustomerSiteApSets 50");
        String expResult = "{\"customerAccount\":\"77777\",\"siteId\":\"6102feaa-98e4-417b-a6b6-939af94e8194\",\"apSetIds\":\"ca610979-890b-4ee8-84e2-0e240933ae21,bad01d96-c2a1-40fc-87b3-d17a8b64f067\",\"SiteApAlSets\":[{\"customerAccount\":\"77777\",\"siteId\":\"6102feaa-98e4-417b-a6b6-939af94e8194\",\"apSetId\":\"bad01d96-c2a1-40fc-87b3-d17a8b64f067\",\"alSetId\":\"bad01d96-c2a1-40fc-87b3-d17a8b64f067\",\"paramName\":\"paramName Test 1\",\"apSetName\":\"apSetName Test 1\",\"alSetName\":\"alSetName Test 1\",\"demodHighPass\":9.0,\"demodLowPass\":2.0,\"dwell\":5,\"fmax\":8,\"frequencyUnits\":\"frequency Test Unit 1\",\"highAlert\":6.0,\"highAlertActive\":true,\"highFault\":6.0,\"highFaultActive\":true,\"demod\":true,\"lowAlert\":2.0,\"lowAlertActive\":true,\"lowFault\":1.0,\"lowFaultActive\":true,\"maxFrequency\":10.0,\"minFrequency\":1.0,\"paramAmpFactor\":\"paramAmp Test Factor 1\",\"paramType\":\"param Test Type 1\",\"paramUnits\":\"param Test Units 1\",\"period\":5.0,\"resolution\":5,\"sampleRate\":5.0,\"sensorType\":\"sensorTest Type 1\"},{\"customerAccount\":\"77777\",\"siteId\":\"6102feaa-98e4-417b-a6b6-939af94e8194\",\"apSetId\":\"ca610979-890b-4ee8-84e2-0e240933ae21\",\"alSetId\":\"bad01d96-c2a1-40fc-87b3-d17a8b64f067\",\"paramName\":\"paramName Test 1\",\"apSetName\":\"apSetName Test 1\",\"alSetName\":\"alSetName Test 1\",\"demodHighPass\":9.0,\"demodLowPass\":2.0,\"dwell\":5,\"fmax\":8,\"frequencyUnits\":\"frequency Test Unit 1\",\"highAlert\":6.0,\"highAlertActive\":true,\"highFault\":6.0,\"highFaultActive\":true,\"demod\":true,\"lowAlert\":2.0,\"lowAlertActive\":true,\"lowFault\":1.0,\"lowFaultActive\":true,\"maxFrequency\":10.0,\"minFrequency\":1.0,\"paramAmpFactor\":\"paramAmp Test Factor 1\",\"paramType\":\"param Test Type 1\",\"paramUnits\":\"param Test Units 1\",\"period\":5.0,\"resolution\":5,\"sampleRate\":5.0,\"sensorType\":\"sensorTest Type 1\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByCustomerSiteApSets(CUSTOMER_ACCOUNT_1, SITE_ID_1, AP_SET_ID_1 + "," + AP_SET_ID_2_1);
        System.out.println("getByCustomerSiteApSet result 50 -" + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of deleteSiteApAlSets method, of class SiteApAlSetsController.
     */
    @Test
    public void test60_DeleteSiteApAlSets() {
        System.out.println("deleteSiteApAlSets");
        String expResult = "{\"outcome\":\"Deleted Site ApAlSets successfully.\"}";
        String result = instance.deleteSiteApAlSets(String.format(content, CUSTOMER_ACCOUNT, SITE_ID, AL_SET_ID, AP_SET_ID, AP_SET_NAME, AL_SET_NAME, PARAM_NAME, SENSOR_TYPE));
        assertEquals(expResult, result);
    }
    
    /**
     * Test of deleteSiteApAlSets method, of class SiteApAlSetsController.
     */
    @Test
    public void test61_DeleteSiteApAlSets() {
        System.out.println("deleteSiteApAlSets");
        String expResult = "{\"outcome\":\"Insufficient data given in json\"}";
        String result = instance.deleteSiteApAlSets(String.format(content, null, SITE_ID, AL_SET_ID, AP_SET_ID, AP_SET_NAME, AL_SET_NAME, PARAM_NAME, SENSOR_TYPE));
        assertEquals(expResult, result);
    }
    
    /**
     * Test of deleteSiteApAlSets method, of class SiteApAlSetsController.
     */
    @Test
    public void test62_DeleteSiteApAlSets() {
        System.out.println("deleteSiteApAlSets");
        String expResult = "{\"outcome\":\"Insufficient data given in json\"}";
        String result = instance.deleteSiteApAlSets(String.format(content, CUSTOMER_ACCOUNT, null, AL_SET_ID, AP_SET_ID, AP_SET_NAME, AL_SET_NAME, PARAM_NAME, SENSOR_TYPE));
        assertEquals(expResult, result);
    }
    
    /**
     * Test of deleteSiteApAlSets method, of class SiteApAlSetsController.
     */
    @Test
    public void test63_DeleteSiteApAlSets() {
        System.out.println("deleteSiteApAlSets");
        String expResult = "{\"outcome\":\"Insufficient data given in json\"}";
        String result = instance.deleteSiteApAlSets(String.format(content, CUSTOMER_ACCOUNT, SITE_ID, null, AP_SET_ID, AP_SET_NAME, AL_SET_NAME, PARAM_NAME, SENSOR_TYPE));
        assertEquals(expResult, result);
    }
    
    /**
     * Test of deleteSiteApAlSets method, of class SiteApAlSetsController.
     */
    @Test
    public void test64_DeleteSiteApAlSets() {
        System.out.println("deleteSiteApAlSets");
        String expResult = "{\"outcome\":\"Insufficient data given in json\"}";
        String result = instance.deleteSiteApAlSets(String.format(content, CUSTOMER_ACCOUNT, SITE_ID, AL_SET_ID, null, AP_SET_NAME, AL_SET_NAME, PARAM_NAME, SENSOR_TYPE));
        assertEquals(expResult, result);
    }
    
    /**
     * Test of deleteSiteApAlSets method, of class SiteApAlSetsController.
     */
    @Test
    public void test65_DeleteSiteApAlSets() {
        System.out.println("deleteSiteApAlSets");
        String expResult = "{\"outcome\":\"Insufficient data given in json\"}";
        String result = instance.deleteSiteApAlSets(String.format(content, CUSTOMER_ACCOUNT, SITE_ID, AL_SET_ID, AP_SET_ID, AP_SET_NAME, AL_SET_NAME, null, SENSOR_TYPE));
        assertEquals(expResult, result);
    }

}
