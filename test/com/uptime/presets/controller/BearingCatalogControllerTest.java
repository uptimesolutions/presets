/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.presets.controller;

import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author joseph
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BearingCatalogControllerTest {

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//presets.cql", true, true, "worldview_dev1"));
    private static String content = "";
    private static BearingCatalogController instance;

    public BearingCatalogControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        instance = new BearingCatalogController();
        content = "{\n"
                + "\"vendorId\":\"Test Vendor\",\n"
                + "\"bearingId\":\"Test Bearing\",\n"
                + "\"bc\":\"1\",\n"
                + "\"ftf\":2,\n"
                + "\"bsf\":10,\n"
                + "\"bpfo\":20,\n"
                + "\"bpfi\":30\n"
                + "}";
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getByPK method, of class BearingCatalogController.
     */
    @Test
    public void test20_GetByPK() throws Exception {
        System.out.println("getByPK");

        String vendorId = "Test Vendor";
        String bearingId = "Test Bearing";
        String expResult = "{\"vendorId\":\"Test Vendor\",\"bearingId\":\"Test Bearing\",\"BearingCatalog\":[{\"vendorId\":\"Test Vendor\",\"bearingId\":\"Test Bearing\",\"bc\":1,\"bpfi\":30.0,\"bpfo\":20.0,\"bsf\":10.0,\"ftf\":2.0}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByPK(vendorId, bearingId);
        System.out.println("getByPK result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByPK method, of class BearingCatalogController.
     */
    @Test
    public void test21_GetByPK() throws Exception {
        System.out.println("getByPK");
        String vendorId = "Test Vendor ABC";
        String bearingId = "Test Bearing ABC";
        String expResult = "{\"outcome\":\"Bearing Catalog not found.\"}";
        String result = instance.getByPK(vendorId, bearingId);
        System.out.println("findByPK 21 result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByPK method, of class BearingCatalogController.
     */
    @Test(expected = IllegalArgumentException.class)
    public void test22_GetByPK() throws Exception {
        System.out.println("getByPK");
        String vendorId = null;
        String bearingId = "Test Bearing ABC";
        fail(instance.getByPK(vendorId, bearingId));
    }

    /**
     * Test of getByPK method, of class BearingCatalogController.
     */
    @Test(expected = IllegalArgumentException.class)
    public void test23_GetByPK() throws Exception {
        System.out.println("getByPK");
        String vendorId = "Test Vendor";
        String bearingId = null;
        fail(instance.getByPK(vendorId, bearingId));
    }

    /**
     * Test of getByVendorId method, of class BearingCatalogController.
     */
    @Test
    public void test30_GetByVendorId() throws Exception {
        System.out.println("getByVendorId");
        String vendorId = "Test Vendor";
        String expResult = "{\"vendorId\":\"Test Vendor\",\"BearingCatalog\":[{\"vendorId\":\"Test Vendor\",\"bearingId\":\"Test Bearing\",\"bc\":1,\"bpfi\":30.0,\"bpfo\":20.0,\"bsf\":10.0,\"ftf\":2.0}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByVendorId(vendorId);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByVendorId method, of class BearingCatalogController.
     */
    @Test
    public void test31_GetByVendorId() throws Exception {
        System.out.println("getByVendorId");
        String vendorId = "Test VendorABC";
        String expResult = "{\"outcome\":\"Bearing Catalog not found.\"}";
        String result = instance.getByVendorId(vendorId);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByVendorId method, of class BearingCatalogController.
     */
    @Test(expected = IllegalArgumentException.class)
    public void test32_GetByVendorId() throws Exception {
        System.out.println("getByVendorId");
        String vendorId = null;
        fail(instance.getByVendorId(vendorId));
    }

    /**
     * Test of createBearingCatalog method, of class BearingCatalogController.
     */
    @Test
    public void test10_CreateBearingCatalog() {
        System.out.println("createBearingCatalog");
        String result = instance.createBearingCatalog(content);
        String expResult = "{\"outcome\":\"New Bearing Catalog created successfully.\"}";

        assertEquals(expResult, result);
    }

    /**
     * Test of createBearingCatalog method, of class BearingCatalogController.
     */
    @Test
    public void test11_CreateBearingCatalog() {
        System.out.println("createBearingCatalog");
        content = "{\n"
                + "\"vendorId\":\"Test Vendor\",\n"
                // + "\"bearingId\":\"Test Bearing\",\n"
                + "\"bc\":\"1\",\n"
                + "\"ftf\":2,\n"
                + "\"bsf\":10,\n"
                + "\"bpfo\":20,\n"
                + "\"bpfi\":30\n"
                + "}";
        String result = instance.createBearingCatalog(content);
        String expResult = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        assertEquals(expResult, result);
    }

    /**
     * Test of createBearingCatalog method, of class BearingCatalogController.
     */
    @Test
    public void test12_CreateBearingCatalog() {
        System.out.println("createBearingCatalog");
        String result = instance.createBearingCatalog(null);
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        assertEquals(expResult, result);
    }

    /**
     * Test of updateBearingCatalog method, of class BearingCatalogController.
     */
    @Test
    public void test40_UpdateBearingCatalog() {
        System.out.println("updateBearingCatalog");
        content = "{\n"
                + "\"vendorId\":\"Test Vendor\",\n"
                + "\"bearingId\":\"Test Bearing\",\n"
                + "\"bc\":\"2\",\n"
                + "\"ftf\":2,\n"
                + "\"bsf\":10,\n"
                + "\"bpfo\":20,\n"
                + "\"bpfi\":30\n"
                + "}";
        String expResult = "{\"outcome\":\"Updated Bearing Catalog successfully.\"}";
        String result = instance.updateBearingCatalog(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateBearingCatalog method, of class BearingCatalogController.
     */
    @Test
    public void test41_UpdateBearingCatalog() {
        System.out.println("updateBearingCatalog");
        content = "{\n"
                + "\"vendorId\":\"Test Vendor\",\n"
                // + "\"bearingId\":\"Test Bearing\",\n"
                + "\"bc\":\"2\",\n"
                + "\"ftf\":2,\n"
                + "\"bsf\":10,\n"
                + "\"bpfo\":20,\n"
                + "\"bpfi\":30\n"
                + "}";
        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.updateBearingCatalog(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateBearingCatalog method, of class BearingCatalogController.
     */
    @Test
    public void test42_UpdateBearingCatalog() {
        System.out.println("updateBearingCatalog");
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.updateBearingCatalog("");
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteBearingCatalog method, of class BearingCatalogController.
     */
    @Test
    public void test50_DeleteBearingCatalog() {
        System.out.println("deleteBearingCatalog");
        content = "{\n"
                + "\"vendorId\":\"Test Vendor\",\n"
                + "\"bearingId\":\"Test Bearing\",\n"
                + "\"bc\":\"2\",\n"
                + "\"ftf\":2,\n"
                + "\"bsf\":10,\n"
                + "\"bpfo\":20,\n"
                + "\"bpfi\":30\n"
                + "}";
        String expResult = "{\"outcome\":\"Deleted Bearing Catalog successfully.\"}";
        String result = instance.deleteBearingCatalog(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteBearingCatalog method, of class BearingCatalogController.
     */
    @Test
    public void test51_DeleteBearingCatalog() {
        System.out.println("deleteBearingCatalog");
        content = "{\n"
                + "\"vendorId\":\"Test Vendor\",\n"
                // + "\"bearingId\":\"Test Bearing\",\n"
                + "\"bc\":\"2\",\n"
                + "\"ftf\":2,\n"
                + "\"bsf\":10,\n"
                + "\"bpfo\":20,\n"
                + "\"bpfi\":30\n"
                + "}";
        String expResult = "{\"outcome\":\"Insufficient data given in json\"}";
        String result = instance.deleteBearingCatalog(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteBearingCatalog method, of class BearingCatalogController.
     */
    @Test
    public void test52_DeleteBearingCatalog() {
        System.out.println("deleteBearingCatalog");
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.deleteBearingCatalog(null);
        assertEquals(expResult, result);
    }
    
     /**
     * Test of deleteBearingCatalog method, of class BearingCatalogController.
     */
    @Test
    public void test53_DeleteBearingCatalog() {
        System.out.println("deleteBearingCatalog");
        content = "{\n"
                + "\"vendorId\":\"Test Vendor\",\n"
                 + "\"bearingId\":\"Test Bearing 111\",\n"
                + "\"bc\":\"2\",\n"
                + "\"ftf\":2,\n"
                + "\"bsf\":10,\n"
                + "\"bpfo\":20,\n"
                + "\"bpfi\":30\n"
                + "}";
        String expResult = "{\"outcome\":\"No Bearing Catalog found to delete.\"}";
        String result = instance.deleteBearingCatalog(content);
        assertEquals(expResult, result);
    }

}
