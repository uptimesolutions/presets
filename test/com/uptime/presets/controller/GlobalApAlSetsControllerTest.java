/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.presets.controller;

import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author Joseph
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GlobalApAlSetsControllerTest {

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//presets.cql", true, true, "worldview_dev1"));
    private static GlobalApAlSetsController instance;
    private static String content;
    private static final String CUSTOMER_ACCOUNT = "\"77777\"";
    private static final String CUSTOMER_ACCOUNT_1 = "77777";
    private static final String AP_SET_ID = "\"ca610979-890b-4ee8-84e2-0e240933ae21\"";
    private static final String AP_SET_ID_2 = "\"6102feaa-98e4-417b-a6b6-939af94e8194\"";
    private static final String AP_SET_ID_2_1 = "6102feaa-98e4-417b-a6b6-939af94e8194";
    private static final String AP_SET_ID_1 = "ca610979-890b-4ee8-84e2-0e240933ae21";
    private static final String AL_SET_ID = "\"bad01d96-c2a1-40fc-87b3-d17a8b64f067\"";
    private static final String AL_SET_ID_1 = "bad01d96-c2a1-40fc-87b3-d17a8b64f067";
    private static final String PARAM_NAME = "\"paramName\"";
    private static final String PARAM_NAME_1 = "paramName";
    private static final String SENSOR_TYPE = "sensorType";

    public GlobalApAlSetsControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        
        instance = new GlobalApAlSetsController();
        content = "{\n"
                + "\"customerAccount\": %s,\n"
                + "\"apSetName\":\"apSetName2\",\n"
                + "\"alSetName\":\"alSetName\",\n"
                + "\"alSetId\":%s,\n"
                + "\"apSetId\":%s,\n"
                + "\"paramName\":%s,\n"
                + "\"demodHighPass\":9,\n"
                + "\"demodLowPass\":2,\n"
                + "\"dwell\":5,\n"
                + "\"fmax\":8,\n"
                + "\"frequencyUnits\":\"frequencyUnits\",\n"
                + "\"highAlert\":6,\n"
                + "\"highAlertActive\":true,\n"
                + "\"highFault\":6,\n"
                + "\"highFaultActive\":true,\n"
                + "\"demod\":true,\n"
                + "\"lowAlert\":2,\n"
                + "\"lowAlertActive\":true,\n"
                + "\"lowFault\":1,\n"
                + "\"lowFaultActive\":true,\n"
                + "\"maxFrequency\":10,\n"
                + "\"minFrequency\":1,\n"
                + "\"paramAmpFactor\":\"paramAmpFactor\",\n"
                + "\"paramType\":\"paramType\",\n"
                + "\"paramUnits\":\"paramUnits\",\n"
                + "\"period\":5,\n"
                + "\"resolution\":5,\n"
                + "\"sampleRate\":5,\n"
                + "\"sensorType\":\"sensorType\"\n"
                + "}";

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of createGlobalApAlSets method, of class GlobalApAlSetsController.
     */
    @Test
    public void test10_CreateGlobalApAlSets() {
        System.out.println("createGlobalApAlSets");
        String expResult = "{\"outcome\":\"New Global ApAlSets created successfully.\"}";
        String result = instance.createGlobalApAlSets(String.format(content, CUSTOMER_ACCOUNT, AL_SET_ID, AP_SET_ID, PARAM_NAME));
        assertEquals(expResult, result);
    }

    /**
     * Test of createGlobalApAlSets method, of class GlobalApAlSetsController.
     */
    @Test
    public void test11_CreateGlobalApAlSets() {
        System.out.println("createGlobalApAlSets fail customerAccount");

        String expResult = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        String result = instance.createGlobalApAlSets(String.format(content, null, AL_SET_ID, AP_SET_ID, PARAM_NAME));
        assertEquals(expResult, result);
    }
    /**
     * Test of createGlobalApAlSets method, of class GlobalApAlSetsController.
     */
    @Test
    public void test12_CreateGlobalApAlSets() {
        System.out.println("createGlobalApAlSets fail customerAccount");
        content = "{\n"
                + "\"customerAccount\": %s,\n"
                + "\"alSetName\":\"alSetName\",\n"
                + "\"alSetId\":%s,\n"
                + "\"apSetId\":%s,\n"
                + "\"paramName\":%s,\n"
                + "\"demodHighPass\":9,\n"
                + "\"demodLowPass\":2,\n"
                + "\"dwell\":5,\n"
                + "\"fmax\":8,\n"
                + "\"frequencyUnits\":\"frequencyUnits\",\n"
                + "\"highAlert\":6,\n"
                + "\"highAlertActive\":true,\n"
                + "\"highFault\":6,\n"
                + "\"highFaultActive\":true,\n"
                + "\"demod\":true,\n"
                + "\"lowAlert\":2,\n"
                + "\"lowAlertActive\":true,\n"
                + "\"lowFault\":1,\n"
                + "\"lowFaultActive\":true,\n"
                + "\"maxFrequency\":10,\n"
                + "\"minFrequency\":1,\n"
                + "\"paramAmpFactor\":\"paramAmpFactor\",\n"
                + "\"paramType\":\"paramType\",\n"
                + "\"paramUnits\":\"paramUnits\",\n"
                + "\"period\":5,\n"
                + "\"resolution\":5,\n"
                + "\"sampleRate\":5,\n"
                + "\"sensorType\":\"sensorType\"\n"
                + "}";
        String expResult = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        String result = instance.createGlobalApAlSets(String.format(content, CUSTOMER_ACCOUNT, AL_SET_ID, null, PARAM_NAME));
        assertEquals(expResult, result);
    }
    /**
     * Test of createGlobalApAlSets method, of class GlobalApAlSetsController.
     */
    @Test
    public void test13_CreateGlobalApAlSets() {
        System.out.println("createGlobalApAlSets fail customerAccount");

        String expResult = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        String result = instance.createGlobalApAlSets(String.format(content, CUSTOMER_ACCOUNT, AL_SET_ID, null, PARAM_NAME));
        assertEquals(expResult, result);
    }
    /**
     * Test of createGlobalApAlSets method, of class GlobalApAlSetsController.
     */
    @Test
    public void test14_CreateGlobalApAlSets() {
        System.out.println("createGlobalApAlSets fail customerAccount");

        String expResult = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        String result = instance.createGlobalApAlSets(String.format(content, CUSTOMER_ACCOUNT, AL_SET_ID, AP_SET_ID, null));
        assertEquals(expResult, result);
    }

    /**
     * Test of updateGlobalApAlSets method, of class GlobalApAlSetsController.
     */
    @Test
    public void test50_UpdateGlobalApAlSets() {
        System.out.println("updateGlobalApAlSets");
        String expResult = "{\"outcome\":\"Updated Global ApAlSets successfully.\"}";
        String result = instance.updateGlobalApAlSets(String.format(content, CUSTOMER_ACCOUNT, AL_SET_ID, AP_SET_ID, PARAM_NAME));
        assertEquals(expResult, result);
    }

    /**
     * Test of updateGlobalApAlSets method, of class GlobalApAlSetsController.
     */
    @Test
    public void test51_UpdateGlobalApAlSets() {
        System.out.println("updateGlobalApAlSets");
        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.updateGlobalApAlSets(String.format(content, null, AL_SET_ID, AP_SET_ID, PARAM_NAME));
        assertEquals(expResult, result);
    }
    /**
     * Test of updateGlobalApAlSets method, of class GlobalApAlSetsController.
     */
    @Test
    public void test52_UpdateGlobalApAlSets() {
        System.out.println("updateGlobalApAlSets");
        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.updateGlobalApAlSets(String.format(content, CUSTOMER_ACCOUNT, null, AP_SET_ID, PARAM_NAME));
        assertEquals(expResult, result);
    }
    /**
     * Test of updateGlobalApAlSets method, of class GlobalApAlSetsController.
     */
    @Test
    public void test53_UpdateGlobalApAlSets() {
        System.out.println("updateGlobalApAlSets");
        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.updateGlobalApAlSets(String.format(content, CUSTOMER_ACCOUNT, AL_SET_ID, null, PARAM_NAME));
        assertEquals(expResult, result);
    }
    /**
     * Test of updateGlobalApAlSets method, of class GlobalApAlSetsController.
     */
    @Test
    public void test54_UpdateGlobalApAlSets() {
        System.out.println("updateGlobalApAlSets");
        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.updateGlobalApAlSets(String.format(content, CUSTOMER_ACCOUNT, AL_SET_ID, AP_SET_ID, null));
        assertEquals(expResult, result);
    }

    /**
     * Test of getByPK method, of class GlobalApAlSetsController.
     * @throws java.lang.Exception
     */
    @Test
    public void test20_FindByPK() throws Exception {
        System.out.println("findByPK");

        String expResult = "{\"customerAccount\":\"77777\",\"apSetId\":\"ca610979-890b-4ee8-84e2-0e240933ae21\",\"alSetId\":\"bad01d96-c2a1-40fc-87b3-d17a8b64f067\",\"paramName\":\"paramName\",\"GlobalApAlSets\":[{\"customerAccount\":\"77777\",\"apSetId\":\"ca610979-890b-4ee8-84e2-0e240933ae21\",\"alSetId\":\"bad01d96-c2a1-40fc-87b3-d17a8b64f067\",\"paramName\":\"paramName\",\"apSetName\":\"apSetName2\",\"alSetName\":\"alSetName\",\"demodHighPass\":9.0,\"demodLowPass\":2.0,\"dwell\":5,\"fmax\":8,\"frequencyUnits\":\"frequencyUnits\",\"highAlert\":6.0,\"highAlertActive\":true,\"highFault\":6.0,\"highFaultActive\":true,\"demod\":true,\"lowAlert\":2.0,\"lowAlertActive\":true,\"lowFault\":1.0,\"lowFaultActive\":true,\"maxFrequency\":10.0,\"minFrequency\":1.0,\"paramAmpFactor\":\"paramAmpFactor\",\"paramType\":\"paramType\",\"paramUnits\":\"paramUnits\",\"period\":5.0,\"resolution\":5,\"sampleRate\":5.0,\"sensorType\":\"sensorType\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByPK(CUSTOMER_ACCOUNT_1, AP_SET_ID_1, AL_SET_ID_1, PARAM_NAME_1);
        System.out.println("findByPK result 20 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByPK method, of class GlobalApAlSetsController.
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test21_FindByPK() throws Exception {
        System.out.println("test21_FindByPK");
        fail(instance.getByPK(null, AP_SET_ID_1, AL_SET_ID_1, PARAM_NAME_1));
    }
    /**
     * Test of getByPK method, of class GlobalApAlSetsController.
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test22_FindByPK() throws Exception {
        System.out.println("test22_FindByPK");
        fail(instance.getByPK(CUSTOMER_ACCOUNT_1, null, AL_SET_ID_1, PARAM_NAME_1));
    }
    /**
     * Test of getByPK method, of class GlobalApAlSetsController.
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test23_FindByPK() throws Exception {
        System.out.println("test23_FindByPK");
        fail(instance.getByPK(CUSTOMER_ACCOUNT_1, AP_SET_ID_1, null, PARAM_NAME_1));
    }
    /**
     * Test of getByPK method, of class GlobalApAlSetsController.
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test24_FindByPK() throws Exception {
        System.out.println("test24_FindByPK");
        fail(instance.getByPK(CUSTOMER_ACCOUNT_1, AP_SET_ID_1, AL_SET_ID_1, null));
    }

    /**
     * Test of getByCustomerApSetAlSet method, of class
     * GlobalApAlSetsController.
     * @throws java.lang.Exception
     */
    @Test
    public void test30_FindByCustomerApSetAlSet() throws Exception {
        System.out.println("findByCustomerApSetAlSet");
        String expResult = "{\"customerAccount\":\"77777\",\"apSetId\":\"ca610979-890b-4ee8-84e2-0e240933ae21\",\"alSetId\":\"bad01d96-c2a1-40fc-87b3-d17a8b64f067\",\"GlobalApAlSets\":[{\"customerAccount\":\"77777\",\"apSetId\":\"ca610979-890b-4ee8-84e2-0e240933ae21\",\"alSetId\":\"bad01d96-c2a1-40fc-87b3-d17a8b64f067\",\"paramName\":\"paramName\",\"apSetName\":\"apSetName2\",\"alSetName\":\"alSetName\",\"demodHighPass\":9.0,\"demodLowPass\":2.0,\"dwell\":5,\"fmax\":8,\"frequencyUnits\":\"frequencyUnits\",\"highAlert\":6.0,\"highAlertActive\":true,\"highFault\":6.0,\"highFaultActive\":true,\"demod\":true,\"lowAlert\":2.0,\"lowAlertActive\":true,\"lowFault\":1.0,\"lowFaultActive\":true,\"maxFrequency\":10.0,\"minFrequency\":1.0,\"paramAmpFactor\":\"paramAmpFactor\",\"paramType\":\"paramType\",\"paramUnits\":\"paramUnits\",\"period\":5.0,\"resolution\":5,\"sampleRate\":5.0,\"sensorType\":\"sensorType\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByCustomerApSetAlSet(CUSTOMER_ACCOUNT_1, AP_SET_ID_1, AL_SET_ID_1);
        System.out.println("findByCustomerApSetAlSet result 30 - " + result);
        assertEquals(expResult, result);
    }
    /**
     * Test of getByCustomerApSetAlSet method, of class
     * GlobalApAlSetsController.
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test31_FindByCustomerApSetAlSet() throws Exception {
        System.out.println("test31_FindByCustomerApSetAlSet");
        fail(instance.getByCustomerApSetAlSet(null, AP_SET_ID_1, AL_SET_ID_1));
    }
    /**
     * Test of getByCustomerApSetAlSet method, of class
     * GlobalApAlSetsController.
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test32_FindByCustomerApSetAlSet() throws Exception {
        System.out.println("test32_FindByCustomerApSetAlSet");
        fail(instance.getByCustomerApSetAlSet(CUSTOMER_ACCOUNT_1, null, AL_SET_ID_1));
    }
    /**
     * Test of getByCustomerApSetAlSet method, of class
     * GlobalApAlSetsController.
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test33_FindByCustomerApSetAlSet() throws Exception {
        System.out.println("test33_FindByCustomerApSetAlSet");
        fail(instance.getByCustomerApSetAlSet(CUSTOMER_ACCOUNT_1, AP_SET_ID_1, null));
    }

    /**
     * Test of findByCustomerApSet method, of class GlobalApAlSetsController.
     * @throws java.lang.Exception
     */
    @Test
    public void test35_FindByCustomerApSets() throws Exception {
        System.out.println("findByCustomerApSet");
        content = "{\n"
                + "\"customerAccount\": %s,\n"
                + "\"apSetName\":\"apSetName 22\",\n"
                + "\"alSetName\":\"alSetName 11\",\n"
                + "\"alSetId\":%s,\n"
                + "\"apSetId\":%s,\n"
                + "\"paramName\":%s,\n"
                + "\"demodHighPass\":9,\n"
                + "\"demodLowPass\":2,\n"
                + "\"dwell\":5,\n"
                + "\"fmax\":8,\n"
                + "\"frequencyUnits\":\"frequencyUnits\",\n"
                + "\"highAlert\":6,\n"
                + "\"highAlertActive\":true,\n"
                + "\"highFault\":6,\n"
                + "\"highFaultActive\":true,\n"
                + "\"demod\":true,\n"
                + "\"lowAlert\":2,\n"
                + "\"lowAlertActive\":true,\n"
                + "\"lowFault\":1,\n"
                + "\"lowFaultActive\":true,\n"
                + "\"maxFrequency\":10,\n"
                + "\"minFrequency\":1,\n"
                + "\"paramAmpFactor\":\"paramAmpFactor\",\n"
                + "\"paramType\":\"paramType\",\n"
                + "\"paramUnits\":\"paramUnits\",\n"
                + "\"period\":5,\n"
                + "\"resolution\":5,\n"
                + "\"sampleRate\":5,\n"
                + "\"sensorType\":\"sensorType\"\n"
                + "}";
        String result = instance.createGlobalApAlSets(String.format(content, CUSTOMER_ACCOUNT, AL_SET_ID, AP_SET_ID_2, PARAM_NAME));
        String expResult = "{\"customerAccount\":\"77777\",\"apSetIds\":\"ca610979-890b-4ee8-84e2-0e240933ae21,6102feaa-98e4-417b-a6b6-939af94e8194\",\"GlobalApAlSets\":[{\"customerAccount\":\"77777\",\"apSetId\":\"6102feaa-98e4-417b-a6b6-939af94e8194\",\"alSetId\":\"bad01d96-c2a1-40fc-87b3-d17a8b64f067\",\"paramName\":\"paramName\",\"apSetName\":\"apSetName 22\",\"alSetName\":\"alSetName 11\",\"demodHighPass\":9.0,\"demodLowPass\":2.0,\"dwell\":5,\"fmax\":8,\"frequencyUnits\":\"frequencyUnits\",\"highAlert\":6.0,\"highAlertActive\":true,\"highFault\":6.0,\"highFaultActive\":true,\"demod\":true,\"lowAlert\":2.0,\"lowAlertActive\":true,\"lowFault\":1.0,\"lowFaultActive\":true,\"maxFrequency\":10.0,\"minFrequency\":1.0,\"paramAmpFactor\":\"paramAmpFactor\",\"paramType\":\"paramType\",\"paramUnits\":\"paramUnits\",\"period\":5.0,\"resolution\":5,\"sampleRate\":5.0,\"sensorType\":\"sensorType\"},{\"customerAccount\":\"77777\",\"apSetId\":\"ca610979-890b-4ee8-84e2-0e240933ae21\",\"alSetId\":\"bad01d96-c2a1-40fc-87b3-d17a8b64f067\",\"paramName\":\"paramName\",\"apSetName\":\"apSetName2\",\"alSetName\":\"alSetName\",\"demodHighPass\":9.0,\"demodLowPass\":2.0,\"dwell\":5,\"fmax\":8,\"frequencyUnits\":\"frequencyUnits\",\"highAlert\":6.0,\"highAlertActive\":true,\"highFault\":6.0,\"highFaultActive\":true,\"demod\":true,\"lowAlert\":2.0,\"lowAlertActive\":true,\"lowFault\":1.0,\"lowFaultActive\":true,\"maxFrequency\":10.0,\"minFrequency\":1.0,\"paramAmpFactor\":\"paramAmpFactor\",\"paramType\":\"paramType\",\"paramUnits\":\"paramUnits\",\"period\":5.0,\"resolution\":5,\"sampleRate\":5.0,\"sensorType\":\"sensorType\"}],\"outcome\":\"GET worked successfully.\"}";
        String result1 = instance.getByCustomerApSets(CUSTOMER_ACCOUNT_1, AP_SET_ID_1 + "," + AP_SET_ID_2_1);
        System.out.println("findByCustomerApSet result 35 - " + result1);
        assertEquals(expResult, result1);
        String result2 = instance.deleteGlobalApAlSets(String.format(content, CUSTOMER_ACCOUNT, AL_SET_ID, AP_SET_ID_2, PARAM_NAME));
        System.out.println("result2 - " + result2);
        
    }

    /**
     * Test of findByCustomerApSet method, of class GlobalApAlSetsController.
     * @throws java.lang.Exception
     */
    @Test
    public void test40_FindByCustomerApSet() throws Exception {
        System.out.println("findByCustomerApSet");
        String expResult = "{\"customerAccount\":\"77777\",\"apSetId\":\"ca610979-890b-4ee8-84e2-0e240933ae21\",\"GlobalApAlSets\":[{\"customerAccount\":\"77777\",\"apSetId\":\"ca610979-890b-4ee8-84e2-0e240933ae21\",\"alSetId\":\"bad01d96-c2a1-40fc-87b3-d17a8b64f067\",\"paramName\":\"paramName\",\"apSetName\":\"apSetName2\",\"alSetName\":\"alSetName\",\"demodHighPass\":9.0,\"demodLowPass\":2.0,\"dwell\":5,\"fmax\":8,\"frequencyUnits\":\"frequencyUnits\",\"highAlert\":6.0,\"highAlertActive\":true,\"highFault\":6.0,\"highFaultActive\":true,\"demod\":true,\"lowAlert\":2.0,\"lowAlertActive\":true,\"lowFault\":1.0,\"lowFaultActive\":true,\"maxFrequency\":10.0,\"minFrequency\":1.0,\"paramAmpFactor\":\"paramAmpFactor\",\"paramType\":\"paramType\",\"paramUnits\":\"paramUnits\",\"period\":5.0,\"resolution\":5,\"sampleRate\":5.0,\"sensorType\":\"sensorType\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByCustomerApSet(CUSTOMER_ACCOUNT_1, AP_SET_ID_1);
        System.out.println("findByCustomerApSet result 40 - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of findByCustomerApSet method, of class GlobalApAlSetsController.
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test41_FindByCustomerApSet() throws Exception {
        System.out.println("test41_FindByCustomerApSet");
        fail(instance.getByCustomerApSet(null, AP_SET_ID_1));
    }
    /**
     * Test of findByCustomerApSet method, of class GlobalApAlSetsController.
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test42_FindByCustomerApSet() throws Exception {
        System.out.println("test42_FindByCustomerApSet");
        fail(instance.getByCustomerApSet(CUSTOMER_ACCOUNT_1, null));
    }

    /**
     * Test of findByCustomerSensorType method, of class
     * GlobalApAlSetsController.
     * @throws java.lang.Exception
     */
    @Test
    public void test70_FindByCustomerApSet() throws Exception {
        System.out.println("findByCustomerApSet");
        String expResult = "{\"customerAccount\":\"77777\",\"sensorType\":\"sensorType\",\"GlobalApAlSetsByCustomer\":[{\"customerAccount\":\"77777\",\"sensorType\":\"sensorType\",\"apSetId\":\"ca610979-890b-4ee8-84e2-0e240933ae21\",\"alSetId\":\"bad01d96-c2a1-40fc-87b3-d17a8b64f067\",\"alSetName\":\"alSetName 11\",\"apSetName\":\"apSetName 22\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByCustomerSensorType(CUSTOMER_ACCOUNT_1, SENSOR_TYPE,false);
        System.out.println("test7_FindByCustomerApSet " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of findByCustomerSensorType method, of class
     * GlobalApAlSetsController.
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test71_FindByCustomerApSet() throws Exception {
        System.out.println("findByCustomerApSet");
        String expResult = "{\"customerAccount\":\"77777\",\"sensorType\":\"sensorType\",\"GlobalApAlSetsByCustomer\":[{\"customerAccount\":\"77777\",\"sensorType\":\"sensorType\",\"apSetId\":\"ca610979-890b-4ee8-84e2-0e240933ae21\",\"alSetId\":\"bad01d96-c2a1-40fc-87b3-d17a8b64f067\",\"alSetName\":\"alSetName\",\"apSetName\":\"apSetName2\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByCustomerSensorType(null, SENSOR_TYPE,false);
        System.out.println("test7_FindByCustomerApSet " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of findByCustomerSensorType method, of class
     * GlobalApAlSetsController.
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test72_FindByCustomerApSet() throws Exception {
        System.out.println("findByCustomerApSet");
        String expResult = "{\"customerAccount\":\"77777\",\"sensorType\":\"sensorType\",\"GlobalApAlSetsByCustomer\":[{\"customerAccount\":\"77777\",\"sensorType\":\"sensorType\",\"apSetId\":\"ca610979-890b-4ee8-84e2-0e240933ae21\",\"alSetId\":\"bad01d96-c2a1-40fc-87b3-d17a8b64f067\",\"alSetName\":\"alSetName\",\"apSetName\":\"apSetName2\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByCustomerSensorType(CUSTOMER_ACCOUNT_1, null,false);
        System.out.println("test7_FindByCustomerApSet " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of findByCustomerSensorTypeApSet method, of class
     * GlobalApAlSetsController.
     * @throws java.lang.Exception
     */
    @Test
    public void test60_findByCustomerSensorTypeApSet() throws Exception {
        System.out.println("findByCustomerApSet");
        String expResult = "{\"customerAccount\":\"77777\",\"sensorType\":\"sensorType\",\"GlobalApAlSetsByCustomer\":[{\"customerAccount\":\"77777\",\"sensorType\":\"sensorType\",\"apSetId\":\"ca610979-890b-4ee8-84e2-0e240933ae21\",\"alSetId\":\"bad01d96-c2a1-40fc-87b3-d17a8b64f067\",\"alSetName\":\"alSetName 11\",\"apSetName\":\"apSetName 22\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByCustomerSensorTypeApSet(CUSTOMER_ACCOUNT_1, SENSOR_TYPE, AP_SET_ID_1);
        System.out.println("findByCustomerApSet 60 - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of findByCustomerSensorTypeApSet method, of class
     * GlobalApAlSetsController.
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test61_findByCustomerSensorTypeApSet() throws Exception {
        System.out.println("findByCustomerApSet");
        String expResult = "{\"outcome\":\"Global ApAlSets not found.\"}";
        String result = instance.getByCustomerSensorTypeApSet(null, SENSOR_TYPE, AP_SET_ID_1);

        assertEquals(expResult, result);
    }
    
    /**
     * Test of findByCustomerSensorTypeApSet method, of class
     * GlobalApAlSetsController.
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test62_findByCustomerSensorTypeApSet() throws Exception {
        System.out.println("findByCustomerApSet");
        String expResult = "{\"outcome\":\"Global ApAlSets not found.\"}";
        String result = instance.getByCustomerSensorTypeApSet(CUSTOMER_ACCOUNT_1, null, AP_SET_ID_1);

        assertEquals(expResult, result);
    }
    
    /**
     * Test of findByCustomerSensorTypeApSet method, of class
     * GlobalApAlSetsController.
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test63_findByCustomerSensorTypeApSet() throws Exception {
        System.out.println("findByCustomerApSet");
        String expResult = "{\"outcome\":\"Global ApAlSets not found.\"}";
        String result = instance.getByCustomerSensorTypeApSet(CUSTOMER_ACCOUNT_1, SENSOR_TYPE, null);

        assertEquals(expResult, result);
    }

    /**
     * Test of deleteGlobalApAlSets method, of class GlobalApAlSetsController.
     */
    @Test
    public void test80_DeleteGlobalApAlSets() {
        System.out.println("deleteGlobalApAlSets");
        String expResult = "{\"outcome\":\"Deleted Global ApAlSets successfully.\"}";
        String result = instance.deleteGlobalApAlSets(String.format(content, CUSTOMER_ACCOUNT, AL_SET_ID, AP_SET_ID, PARAM_NAME));
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteGlobalApAlSets method, of class GlobalApAlSetsController.
     */
    @Test
    public void test81_DeleteGlobalApAlSets() {
        System.out.println("deleteGlobalApAlSets");
        String expResult = "{\"outcome\":\"Insufficient data given in json\"}";
        String result = instance.deleteGlobalApAlSets(String.format(content, null, AL_SET_ID, AP_SET_ID, PARAM_NAME));
        assertEquals(expResult, result);
    }
    /**
     * Test of deleteGlobalApAlSets method, of class GlobalApAlSetsController.
     */
    @Test
    public void test82_DeleteGlobalApAlSets() {
        System.out.println("deleteGlobalApAlSets");
        String expResult = "{\"outcome\":\"Insufficient data given in json\"}";
        String result = instance.deleteGlobalApAlSets(String.format(content, CUSTOMER_ACCOUNT, null, AP_SET_ID, PARAM_NAME));
        assertEquals(expResult, result);
    }
    /**
     * Test of deleteGlobalApAlSets method, of class GlobalApAlSetsController.
     */
    @Test
    public void test83_DeleteGlobalApAlSets() {
        System.out.println("deleteGlobalApAlSets");
        String expResult = "{\"outcome\":\"Insufficient data given in json\"}";
        String result = instance.deleteGlobalApAlSets(String.format(content, CUSTOMER_ACCOUNT, AL_SET_ID, null, PARAM_NAME));
        assertEquals(expResult, result);
    }
    /**
     * Test of deleteGlobalApAlSets method, of class GlobalApAlSetsController.
     */
    @Test
    public void test84_DeleteGlobalApAlSets() {
        System.out.println("deleteGlobalApAlSets");
        String expResult = "{\"outcome\":\"Insufficient data given in json\"}";
        String result = instance.deleteGlobalApAlSets(String.format(content, CUSTOMER_ACCOUNT, AL_SET_ID, AP_SET_ID, null));
        assertEquals(expResult, result);
    }

}
