/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.presets.controller;

import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author Joseph
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SiteTachometersControllerTest {

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//presets.cql", true, true, "worldview_dev1"));
    private static SiteTachometersController instance;
    private static String content;

    public SiteTachometersControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        instance = new SiteTachometersController();
        content = "{\n"
                + "  \"customerAccount\":\"customer_001\",\n"
                + "  \"tachName\":\"TachName1\",\n"
                + "  \"siteId\": \"54964cf6-cc29-4796-aa27-17b6ce01a75f\",\n"
                + "  \"tachId\": \"f9dbaf10-e394-418a-ad91-381700ee0850\",\n"
                + "  \"tachHardwareUnitChannel\":1,\n"
                + "  \"tachHardwareUnitSerialNumber\":\"Serial1\",\n"
                + "  \"tachMaxRpm\":4,\n"
                + "  \"tachMinRpm\":1,\n"
                + "  \"tachPulsesPerRev\":2,\n"
                + "  \"tachReferenceSpeed\":3,\n"
                + "  \"tachReferenceUnits\":\"RPM\",\n"
                + "  \"tachRisingEdge\":true,\n"
                + "  \"tachSpeedRatio\":3,\n"
                + "  \"tachTriggerPercent\":2,\n"
                + "  \"tachType\":\"TachType1\"\n"
                + "}";
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of createTachometers method, of class SiteTachometersController.
     */
    @Test
    public void test10_CreateSiteTachometers() {
        System.out.println("createSiteTachometers");
        String expResult = "{\"outcome\":\"New SiteTachometers created successfully.\"}";
        String result = instance.createSiteTachometers(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of createTachometers method, of class SiteTachometersController.
     * Unhappy path: Passing an invalid input json with missing siteId. 
     */
    @Test
    public void test12_CreateSiteTachometers() {
        System.out.println("createSiteTachometers");
        content = "{\n"
                + "  \"customerAccount\":\"customer_001\",\n"
                + "  \"tachName\":\"TachName1\",\n"
                + "  \"tachId\": \"f9dbaf10-e394-418a-ad91-381700ee0850\",\n"
                + "  \"tachHardwareUnitChannel\":1,\n"
                + "  \"tachHardwareUnitSerialNumber\":\"Serial1\",\n"
                + "  \"tachMaxRpm\":4,\n"
                + "  \"tachMinRpm\":1,\n"
                + "  \"tachPulsesPerRev\":2,\n"
                + "  \"tachReferenceSpeed\":3,\n"
                + "  \"tachReferenceUnits\":\"RPM\",\n"
                + "  \"tachRisingEdge\":true,\n"
                + "  \"tachSpeedRatio\":3,\n"
                + "  \"tachTriggerPercent\":2,\n"
                + "  \"tachType\":\"TachType1\"\n"
                + "}";
        String expResult = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        String result = instance.createSiteTachometers(content);
        System.out.println("result 12 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateTachometers method, of class SiteTachometersController.
     */
    @Test
    public void test20_UpdateSiteTachometers() {
        System.out.println("updateSiteTachometers");
        content = "{\n"
                + "  \"customerAccount\":\"customer_001\",\n"
                + "  \"tachName\":\"TachName1\",\n"
                + "  \"siteId\": \"54964cf6-cc29-4796-aa27-17b6ce01a75f\",\n"
                + "  \"tachId\": \"f9dbaf10-e394-418a-ad91-381700ee0850\",\n"
                + "  \"tachHardwareUnitChannel\":1,\n"
                + "  \"tachHardwareUnitSerialNumber\":\"Serial1\",\n"
                + "  \"tachMaxRpm\":4,\n"
                + "  \"tachMinRpm\":1,\n"
                + "  \"tachPulsesPerRev\":2,\n"
                + "  \"tachReferenceSpeed\":3,\n"
                + "  \"tachReferenceUnits\":\"FPM\",\n"
                + "  \"tachRisingEdge\":true,\n"
                + "  \"tachSpeedRatio\":3,\n"
                + "  \"tachTriggerPercent\":2,\n"
                + "  \"tachType\":\"TachType1\"\n"
                + "}";
        String expResult = "{\"outcome\":\"Updated Site Tachometers successfully.\"}";
        String result = instance.updateSiteTachometers(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateTachometers method, of class SiteTachometersController.
     * Unhappy path: Missing required field 'tachId' in input json. 
     */
    @Test
    public void test21_UpdateSiteTachometers() {
        System.out.println("updateSiteTachometers");
        content = "{\n"
                + "  \"customerAccount\":\"customer_001\",\n"
                + "  \"tachName\":\"TachName1\",\n"
                + "  \"siteId\": \"54964cf6-cc29-4796-aa27-17b6ce01a75f\",\n"
                + "  \"tachHardwareUnitChannel\":1,\n"
                + "  \"tachHardwareUnitSerialNumber\":\"Serial1\",\n"
                + "  \"tachMaxRpm\":4,\n"
                + "  \"tachMinRpm\":1,\n"
                + "  \"tachPulsesPerRev\":2,\n"
                + "  \"tachReferenceSpeed\":3,\n"
                + "  \"tachReferenceUnits\":\"FPM\",\n"
                + "  \"tachRisingEdge\":true,\n"
                + "  \"tachSpeedRatio\":3,\n"
                + "  \"tachTriggerPercent\":2,\n"
                + "  \"tachType\":\"TachType1\"\n"
                + "}";
        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.updateSiteTachometers(content);
        System.out.println("result 21 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateTachometers method, of class SiteTachometersController.
     * Unhappy path: Passing empty json. 
     */
    @Test
    public void test22_UpdateSiteTachometers() {
        System.out.println("updateSiteTachometers");
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.updateSiteTachometers("");
        System.out.println("result 22 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByPK method, of class SiteTachometersController.
     * @throws java.lang.Exception
     */
    @Test
    public void test30_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = "customer_001";
        String tachId = "f9dbaf10-e394-418a-ad91-381700ee0850";
        String siteId = "54964cf6-cc29-4796-aa27-17b6ce01a75f";
        String expResult = "{\"customerAccount\":\"customer_001\",\"siteId\":\"54964cf6-cc29-4796-aa27-17b6ce01a75f\",\"tachId\":\"f9dbaf10-e394-418a-ad91-381700ee0850\",\"SiteTachometers\":[{\"customerAccount\":\"customer_001\",\"siteId\":\"54964cf6-cc29-4796-aa27-17b6ce01a75f\",\"tachId\":\"f9dbaf10-e394-418a-ad91-381700ee0850\",\"tachName\":\"TachName1\",\"tachReferenceSpeed\":3.0,\"tachReferenceUnits\":\"FPM\",\"tachType\":\"TachType1\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByPK(customerAccount, siteId, tachId);
        System.out.println("result 30 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByPK method, of class SiteTachometersController.
     * Unhappy path: When customerAccount is passed is not available in DB, no data is received from the database.
     * @throws java.lang.Exception
     */
    @Test
    public void test31_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = "customer_002";
        String tachId = "f9dbaf10-e394-418a-ad91-381700ee0850";
        String siteId = "54964cf6-cc29-4796-aa27-17b6ce01a75f";
        String expResult = "{\"outcome\":\"Site Tachometers not found.\"}";
        String result = instance.getByPK(customerAccount, siteId, tachId);
        System.out.println("result 31 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByPK method, of class SiteTachometersController.
     * Unhappy path: When customer is passed as null
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test32_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = null;
        String tachId = "f9dbaf10-e394-418a-ad91-381700ee0850";
        String siteId = "54964cf6-cc29-4796-aa27-17b6ce01a75f";
        fail(instance.getByPK(customerAccount, siteId, tachId));
    }

    /**
     * Test of getByPK method, of class SiteTachometersController.
     * Unhappy path: When siteId is passed as null
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test33_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = "Custoomer_001";
        String tachId = "f9dbaf10-e394-418a-ad91-381700ee0850";
        String siteId = null;
        fail(instance.getByPK(customerAccount, siteId, tachId));
    }

    /**
     * Test of findByCustomer method, of class SiteTachometersController.
     * @throws java.lang.Exception
     */
    @Test
    public void test40_FindByCustomerSite() throws Exception {
        System.out.println("findByCustomerSite");
        String customerAccount = "customer_001";
        String siteId = "54964cf6-cc29-4796-aa27-17b6ce01a75f";
        String expResult = "{\"customerAccount\":\"customer_001\",\"siteId\":\"54964cf6-cc29-4796-aa27-17b6ce01a75f\",\"SiteTachometers\":[{\"customerAccount\":\"customer_001\",\"siteId\":\"54964cf6-cc29-4796-aa27-17b6ce01a75f\",\"tachId\":\"f9dbaf10-e394-418a-ad91-381700ee0850\",\"tachName\":\"TachName1\",\"tachReferenceSpeed\":3.0,\"tachReferenceUnits\":\"FPM\",\"tachType\":\"TachType1\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByCustomerSite(customerAccount, siteId);
        System.out.println("result 40 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of findByCustomer method, of class SiteTachometersController.
     * Unhappy path: Controller throws IllegalArgumentException 
     * when siteId is passed as "null".
     * @throws java.lang.Exception 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test41_FindByCustomerSite() throws Exception {
        System.out.println("findByCustomerSite");
        String customerAccount = "customer_001";
        String siteId = "null";
        fail(instance.getByCustomerSite(customerAccount, siteId));
    }

    /**
     * Test of findByCustomer method, of class SiteTachometersController.Unhappy path: Controller throws NullPointerException when customerAccount is passed as null.
     * @throws java.lang.Exception 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test42_FindByCustomerSite() throws Exception {
        System.out.println("findByCustomerSite");
        String customerAccount = null;
        String siteId = "54964cf6-cc29-4796-aa27-17b6ce01a75f";
        fail(instance.getByCustomerSite(customerAccount, siteId));
    }
    
    /**
     * Test of findByCustomer method, of class SiteTachometersController.
     * Unhappy path: Controller throws NullPointerException 
     * when siteId is passed as null.
     * @throws java.lang.Exception 
     */
    @Test(expected = NullPointerException.class)
    public void test43_FindByCustomerSite() throws Exception {
        System.out.println("findByCustomerSite");
        String customerAccount = "customer_001";
        String siteId = null;
        fail(instance.getByCustomerSite(customerAccount, siteId));
    }
    
    /**
     * Test of findByCustomer method, of class SiteTachometersController.
     * Unhappy path: Passing a siteId that does not exist in DB
     * @throws java.lang.Exception
     */
    @Test
    public void test44_FindByCustomerSite() throws Exception {
        System.out.println("findByCustomerSite");
        String customerAccount = "customer_001";
        String siteId = "f9dbaf10-e394-418a-ad91-381700ee0850";
        String expResult = "{\"outcome\":\"Site Tachometers not found.\"}";
        String result = instance.getByCustomerSite(customerAccount, siteId);
        System.out.println("result 44 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteTachometers method, of class SiteTachometersController.
     */
    @Test
    public void test50_DeleteSiteTachometers() {
        System.out.println("deleteSiteTachometers");
        content = "{\n"
                + "  \"customerAccount\":\"customer_001\",\n"
                + "  \"tachName\":\"TachName1\",\n"
                + "  \"siteId\": \"54964cf6-cc29-4796-aa27-17b6ce01a75f\",\n"
                + "  \"tachId\": \"f9dbaf10-e394-418a-ad91-381700ee0850\",\n"
                + "  \"tachHardwareUnitChannel\":1,\n"
                + "  \"tachHardwareUnitSerialNumber\":\"Serial1\",\n"
                + "  \"tachMaxRpm\":4,\n"
                + "  \"tachMinRpm\":1,\n"
                + "  \"tachPulsesPerRev\":2,\n"
                + "  \"tachReferenceSpeed\":3,\n"
                + "  \"tachReferenceUnits\":\"FPM\",\n"
                + "  \"tachRisingEdge\":true,\n"
                + "  \"tachSpeedRatio\":3,\n"
                + "  \"tachTriggerPercent\":2,\n"
                + "  \"tachType\":\"TachType1\"\n"
                + "}";
        String expResult = "{\"outcome\":\"Deleted Site Tachometers successfully.\"}";
        String result = instance.deleteSiteTachometers(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteTachometers method, of class SiteTachometersController.
     * Unhappy path: Missing required field 'tachId' in input json. 
     */
    @Test
    public void test51_DeleteSiteTachometers() {
        System.out.println("deleteSiteTachometers");
        content = "{\n"
                + "  \"customerAccount\":\"customer_001\",\n"
                + "  \"tachName\":\"TachName1\",\n"
                + "  \"siteId\": \"54964cf6-cc29-4796-aa27-17b6ce01a75f\",\n"
                + "  \"tachHardwareUnitChannel\":1,\n"
                + "  \"tachHardwareUnitSerialNumber\":\"Serial1\",\n"
                + "  \"tachMaxRpm\":4,\n"
                + "  \"tachMinRpm\":1,\n"
                + "  \"tachPulsesPerRev\":2,\n"
                + "  \"tachReferenceSpeed\":3,\n"
                + "  \"tachReferenceUnits\":\"FPM\",\n"
                + "  \"tachRisingEdge\":true,\n"
                + "  \"tachSpeedRatio\":3,\n"
                + "  \"tachTriggerPercent\":2,\n"
                + "  \"tachType\":\"TachType1\"\n"
                + "}";
        String expResult = "{\"outcome\":\"Insufficient data given in json\"}";
        String result = instance.deleteSiteTachometers(content);
        System.out.println("result 51 - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of deleteTachometers method, of class SiteTachometersController.
     * Unhappy path: Passing a  'tachId' in input json that does not exist. 
     */
    @Test
    public void test52_DeleteSiteTachometers() {
        System.out.println("deleteSiteTachometers");
        content = "{\n"
                + "  \"customerAccount\":\"customer_001\",\n"
                + "  \"tachName\":\"TachName1\",\n"
                + "  \"siteId\": \"54964cf6-cc29-4796-aa27-17b6ce01a75f\",\n"
                + "  \"tachId\": \"f9dbaf11-e394-418a-ad91-381700ee0850\",\n"
                + "  \"tachHardwareUnitChannel\":1,\n"
                + "  \"tachHardwareUnitSerialNumber\":\"Serial1\",\n"
                + "  \"tachMaxRpm\":4,\n"
                + "  \"tachMinRpm\":1,\n"
                + "  \"tachPulsesPerRev\":2,\n"
                + "  \"tachReferenceSpeed\":3,\n"
                + "  \"tachReferenceUnits\":\"FPM\",\n"
                + "  \"tachRisingEdge\":true,\n"
                + "  \"tachSpeedRatio\":3,\n"
                + "  \"tachTriggerPercent\":2,\n"
                + "  \"tachType\":\"TachType1\"\n"
                + "}";
        String expResult = "{\"outcome\":\"No Site Tachometers found to delete.\"}";
        String result = instance.deleteSiteTachometers(content);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of deleteTachometers method, of class SiteTachometersController.
     * Unhappy path: Passing a  null input json. 
     */
    @Test
    public void test53_DeleteSiteTachometers() {
        System.out.println("deleteSiteTachometers");
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.deleteSiteTachometers(null);
        System.out.println("result 53 - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of deleteTachometers method, of class SiteTachometersController.
     * Unhappy path: Passing an invalid tachId in input json. 
     */
    @Test
    public void test54_DeleteSiteTachometers() {
        System.out.println("deleteSiteTachometers");
        content = "{\n"
                + "  \"customerAccount\":\"customer_001\",\n"
                + "  \"tachName\":\"TachName1\",\n"
                + "  \"siteId\": \"54964cf6-cc29-4796-aa27-17b6ce01a75f\",\n"
               // + "  \"tachId\": \"f9dbaf11-e394-418a-ad91-381700ee0850\",\n"
                + "  \"tachHardwareUnitChannel\":1,\n"
                + "  \"tachHardwareUnitSerialNumber\":\"Serial1\",\n"
                + "  \"tachMaxRpm\":4,\n"
                + "  \"tachMinRpm\":1,\n"
                + "  \"tachPulsesPerRev\":2,\n"
                + "  \"tachReferenceSpeed\":3,\n"
                + "  \"tachReferenceUnits\":\"FPM\",\n"
                + "  \"tachRisingEdge\":true,\n"
                + "  \"tachSpeedRatio\":3,\n"
                + "  \"tachTriggerPercent\":2,\n"
                + "  \"tachType\":\"TachType1\"\n"
                + "}";
        String expResult = "{\"outcome\":\"Insufficient data given in json\"}";
        String result = instance.deleteSiteTachometers(content);
        System.out.println("result 54 - " + result);
        assertEquals(expResult, result);
    }

}
