/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.controller;

import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 *
 * @author venky
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GlobalAssetPresetControllerTest {

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//presets.cql", true, true, "worldview_dev1"));
    private static GlobalAssetPresetController instance;
    private static String content;
    private static String insuffDataPayload;

    public GlobalAssetPresetControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        instance = new GlobalAssetPresetController();

        content = "[{\n"
                + "\"customerAccount\": \"77777\",\n"
                + "\"assetTypeName\":\"Test Asset Type\",\n"
                + "\"assetPresetId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n"
                + "\"pointLocationName\":\"Point Location Name\",\n"
                + "\"assetPresetName\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\n"
                + "\"devicePresetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\n"
                + "\"devicePresetType\":\"MistLx\",\n"
                + "\"sampleInterval\":30,\n"
                + "\"ffSetIds\":\"Test ffSEt Ids\",\n"
                + "\"rollDiameter\":\"10\",\n"
                + "\"rollDiameterUnits\":Test,\n"
                + "\"speedRatio\":0.55,\n"
                + "\"tachId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n"
                + "\"description\":\"Celcius\"\n"
                + "}]";

        insuffDataPayload = "[{\n"
                + "\"customerAccount\": \"77777\",\n"
                + "\"assetPresetId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n"
                + "\"pointLocationName\":\"Point Location Name\",\n"
                + "\"assetPresetName\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\n"
                + "\"devicePresetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\n"
                + "\"devicePresetType\":\"MistLx\",\n"
                + "\"sampleInterval\":30,\n"
                + "\"ffSetIds\":\"Test ffSEt Ids\",\n"
                + "\"rollDiameter\":\"10\",\n"
                + "\"rollDiameterUnits\":Test,\n"
                + "\"speedRatio\":0.55,\n"
                + "\"tachId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n"
                + "\"description\":\"Celcius\"\n"
                + "}]";
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of createGlobalAssetPreset method, of class
     * GlobalAssetPresetController.
     */
    @Test
    public void test10_CreateGlobalAssetPreset() {
        System.out.println("createGlobalAssetPreset");
        String expResult = "{\"outcome\":\"New Global Asset Preset created successfully.\"}";
        String result = instance.createGlobalAssetPreset(content);
        assertEquals(expResult, result);
        System.out.println("createGlobalAssetPreset Done");
    }

    /**
     * Test of createGlobalAssetPreset method, of class
     * GlobalAssetPresetController.
     */
    @Test
    public void test11_CreateGlobalAssetPreset() {
        System.out.println("createGlobalAssetPreset");
        String expResult = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        String result = instance.createGlobalAssetPreset(insuffDataPayload);
        assertEquals(expResult, result);
        System.out.println("createGlobalAssetPreset Done");
    }

    /**
     * Test of getByPK method, of class GlobalAssetPresetController.
     */
    @Test
    public void test21_GetByPK() throws Exception {
        System.out.println("getByPK");
        String customerAccount = "77777";
        String assetTypeName = "Test Asset Type";
        String assetPresetId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        String pointLocationName = "Point Location Name";
        String expResult = "{\"customerAccount\":\"77777\",\"assetTypeName\":\"Test Asset Type\",\"assetPresetId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"pointLocationName\":\"Point Location Name\",\"GlobalAssetPreset\":[{\"customerAccount\":\"77777\",\"assetTypeName\":\"Test Asset Type\",\"assetPresetId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"pointLocationName\":\"Point Location Name\",\"assetPresetName\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\"devicePresetType\":\"MistLx\",\"devicePresetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\"sampleInterval\":30,\"ffSetIds\":\"Test ffSEt Ids\",\"rollDiameter\":10.0,\"rollDiameterUnits\":\"Test\",\"speedRatio\":0.55,\"tachId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"description\":\"Celcius\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByPK(customerAccount, assetTypeName, assetPresetId, pointLocationName);
        assertEquals(expResult, result);
        System.out.println("getByPK Done");
    }

    /**
     * Test of getByPK method, of class GlobalAssetPresetController.
     */
    @Test
    public void test22_GetByPK() throws Exception {
        System.out.println("getByPK");
        String customerAccount = "77777";
        String assetTypeName = "Test Asset Type";
        String assetPresetId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        String pointLocationName = "Point Location Name";
        String expResult = "{\"customerAccount\":\"77777\",\"assetTypeName\":\"Test Asset Type\",\"assetPresetId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"pointLocationName\":\"Point Location Name\",\"GlobalAssetPreset\":[{\"customerAccount\":\"77777\",\"assetTypeName\":\"Test Asset Type\",\"assetPresetId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"pointLocationName\":\"Point Location Name\",\"assetPresetName\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\"devicePresetType\":\"MistLx\",\"devicePresetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\"sampleInterval\":30,\"ffSetIds\":\"Test ffSEt Ids\",\"rollDiameter\":10.0,\"rollDiameterUnits\":\"Test\",\"speedRatio\":0.55,\"tachId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"description\":\"Celcius\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByPK(customerAccount, assetTypeName, assetPresetId, pointLocationName);
        assertEquals(expResult, result);
        System.out.println("getByPK Done");
    }

    /**
     * Test of getByCustomerAssetTypePresetId method, of class
     * GlobalAssetPresetController.
     */
    @Test
    public void test23_GetByCustomerGlobalAssetTypePresetId() throws Exception {
        System.out.println("getByCustomerGlobalAssetTypePresetId");
        String customerAccount = "77777";
        String assetTypeName = "Test Asset Type";
        String assetPresetId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        String expResult = "{\"customerAccount\":\"77777\",\"assetTypeName\":\"Test Asset Type\",\"assetPresetId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"GlobalAssetPreset\":[{\"customerAccount\":\"77777\",\"assetTypeName\":\"Test Asset Type\",\"assetPresetId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"pointLocationName\":\"Point Location Name\",\"assetPresetName\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\"devicePresetType\":\"MistLx\",\"devicePresetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\"sampleInterval\":30,\"ffSetIds\":\"Test ffSEt Ids\",\"rollDiameter\":10.0,\"rollDiameterUnits\":\"Test\",\"speedRatio\":0.55,\"tachId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"description\":\"Celcius\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByCustomerAssetTypePresetId(customerAccount, assetTypeName, assetPresetId);
        assertEquals(expResult, result);
        System.out.println("getByCustomerGlobalAssetTypePresetId Done");
    }

    /**
     * Test of getByCustomerAssetTypePresetId method, of class
     * GlobalAssetPresetController.
     */
    @Test
    public void test24_GetByCustomerGlobalAssetTypePresetId() throws Exception {
        System.out.println("getByCustomerGlobalAssetTypePresetId");
        String customerAccount = "77777";
        String assetTypeName = "Test Asset Type";
        String assetPresetId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        String expResult = "{\"customerAccount\":\"77777\",\"assetTypeName\":\"Test Asset Type\",\"assetPresetId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"GlobalAssetPreset\":[{\"customerAccount\":\"77777\",\"assetTypeName\":\"Test Asset Type\",\"assetPresetId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"pointLocationName\":\"Point Location Name\",\"assetPresetName\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\"devicePresetType\":\"MistLx\",\"devicePresetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\"sampleInterval\":30,\"ffSetIds\":\"Test ffSEt Ids\",\"rollDiameter\":10.0,\"rollDiameterUnits\":\"Test\",\"speedRatio\":0.55,\"tachId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"description\":\"Celcius\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByCustomerAssetTypePresetId(customerAccount, assetTypeName, assetPresetId);
        assertEquals(expResult, result);
        System.out.println("getByCustomerGlobalAssetTypePresetId Done");
    }

    /**
     * Test of getByCustomerGlobalAssetType method, of class
     * GlobalAssetPresetController.
     */
    @Test
    public void test25_GetByCustomerGlobalAssetType() throws Exception {
        System.out.println("getByCustomerGlobalAssetType");
        String customerAccount = "77777";
        String assetTypeName = "Test Asset Type";
        String expResult = "{\"customerAccount\":\"77777\",\"assetTypeName\":\"Test Asset Type\",\"GlobalAssetPreset\":[{\"customerAccount\":\"77777\",\"assetTypeName\":\"Test Asset Type\",\"assetPresetId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"pointLocationName\":\"Point Location Name\",\"assetPresetName\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\"devicePresetType\":\"MistLx\",\"devicePresetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\"sampleInterval\":30,\"ffSetIds\":\"Test ffSEt Ids\",\"rollDiameter\":10.0,\"rollDiameterUnits\":\"Test\",\"speedRatio\":0.55,\"tachId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"description\":\"Celcius\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByCustomerAssetType(customerAccount, assetTypeName);
        assertEquals(expResult, result);
        System.out.println("getByCustomerGlobalAssetType**** " + result);
    }

    /**
     * Test of getByCustomerGlobalAssetType method, of class
     * GlobalAssetPresetController.
     */
    @Test
    public void test26_GetByCustomerGlobalAssetType() throws Exception {
        System.out.println("getByCustomerGlobalAssetType");
        String customerAccount = "77777";
        String assetTypeName = "Test Asset Type";
        String expResult = "{\"customerAccount\":\"77777\",\"assetTypeName\":\"Test Asset Type\",\"GlobalAssetPreset\":[{\"customerAccount\":\"77777\",\"assetTypeName\":\"Test Asset Type\",\"assetPresetId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"pointLocationName\":\"Point Location Name\",\"assetPresetName\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\"devicePresetType\":\"MistLx\",\"devicePresetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\"sampleInterval\":30,\"ffSetIds\":\"Test ffSEt Ids\",\"rollDiameter\":10.0,\"rollDiameterUnits\":\"Test\",\"speedRatio\":0.55,\"tachId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"description\":\"Celcius\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByCustomerAssetType(customerAccount, assetTypeName);
        assertEquals(expResult, result);
        System.out.println("getByCustomerGlobalAssetType**** " + result);
    }

    /**
     * Test of getByCustomerGlobalAssetType method, of class
     * GlobalAssetPresetController.
     */
    @Test
    public void test27_GetByCustomer() throws Exception {
        System.out.println("getByCustomer");
        String customerAccount = "77777";
        String expResult = "{\"customerAccount\":\"77777\",\"GlobalAssetPreset\":[{\"customerAccount\":\"77777\",\"assetTypeName\":\"Test Asset Type\",\"assetPresetId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"pointLocationName\":\"Point Location Name\",\"assetPresetName\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\"devicePresetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\"devicePresetType\":\"MistLx\",\"sampleInterval\":30,\"ffSetIds\":\"Test ffSEt Ids\",\"rollDiameter\":10.0,\"rollDiameterUnits\":\"Test\",\"speedRatio\":0.55,\"tachId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"description\":\"Celcius\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByCustomer(customerAccount);
        assertEquals(expResult, result);
        System.out.println("getByCustomer**** " + result);
    }

    /**
     * Test of getByCustomer method, of class GlobalAssetPresetController.
     */
    @Test
    public void test28_GetByCustomer() throws Exception {
        System.out.println("getByCustomer");
        String customerAccount = "77777";
        String expResult = "{\"customerAccount\":\"77777\",\"assetTypeName\":\"Test Asset Type\",\"GlobalAssetPreset\":[{\"customerAccount\":\"77777\",\"assetTypeName\":\"Test Asset Type\",\"assetPresetId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"pointLocationName\":\"Point Location Name\",\"assetPresetName\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\"devicePresetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\"alarmEnabled\":true,\"sampleInterval\":30,\"ffSetIds\":\"Test ffSEt Ids\",\"rollDiameter\":10.0,\"rollDiameterUnits\":\"Test\",\"speedRatio\":0.55,\"tachId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"description\":\"Celcius\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByCustomer(customerAccount);
        assertNotEquals(expResult, result);
        System.out.println("getByCustomer**** " + result);
    }

    /**
     * Test of updateGlobalAssetPreset method, of class
     * GlobalAssetPresetController.
     */
    @Test
    public void test31_UpdateGlobalAssetPreset() {
        System.out.println("updateGlobalAssetPreset");
        String expResult = "{\"outcome\":\"Updated GlobalAssetPreset successfully.\"}";
        String result = instance.updateGlobalAssetPreset(content);
        assertEquals(expResult, result);
        System.out.println("updateGlobalAssetPreset");
    }

    /**
     * Test of updateGlobalAssetPreset method, of class
     * GlobalAssetPresetController.
     */
    @Test
    public void test32_UpdateGlobalAssetPreset() {
        System.out.println("updateGlobalAssetPreset");
        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.updateGlobalAssetPreset(insuffDataPayload);
        assertEquals(expResult, result);
        System.out.println("updateGlobalAssetPreset");
    }

    /**
     * Test of deleteGlobalAssetPreset method, of class
     * GlobalAssetPresetController.
     */
    @Test
    public void test41_DeleteGlobalAssetPreset() {
        System.out.println("deleteGlobalAssetPreset");
        String expResult = "{\"outcome\":\"Deleted Global Asset Preset successfully.\"}";
        String result = instance.deleteGlobalAssetPreset(content);
        assertEquals(expResult, result);
        System.out.println("deleteGlobalAssetPreset Done");
    }

    /**
     * Test of deleteGlobalAssetPreset method, of class
     * GlobalAssetPresetController.
     */
    @Test
    public void test42_DeleteGlobalAssetPreset() {
        System.out.println("deleteGlobalAssetPreset");
        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.deleteGlobalAssetPreset(insuffDataPayload);
        assertEquals(expResult, result);
        System.out.println("deleteGlobalAssetPreset Done");
    }
}
