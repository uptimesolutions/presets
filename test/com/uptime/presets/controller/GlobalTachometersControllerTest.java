/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.presets.controller;

import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author Joseph
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GlobalTachometersControllerTest {
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//presets.cql", true, true, "worldview_dev1"));
    private static GlobalTachometersController instance;
    private static String content;
    public GlobalTachometersControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = new GlobalTachometersController();
        content = "{\n" +
            "  \"customerAccount\":\"customer_001\",\n" +
            "  \"tachName\":\"TachName1\",\n" +
            "  \"tachMaxRpm\":4,\n" +
            "  \"tachMinRpm\":1,\n" +
            "  \"tachId\": \"f9dbaf10-e394-418a-ad91-381700ee0850\",\n" +
            "  \"tachPulsesPerRev\":2,\n" +
            "  \"tachReferenceSpeed\":3,\n" +
            "  \"tachReferenceUnits\":\"FPM\",\n" +
            "  \"tachRisingEdge\":true,\n" +
            "  \"tachSpeedRatio\":3,\n" +
            "  \"tachTriggerPercent\":2,\n" +
            "  \"tachType\":\"TachType1\"\n" +
            "}";
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of createGlobalTachometers method, of class GlobalTachometersController.
     */
    @Test
    public void test10_CreateGlobalTachometers() {
        System.out.println("createGlobalTachometers");       
        String expResult = "{\"outcome\":\"New GlobalTachometers created successfully.\"}";
        String result = instance.createGlobalTachometers(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of createGlobalTachometers method, of class GlobalTachometersController.
     * Unhappy path: Passing an invalid input json with missing tachName. 
     */
    @Test
    public void test12_CreateGlobalTachometers() {
        System.out.println("createGlobalTachometers");    
        content = "{\n" +
            "  \"customerAccount\":\"customer_001\",\n" +
            //"  \"tachName\":\"TachName1\",\n" +
            "  \"tachMaxRpm\":4,\n" +
            "  \"tachMinRpm\":1,\n" +
            "  \"tachId\": \"f9dbaf10-e394-418a-ad91-381700ee0850\",\n" +
            "  \"tachPulsesPerRev\":2,\n" +
            "  \"tachReferenceSpeed\":3,\n" +
            "  \"tachRisingEdge\":true,\n" +
            "  \"tachSpeedRatio\":3,\n" +
            "  \"tachTriggerPercent\":2,\n" +
            "  \"tachType\":\"TachType1\"\n" +
            "}";
        String expResult = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        String result = instance.createGlobalTachometers(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of createGlobalTachometers method, of class GlobalTachometersController.
     * Unhappy path: Passing null as input json. 
     */
    @Test
    public void test13_CreateGlobalTachometers() {
        System.out.println("createGlobalTachometers");    
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.createGlobalTachometers(null);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateGlobalTachometers method, of class GlobalTachometersController.
     */
    @Test
    public void test20_UpdateGlobalTachometers() {
        System.out.println("updateGlobalTachometers");
        content = "{\n" +
            "  \"customerAccount\":\"customer_001\",\n" +
            "  \"tachName\":\"TachName1\",\n" +
            "  \"tachMaxRpm\":4,\n" +
            "  \"tachMinRpm\":1,\n" +
            "  \"tachId\": \"f9dbaf10-e394-418a-ad91-381700ee0850\",\n" +
            "  \"tachPulsesPerRev\":2,\n" +
            "  \"tachReferenceSpeed\":3,\n" +
            "  \"tachReferenceUnits\":\"RPM\",\n" +
            "  \"tachRisingEdge\":true,\n" +
            "  \"tachSpeedRatio\":3,\n" +
            "  \"tachTriggerPercent\":2,\n" +
            "  \"tachType\":\"TachType1\"\n" +
            "}";
        String expResult = "{\"outcome\":\"Updated Global Tachometers successfully.\"}";
        String result = instance.updateGlobalTachometers(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateGlobalTachometers method, of class GlobalTachometersController.
     * Unhappy path: Missing required field 'tachId' in input json.
     */
    @Test
    public void test21_UpdateGlobalTachometers() {
        System.out.println("updateGlobalTachometers");
        content = "{\n" +
            "  \"customerAccount\":\"customer_001\",\n" +
            "  \"tachName\":\"TachName1\",\n" +
            "  \"tachMaxRpm\":4,\n" +
            "  \"tachMinRpm\":1,\n" +
//            "  \"tachId\": \"f9dbaf10-e394-418a-ad91-381700ee0850\",\n" +
            "  \"tachPulsesPerRev\":2,\n" +
            "  \"tachReferenceSpeed\":3,\n" +
            "  \"tachReferenceUnits\":\"RPM\",\n" +
            "  \"tachRisingEdge\":true,\n" +
            "  \"tachSpeedRatio\":3,\n" +
            "  \"tachTriggerPercent\":2,\n" +
            "  \"tachType\":\"TachType1\"\n" +
            "}";
        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.updateGlobalTachometers(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateGlobalTachometers method, of class GlobalTachometersController.
     * Unhappy path: Passing empty json.
     */
    @Test
    public void test22_UpdateGlobalTachometers() {
        System.out.println("updateGlobalTachometers");
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.updateGlobalTachometers("");
        assertEquals(expResult, result);
    }

    /**
     * Test of getByPK method, of class GlobalTachometersController.
     */
    @Test
    public void test30_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = "customer_001";
        String tachId = "f9dbaf10-e394-418a-ad91-381700ee0850";       
        String expResult = "{\"customerAccount\":\"customer_001\",\"tachId\":\"f9dbaf10-e394-418a-ad91-381700ee0850\",\"GlobalTachometers\":[{\"customerAccount\":\"customer_001\",\"tachId\":\"f9dbaf10-e394-418a-ad91-381700ee0850\",\"tachName\":\"TachName1\",\"tachReferenceSpeed\":3.0,\"tachReferenceUnits\":\"RPM\",\"tachType\":\"TachType1\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByPK(customerAccount, tachId);
        System.out.println("result 30 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByPK method, of class GlobalTachometersController.
     * Unhappy path: When customerAccount is passed is not available in DB, no data is received from the database.
     * @throws java.lang.Exception
     */
    @Test
    public void test31_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = "customer_002";
        String tachId = "f9dbaf10-e394-418a-ad91-381700ee0850";       
        String expResult = "{\"outcome\":\"Global Tachometers not found.\"}";
        String result = instance.getByPK(customerAccount, tachId);
        System.out.println("result 31 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByPK method, of class GlobalTachometersController.
     * Unhappy path: When customer is passed as null
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test32_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = null;
        String tachId = "f9dbaf10-e394-418a-ad91-381700ee0850";       
        fail(instance.getByPK(customerAccount, tachId));
    }

    /**
     * Test of getByPK method, of class GlobalTachometersController.
     * Unhappy path: When siteId is passed as null
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test33_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = "customer_001";
        String tachId = null;       
        fail(instance.getByPK(customerAccount, tachId));
    }

    /**
     * Test of getByCustomer method, of class GlobalTachometersController.
     */
    @Test
    public void test40_FindByCustomer() throws Exception {
        System.out.println("findByCustomer");
        String customerAccount = "customer_001";
        String expResult = "{\"customerAccount\":\"customer_001\",\"GlobalTachometers\":[{\"customerAccount\":\"customer_001\",\"tachId\":\"f9dbaf10-e394-418a-ad91-381700ee0850\",\"tachName\":\"TachName1\",\"tachReferenceSpeed\":3.0,\"tachReferenceUnits\":\"RPM\",\"tachType\":\"TachType1\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByCustomer(customerAccount);
        System.out.println("result 40 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByCustomer method, of class GlobalTachometersController.
     * Unhappy path: Controller throws IllegalArgumentException when customerAccount is passed as "null".
     * @throws java.lang.Exception 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test41_FindByCustomer() throws Exception {
        System.out.println("findByCustomer");
        String customerAccount = null;
        fail(instance.getByCustomer(customerAccount));
    }

    /**
     * Test of getByCustomer method, of class GlobalTachometersController.
     * Unhappy path: Passing a customerAccount that does not exist in DB
     * @throws java.lang.Exception
     */
    @Test
    public void test42_FindByCustomer() throws Exception {
        System.out.println("findByCustomer");
        String customerAccount = "customer_002";
        String expResult = "{\"outcome\":\"Global Tachometers not found.\"}";
        String result = instance.getByCustomer(customerAccount);
        System.out.println("result 42 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteGlobalTachometers method, of class GlobalTachometersController.
     */
    @Test
    public void test50_DeleteGlobalTachometers() {
        System.out.println("deleteGlobalTachometers");
        content = "{\n" +
            "  \"customerAccount\":\"customer_001\",\n" +
            "  \"tachName\":\"TachName1\",\n" +
            "  \"tachMaxRpm\":4,\n" +
            "  \"tachMinRpm\":1,\n" +
            "  \"tachId\": \"f9dbaf10-e394-418a-ad91-381700ee0850\",\n" +
            "  \"tachPulsesPerRev\":2,\n" +
            "  \"tachReferenceSpeed\":3,\n" +
            "  \"tachReferenceUnits\":\"RPM\",\n" +
            "  \"tachRisingEdge\":true,\n" +
            "  \"tachSpeedRatio\":3,\n" +
            "  \"tachTriggerPercent\":2,\n" +
            "  \"tachType\":\"TachType1\"\n" +
            "}";
        String expResult = "{\"outcome\":\"Deleted Global Tachometers successfully.\"}";
        String result = instance.deleteGlobalTachometers(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteGlobalTachometers method, of class GlobalTachometersController.
     * Unhappy path: Missing required field 'tachId' in input json. 
     */
    @Test
    public void test51_DeleteGlobalTachometers() {
        System.out.println("deleteGlobalTachometers");
        content = "{\n" +
            "  \"customerAccount\":\"customer_001\",\n" +
            "  \"tachName\":\"TachName1\",\n" +
            "  \"tachMaxRpm\":4,\n" +
            "  \"tachMinRpm\":1,\n" +
            //"  \"tachId\": \"f9dbaf10-e394-418a-ad91-381700ee0850\",\n" +
            "  \"tachPulsesPerRev\":2,\n" +
            "  \"tachReferenceSpeed\":3,\n" +
            "  \"tachReferenceUnits\":\"RPM\",\n" +
            "  \"tachRisingEdge\":true,\n" +
            "  \"tachSpeedRatio\":3,\n" +
            "  \"tachTriggerPercent\":2,\n" +
            "  \"tachType\":\"TachType1\"\n" +
            "}";
        String expResult = "{\"outcome\":\"Insufficient data given in json\"}";
        String result = instance.deleteGlobalTachometers(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteGlobalTachometers method, of class GlobalTachometersController.
     * Unhappy path: Passing a  'tachId' in input json that does not exist. 
     */
    @Test
    public void test52_DeleteGlobalTachometers() {
        System.out.println("deleteGlobalTachometers");
        content = "{\n" +
            "  \"customerAccount\":\"customer_001\",\n" +
            "  \"tachName\":\"TachName1\",\n" +
            "  \"tachMaxRpm\":4,\n" +
            "  \"tachMinRpm\":1,\n" +
            "  \"tachId\": \"f9dbaf10-e394-418a-ad91-381700ee0878\",\n" +
            "  \"tachPulsesPerRev\":2,\n" +
            "  \"tachReferenceSpeed\":3,\n" +
            "  \"tachReferenceUnits\":\"RPM\",\n" +
            "  \"tachRisingEdge\":true,\n" +
            "  \"tachSpeedRatio\":3,\n" +
            "  \"tachTriggerPercent\":2,\n" +
            "  \"tachType\":\"TachType1\"\n" +
            "}";
        String expResult = "{\"outcome\":\"No Global Tachometers found to delete.\"}";
        String result = instance.deleteGlobalTachometers(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteGlobalTachometers method, of class GlobalTachometersController.
     * Unhappy path: Passing a  null input json. 
     */
    @Test
    public void test53_DeleteGlobalTachometers() {
        System.out.println("deleteGlobalTachometers");
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.deleteGlobalTachometers(null);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteGlobalTachometers method, of class GlobalTachometersController.
     * Unhappy path: Passing an invalid tachId in input json.
     */
    @Test
    public void test54_DeleteGlobalTachometers() {
        System.out.println("deleteGlobalTachometers");
        content = "{\n" +
            "  \"customerAccount\":\"customer_001\",\n" +
            "  \"tachName\":\"TachName1\",\n" +
            "  \"tachMaxRpm\":4,\n" +
            "  \"tachMinRpm\":1,\n" +
            "  \"tachId\": \"f9dbaf10-e394-418a-a\",\n" +
            "  \"tachPulsesPerRev\":2,\n" +
            "  \"tachReferenceSpeed\":3,\n" +
            "  \"tachReferenceUnits\":\"RPM\",\n" +
            "  \"tachRisingEdge\":true,\n" +
            "  \"tachSpeedRatio\":3,\n" +
            "  \"tachTriggerPercent\":2,\n" +
            "  \"tachType\":\"TachType1\"\n" +
            "}";
        String expResult = "{\"outcome\":\"Insufficient data given in json\"}";
        String result = instance.deleteGlobalTachometers(content);
        assertEquals(expResult, result);
    }
    
}
