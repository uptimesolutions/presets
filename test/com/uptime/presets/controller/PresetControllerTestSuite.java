/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.presets.controller;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author madhavi
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
    BearingCatalogControllerTest.class,
    CustomerApSetControllerTest.class,
    GlobalApAlSetsControllerTest.class,
    GlobalDevicePresetControllerTest.class,
    GlobalFaultFrequenciesControllerTest.class,
    GlobalPtLocationNamesControllerTest.class,
    GlobalTachometersControllerTest.class,
    KafkaTachometersControllerTest.class,
    SiteApAlSetsControllerTest.class,
    SiteBearingFavoritesControllerTest.class,
    SiteDevicePresetControllerTest.class,
    SiteFFSetFavoritesControllerTest.class,
    SiteFaultFrequenciesControllerTest.class,
    SitePtLocationNamesControllerTest.class,
    SiteTachometersControllerTest.class,
    // Add more test classes here
})
public class PresetControllerTestSuite {
    // This class doesn't need to have any code
}
