/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.presets.controller;

import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author Joseph
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GlobalFaultFrequenciesControllerTest {

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//presets.cql", true, true, "worldview_dev1"));
    private static GlobalFaultFrequenciesController instance;
    private static String content;

    public GlobalFaultFrequenciesControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        instance = new GlobalFaultFrequenciesController();
        content = "[\n"
                + "	{\n"
                + "		\"customerAccount\": \"77777\",\n"
                + "		\"ffSetName\": \"set 89 global \",\n"
                + "		\"ffName\": \"set 89 f1 GLB\",\n"
                + "		\"ffSetId\": \"b026aa07-3e5c-4c24-b24e-0dc01779375e\",\n"
                + "		\"ffId\": \"f9dbaf10-e394-418a-ad91-381700ee0850\",\n"
                + "		\"ffSetDesc\": \"set 89 global desc\",\n"
                + "		\"ffValue\": 0.0,\n"
                + "		\"ffUnit\": \"Hz\"\n"
                + "	}\n"
                + "]";
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of createGlobalFaultFrequencies method, of class
     * GlobalFaultFrequenciesController.
     */
    @Test
    public void test10_CreateGlobalFaultFrequencies() {
        System.out.println("createGlobalFaultFrequencies");
        String expResult = "{\"outcome\":\"New Global Fault Frequencies created successfully.\"}";
        String result = instance.createGlobalFaultFrequencies(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of createGlobalFaultFrequencies method, of class
     * GlobalFaultFrequenciesController.
     */
    @Test
    public void test11_CreateGlobalFaultFrequencies() {
        System.out.println("createGlobalFaultFrequencies");
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.createGlobalFaultFrequencies(null);
        assertEquals(expResult, result);
    }

    /**
     * Test of createGlobalFaultFrequencies method, of class
     * GlobalFaultFrequenciesController.
     */
    @Test
    public void test12_CreateGlobalFaultFrequencies() {
        System.out.println("createGlobalFaultFrequencies");
        String contentJson = "[\n"
                + "	{\n"
                + "		\"customerAccount\": \"77777\",\n"
                + "		\"ffSetName\": \"set 89 global \",\n"
                + // "		\"ffName\": \"set 89 f1 GLB\",\n" +
                "		\"ffSetId\": \"b026aa07-3e5c-4c24-b24e-0dc01779375e\",\n"
                + "		\"ffId\": \"f9dbaf10-e394-418a-ad91-381700ee0850\",\n"
                + "		\"ffSetDesc\": \"set 89 global desc\",\n"
                + "		\"ffValue\": 0.0,\n"
                + "		\"ffUnit\": \"Hz\"\n"
                + "	}\n"
                + "]";
        String expResult = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        String result = instance.createGlobalFaultFrequencies(contentJson);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateGlobalFaultFrequencies method, of class
     * GlobalFaultFrequenciesController.
     */
    @Test
    public void test20_UpdateGlobalFaultFrequencies() {
        System.out.println("updateGlobalFaultFrequencies");
        String expResult = "{\"outcome\":\"Updated Global Fault Frequencies successfully.\"}";
        String result = instance.updateGlobalFaultFrequencies(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateGlobalFaultFrequencies method, of class
     * GlobalFaultFrequenciesController.
     */
    @Test
    public void test21_UpdateGlobalFaultFrequencies() {
        System.out.println("updateGlobalFaultFrequencies");
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.updateGlobalFaultFrequencies(null);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateGlobalFaultFrequencies method, of class
     * GlobalFaultFrequenciesController.
     */
    @Test
    public void test22_UpdateGlobalFaultFrequencies() {
        System.out.println("updateGlobalFaultFrequencies");
        String contentJSON = "[\n"
                + "	{\n"
                + "		\"customerAccount\": \"77777\",\n"
                + "		\"ffSetName\": \"set 89 global \",\n"
                + "		\"ffName\": \"set 89 f1 GLB\",\n"
                // + "		\"ffSetId\": \"b026aa07-3e5c-4c24-b24e-0dc01779375e\",\n"
                + "		\"ffId\": \"f9dbaf10-e394-418a-ad91-381700ee0850\",\n"
                + "		\"ffSetDesc\": \"set 89 global desc\",\n"
                + "		\"ffValue\": 0.0,\n"
                + "		\"ffUnit\": \"Hz\"\n"
                + "	}\n"
                + "]";
        String expResult = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        String result = instance.updateGlobalFaultFrequencies(contentJSON);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByPK method, of class GlobalFaultFrequenciesController.
     * @throws java.lang.Exception
     */
    @Test
    public void test30_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = "77777";
        String ffSetId = "b026aa07-3e5c-4c24-b24e-0dc01779375e";
        String ffId = "f9dbaf10-e394-418a-ad91-381700ee0850";
        String expResult = "{\"customerAccount\":\"77777\",\"ffSetId\":\"b026aa07-3e5c-4c24-b24e-0dc01779375e\",\"ffId\":\"f9dbaf10-e394-418a-ad91-381700ee0850\",\"GlobalFaultFrequencies\":[{\"customerAccount\":\"77777\",\"ffSetId\":\"b026aa07-3e5c-4c24-b24e-0dc01779375e\",\"ffId\":\"f9dbaf10-e394-418a-ad91-381700ee0850\",\"ffSetName\":\"set 89 global \",\"ffName\":\"set 89 f1 GLB\",\"ffSetDesc\":\"set 89 global desc\",\"ffValue\":0.0,\"ffUnit\":\"Hz\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByPK(customerAccount, ffSetId, ffId);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByPK method, of class GlobalFaultFrequenciesController.
     * @throws java.lang.Exception
     */
    @Test
    public void test31_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = "77788";
        String ffSetId = "b026aa07-3e5c-4c24-b24e-0dc01779375e";
        String ffId = "f9dbaf10-e394-418a-ad91-381700ee0850";
        String expResult = "{\"outcome\":\"Global Fault Frequencies not found.\"}";
        String result = instance.getByPK(customerAccount, ffSetId, ffId);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByPK method, of class GlobalFaultFrequenciesController.
     * @throws java.lang.Exception
     */
    @Test
    public void test32_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = null;
        String ffSetId = "b026aa07-3e5c-4c24-b24e-0dc01779375e";
        String ffId = "f9dbaf10-e394-418a-ad91-381700ee0850";
        String expResult = "Argument customerAccount cannot be null or empty.";
        // String result = instance.getByPK(customerAccount, ffSetId, ffId);
        String expMsg = "";
        try {
            instance.getByPK(customerAccount, ffSetId, ffId);
        } catch (Exception e) {
            expMsg = e.getMessage();
            System.out.println("Exception e - " + expMsg);
        }
        assertEquals(expResult, expMsg);
    }

    /**
     * Test of getByPK method, of class GlobalFaultFrequenciesController.
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test33_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = "77777";
        String ffSetId = null;
        String ffId = "f9dbaf10-e394-418a-ad91-381700ee0850";
        fail(instance.getByPK(customerAccount, ffSetId, ffId));
    }

    /**
     * Test of getByPK method, of class GlobalFaultFrequenciesController.
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test34_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = "77777";
        String ffSetId = "b026aa07-3e5c-4c24-b24e-0dc01779375e";
        String ffId = null;
        fail(instance.getByPK(customerAccount, ffSetId, ffId));
    }

    /**
     * Test of getByCustomerSetId method, of class
     * GlobalFaultFrequenciesController.
     * @throws java.lang.Exception
     */
    @Test
    public void test40_FindByCustomerSetId() throws Exception {
        System.out.println("findByCustomerSetId");
        String customerAccount = "77777";
        String ffSetId = "b026aa07-3e5c-4c24-b24e-0dc01779375e";
        String expResult = "{\"customerAccount\":\"77777\",\"ffSetId\":\"b026aa07-3e5c-4c24-b24e-0dc01779375e\",\"GlobalFaultFrequencies\":[{\"customerAccount\":\"77777\",\"ffSetId\":\"b026aa07-3e5c-4c24-b24e-0dc01779375e\",\"ffId\":\"f9dbaf10-e394-418a-ad91-381700ee0850\",\"ffSetName\":\"set 89 global \",\"ffName\":\"set 89 f1 GLB\",\"ffSetDesc\":\"set 89 global desc\",\"ffValue\":0.0,\"ffUnit\":\"Hz\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByCustomerSetId(customerAccount, ffSetId);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByCustomerSetId method, of class
     * GlobalFaultFrequenciesController.
     * @throws java.lang.Exception
     */
    @Test
    public void test41_FindByCustomerSetId() throws Exception {
        System.out.println("findByCustomerSetId");
        String customerAccount = "77789";
        String ffSetId = "b026aa07-3e5c-4c24-b24e-0dc01779375e";
        String expResult = "{\"outcome\":\"Global Fault Frequencies not found.\"}";
        String result = instance.getByCustomerSetId(customerAccount, ffSetId);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByCustomerSetId method, of class
     * GlobalFaultFrequenciesController.
     * @throws java.lang.Exception
     */
    @Test
    public void test42_FindByCustomerSetId() throws Exception {
        System.out.println("findByCustomerSetId");
        String customerAccount = null;
        String ffSetId = "b026aa07-3e5c-4c24-b24e-0dc01779375e";
        String expResult = "Argument customerAccount cannot be null or empty.";
        String  expMsg = "";
        try {
            instance.getByCustomerSetId(customerAccount, ffSetId);
        } catch (Exception e) {
            expMsg = e.getMessage();
            System.out.println("Exception e - " + expMsg);
        }
        assertEquals(expResult, expMsg);
    }

    /**
     * Test of getByCustomerSetId method, of class
     * GlobalFaultFrequenciesController.
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test43_FindByCustomerSetId() throws Exception {
        System.out.println("findByCustomerSetId");
        String customerAccount = "77777";
        String ffSetId = null;
        fail(instance.getByCustomerSetId(customerAccount, ffSetId));
    }

    /**
     * Test of getByCustomer method, of class GlobalFaultFrequenciesController.
     * @throws java.lang.Exception
     */
    @Test
    public void test50_FindByCustomer() throws Exception {
        System.out.println("findByCustomer");
        String customerAccount = "77777";
        String expResult = "{\"customerAccount\":\"77777\",\"GlobalFaultFrequencies\":[{\"customerAccount\":\"77777\",\"ffSetId\":\"b026aa07-3e5c-4c24-b24e-0dc01779375e\",\"ffId\":\"f9dbaf10-e394-418a-ad91-381700ee0850\",\"ffSetName\":\"set 89 global \",\"ffName\":\"set 89 f1 GLB\",\"ffSetDesc\":\"set 89 global desc\",\"ffValue\":0.0,\"ffUnit\":\"Hz\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByCustomer(customerAccount);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByCustomer method, of class GlobalFaultFrequenciesController.
     * @throws java.lang.Exception
     */
    @Test
    public void test51_FindByCustomer() throws Exception {
        System.out.println("findByCustomer");
        String customerAccount = "77788";
        String expResult = "{\"outcome\":\"Global Fault Frequencies not found.\"}";
        String result = instance.getByCustomer(customerAccount);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByCustomer method, of class GlobalFaultFrequenciesController.
     * @throws java.lang.Exception
     */
    @Test
    public void test52_FindByCustomer() throws Exception {
        System.out.println("findByCustomer");
        String customerAccount = null;
        String expResult = "Argument customerAccount cannot be null or empty.";
        String expMsg = "";
        try {
            instance.getByCustomer(customerAccount);
        } catch (Exception e) {
            expMsg = e.getMessage();
            System.out.println("Exception e - " + expMsg);
        }
        assertEquals(expResult, expMsg);
    }

    /**
     * Test of deleteGlobalFaultFrequencies method, of class
     * GlobalFaultFrequenciesController.
     */
    @Test
    public void test60_DeleteGlobalFaultFrequencies() {
        System.out.println("deleteGlobalFaultFrequencies");
        String expResult = "{\"outcome\":\"Deleted Global Fault Frequencies successfully.\"}";
        String result = instance.deleteGlobalFaultFrequencies(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteGlobalFaultFrequencies method, of class
     * GlobalFaultFrequenciesController.
     */
    @Test
    public void test61_DeleteGlobalFaultFrequencies() {
        System.out.println("deleteGlobalFaultFrequencies");
        content = "[\n"
                + "	{\n"
                + "		\"customerAccount\": \"77788\",\n"
                + "		\"ffSetName\": \"set 89 global \",\n"
                + "		\"ffName\": \"set 89 f1 GLB\",\n"
                + "		\"ffSetId\": \"b026aa07-3e5c-4c24-b24e-0dc01779375e\",\n"
                + "		\"ffId\": \"f9dbaf10-e394-418a-ad91-381700ee0850\",\n"
                + "		\"ffSetDesc\": \"set 89 global desc\",\n"
                + "		\"ffValue\": 0.0,\n"
                + "		\"ffUnit\": \"Hz\"\n"
                + "	}\n"
                + "]";
        String expResult = "{\"outcome\":\"No Global Fault Frequencies found to delete.\"}";
        String result = instance.deleteGlobalFaultFrequencies(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteGlobalFaultFrequencies method, of class
     * GlobalFaultFrequenciesController.
     */
    @Test
    public void test62_DeleteGlobalFaultFrequencies() {
        System.out.println("deleteGlobalFaultFrequencies");
        content = "[\n"
                + "	{\n"
                + "		\"customerAccount\": \"77777\",\n"
                + "		\"ffSetName\": \"set 89 global \",\n"
                + "		\"ffName\": \"set 89 f1 GLB\",\n"
                //  + "		\"ffSetId\": \"b026aa07-3e5c-4c24-b24e-0dc01779375e\",\n"
                //  + "		\"ffId\": \"f9dbaf10-e394-418a-ad91-381700ee0850\",\n"
                + "		\"ffSetDesc\": \"set 89 global desc\",\n"
                + "		\"ffValue\": 0.0,\n"
                + "		\"ffUnit\": \"Hz\"\n"
                + "	}\n"
                + "]";
        String expResult = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        String result = instance.deleteGlobalFaultFrequencies(content);
        assertEquals(expResult, result);
    }

}
