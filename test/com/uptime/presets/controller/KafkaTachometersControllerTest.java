/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.controller;

import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author kpati
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class KafkaTachometersControllerTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//presets.cql", true, true, "worldview_dev1"));
    private static KafkaTachometersController instance;
    private static String content;
    private static String globalcontent;
    
    public KafkaTachometersControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = new KafkaTachometersController();
        content = "{\n"
                + "  \"customerAccount\":\"customer_001\",\n"
                + "  \"tachName\":\"TachName1\",\n"
                + "  \"siteId\": \"54964cf6-cc29-4796-aa27-17b6ce01a75f\",\n"
                + "  \"tachId\": \"f9dbaf10-e394-418a-ad91-381700ee0850\",\n"
                + "  \"tachHardwareUnitChannel\":1,\n"
                + "  \"tachHardwareUnitSerialNumber\":\"Serial1\",\n"
                + "  \"tachMaxRpm\":4,\n"
                + "  \"tachMinRpm\":1,\n"
                + "  \"tachPulsesPerRev\":2,\n"
                + "  \"tachReferenceRpm\":3,\n"
                + "  \"tachRisingEdge\":true,\n"
                + "  \"tachSpeedRatio\":3,\n"
                + "  \"tachTriggerPercent\":2,\n"
                + "  \"tachType\":\"TachType1\"\n"
                + "}";
        globalcontent = "{\n"
                + "  \"customerAccount\":\"customer_001\",\n"
                + "  \"tachName\":\"TachName1\",\n"
                + "  \"siteId\": \"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\n"
                + "  \"tachId\": \"ad3def9a-be09-46a8-8d9f-b729c674b6e9\",\n"
                + "  \"tachHardwareUnitChannel\":1,\n"
                + "  \"tachHardwareUnitSerialNumber\":\"Serial1\",\n"
                + "  \"tachMaxRpm\":4,\n"
                + "  \"tachMinRpm\":1,\n"
                + "  \"tachPulsesPerRev\":2,\n"
                + "  \"tachReferenceRpm\":3,\n"
                + "  \"tachRisingEdge\":true,\n"
                + "  \"tachSpeedRatio\":3,\n"
                + "  \"tachTriggerPercent\":2,\n"
                + "  \"tachType\":\"TachType1\"\n"
                + "}";
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    /**
     * Test of createTachometers method, of class SiteTachometersController.
     */
    @Test
    public void test1_CreateSiteTachometers() {
        System.out.println("createSiteTachometers");
        String expResult = "{\"outcome\":\"New SiteTachometers created successfully.\"}";
        SiteTachometersController siteTachoObj = new SiteTachometersController();
        String result = siteTachoObj.createSiteTachometers(content);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of createTachometers method, of class SiteTachometersController.
     */
    @Test
    public void test2_CreateGlobalTachometers() {
        System.out.println("CreateGlobalTachometers");
        String expResult = "{\"outcome\":\"New GlobalTachometers created successfully.\"}";
        GlobalTachometersController globalTachoObj = new GlobalTachometersController();
        String result = globalTachoObj.createGlobalTachometers(globalcontent);
        assertEquals(expResult, result);
    }
    /**
     * Test of getByPK method, of class KafkaTachometersController.
     * @throws java.lang.Exception
     */
    @Test
    public void test3_GetByPK() throws Exception {
        System.out.println("getByPK");
        String customerAccount = "customer_001";
        String siteId = "54964cf6-cc29-4796-aa27-17b6ce01a75f";
        String tachId = "f9dbaf10-e394-418a-ad91-381700ee0850";
        String expResult = "{\"customerAccount\":\"customer_001\",\"siteId\":\"54964cf6-cc29-4796-aa27-17b6ce01a75f\",\"tachId\":\"f9dbaf10-e394-418a-ad91-381700ee0850\",\"tachName\":\"TachName1\",\"tachReferenceSpeed\":0.0,\"tachType\":\"TachType1\"}";
        String result = instance.getByPK(customerAccount, siteId, tachId);
        System.out.println("test3_GetByPK - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getByPK method, of class KafkaTachometersController.
     * @throws java.lang.Exception
     */
    @Test
    public void test4_GetByPKNotFound() throws Exception {
        System.out.println("GetByPKNotFound");
        String customerAccount = "customer_001";
        String siteId = "8b086e15-371f-4a8d-bedb-c1b67b49be3e";
        String tachId = "f9dbaf10-e394-418a-ad91-381700ee0850";
        String expResult = "";
        String result = instance.getByPK(customerAccount, siteId, tachId);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getByPK method, of class KafkaTachometersController.
     * @throws java.lang.Exception
     */
    @Test
    public void test5_GetByPKGlobalTacho() throws Exception {
        System.out.println("GetByPKGlobalTacho");
        String customerAccount = "customer_001";
        String siteId = "54964cf6-cc29-4796-aa27-17b6ce01a75f";
        String tachId = "ad3def9a-be09-46a8-8d9f-b729c674b6e9";
        String expResult = "{\"customerAccount\":\"customer_001\",\"tachId\":\"ad3def9a-be09-46a8-8d9f-b729c674b6e9\",\"tachName\":\"TachName1\",\"tachReferenceSpeed\":0.0,\"tachType\":\"TachType1\"}";
        String result = instance.getByPK(customerAccount, siteId, tachId);
        System.out.println("test5_GetByPKGlobalTacho - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getByPK method, of class KafkaTachometersController.
     * @throws java.lang.Exception
     */
    @Test
    public void test6_GetByPKGlobalTachoNotFound() throws Exception {
        System.out.println("GetByPKGlobalTachoNotFound");
        String customerAccount = "customer_001";
        String siteId = "8b086e15-371f-4a8d-bedb-c1b67b49be3e";
        String tachId = "f9dbaf10-e394-418a-ad91-381700ee0950";
        String expResult = "";
        String result = instance.getByPK(customerAccount, siteId, tachId);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of delete method, of class KafkaTachometersController.
     * @throws java.lang.Exception
     */
    @Test
    public void test7_deleteSiteTachometer() throws Exception {
        System.out.println("deleteSiteTachometer");
        SiteTachometersController siteTachoObj = new SiteTachometersController();
        String expResult = "{\"outcome\":\"Deleted Site Tachometers successfully.\"}";
        String result = siteTachoObj.deleteSiteTachometers(content);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of delete method, of class KafkaTachometersController.
     * @throws java.lang.Exception
     */
    @Test
    public void test8_deleteGlobalTachometer() throws Exception {
        System.out.println("deleteGlobalTachometer");
        GlobalTachometersController globalTachoObj = new GlobalTachometersController();
        String expResult = "{\"outcome\":\"Deleted Global Tachometers successfully.\"}";
        String result = globalTachoObj.deleteGlobalTachometers(globalcontent);
        assertEquals(expResult, result);
    }

    /**
     * Test of getSiteTachometerByPK method, of class KafkaTachometersController.
     */
//    @Test
//    public void testGetSiteTachometerByPK() throws Exception {
//        System.out.println("getSiteTachometerByPK");
//        String customerAccount = "";
//        String siteId = "";
//        String tachId = "";
//        String expResult = "";
//        String result = instance.getSiteTachometerByPK(customerAccount, siteId, tachId);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getGlobalTachometerByPK method, of class KafkaTachometersController.
//     */
//    @Test
//    public void testGetGlobalTachometerByPK() throws Exception {
//        System.out.println("getGlobalTachometerByPK");
//        String customerAccount = "";
//        String tachId = "";
//        String expResult = "";
//        String result = instance.getGlobalTachometerByPK(customerAccount, tachId);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    
}
