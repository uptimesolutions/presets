/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.controller;

import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GlobalPtLocationNamesControllerTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//presets.cql", true, true, "worldview_dev1"));    
    private static String content = "";
    private static GlobalPtLocationNamesController instance;
    
    public GlobalPtLocationNamesControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = new GlobalPtLocationNamesController();
        content = "{\n" +
                "\"customerAccount\": \"77777\",\n" +
                "\"pointLocationName\":\"pointLocationName Test 1\",\n" +
                "\"description\":\"pointLocationName description Test 1\"\n" +
                "}";
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of createGlobalPtLocationNames method, of class GlobalPtLocationNamesController.
     */
    @Test
    public void test10_CreateGlobalPtLocationNames() {
        System.out.println("createGlobalPtLocationNames");
        String expResult = "{\"outcome\":\"New GlobalPtLocationNames created successfully.\"}";
        String result = instance.createGlobalPtLocationNames(content);
        System.out.println("createGlobalPtLocationNames result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of createGlobalPtLocationNames method, of class GlobalPtLocationNamesController.
     * Unhappy path: Passing an invalid input json with missing pointLocationName. 
     */
    @Test
    public void test12_CreateGlobalPtLocationNames() {
        System.out.println("createGlobalPtLocationNames");
        content = "{\n" +
                "\"customerAccount\": \"77777\",\n" +
                //"\"pointLocationName\":\"pointLocationName Test 1\",\n" +
                "\"description\":\"pointLocationName description Test 1\"\n" +
                "}";
        String expResult = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        String result = instance.createGlobalPtLocationNames(content);
        System.out.println("createGlobalPtLocationNames result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of createGlobalPtLocationNames method, of class GlobalPtLocationNamesController.
     * Unhappy path: Passing null as input json.
     */
    @Test
    public void test13_CreateGlobalPtLocationNames() {
        System.out.println("createGlobalPtLocationNames");
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.createGlobalPtLocationNames(null);
        System.out.println("createGlobalPtLocationNames result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByPK method, of class GlobalPtLocationNamesController.
     * @throws java.lang.Exception
     */
    @Test
    public void test20_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = "77777";
        String pointLocationName = "pointLocationName Test 1";
        String expResult = "{\"customerAccount\":\"77777\",\"pointLocationName\":\"pointLocationName Test 1\",\"GlobalPtLocationNames\":[{\"customerAccount\":\"77777\",\"pointLocationName\":\"pointLocationName Test 1\",\"description\":\"pointLocationName description Test 1\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByPK(customerAccount, pointLocationName);
        System.out.println("findByPK result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByPK method, of class GlobalPtLocationNamesController.
     * Unhappy path: When customerAccount is passed is not available in DB, no data is received from the database.
     * @throws java.lang.Exception
     */
    @Test
    public void test21_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = "77788";
        String pointLocationName = "pointLocationName Test 1";
        String expResult = "{\"outcome\":\"GlobalPtLocationNames not found.\"}";
        String result = instance.getByPK(customerAccount, pointLocationName);
        System.out.println("findByPK result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByPK method, of class GlobalPtLocationNamesController.
     * Unhappy path: When customer is passed as null
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test22_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = null;
        String pointLocationName = "pointLocationName Test 1";
        fail(instance.getByPK(customerAccount, pointLocationName));
    }

    /**
     * Test of getByPK method, of class GlobalPtLocationNamesController.
     * Unhappy path: When pointLocationName is passed as null
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test23_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = "77777";
        String pointLocationName = null;
        String expResult = "{\"customerAccount\":\"77777\",\"pointLocationName\":\"pointLocationName Test 1\",\"GlobalPtLocationNames\":[{\"customerAccount\":\"77777\",\"pointLocationName\":\"pointLocationName Test 1\",\"description\":\"pointLocationName description Test 1\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByPK(customerAccount, pointLocationName);
        System.out.println("findByPK result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByCustomer method, of class GlobalPtLocationNamesController.
     * @throws java.lang.Exception
     */
    @Test
    public void test30_FindByCustomer() throws Exception {
        System.out.println("findByCustomer");
        String customerAccount = "77777";
        String expResult = "{\"customerAccount\":\"77777\",\"GlobalPtLocationNames\":[{\"customerAccount\":\"77777\",\"pointLocationName\":\"pointLocationName Test 1\",\"description\":\"pointLocationName description Test 1\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByCustomer(customerAccount);
        System.out.println("findByCustomer result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByCustomer method, of class GlobalPtLocationNamesController.
     * Unhappy path: Controller throws IllegalArgumentException when customerAccount is passed as "null".
     * @throws java.lang.Exception 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test31_FindByCustomer() throws Exception {
        System.out.println("findByCustomer");
        String customerAccount = null;
        fail(instance.getByCustomer(customerAccount));
    }

    /**
     * Test of getByCustomer method, of class GlobalPtLocationNamesController.
     * Unhappy path: Passing a customerAccount that does not exist in DB
     * @throws java.lang.Exception
     */
    @Test
    public void test32_FindByCustomer() throws Exception {
        System.out.println("findByCustomer");
        String customerAccount = "77788";
        String expResult = "{\"outcome\":\"GlobalPtLocationNames not found.\"}";
        String result = instance.getByCustomer(customerAccount);
        System.out.println("findByCustomer result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateGlobalPtLocationNames method, of class GlobalPtLocationNamesController.
     */
    @Test
    public void test40_UpdateGlobalPtLocationNames() {
        System.out.println("updateGlobalPtLocationNames");
        content = "{\n" +
                "\"customerAccount\": \"77777\",\n" +
                "\"pointLocationName\":\"pointLocationName Test 1\",\n" +
                "\"description\":\"pointLocationName description Test 1\"\n" +
                "}";
        String expResult = "{\"outcome\":\"Updated GlobalPtLocationNames successfully.\"}";
        String result = instance.updateGlobalPtLocationNames(content);
        System.out.println("updateGlobalPtLocationNames result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateGlobalPtLocationNames method, of class GlobalPtLocationNamesController.
     * Unhappy path: Missing required field 'pointLocationName' in input json. 
     */
    @Test
    public void test41_UpdateGlobalPtLocationNames() {
        System.out.println("updateGlobalPtLocationNames");
        content = "{\n" +
                "\"customerAccount\": \"77777\",\n" +
                //"\"pointLocationName\":\"pointLocationName Test 1\",\n" +
                "\"description\":\"pointLocationName description Test 1\"\n" +
                "}";
        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.updateGlobalPtLocationNames(content);
        System.out.println("updateGlobalPtLocationNames result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateGlobalPtLocationNames method, of class GlobalPtLocationNamesController.
     * Unhappy path: Passing empty json. 
     */
    @Test
    public void test42_UpdateGlobalPtLocationNames() {
        System.out.println("updateGlobalPtLocationNames");
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.updateGlobalPtLocationNames("");
        System.out.println("updateGlobalPtLocationNames result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteGlobalPtLocationNames method, of class GlobalPtLocationNamesController.
     */
    @Test
    public void test50_DeleteGlobalPtLocationNames() {
        System.out.println("deleteGlobalPtLocationNames");
        content = "{\n" +
                "\"customerAccount\": \"77777\",\n" +
                "\"pointLocationName\":\"pointLocationName Test 1\",\n" +
                "\"description\":\"pointLocationName description Test 1\"\n" +
                "}";
        String expResult = "{\"outcome\":\"Deleted GlobalPtLocationNames successfully.\"}";
        String result = instance.deleteGlobalPtLocationNames(content);
        System.out.println("deleteGlobalPtLocationNames result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteGlobalPtLocationNames method, of class GlobalPtLocationNamesController.
     * Unhappy path: Missing required field 'pointLocationName' in input json. 
     */
    @Test
    public void test51_DeleteGlobalPtLocationNames() {
        System.out.println("deleteGlobalPtLocationNames");
        content = "{\n" +
                "\"customerAccount\": \"77777\",\n" +
                //"\"pointLocationName\":\"pointLocationName Test 1\",\n" +
                "\"description\":\"pointLocationName description Test 1\"\n" +
                "}";
        String expResult = "{\"outcome\":\"Insufficient data given in json\"}";
        String result = instance.deleteGlobalPtLocationNames(content);
        System.out.println("deleteGlobalPtLocationNames result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteGlobalPtLocationNames method, of class GlobalPtLocationNamesController.
     * Unhappy path: Passing a  'pointLocationName' in input json that does not exist. 
     */
    @Test
    public void test52_DeleteGlobalPtLocationNames() {
        System.out.println("deleteGlobalPtLocationNames");
        content = "{\n" +
                "\"customerAccount\": \"77777\",\n" +
                "\"pointLocationName\":\"pointLocationName Test 111\",\n" +
                "\"description\":\"pointLocationName description Test 1\"\n" +
                "}";
        String expResult = "{\"outcome\":\"No GlobalPtLocationNames found to delete.\"}";
        String result = instance.deleteGlobalPtLocationNames(content);
        System.out.println("deleteGlobalPtLocationNames result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteGlobalPtLocationNames method, of class GlobalPtLocationNamesController.
     * Unhappy path: Passing a  null input json. 
     */
    @Test
    public void test53_DeleteGlobalPtLocationNames() {
        System.out.println("deleteGlobalPtLocationNames");
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.deleteGlobalPtLocationNames(null);
        System.out.println("deleteGlobalPtLocationNames result - " + result);
        assertEquals(expResult, result);
    }
    
}
