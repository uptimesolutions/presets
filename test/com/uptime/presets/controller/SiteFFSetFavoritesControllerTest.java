/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.presets.controller;

import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author joseph
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SiteFFSetFavoritesControllerTest {

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//presets.cql", true, true, "worldview_dev1"));
    private static SiteFFSetFavoritesController instance;
    private static String content;

    public SiteFFSetFavoritesControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        instance = new SiteFFSetFavoritesController();
        content = "[\n"
                + "	{\n"
                + "		\"customerAccount\": \"77777\",\n"
                + "		\"ffSetId\": \"b026aa07-3e5c-4c24-b24e-0dc01779375e\",\n"
                + "		\"ffId\": \"f9dbaf10-e394-418a-ad91-381700ee0850\",\n"
                + "		\"ffName\": \"fname123\",\n"
                + "		\"ffSetDesc\": \"Set name 55 desc\",\n"
                + "		\"ffSetName\": \"Set name 55\",\n"
                + "		\"ffUnit\": \"RPM\",\n"
                + "		\"ffValue\": 0.0,\n"
                + "		\"global\": true,\n"
                + "		\"siteId\": \"7ac748c0-afca-400c-a315-53c94a83aa67\"\n"
                + "	}\n"
                + "]";
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getByPK method, of class SiteFFSetFavoritesController.
     */
    @Test
    public void test30_GetByPK() throws Exception {
        System.out.println("getByPK");
        String customerAccount = "77777";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String ffSetId = "b026aa07-3e5c-4c24-b24e-0dc01779375e";
        String ffId = "f9dbaf10-e394-418a-ad91-381700ee0850";
        String expResult = "{\"customerAccount\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"ffSetId\":\"b026aa07-3e5c-4c24-b24e-0dc01779375e\",\"ffId\":\"f9dbaf10-e394-418a-ad91-381700ee0850\",\"SiteFFSetFavorites\":[{\"customerAccount\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"ffSetId\":\"b026aa07-3e5c-4c24-b24e-0dc01779375e\",\"ffId\":\"f9dbaf10-e394-418a-ad91-381700ee0850\",\"ffName\":\"fname123\",\"ffSetDesc\":\"Set name 55 desc\",\"ffSetName\":\"Set name 55\",\"ffUnit\":\"RPM\",\"ffValue\":0.0,\"global\":true}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByPK(customerAccount, siteId, ffSetId, ffId);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByPK method, of class SiteFFSetFavoritesController.
     */
    @Test
    public void test31_GetByPK() throws Exception {
        System.out.println("getByPK");
        String customerAccount = "77798";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa68";
        String ffSetId = "b026aa07-3e5c-4c24-b24e-0dc01779375e";
        String ffId = "f9dbaf10-e394-418a-ad91-381700ee0850";
        String expResult = "{\"outcome\":\"Site FF Set Favorites not found.\"}";
        String result = instance.getByPK(customerAccount, siteId, ffSetId, ffId);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByPK method, of class SiteFFSetFavoritesController.
     */
    @Test
    public void test32_GetByPK() throws Exception {
        System.out.println("getByPK");
        String customerAccount = null;
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String ffSetId = "b026aa07-3e5c-4c24-b24e-0dc01779375e";
        String ffId = "f9dbaf10-e394-418a-ad91-381700ee0850";
        String expResult = "Argument customerAccount cannot be null or empty.";
        String expMsg = "";
        try {
            instance.getByPK(customerAccount, siteId, ffSetId, ffId);
        } catch (Exception e) {
            expMsg = e.getMessage();
            System.out.println("Exception e - " + expMsg);
        }
        assertEquals(expResult, expMsg);
    }

    /**
     * Test of getByPK method, of class SiteFFSetFavoritesController.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test33_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = "777777";
        String siteId = null;
        String ffSetId = "b026aa07-3e5c-4c24-b24e-0dc01779375e";
        String ffId = "f9dbaf10-e394-418a-ad91-381700ee0850";
        fail(instance.getByPK(customerAccount, siteId, ffSetId, ffId));
    }

    /**
     * Test of getByPK method, of class SiteFFSetFavoritesController.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test34_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = "777777";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String ffSetId = null;
        String ffId = "f9dbaf10-e394-418a-ad91-381700ee0850";
        fail(instance.getByPK(customerAccount, siteId, ffSetId, ffId));
    }

    /**
     * Test of getByPK method, of class SiteFFSetFavoritesController.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test35_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = "777777";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String ffSetId = "b026aa07-3e5c-4c24-b24e-0dc01779375e";
        String ffId = null;
        fail(instance.getByPK(customerAccount, siteId, ffSetId, ffId));
    }

    /**
     * Test of findByCustomerSiteId method, of class SiteFFSetFavoritesController.
     */
    @Test
    public void test40_FindByCustomerSiteId() throws Exception {
        System.out.println("findByCustomerSiteId");
        String customerAccount = "77777";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String expResult = "{\"customerAccount\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"SiteFFSetFavorites\":[{\"customerAccount\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"ffSetId\":\"b026aa07-3e5c-4c24-b24e-0dc01779375e\",\"ffId\":\"f9dbaf10-e394-418a-ad91-381700ee0850\",\"ffName\":\"fname123\",\"ffSetDesc\":\"Set name 55 desc\",\"ffSetName\":\"Set name 55\",\"ffUnit\":\"RPM\",\"ffValue\":0.0,\"global\":true}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.findByCustomerSiteId(customerAccount, siteId);
        assertEquals(expResult, result);
    }

    /**
     * Test of findByCustomerSiteId method, of class SiteFFSetFavoritesController.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test41_FindByCustomerSiteId() throws Exception {
        System.out.println("findByCustomerSiteId");
        String customerAccount = "77798";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa68";
        String expResult = "{\"outcome\":\"Site FF Set Favorites not found.\"}";
        String result = instance.findByCustomerSiteId(customerAccount, siteId);
        assertEquals(expResult, result);
    }

    /**
     * Test of findByCustomerSiteId method, of class SiteFFSetFavoritesController.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test42_FindByCustomerSiteId() throws Exception {
        System.out.println("findByCustomerSiteId");
        String customerAccount = null;
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String expResult = "Argument customerAccount cannot be null or empty.";
        String expMsg = "";
        try {
            instance.findByCustomerSiteId(customerAccount, siteId);
        } catch (Exception e) {
            expMsg = e.getMessage();
            System.out.println("Exception e - " + expMsg);
        }
        assertEquals(expResult, expMsg);
    }

    /**
     * Test of findByCustomerSiteId method, of class SiteFFSetFavoritesController.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test43_FindByCustomerSiteId() throws Exception {
        System.out.println("findByCustomerSiteId");
        String customerAccount = "77788";
        String siteId = null;
        fail(instance.findByCustomerSiteId(customerAccount, siteId));
    }

    /**
     * Test of createSiteFFSetFavorites method, of class SiteFFSetFavoritesController.
     */
    @Test
    public void test10_CreateSiteFFSetFavorites() {
        System.out.println("createSiteFFSetFavorites");
        String expResult = "{\"outcome\":\"New Site FF Set Favorites created successfully.\"}";
        String result = instance.createSiteFFSetFavorites(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of createSiteFFSetFavorites method, of class SiteFFSetFavoritesController.
     */
    @Test
    public void test11_CreateSiteFFSetFavorites() {
        System.out.println("createSiteFFSetFavorites");
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.createSiteFFSetFavorites(null);
        assertEquals(expResult, result);
    }

    /**
     * Test of createSiteFFSetFavorites method, of class SiteFFSetFavoritesController.
     */
    @Test
    public void test12_CreateSiteFFSetFavorites() {
        System.out.println("createSiteFFSetFavorites");
        content = "[\n"
                + "	{\n"
                //  + "		\"customerAccount\": \"77777\",\n"
                + "		\"ffSetId\": \"b026aa07-3e5c-4c24-b24e-0dc01779375e\",\n"
                + "		\"ffId\": \"f9dbaf10-e394-418a-ad91-381700ee0850\",\n"
                + "		\"ffName\": \"fname123\",\n"
                + "		\"ffSetDesc\": \"Set name 55 desc\",\n"
                + "		\"ffSetName\": \"Set name 55\",\n"
                + "		\"ffUnit\": \"RPM\",\n"
                + "		\"ffValue\": 0.0,\n"
                + "		\"global\": true,\n"
                + "		\"siteId\": \"7ac748c0-afca-400c-a315-53c94a83aa67\"\n"
                + "	}\n"
                + "]";
        String expResult = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        String result = instance.createSiteFFSetFavorites(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateSiteFFSetFavorites method, of class SiteFFSetFavoritesController.
     */
    @Test
    public void test20_UpdateSiteFFSetFavorites() {
        System.out.println("updateSiteFFSetFavorites");
        content = "{createSiteFFSetFavoritesVOList: [\n"
                + "	{\n"
                + "		\"customerAccount\": \"77777\",\n"
                + "		\"ffSetId\": \"b026aa07-3e5c-4c24-b24e-0dc01779375e\",\n"
                + "		\"ffId\": \"f9dbaf10-e394-418a-ad91-381700ee0850\",\n"
                + "		\"ffName\": \"fname123\",\n"
                + "		\"ffSetDesc\": \"Set name 55 desc\",\n"
                + "		\"ffSetName\": \"Set name 55\",\n"
                + "		\"ffUnit\": \"RPM\",\n"
                + "		\"ffValue\": 0.0,\n"
                + "		\"global\": true,\n"
                + "		\"siteId\": \"7ac748c0-afca-400c-a315-53c94a83aa67\"\n"
                + "	}\n"
                + "]}";
        String expResult = "{\"outcome\":\"Updated Site FF Set Favorites successfully.\"}";
        String result = instance.updateSiteFFSetFavorites(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateSiteFFSetFavorites method, of class SiteFFSetFavoritesController.
     */
    @Test
    public void test21_UpdateSiteFFSetFavorites() {
        System.out.println("updateSiteFFSetFavorites");
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.updateSiteFFSetFavorites(null);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateSiteFFSetFavorites method, of class SiteFFSetFavoritesController.
     */
    @Test
    public void test22_UpdateSiteFFSetFavorites() {
        System.out.println("updateSiteFFSetFavorites");
        content = "{createSiteFFSetFavoritesVOList:[\n"
                + "	{\n"
                // + "		\"customerAccount\": \"77777\",\n"
                + "		\"ffSetId\": \"b026aa07-3e5c-4c24-b24e-0dc01779375e\",\n"
                + "		\"ffId\": \"f9dbaf10-e394-418a-ad91-381700ee0850\",\n"
                + "		\"ffName\": \"fname123\",\n"
                + "		\"ffSetDesc\": \"Set name 55 desc\",\n"
                + "		\"ffSetName\": \"Set name 55\",\n"
                + "		\"ffUnit\": \"RPM\",\n"
                + "		\"ffValue\": 0.0,\n"
                + "		\"global\": true,\n"
                + "		\"siteId\": \"7ac748c0-afca-400c-a315-53c94a83aa67\"\n"
                + "	}\n"
                + "]}";
        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.updateSiteFFSetFavorites(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteSiteFFSetFavorites method, of class SiteFFSetFavoritesController.
     */
    @Test
    public void test50_DeleteSiteFFSetFavorites() {
        System.out.println("deleteSiteFFSetFavorites");
        content = "[\n"
                + "	{\n"
                + "		\"customerAccount\": \"77777\",\n"
                + "		\"ffSetId\": \"b026aa07-3e5c-4c24-b24e-0dc01779375e\",\n"
                + "		\"ffId\": \"f9dbaf10-e394-418a-ad91-381700ee0850\",\n"
                + "		\"ffName\": \"fname123\",\n"
                + "		\"ffSetDesc\": \"Set name 55 desc\",\n"
                + "		\"ffSetName\": \"Set name 55\",\n"
                + "		\"ffUnit\": \"RPM\",\n"
                + "		\"ffValue\": 0.0,\n"
                + "		\"global\": true,\n"
                + "		\"siteId\": \"7ac748c0-afca-400c-a315-53c94a83aa67\"\n"
                + "	}\n"
                + "]";
        String expResult = "{\"outcome\":\"Deleted Site FF Set Favorites successfully.\"}";
        String result = instance.deleteSiteFFSetFavorites(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteSiteFFSetFavorites method, of class SiteFFSetFavoritesController.
     */
    @Test
    public void test51_DeleteSiteFFSetFavorites() {
        System.out.println("deleteSiteFFSetFavorites");
        content = "[\n"
                + "	{\n"
                + "		\"customerAccount\": \"77798\",\n"
                + "		\"ffSetId\": \"b026aa07-3e5c-4c24-b24e-0dc01779375e\",\n"
                + "		\"ffId\": \"f9dbaf10-e394-418a-ad91-381700ee0850\",\n"
                + "		\"ffName\": \"fname123\",\n"
                + "		\"ffSetDesc\": \"Set name 55 desc\",\n"
                + "		\"ffSetName\": \"Set name 55\",\n"
                + "		\"ffUnit\": \"RPM\",\n"
                + "		\"ffValue\": 0.0,\n"
                + "		\"global\": true,\n"
                + "		\"siteId\": \"7ac748c0-afca-400c-a315-53c94a83aa68\"\n"
                + "	}\n"
                + "]";
        String expResult = "{\"outcome\":\"No Site FF Set Favorites found to delete.\"}";
        String result = instance.deleteSiteFFSetFavorites(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteSiteFFSetFavorites method, of class SiteFFSetFavoritesController.
     */
    @Test
    public void test52_DeleteSiteFFSetFavorites() {
        System.out.println("deleteSiteFFSetFavorites");
        content = "[\n"
                + "	{\n"
                //  + "		\"customerAccount\": \"77788\",\n"
                + "		\"ffSetId\": \"b026aa07-3e5c-4c24-b24e-0dc01779375e\",\n"
                + "		\"ffId\": \"f9dbaf10-e394-418a-ad91-381700ee0850\",\n"
                + "		\"ffName\": \"fname123\",\n"
                + "		\"ffSetDesc\": \"Set name 55 desc\",\n"
                + "		\"ffSetName\": \"Set name 55\",\n"
                + "		\"ffUnit\": \"RPM\",\n"
                + "		\"ffValue\": 0.0,\n"
                + "		\"global\": true,\n"
                + "		\"siteId\": \"7ac748c0-afca-400c-a315-53c94a83aa67\"\n"
                + "	}\n"
                + "]";
        String expResult = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        String result = instance.deleteSiteFFSetFavorites(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteSiteFFSetFavorites method, of class SiteFFSetFavoritesController.
     */
    @Test
    public void test60_DeleteSiteFFSetFavorites() {
        System.out.println("deleteSiteFFSetFavorites");
        String customerAccount = "77777";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String ffSetId = "b026aa07-3e5c-4c24-b24e-0dc01779375e";
        String expResult = "{\"outcome\":\"Deleted Site FF Set Favorites successfully.\"}";
        String result = instance.deleteSiteFFSetFavorites(customerAccount, siteId, ffSetId);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteSiteFFSetFavorites method, of class SiteFFSetFavoritesController.
     */
    @Test
    public void test61_DeleteSiteFFSetFavorites() {
        System.out.println("deleteSiteFFSetFavorites");
        String customerAccount = null;
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String ffSetId = "b026aa07-3e5c-4c24-b24e-0dc01779375e";
        String expResult = "{\"outcome\":\"Error: Failed to delete Site FF Set Favorites.\"}";
        String result = instance.deleteSiteFFSetFavorites(customerAccount, siteId, ffSetId);
        assertEquals(expResult, result);

    }

    /**
     * Test of deleteSiteFFSetFavorites method, of class SiteFFSetFavoritesController.
     */
    @Test
    public void test70_DeleteSiteFFSetFavorites() {
        System.out.println("deleteSiteFFSetFavorites");
        String customerAccount = "77777";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String expResult = "{\"outcome\":\"Deleted Site FF Set Favorites successfully.\"}";
        String result = instance.deleteSiteFFSetFavorites(customerAccount, siteId);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteSiteFFSetFavorites method, of class SiteFFSetFavoritesController.
     */
    @Test
    public void test71_DeleteSiteFFSetFavorites() {
        System.out.println("deleteSiteFFSetFavorites");
        String customerAccount = null;
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String expResult = "{\"outcome\":\"Error: Failed to delete Site FF Set Favorites.\"}";
        String result = instance.deleteSiteFFSetFavorites(customerAccount, siteId);
        assertEquals(expResult, result);
    }

}
