/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.controller;

import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GlobalDevicePresetControllerTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//presets.cql", true, true, "worldview_dev1"));
    private static GlobalDevicePresetController instance;
    private static String content;
    private static String presetId;
    
    public GlobalDevicePresetControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = new GlobalDevicePresetController();
        content = "[{\n"
                + "\"customerAccount\": \"77777\",\n"
                + "\"deviceType\":\"MIST\",\n"
                + "\"channelType\":\"DC\",\n"
                + "\"channelNumber\":\"1\",\n"
                + "\"apSetId\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\n"
                + "\"alSetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\n"
                + "\"alarmEnabled\":true,\n"
                + "\"autoAcknowledged\":false,\n"
                + "\"disabled\":false,\n"
                + "\"name\":\"Global Device Preset 1\",\n"
                + "\"sampleInterval\":30,\n"
                + "\"sensorOffset\":0.44,\n"
                + "\"sensorSensitivity\":0.55,\n"
                + "\"sensorType\":\"sensorType\",\n"
                + "\"sensorUnits\":\"Celcius\",\n"
                + "\"sensorSubType\":\"Test SensorSubType\",\n"
                + "\"sensorLocalOrientation\":\"H\"\n"
                + "}]";
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of createGlobalDevicePreset method, of class GlobalDevicePresetController.
     * @throws java.lang.Exception
     */
    @Test
    public void test10_CreateGlobalDevicePreset() throws Exception {
        System.out.println("createGlobalDevicePreset");
        String expResult = "{\"outcome\":\"New Global Device Preset created successfully.\"}";
        String result = instance.createGlobalDevicePreset(content);
        String result1 = instance.getByCustomer("77777");
        System.out.println("result1 = " + result1);
        presetId = result1.substring(result1.indexOf("\"presetId\":")+12, result1.indexOf("\"presetId\":")+48);
        System.out.println("presetId " + presetId);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of createGlobalDevicePreset method, of class GlobalDevicePresetController.
     */
    @Test
    public void test11_CreateGlobalDevicePreset() {
        System.out.println("createGlobalDevicePreset");
        content = null;
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.createGlobalDevicePreset(content);
        System.out.println("result = " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of createGlobalDevicePreset method, of class GlobalDevicePresetController.
     */
    @Test
    public void test12_CreateGlobalDevicePreset() {
        System.out.println("createGlobalDevicePreset");
        content = "[{\n"
                + "\"customerAccount\": \"77777\",\n"
                + "\"deviceType\":\"MIST\",\n"
                + "\"channelNumber\":\"1\",\n"
                + "\"apSetId\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\n"
                + "\"alSetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\n"
                + "\"alarmEnabled\":true,\n"
                + "\"autoAcknowledged\":false,\n"
                + "\"disabled\":false,\n"
                + "\"name\":\"Global Device Preset 1\",\n"
                + "\"sampleInterval\":30,\n"
                + "\"sensorOffset\":0.44,\n"
                + "\"sensorSensitivity\":0.55,\n"
                + "\"sensorType\":\"sensorType\",\n"
                + "\"sensorUnits\":\"Celcius\",\n"
                + "\"sensorSubType\":\"Test SensorSubType\",\n"
                + "\"sensorLocalOrientation\":\"H\"\n"
                + "}]";
        System.out.println("content = " + content);
        String expResult = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        String result = instance.createGlobalDevicePreset(content);
        assertEquals(expResult, result);
    }
     
    /**
     * Test of findByCustomer method, of class GlobalDevicePresetController.
     * @throws java.lang.Exception
     */
    @Test
    public void test20_FindByCustomer() throws Exception {
        System.out.println("findByCustomer");
        String customerAccount = "77777";
        String result = instance.getByCustomer(customerAccount);
        String expResult = "{\"customerAccount\":\"77777\",\"GlobalDevicePreset\":[{\"customerAccount\":\"77777\",\"deviceType\":\"MIST\",\"presetId\":\"" + presetId + "\",\"channelType\":\"DC\",\"channelNumber\":1,\"apSetId\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\"alSetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\"alarmEnabled\":true,\"autoAcknowledge\":false,\"disabled\":false,\"name\":\"Global Device Preset 1\",\"sampleInterval\":30,\"sensorOffset\":0.44,\"sensorSensitivity\":0.55,\"sensorType\":\"sensorType\",\"sensorUnits\":\"Celcius\",\"sensorSubType\":\"Test SensorSubType\",\"sensorLocalOrientation\":\"H\"}],\"outcome\":\"GET worked successfully.\"}";        System.out.println("findByCustomer result - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of findByCustomer method, of class GlobalDevicePresetController.
     * @throws java.lang.Exception
     */
    @Test
    public void test21_FindByCustomer() throws Exception {
        System.out.println("findByCustomer");
        String customerAccount = "77788";
        String result = instance.getByCustomer(customerAccount);
        String expResult = "{\"outcome\":\"Global Device Preset not found.\"}";
        System.out.println("findByCustomer result - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of findByPK method, of class GlobalDevicePresetController.
     * @throws java.lang.Exception
     */
    
    @Test
    public void test30_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = "77777";
        String deviceType = "MIST";
        String channelType = "DC";
        byte channelNumber = 1;
        String expResult = "{\"customerAccount\":\"77777\",\"deviceType\":\"MIST\",\"presetId\":\"" + presetId + "\",\"channelType\":\"DC\",\"channelNumber\":\"1\",\"GlobalDevicePreset\":[{\"customerAccount\":\"77777\",\"deviceType\":\"MIST\",\"presetId\":\"" + presetId + "\",\"channelType\":\"DC\",\"channelNumber\":1,\"apSetId\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\"alSetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\"alarmEnabled\":true,\"autoAcknowledge\":false,\"disabled\":false,\"name\":\"Global Device Preset 1\",\"sampleInterval\":30,\"sensorOffset\":0.44,\"sensorSensitivity\":0.55,\"sensorType\":\"sensorType\",\"sensorUnits\":\"Celcius\",\"sensorSubType\":\"Test SensorSubType\",\"sensorLocalOrientation\":\"H\"}],\"outcome\":\"GET worked successfully.\"}";        String result = instance.getByPK(customerAccount, deviceType, presetId, channelType, channelNumber);
        System.out.println("findByPK result - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of findByPK method, of class GlobalDevicePresetController.
     * @throws java.lang.Exception
     */
    @Test
    public void test31_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = "77788";
        String deviceType = "MIST";
        String channelType = "DC";
        byte channelNumber = 1;
        String expResult = "{\"outcome\":\"Global Device Preset not found.\"}";
        String result = instance.getByPK(customerAccount, deviceType, presetId, channelType, channelNumber);
        System.out.println("findByPK result - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of findByPK method, of class GlobalDevicePresetController.
     * @throws java.lang.Exception
     */
    @Test
    public void test32_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = "77788";
        String deviceType = "";
        String channelType = "DC";
        byte channelNumber = 1;
        String expResult = "Argument Device Type cannot be null or empty.";
        String expMsg = "";
        try {
            instance.getByPK(customerAccount, deviceType, presetId, channelType, channelNumber);
        }
        catch (Exception e) {
            expMsg = e.getMessage();
            System.out.println("Exception e - " + expMsg);
        }
        assertEquals(expResult, expMsg);
    }
    
    /**
     * Test of findByPK method, of class GlobalDevicePresetController.
     * @throws java.lang.Exception
     */
    @Test
    public void test33_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = null;
        String deviceType = "MIST";
        String channelType = "DC";
        byte channelNumber = 1;
        String expResult = "Argument Customer Account cannot be null or empty.";
        String expMsg = "";
        try {
            instance.getByPK(customerAccount, deviceType, presetId, channelType, channelNumber);
        }
        catch (Exception e) {
            expMsg = e.getMessage();
            System.out.println("Exception e - " + expMsg);
        }
        assertEquals(expResult, expMsg);
    }
    
    
    /**
     * Test of findByPK method, of class GlobalDevicePresetController.
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test34_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = null;
        String deviceType = "MIST";
        String channelType = "DC";
        byte channelNumber = 1;
        fail(instance.getByPK(customerAccount, deviceType, null, channelType, channelNumber));
    }
    
    /**
     * Test of findByCustomerDeviceTypePreset method, of class
     * GlobalDevicePresetController.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test33_FindByCustomerDeviceTypePreset() throws Exception {
        System.out.println("findByCustomerDeviceTypePreset");
        String customerAccount = "77777";
        String deviceType = "MIST";
        String expResult = "{\"customerAccount\":\"77777\",\"deviceType\":\"MIST\",\"presetId\":\"" + presetId + "\",\"GlobalDevicePreset\":[{\"customerAccount\":\"77777\",\"deviceType\":\"MIST\",\"presetId\":\"" + presetId + "\",\"channelType\":\"DC\",\"channelNumber\":1,\"apSetId\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\"alSetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\"alarmEnabled\":true,\"autoAcknowledge\":false,\"disabled\":false,\"name\":\"Global Device Preset 1\",\"sampleInterval\":30,\"sensorOffset\":0.44,\"sensorSensitivity\":0.55,\"sensorType\":\"sensorType\",\"sensorUnits\":\"Celcius\",\"sensorSubType\":\"Test SensorSubType\",\"sensorLocalOrientation\":\"H\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByCustomerDeviceTypePreset(customerAccount, deviceType, presetId);
        System.out.println("findByCustomer result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of findByCustomerDeviceTypePreset method, of class GlobalDevicePresetController.
     * @throws java.lang.Exception
     */
    @Test
    public void test34_FindByCustomerDeviceTypePreset() throws Exception {
        System.out.println("findByCustomerDeviceTypePreset");
        String customerAccount = "77779";
        String deviceType = "MIST";
        String expResult = "{\"outcome\":\"Global Device Preset not found.\"}";
        String result = instance.getByCustomerDeviceTypePreset(customerAccount, deviceType, presetId);
        System.out.println("findByCustomerDeviceTypePreset result - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of findByCustomerDeviceTypePreset method, of class GlobalDevicePresetController.
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test35_FindByCustomerDeviceTypePreset() throws Exception {
        System.out.println("findByCustomerDeviceTypePreset");
        String customerAccount = null;
        String deviceType = "MIST";
        fail(instance.getByCustomerDeviceTypePreset(customerAccount, deviceType, presetId));
        
    }
    
    /**
     * Test of findByCustomerDeviceTypePreset method, of class GlobalDevicePresetController.
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test36_FindByCustomerDeviceTypePreset() throws Exception {
        System.out.println("findByCustomerDeviceTypePreset");
        String customerAccount = "77777";
        String deviceType = "MIST";
        fail(instance.getByCustomerDeviceTypePreset(customerAccount, deviceType, "test Preset Id"));
        
    }

    /**
     * Test of findByCustomerDeviceType method, of class GlobalDevicePresetController.
     * @throws java.lang.Exception
     */
    @Test
    public void test4_FindByCustomerDeviceType() throws Exception {
        System.out.println("findByCustomerDeviceType");
        String customerAccount = "77777";
        String deviceType = "MIST";
        String result = instance.getByCustomerDeviceType(customerAccount, deviceType);
        String expResult = "{\"customerAccount\":\"77777\",\"deviceType\":\"MIST\",\"GlobalDevicePreset\":[{\"customerAccount\":\"77777\",\"deviceType\":\"MIST\",\"presetId\":\"" + presetId + "\",\"channelType\":\"DC\",\"channelNumber\":1,\"apSetId\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\"alSetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\"alarmEnabled\":true,\"autoAcknowledge\":false,\"disabled\":false,\"name\":\"Global Device Preset 1\",\"sampleInterval\":30,\"sensorOffset\":0.44,\"sensorSensitivity\":0.55,\"sensorType\":\"sensorType\",\"sensorUnits\":\"Celcius\",\"sensorSubType\":\"Test SensorSubType\",\"sensorLocalOrientation\":\"H\"}],\"outcome\":\"GET worked successfully.\"}";
        System.out.println("findByCustomerDeviceType result - " + result);
        assertEquals(expResult, result);

    }

    /**
     * Test of findByCustomerDeviceType method, of class GlobalDevicePresetController.
     * @throws java.lang.Exception
     */
    @Test
    public void test41_FindByCustomerDeviceType() throws Exception {
        System.out.println("findByCustomerDeviceType");
        String customerAccount = "77779";
        String deviceType = "MIST";
        String result = instance.getByCustomerDeviceType(customerAccount, deviceType);
        String expResult = "{\"outcome\":\"Global Device Preset not found.\"}";
        System.out.println("findByCustomerDeviceType result - " + result);
        assertEquals(expResult, result);

    }
    
    /**
     * Test of findByCustomerDeviceType method, of class GlobalDevicePresetController.
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test42_FindByCustomerDeviceType() throws Exception {
        System.out.println("findByCustomerDeviceType");
        String customerAccount = "77777";
        String deviceType = "";
        fail(instance.getByCustomerDeviceType(customerAccount, deviceType));
    }

    /**
     * Test of updateGlobalDevicePreset method, of class GlobalDevicePresetController.
     */
    @Test
    public void test50_UpdateGlobalDevicePreset() {
        System.out.println("updateGlobalDevicePreset");
        content = "[{\n"
                + "\"customerAccount\": \"77777\",\n"
                + "\"deviceType\":\"MIST\",\n"
                + "\"presetId\":\"" + presetId + "\",\n"
                + "\"channelType\":\"DC\",\n"
                + "\"channelNumber\":\"1\",\n"
                + "\"apSetId\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\n"
                + "\"alSetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\n"
                + "\"alarmEnabled\":true,\n"
                + "\"autoAcknowledged\":false,\n"
                + "\"disabled\":false,\n"
                + "\"name\":\"Global Device Preset 1\",\n"
                + "\"sampleInterval\":30,\n"
                + "\"sensorOffset\":0.44,\n"
                + "\"sensorSensitivity\":0.55,\n"
                + "\"sensorType\":\"sensorType\",\n"
                + "\"sensorUnits\":\"Celcius\",\n"
                + "\"sensorSubType\":\"Test SensorSubType\",\n"
                + "\"sensorLocalOrientation\":\"H\"\n"
                + "}]";
        String expResult = "{\"outcome\":\"Updated Global Device Preset successfully.\"}";
        String result = instance.updateGlobalDevicePreset(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateGlobalDevicePreset method, of class GlobalDevicePresetController.
     */
    @Test
    public void test51_UpdateGlobalDevicePreset() {
        System.out.println("updateGlobalDevicePreset");
        content = "[{\n"
                + "\"customerAccount\": \"77777\",\n"
                + "\"deviceType\":\"MIST\",\n"
                + "\"presetId\":\"" + presetId + "\",\n"
                + "\"channelType\":\"DC\",\n"
               // + "\"channelNumber\":\"1\",\n"
                + "\"apSetId\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\n"
                + "\"alSetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\n"
                + "\"alarmEnabled\":true,\n"
                + "\"autoAcknowledged\":false,\n"
                + "\"disabled\":false,\n"
                + "\"name\":\"Global Device Preset 1\",\n"
                + "\"sampleInterval\":30,\n"
                + "\"sensorOffset\":0.44,\n"
                + "\"sensorSensitivity\":0.55,\n"
                + "\"sensorType\":\"sensorType\",\n"
                + "\"sensorUnits\":\"Celcius\",\n"
                + "\"sensorSubType\":\"Test SensorSubType\",\n"
                + "\"sensorLocalOrientation\":\"H\"\n"
                + "}]";
        String expResult = "{\"outcome\":\"Global Device Preset not found to update.\"}";
        String result = instance.updateGlobalDevicePreset(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateGlobalDevicePreset method, of class GlobalDevicePresetController.
     */
    @Test
    public void test52_UpdateGlobalDevicePreset() {
        System.out.println("updateGlobalDevicePreset");
        content = "[{\n"
                + "\"customerAccount\": \"77777\",\n"
                + "\"deviceType\":\"MIST\",\n"
                + "\"presetId\":\"" + presetId + "\",\n"
                //+ "\"channelType\":\"DC\",\n"
                + "\"channelNumber\":\"1\",\n"
                + "\"apSetId\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\n"
                + "\"alSetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\n"
                + "\"alarmEnabled\":true,\n"
                + "\"autoAcknowledged\":false,\n"
                + "\"disabled\":false,\n"
                + "\"name\":\"Global Device Preset 1\",\n"
                + "\"sampleInterval\":30,\n"
                + "\"sensorOffset\":0.44,\n"
                + "\"sensorSensitivity\":0.55,\n"
                + "\"sensorType\":\"sensorType\",\n"
                + "\"sensorUnits\":\"Celcius\",\n"
                + "\"sensorSubType\":\"Test SensorSubType\",\n"
                + "\"sensorLocalOrientation\":\"H\"\n"
                + "}]";
        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.updateGlobalDevicePreset(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateGlobalDevicePreset method, of class GlobalDevicePresetController.
     */
    @Test
    public void test53_UpdateGlobalDevicePreset() {
        System.out.println("updateGlobalDevicePreset");
        content = "[{\n"
                + "\"customerAccount\": \"77777\",\n"
                + "\"deviceType\":\"STORMX\",\n"
                + "\"presetId\":\"" + presetId + "\",\n"
                + "\"channelType\":\"DC\",\n"
                + "\"channelNumber\":\"1\",\n"
                + "\"apSetId\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\n"
                + "\"alSetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\n"
                + "\"alarmEnabled\":true,\n"
                + "\"autoAcknowledged\":false,\n"
                + "\"disabled\":false,\n"
                + "\"name\":\"Global Device Preset 1\",\n"
                + "\"sampleInterval\":30,\n"
                + "\"sensorOffset\":0.44,\n"
                + "\"sensorSensitivity\":0.55,\n"
                + "\"sensorType\":\"sensorType\",\n"
                + "\"sensorUnits\":\"Celcius\",\n"
                + "\"sensorSubType\":\"Test SensorSubType\",\n"
                + "\"sensorLocalOrientation\":\"H\"\n"
                + "}]";
        String expResult = "{\"outcome\":\"Global Device Preset not found to update.\"}";
        String result = instance.updateGlobalDevicePreset(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateGlobalDevicePreset method, of class GlobalDevicePresetController.
     */
    @Test
    public void test54_UpdateGlobalDevicePreset() {
        System.out.println("updateGlobalDevicePreset");
        content = "[{\n"
                + "\"customerAccount\": \"77777\",\n"
                + "\"deviceType\":\"\",\n"
                + "\"presetId\":\"" + presetId + "\",\n"
                + "\"channelType\":\"DC\",\n"
                + "\"channelNumber\":\"1\",\n"
                + "\"apSetId\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\n"
                + "\"alSetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\n"
                + "\"alarmEnabled\":true,\n"
                + "\"autoAcknowledged\":false,\n"
                + "\"disabled\":false,\n"
                + "\"name\":\"Global Device Preset 1\",\n"
                + "\"sampleInterval\":30,\n"
                + "\"sensorOffset\":0.44,\n"
                + "\"sensorSensitivity\":0.55,\n"
                + "\"sensorType\":\"sensorType\",\n"
                + "\"sensorUnits\":\"Celcius\",\n"
                + "\"sensorSubType\":\"Test SensorSubType\",\n"
                + "\"sensorLocalOrientation\":\"H\"\n"
                + "}]";
        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.updateGlobalDevicePreset(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteGlobalDevicePreset method, of class GlobalDevicePresetController.
     */
    @Test
    public void test6_DeleteGlobalDevicePreset() {
        System.out.println("deleteGlobalDevicePreset");
        content = "[{\n"
                + "\"customerAccount\": \"77777\",\n"
                + "\"deviceType\":\"MIST\",\n"
                + "\"presetId\":\"" + presetId + "\",\n"
                + "\"channelType\":\"DC\",\n"
                + "\"channelNumber\":\"1\",\n"
                + "\"apSetId\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\n"
                + "\"alSetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\n"
                + "\"alarmEnabled\":true,\n"
                + "\"autoAcknowledged\":false,\n"
                + "\"disabled\":false,\n"
                + "\"name\":\"Global Device Preset 1\",\n"
                + "\"sampleInterval\":30,\n"
                + "\"sensorOffset\":0.44,\n"
                + "\"sensorSensitivity\":0.55,\n"
                + "\"sensorType\":\"sensorType\",\n"
                + "\"sensorUnits\":\"Celcius\",\n"
                + "\"sensorSubType\":\"Test SensorSubType\",\n"
                + "\"sensorLocalOrientation\":\"H\"\n"
                + "}]";
        String expResult = "{\"outcome\":\"Deleted Global Device Preset successfully.\"}";
        String result = instance.deleteGlobalDevicePreset(content);
        assertEquals(expResult, result);
    }
    

    /**
     * Test of deleteGlobalDevicePreset method, of class GlobalDevicePresetController.
     */
    @Test
    public void test61_DeleteGlobalDevicePreset() {
        System.out.println("deleteGlobalDevicePreset");
        content = "[{\n"
                + "\"customerAccount\": \"77777\",\n"
                + "\"deviceType\":\"MIST\",\n"
                + "\"presetId\":\"" + presetId + "\",\n"
                + "\"channelType\":\"DC\",\n"
                //+ "\"channelNumber\":\"1\",\n"
                + "\"apSetId\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\n"
                + "\"alSetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\n"
                + "\"alarmEnabled\":true,\n"
                + "\"autoAcknowledged\":false,\n"
                + "\"disabled\":false,\n"
                + "\"name\":\"Global Device Preset 1\",\n"
                + "\"sampleInterval\":30,\n"
                + "\"sensorOffset\":0.44,\n"
                + "\"sensorSensitivity\":0.55,\n"
                + "\"sensorType\":\"sensorType\",\n"
                + "\"sensorUnits\":\"Celcius\",\n"
                + "\"sensorSubType\":\"Test SensorSubType\",\n"
                + "\"sensorLocalOrientation\":\"H\"\n"
                + "}]";
        String expResult = "{\"outcome\":\"No Global Device Preset found to delete.\"}";
        String result = instance.deleteGlobalDevicePreset(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteGlobalDevicePreset method, of class GlobalDevicePresetController.
     */
    @Test
    public void test62_DeleteGlobalDevicePreset() {
        System.out.println("deleteGlobalDevicePreset");
        content = "[{\n"
                + "\"customerAccount\": \"77777\",\n"
                + "\"deviceType\":\"MIST\",\n"
                + "\"presetId\":\"" + presetId + "\",\n"
                //+ "\"channelType\":\"DC\",\n"
                + "\"channelNumber\":\"1\",\n"
                + "\"apSetId\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\n"
                + "\"alSetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\n"
                + "\"alarmEnabled\":true,\n"
                + "\"autoAcknowledged\":false,\n"
                + "\"disabled\":false,\n"
                + "\"name\":\"Global Device Preset 1\",\n"
                + "\"sampleInterval\":30,\n"
                + "\"sensorOffset\":0.44,\n"
                + "\"sensorSensitivity\":0.55,\n"
                + "\"sensorType\":\"sensorType\",\n"
                + "\"sensorUnits\":\"Celcius\",\n"
                + "\"sensorSubType\":\"Test SensorSubType\",\n"
                + "\"sensorLocalOrientation\":\"H\"\n"
                + "}]";
        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.deleteGlobalDevicePreset(content);
        assertEquals(expResult, result);
    }
    
}
