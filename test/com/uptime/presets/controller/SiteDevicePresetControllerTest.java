/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.controller;

import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SiteDevicePresetControllerTest {

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//presets.cql", true, true, "worldview_dev1"));
    private static SiteDevicePresetController instance;
    private static String content;
    private static String presetId;

    public SiteDevicePresetControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        instance = new SiteDevicePresetController();
        content = "[{\n"
                + "\"customerAccount\": \"77777\",\n"
                + "\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n"
                + "\"deviceType\":\"MIST\",\n"
                + "\"channelType\":\"DC\",\n"
                + "\"channelNumber\":\"1\",\n"
                + "\"apSetId\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\n"
                + "\"alSetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\n"
                + "\"alarmEnabled\":true,\n"
                + "\"autoAcknowledged\":false,\n"
                + "\"disabled\":false,\n"
                + "\"name\":\"Site Device Preset 1\",\n"
                + "\"sampleInterval\":30,\n"
                + "\"sensorOffset\":0.44,\n"
                + "\"sensorSensitivity\":0.55,\n"
                + "\"sensorType\":\"sensorType\",\n"
                + "\"sensorUnits\":\"Celcius\",\n"
                + "\"sensorSubType\":\"Test SensorSubType\",\n"
                + "\"sensorLocalOrientation\":\"H\"\n"
                + "}]";
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of createSiteDevicePreset method, of class
     * SiteDevicePresetController.
     * @throws java.lang.Exception
     */
    @Test
    public void test10_CreateSiteDevicePreset() throws Exception {
        System.out.println("createSiteDevicePreset");
        System.out.println("content = " + content);
        String expResult = "{\"outcome\":\"New Site Device Preset created successfully.\"}";
        String result = instance.createSiteDevicePreset(content);
        String result1 = instance.getByCustomerSite("77777", "dee5158a-30b0-4c12-b6b9-40cb91d1206d");
        System.out.println("result1 = " + result1);
        presetId = result1.substring(result1.indexOf("\"presetId\":") + 12, result1.indexOf("\"presetId\":") + 48);
        System.out.println("presetId " + presetId);
        assertEquals(expResult, result);
    }

    /**
     * Test of createSiteDevicePreset method, of class
     * SiteDevicePresetController.
     */
    @Test
    public void test11_CreateSiteDevicePreset() {
        System.out.println("createSiteDevicePreset");
        System.out.println("content = " + content);
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.createSiteDevicePreset(null);
        assertEquals(expResult, result);
    }

    /**
     * Test of createSiteDevicePreset method, of class
     * SiteDevicePresetController.
     */
    @Test
    public void test12_CreateSiteDevicePreset() {
        System.out.println("createSiteDevicePreset");
        String contentJson = "[{\n"
                + "\"customerAccount\": \"77777\",\n"
                + "\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n"
                // + "\"deviceType\":\"MIST\",\n"
                + "\"channelType\":\"DC\",\n"
                + "\"channelNumber\":\"1\",\n"
                + "\"apSetId\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\n"
                + "\"alSetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\n"
                + "\"alarmEnabled\":true,\n"
                + "\"autoAcknowledged\":false,\n"
                + "\"disabled\":false,\n"
                + "\"name\":\"Site Device Preset 1\",\n"
                + "\"sampleInterval\":30,\n"
                + "\"sensorOffset\":0.44,\n"
                + "\"sensorSensitivity\":0.55,\n"
                + "\"sensorType\":\"sensorType\",\n"
                + "\"sensorUnits\":\"Celcius\",\n"
                + "\"sensorSubType\":\"Test SensorSubType\",\n"
                + "\"sensorLocalOrientation\":\"H\"\n"
                + "}]";
        System.out.println("content = " + contentJson);
        String expResult = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        String result = instance.createSiteDevicePreset(contentJson);
        assertEquals(expResult, result);
    }

    /**
     * Test of findByCustomer method, of class SiteDevicePresetController.
     * @throws java.lang.Exception
     */
    @Test
    public void test20_FindByCustomerSite() throws Exception {
        System.out.println("findByCustomerSite");
        String customerAccount = "77777";
        String siteId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        String result = instance.getByCustomerSite(customerAccount, siteId);
        //  presetId = result.substring(result.indexOf("\"presetId\":")+12, result.indexOf("\"presetId\":")+48);
        String expResult = "{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"SiteDevicePreset\":[{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"deviceType\":\"MIST\",\"presetId\":\"" + presetId + "\",\"channelType\":\"DC\",\"channelNumber\":1,\"apSetId\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\"alSetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\"alarmEnabled\":true,\"autoAcknowledge\":false,\"disabled\":false,\"name\":\"Site Device Preset 1\",\"sampleInterval\":30,\"sensorOffset\":0.44,\"sensorSensitivity\":0.55,\"sensorType\":\"sensorType\",\"sensorUnits\":\"Celcius\",\"sensorSubType\":\"Test SensorSubType\",\"sensorLocalOrientation\":\"H\"}],\"outcome\":\"GET worked successfully.\"}";
        System.out.println("findByCustomerSite result    - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of findByCustomer method, of class SiteDevicePresetController.
     * @throws java.lang.Exception
     */
    @Test
    public void test21_FindByCustomerSite() throws Exception {
        System.out.println("findByCustomerSite");
        String customerAccount = "77788";
        String siteId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        String result = instance.getByCustomerSite(customerAccount, siteId);
        //  presetId = result.substring(result.indexOf("\"presetId\":")+12, result.indexOf("\"presetId\":")+48);
        String expResult = "{\"outcome\":\"Site Device Preset not found.\"}";
        System.out.println("findByCustomerSite result    - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of findByCustomer method, of class SiteDevicePresetController.
     * @throws java.lang.Exception
     */
    @Test
    public void test22_FindByCustomerSite() throws Exception {
        System.out.println("findByCustomerSite");
        String customerAccount = null;
        String siteId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        String expResult = "Argument Customer Account cannot be null or empty.";
        String result, expMsg = "";
        try {
            result = instance.getByCustomerSite(customerAccount, siteId);
            System.out.println("findByCustomerSite result    - " + result);
        } catch (Exception e) {
            expMsg = e.getMessage();
            System.out.println("Exception e - " + expMsg);
        }
        assertEquals(expResult, expMsg);
    }

    /**
     * Test of findByCustomer method, of class SiteDevicePresetController.
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test23_FindByCustomerSite() throws Exception {
        System.out.println("findByCustomerSite");
        String customerAccount = "77777";
        String siteId = null;
        fail(instance.getByCustomerSite(customerAccount, siteId));
    }

    /**
     * Test of getByPK method, of class SiteDevicePresetController.
     * @throws java.lang.Exception
     */
    @Test
    public void test30_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = "77777";
        String siteId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        String deviceType = "MIST";
        String channelType = "DC";
        byte channelNumber = 1;
        String expResult = "{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"deviceType\":\"MIST\",\"presetId\":\"" + presetId + "\",\"channelType\":\"DC\",\"channelNumber\":\"1\",\"SiteDevicePreset\":[{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"deviceType\":\"MIST\",\"presetId\":\"" + presetId + "\",\"channelType\":\"DC\",\"channelNumber\":1,\"apSetId\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\"alSetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\"alarmEnabled\":true,\"autoAcknowledge\":false,\"disabled\":false,\"name\":\"Site Device Preset 1\",\"sampleInterval\":30,\"sensorOffset\":0.44,\"sensorSensitivity\":0.55,\"sensorType\":\"sensorType\",\"sensorUnits\":\"Celcius\",\"sensorSubType\":\"Test SensorSubType\",\"sensorLocalOrientation\":\"H\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByPK(customerAccount, siteId, deviceType, presetId, channelType, channelNumber);
        System.out.println("findByPK result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByPK method, of class SiteDevicePresetController.
     * @throws java.lang.Exception
     */
    @Test
    public void test31_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = "77788";
        String siteId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        String deviceType = "MIST";
        String channelType = "DC";
        byte channelNumber = 1;
        String expResult = "{\"outcome\":\"Site Device Preset not found.\"}";
        String result = instance.getByPK(customerAccount, siteId, deviceType, presetId, channelType, channelNumber);
        System.out.println("findByPK result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByPK method, of class SiteDevicePresetController.
     * @throws java.lang.Exception
     */
    @Test
    public void test32_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = null;
        String siteId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        String deviceType = "MIST";
        String channelType = "DC";
        byte channelNumber = 1;
        String expResult = "Argument Customer Account cannot be null or empty.";

        String result, expMsg = "";
        try {
            result = instance.getByPK(customerAccount, siteId, deviceType, presetId, channelType, channelNumber);
            System.out.println("findByCustomerSite result    - " + result);
        } catch (Exception e) {
            expMsg = e.getMessage();
            System.out.println("Exception e - " + expMsg);
        }
        assertEquals(expResult, expMsg);
    }

    /**
     * Test of getByPK method, of class SiteDevicePresetController.
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test33_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = "77777";
        String siteId = null;
        String deviceType = "MIST";
        String channelType = "DC";
        byte channelNumber = 1;
        fail(instance.getByPK(customerAccount, siteId, deviceType, presetId, channelType, channelNumber));
    }

    /**
     * Test of getByPK method, of class SiteDevicePresetController.
     * @throws java.lang.Exception
     */
    @Test
    public void test34_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = "77777";
        String siteId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        String deviceType = null;
        String channelType = "DC";
        byte channelNumber = 1;
        String expResult = "Argument Device Type cannot be null or empty.";

        String result, expMsg = "";
        try {
            result = instance.getByPK(customerAccount, siteId, deviceType, presetId, channelType, channelNumber);
            System.out.println("findByCustomerSite result    - " + result);
        } catch (Exception e) {
            expMsg = e.getMessage();
            System.out.println("Exception e - " + expMsg);
        }
        assertEquals(expResult, expMsg);
    }

    /**
     * Test of getByPK method, of class SiteDevicePresetController.
     * @throws java.lang.Exception
     */
    @Test
    public void test35_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = "77777";
        String siteId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        String deviceType = "MIST";
        String channelType = null;
        byte channelNumber = 1;
        String expResult = "Argument Channel Type cannot be null or empty.";

        String result, expMsg = "";
        try {
            result = instance.getByPK(customerAccount, siteId, deviceType, presetId, channelType, channelNumber);
            System.out.println("findByCustomerSite result    - " + result);
        } catch (Exception e) {
            expMsg = e.getMessage();
            System.out.println("Exception e - " + expMsg);
        }
        assertEquals(expResult, expMsg);
    }

    /**
     * Test of getByPK method, of class SiteDevicePresetController.
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test36_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = "77777";
        String siteId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        String deviceType = "MIST";
        String channelType = "DC";
        byte channelNumber = 1;
        fail(instance.getByPK(customerAccount, siteId, deviceType, null, channelType, channelNumber));
    }

    /**
     * Test of findByCustomerDeviceType method, of class
     * SiteDevicePresetController.
     * @throws java.lang.Exception
     */
    @Test
    public void test40_FindByCustomerSiteDeviceTypePreset() throws Exception {
        System.out.println("findByCustomerSiteDeviceTypePreset");
        String customerAccount = "77777";
        String siteId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        String deviceType = "MIST";
        String result = instance.getByCustomerSiteDeviceTypePreset(customerAccount, siteId, deviceType, presetId);
        String expResult = "{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"deviceType\":\"MIST\",\"presetId\":\"" + presetId + "\",\"SiteDevicePreset\":[{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"deviceType\":\"MIST\",\"presetId\":\"" + presetId + "\",\"channelType\":\"DC\",\"channelNumber\":1,\"apSetId\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\"alSetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\"alarmEnabled\":true,\"autoAcknowledge\":false,\"disabled\":false,\"name\":\"Site Device Preset 1\",\"sampleInterval\":30,\"sensorOffset\":0.44,\"sensorSensitivity\":0.55,\"sensorType\":\"sensorType\",\"sensorUnits\":\"Celcius\",\"sensorSubType\":\"Test SensorSubType\",\"sensorLocalOrientation\":\"H\"}],\"outcome\":\"GET worked successfully.\"}";
        System.out.println("findByCustomerSiteDeviceTypePreset result - " + result);
        assertEquals(expResult, result);

    }

    /**
     * Test of findByCustomerDeviceType method, of class
     * SiteDevicePresetController.
     * @throws java.lang.Exception
     */
    @Test
    public void test41_FindByCustomerSiteDeviceTypePreset() throws Exception {
        System.out.println("findByCustomerSiteDeviceTypePreset");
        String customerAccount = "77788";
        String siteId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        String deviceType = "MIST";
        String result = instance.getByCustomerSiteDeviceTypePreset(customerAccount, siteId, deviceType, presetId);
        String expResult = "{\"outcome\":\"Site Device Preset not found.\"}";
        System.out.println("findByCustomerSiteDeviceTypePreset result - " + result);
        assertEquals(expResult, result);

    }

    /**
     * Test of findByCustomerDeviceType method, of class
     * SiteDevicePresetController.
     * @throws java.lang.Exception
     */
    @Test
    public void test42_FindByCustomerSiteDeviceTypePreset() throws Exception {
        System.out.println("findByCustomerSiteDeviceTypePreset");
        String customerAccount = null;
        String siteId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        String deviceType = "MIST";
        String expResult = "Argument Customer Account cannot be null or empty.";
        String result, expMsg = "";
        try {
            result = instance.getByCustomerSiteDeviceTypePreset(customerAccount, siteId, deviceType, presetId);
            System.out.println("findByCustomerSiteDeviceTypePreset result    - " + result);
        } catch (Exception e) {
            expMsg = e.getMessage();
            System.out.println("Exception e - " + expMsg);
        }
        assertEquals(expResult, expMsg);

    }

    /**
     * Test of findByCustomerDeviceType method, of class
     * SiteDevicePresetController.
     * @throws java.lang.Exception
     */
    @Test
    public void test43_FindByCustomerSiteDeviceTypePreset() throws Exception {
        System.out.println("findByCustomerSiteDeviceTypePreset");
        String customerAccount = "77777";
        String siteId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        String deviceType = null;
        String expResult = "Argument Device Type cannot be null or empty.";
        String result, expMsg = "";
        try {
            result = instance.getByCustomerSiteDeviceTypePreset(customerAccount, siteId, deviceType, presetId);
            System.out.println("findByCustomerSiteDeviceTypePreset result    - " + result);
        } catch (Exception e) {
            expMsg = e.getMessage();
            System.out.println("Exception e - " + expMsg);
        }
        assertEquals(expResult, expMsg);

    }

    /**
     * Test of findByCustomerDeviceType method, of class
     * SiteDevicePresetController.
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test44_FindByCustomerSiteDeviceTypePreset() throws Exception {
        System.out.println("findByCustomerSiteDeviceTypePreset");
        String customerAccount = "77777";
        String siteId = null;
        String deviceType = "MIST";
        fail(instance.getByCustomerSiteDeviceTypePreset(customerAccount, siteId, deviceType, presetId));
    }

    /**
     * Test of findByCustomerDeviceType method, of class
     * SiteDevicePresetController.
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test45_FindByCustomerSiteDeviceTypePreset() throws Exception {
        System.out.println("findByCustomerSiteDeviceTypePreset");
        String customerAccount = "77777";
        String siteId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        String deviceType = "MIST";
        fail(instance.getByCustomerSiteDeviceTypePreset(customerAccount, siteId, deviceType, null));
    }

    /**
     * Test of findByCustomerDeviceType method, of class
     * SiteDevicePresetController.
     * @throws java.lang.Exception
     */
    @Test
    public void test50_FindByCustomerSiteDeviceType() throws Exception {
        System.out.println("findByCustomerSiteDeviceType");
        String customerAccount = "77777";
        String siteId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        String deviceType = "MIST";
        String result = instance.getByCustomerSiteDeviceType(customerAccount, siteId, deviceType);
        String expResult = "{\"customerAccount\":\"77777\",\"deviceType\":\"MIST\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"SiteDevicePreset\":[{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"deviceType\":\"MIST\",\"presetId\":\"" + presetId + "\",\"channelType\":\"DC\",\"channelNumber\":1,\"apSetId\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\"alSetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\"alarmEnabled\":true,\"autoAcknowledge\":false,\"disabled\":false,\"name\":\"Site Device Preset 1\",\"sampleInterval\":30,\"sensorOffset\":0.44,\"sensorSensitivity\":0.55,\"sensorType\":\"sensorType\",\"sensorUnits\":\"Celcius\",\"sensorSubType\":\"Test SensorSubType\",\"sensorLocalOrientation\":\"H\"}],\"outcome\":\"GET worked successfully.\"}";
        System.out.println("findByCustomerSiteDeviceType result - " + result);
        assertEquals(expResult, result);

    }

    /**
     * Test of findByCustomerDeviceType method, of class
     * SiteDevicePresetController.
     * @throws java.lang.Exception
     */
    @Test
    public void test51_FindByCustomerSiteDeviceType() throws Exception {
        System.out.println("findByCustomerSiteDeviceType");
        String customerAccount = "77788";
        String siteId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        String deviceType = "MIST";
        String result = instance.getByCustomerSiteDeviceType(customerAccount, siteId, deviceType);
        String expResult = "{\"outcome\":\"Site Device Preset not found.\"}";
        System.out.println("findByCustomerSiteDeviceType result - " + result);
        assertEquals(expResult, result);

    }

    /**
     * Test of findByCustomerDeviceType method, of class
     * SiteDevicePresetController.
     * @throws java.lang.Exception
     */
    @Test
    public void test52_FindByCustomerSiteDeviceType() throws Exception {
        System.out.println("findByCustomerSiteDeviceType");
        String customerAccount = null;
        String siteId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        String deviceType = "MIST";
        String expResult = "Argument Customer Account cannot be null or empty.";
        String result, expMsg = "";
        try {
            result = instance.getByCustomerSiteDeviceType(customerAccount, siteId, deviceType);
            System.out.println("findByCustomerSiteDeviceType result    - " + result);
        } catch (Exception e) {
            expMsg = e.getMessage();
            System.out.println("Exception e - " + expMsg);
        }
        assertEquals(expResult, expMsg);
    }

    /**
     * Test of findByCustomerDeviceType method, of class
     * SiteDevicePresetController.
     * @throws java.lang.Exception
     */
    @Test
    public void test53_FindByCustomerSiteDeviceType() throws Exception {
        System.out.println("findByCustomerSiteDeviceType");
        String customerAccount = "77777";
        String siteId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        String deviceType = null;
        String expResult = "Argument Device Type cannot be null or empty.";
        String result, expMsg = "";
        try {
            result = instance.getByCustomerSiteDeviceType(customerAccount, siteId, deviceType);
            System.out.println("findByCustomerSiteDeviceType result    - " + result);
        } catch (Exception e) {
            expMsg = e.getMessage();
            System.out.println("Exception e - " + expMsg);
        }
        assertEquals(expResult, expMsg);

    }

    /**
     * Test of findByCustomerDeviceType method, of class
     * SiteDevicePresetController.
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test54_FindByCustomerSiteDeviceType() throws Exception {
        System.out.println("findByCustomerSiteDeviceType");
        String customerAccount = "77777";
        String siteId = null;
        String deviceType = "MIST";
        fail(instance.getByCustomerSiteDeviceType(customerAccount, siteId, deviceType));
    }

    /**
     * Test of updateSiteDevicePreset method, of class
     * SiteDevicePresetController.
     */
    @Test
    public void test60_UpdateSiteDevicePreset() {
        System.out.println("updateSiteDevicePreset");
        content = "[{\n"
                + "\"customerAccount\": \"77777\",\n"
                + "\"siteId\": \"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n"
                + "\"deviceType\":\"MIST\",\n"
                + "\"presetId\":\"" + presetId + "\",\n"
                + "\"channelType\":\"DC\",\n"
                + "\"channelNumber\":\"1\",\n"
                + "\"apSetId\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\n"
                + "\"alSetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\n"
                + "\"alarmEnabled\":true,\n"
                + "\"autoAcknowledged\":false,\n"
                + "\"disabled\":false,\n"
                + "\"name\":\"Site Device Preset 1\",\n"
                + "\"sampleInterval\":30,\n"
                + "\"sensorOffset\":0.44,\n"
                + "\"sensorSensitivity\":0.55,\n"
                + "\"sensorType\":\"sensorType\",\n"
                + "\"sensorUnits\":\"Celcius\",\n"
                + "\"sensorSubType\":\"Test SensorSubType\",\n"
                + "\"sensorLocalOrientation\":\"H\"\n"
                + "}]";
        String expResult = "{\"outcome\":\"Updated Site Device Preset successfully.\"}";
        String result = instance.updateSiteDevicePreset(content);
        assertEquals(expResult, result);
    }

     /**
     * Test of updateSiteDevicePreset method, of class
     * SiteDevicePresetController.
     */
    @Test
    public void test61_UpdateSiteDevicePreset() {
        System.out.println("updateSiteDevicePreset");        
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.updateSiteDevicePreset(null);
        assertEquals(expResult, result);
    }

     /**
     * Test of updateSiteDevicePreset method, of class
     * SiteDevicePresetController.
     */
    @Test
    public void test62_UpdateSiteDevicePreset() {
        System.out.println("updateSiteDevicePreset");
        content = "[{\n"
                + "\"customerAccount\": \"77789\",\n"
                + "\"siteId\": \"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n"
                + "\"deviceType\":\"MIST\",\n"
                + "\"presetId\":\"" + presetId + "\",\n"
                + "\"channelType\":\"DC\",\n"
                + "\"channelNumber\":\"1\",\n"
                + "\"apSetId\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\n"
                + "\"alSetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\n"
                + "\"alarmEnabled\":true,\n"
                + "\"autoAcknowledged\":false,\n"
                + "\"disabled\":false,\n"
                + "\"name\":\"Site Device Preset 1\",\n"
                + "\"sampleInterval\":30,\n"
                + "\"sensorOffset\":0.44,\n"
                + "\"sensorSensitivity\":0.55,\n"
                + "\"sensorType\":\"sensorType\",\n"
                + "\"sensorUnits\":\"Celcius\",\n"
                + "\"sensorSubType\":\"Test SensorSubType\",\n"
                + "\"sensorLocalOrientation\":\"H\"\n"
                + "}]";
        String expResult = "{\"outcome\":\"Site Device Preset not found to update.\"}";
        String result = instance.updateSiteDevicePreset(content);
        assertEquals(expResult, result);
    }
 /**
     * Test of updateSiteDevicePreset method, of class
     * SiteDevicePresetController.
     */
    @Test
    public void test63_UpdateSiteDevicePreset() {
        System.out.println("updateSiteDevicePreset");
        content = "[{\n"
                //+ "\"customerAccount\": \"77777\",\n"
                + "\"siteId\": \"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n"
                + "\"deviceType\":\"MIST\",\n"
                + "\"presetId\":\"" + presetId + "\",\n"
                + "\"channelType\":\"DC\",\n"
                + "\"channelNumber\":\"1\",\n"
                + "\"apSetId\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\n"
                + "\"alSetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\n"
                + "\"alarmEnabled\":true,\n"
                + "\"autoAcknowledged\":false,\n"
                + "\"disabled\":false,\n"
                + "\"name\":\"Site Device Preset 1\",\n"
                + "\"sampleInterval\":30,\n"
                + "\"sensorOffset\":0.44,\n"
                + "\"sensorSensitivity\":0.55,\n"
                + "\"sensorType\":\"sensorType\",\n"
                + "\"sensorUnits\":\"Celcius\",\n"
                + "\"sensorSubType\":\"Test SensorSubType\",\n"
                + "\"sensorLocalOrientation\":\"H\"\n"
                + "}]";
        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.updateSiteDevicePreset(content);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of deleteSiteDevicePreset method, of class
     * SiteDevicePresetController.
     */
    @Test
    public void test70_DeleteSiteDevicePreset() {
        System.out.println("deleteSiteDevicePreset");
        content = "[{\n"
                + "\"customerAccount\": \"77777\",\n"
                + "\"siteId\": \"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n"
                + "\"deviceType\":\"MIST\",\n"
                + "\"presetId\":\"" + presetId + "\",\n"
                + "\"channelType\":\"DC\",\n"
                + "\"channelNumber\":\"1\",\n"
                + "\"apSetId\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\n"
                + "\"alSetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\n"
                + "\"alarmEnabled\":true,\n"
                + "\"autoAcknowledged\":false,\n"
                + "\"disabled\":false,\n"
                + "\"name\":\"Site Device Preset 1\",\n"
                + "\"sampleInterval\":30,\n"
                + "\"sensorOffset\":0.44,\n"
                + "\"sensorSensitivity\":0.55,\n"
                + "\"sensorType\":\"sensorType\",\n"
                + "\"sensorUnits\":\"Celcius\",\n"
                + "\"sensorSubType\":\"Test SensorSubType\",\n"
                + "\"sensorLocalOrientation\":\"H\"\n"
                + "}]";
        String expResult = "{\"outcome\":\"Deleted Site Device Preset successfully.\"}";
        String result = instance.deleteSiteDevicePreset(content);
        assertEquals(expResult, result);
    }

     /**
     * Test of deleteSiteDevicePreset method, of class
     * SiteDevicePresetController.
     */
    @Test
    public void test71_DeleteSiteDevicePreset() {
        System.out.println("deleteSiteDevicePreset");        
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.deleteSiteDevicePreset(null);
        assertEquals(expResult, result);
    }

     /**
     * Test of deleteSiteDevicePreset method, of class
     * SiteDevicePresetController.
     */
    @Test
    public void test72_DeleteSiteDevicePreset() {
        System.out.println("deleteSiteDevicePreset");
        content = "[{\n"
              //  + "\"customerAccount\": \"77788\",\n"
                + "\"siteId\": \"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n"
                + "\"deviceType\":\"MIST\",\n"
                + "\"presetId\":\"" + presetId + "\",\n"
                + "\"channelType\":\"DC\",\n"
                + "\"channelNumber\":\"1\",\n"
                + "\"apSetId\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\n"
                + "\"alSetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\n"
                + "\"alarmEnabled\":true,\n"
                + "\"autoAcknowledged\":false,\n"
                + "\"disabled\":false,\n"
                + "\"name\":\"Site Device Preset 1\",\n"
                + "\"sampleInterval\":30,\n"
                + "\"sensorOffset\":0.44,\n"
                + "\"sensorSensitivity\":0.55,\n"
                + "\"sensorType\":\"sensorType\",\n"
                + "\"sensorUnits\":\"Celcius\",\n"
                + "\"sensorSubType\":\"Test SensorSubType\",\n"
                + "\"sensorLocalOrientation\":\"H\"\n"
                + "}]";
        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.deleteSiteDevicePreset(content);
        assertEquals(expResult, result);
    }

     /**
     * Test of deleteSiteDevicePreset method, of class
     * SiteDevicePresetController.
     */
    @Test
    public void test73_DeleteSiteDevicePreset() {
        System.out.println("deleteSiteDevicePreset");
        content = "[{\n"
                + "\"customerAccount\": \"77788\",\n"
                + "\"siteId\": \"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n"
                + "\"deviceType\":\"MIST\",\n"
                + "\"presetId\":\"" + presetId + "\",\n"
                + "\"channelType\":\"DC\",\n"
                + "\"channelNumber\":\"1\",\n"
                + "\"apSetId\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\n"
                + "\"alSetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\n"
                + "\"alarmEnabled\":true,\n"
                + "\"autoAcknowledged\":false,\n"
                + "\"disabled\":false,\n"
                + "\"name\":\"Site Device Preset 1\",\n"
                + "\"sampleInterval\":30,\n"
                + "\"sensorOffset\":0.44,\n"
                + "\"sensorSensitivity\":0.55,\n"
                + "\"sensorType\":\"sensorType\",\n"
                + "\"sensorUnits\":\"Celcius\",\n"
                + "\"sensorSubType\":\"Test SensorSubType\",\n"
                + "\"sensorLocalOrientation\":\"H\"\n"
                + "}]";
        String expResult = "{\"outcome\":\"Site Device Preset not found to delete.\"}";
        String result = instance.deleteSiteDevicePreset(content);
        assertEquals(expResult, result);
    }

}
