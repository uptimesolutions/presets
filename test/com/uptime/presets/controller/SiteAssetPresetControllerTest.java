/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.controller;

import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author kpati
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SiteAssetPresetControllerTest {

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//presets.cql", true, true, "worldview_dev1"));
    private static SiteAssetPresetController instance;
    private static String content;
    private static String insuffDataPayload;
    private static String presetId;

    public SiteAssetPresetControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        instance = new SiteAssetPresetController();

        content = "[{\n"
                + "\"customerAccount\": \"77777\",\n"
                + "\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n"
                + "\"assetTypeName\":\"Test Asset Type\",\n"
                + "\"assetPresetId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n"
                + "\"pointLocationName\":\"Point Location Name\",\n"
                + "\"assetPresetName\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\n"
                + "\"devicePresetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\n"
                + "\"devicePresetType\":\"MistLx\",\n"
                + "\"sampleInterval\":30,\n"
                + "\"ffSetIds\":\"Test ffSEt Ids\",\n"
                + "\"rollDiameter\":\"10\",\n"
                + "\"rollDiameterUnits\":Test,\n"
                + "\"speedRatio\":0.55,\n"
                + "\"tachId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n"
                + "\"description\":\"Celcius\"\n"
                + "}]";
        
       insuffDataPayload = "[{\n"
                + "\"customerAccount\": \"77777\",\n"
                + "\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n"
                + "\"assetPresetId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n"
                + "\"pointLocationName\":\"Point Location Name\",\n"
                + "\"assetPresetName\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\n"
                + "\"devicePresetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\n"
                + "\"devicePresetType\":\"MistLx\",\n"
                + "\"sampleInterval\":30,\n"
                + "\"ffSetIds\":\"Test ffSEt Ids\",\n"
                + "\"rollDiameter\":\"10\",\n"
                + "\"rollDiameterUnits\":Test,\n"
                + "\"speedRatio\":0.55,\n"
                + "\"tachId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n"
                + "\"description\":\"Celcius\"\n"
                + "}]";
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of createSiteAssetPreset method, of class SiteAssetPresetController.
     */
    @Test
    public void test1_CreateSiteAssetPreset() {
        System.out.println("createSiteAssetPreset");
        String expResult = "{\"outcome\":\"New Site Asset Preset created successfully.\"}";
        String result = instance.createSiteAssetPreset(content);
        assertEquals(expResult, result);
        System.out.println("createSiteAssetPreset Done");
    }
    
    /**
     * Test of createSiteAssetPreset method, of class SiteAssetPresetController.
     */
    @Test
    public void test2_CreateSiteAssetPreset() {
        System.out.println("createSiteAssetPreset");
        
        String expResult = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        String result = instance.createSiteAssetPreset(insuffDataPayload);
        assertEquals(expResult, result);
        System.out.println("createSiteAssetPreset Done");
    }

    /**
     * Test of getByPK method, of class SiteAssetPresetController.
     */
    @Test
    public void test3_GetByPK() throws Exception {
        System.out.println("getByPK");
        String customerAccount = "77777";
        String siteId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        String assetTypeName = "Test Asset Type";
        String assetPresetId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        String pointLocationName = "Point Location Name";

        String expResult = "{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"assetTypeName\":\"Test Asset Type\",\"assetPresetId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"pointLocationName\":\"Point Location Name\",\"SiteAssetPreset\":[{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"assetTypeName\":\"Test Asset Type\",\"assetPresetId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"pointLocationName\":\"Point Location Name\",\"assetPresetName\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\"devicePresetType\":\"MistLx\",\"devicePresetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\"sampleInterval\":30,\"ffSetIds\":\"Test ffSEt Ids\",\"rollDiameter\":10.0,\"rollDiameterUnits\":\"Test\",\"speedRatio\":0.55,\"tachId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"description\":\"Celcius\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByPK(customerAccount, siteId, assetTypeName, assetPresetId, pointLocationName);
        assertEquals(expResult, result);
        System.out.println("getByPK Done");
    }
    
    /**
     * Test of getByPK method, of class SiteAssetPresetController.
     */
    @Test
    public void test4_GetByPK() throws Exception {
        System.out.println("getByPK");
        String customerAccount = "77777";
        String siteId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        String assetTypeName = "Asset Type";
        String assetPresetId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        String pointLocationName = "Point Location Name";

        String expResult = "{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"assetTypeName\":\"Test Asset Type\",\"assetPresetId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"pointLocationName\":\"Point Location Name\",\"SiteAssetPreset\":[{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"assetTypeName\":\"Test Asset Type\",\"assetPresetId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"pointLocationName\":\"Point Location Name\",\"assetPresetName\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\"devicePresetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\"alarmEnabled\":true,\"sampleInterval\":30,\"ffSetIds\":\"Test ffSEt Ids\",\"rollDiameter\":10.0,\"rollDiameterUnits\":\"Test\",\"speedRatio\":0.55,\"tachId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"description\":\"Celcius\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByPK(customerAccount, siteId, assetTypeName, assetPresetId, pointLocationName);
        assertNotEquals(expResult, result);
        System.out.println("getByPK Done");
    }

    /**
     * Test of getByCustomerSiteAssetTypePresetId method, of class SiteAssetPresetController.
     */
    @Test
    public void test5_GetByCustomerSiteAssetTypePresetId() throws Exception {
        System.out.println("getByCustomerSiteAssetTypePresetId");
        String customerAccount = "77777";
        String siteId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        String assetTypeName = "Test Asset Type";
        String assetPresetId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        
        String expResult = "{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"assetTypeName\":\"Test Asset Type\",\"assetPresetId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"SiteAssetPreset\":[{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"assetTypeName\":\"Test Asset Type\",\"assetPresetId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"pointLocationName\":\"Point Location Name\",\"assetPresetName\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\"devicePresetType\":\"MistLx\",\"devicePresetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\"sampleInterval\":30,\"ffSetIds\":\"Test ffSEt Ids\",\"rollDiameter\":10.0,\"rollDiameterUnits\":\"Test\",\"speedRatio\":0.55,\"tachId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"description\":\"Celcius\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByCustomerSiteAssetTypePresetId(customerAccount, siteId, assetTypeName, assetPresetId);
        assertEquals(expResult, result);
        System.out.println("getByCustomerSiteAssetTypePresetId Done");
       
    }
    
    /**
     * Test of getByCustomerSiteAssetTypePresetId method, of class SiteAssetPresetController.
     */
    @Test
    public void test6_GetByCustomerSiteAssetTypePresetId() throws Exception {
        System.out.println("getByCustomerSiteAssetTypePresetId");
        String customerAccount = "77777";
        String siteId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        String assetTypeName = "Asset Type";
        String assetPresetId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        
        String expResult = "{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"assetTypeName\":\"Test Asset Type\",\"assetPresetId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"SiteAssetPreset\":[{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"assetTypeName\":\"Test Asset Type\",\"assetPresetId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"pointLocationName\":\"Point Location Name\",\"assetPresetName\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\"devicePresetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\"alarmEnabled\":true,\"sampleInterval\":30,\"ffSetIds\":\"Test ffSEt Ids\",\"rollDiameter\":10.0,\"rollDiameterUnits\":\"Test\",\"speedRatio\":0.55,\"tachId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"description\":\"Celcius\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByCustomerSiteAssetTypePresetId(customerAccount, siteId, assetTypeName, assetPresetId);
        assertNotEquals(expResult, result);
        System.out.println("getByCustomerSiteAssetTypePresetId Done");
       
    }

    /**
     * Test of getByCustomerSiteAssetType method, of class SiteAssetPresetController.
     */
    @Test
    public void test7_GetByCustomerSiteAssetType() throws Exception {
        System.out.println("getByCustomerSiteAssetType");
        String customerAccount = "77777";
        String siteId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        String assetTypeName = "Test Asset Type";
        
        String expResult = "{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"assetTypeName\":\"Test Asset Type\",\"SiteAssetPreset\":[{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"assetTypeName\":\"Test Asset Type\",\"assetPresetId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"pointLocationName\":\"Point Location Name\",\"assetPresetName\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\"devicePresetType\":\"MistLx\",\"devicePresetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\"sampleInterval\":30,\"ffSetIds\":\"Test ffSEt Ids\",\"rollDiameter\":10.0,\"rollDiameterUnits\":\"Test\",\"speedRatio\":0.55,\"tachId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"description\":\"Celcius\"}],\"outcome\":\"GET worked successfully.\"}";
        
        String result = instance.getByCustomerSiteAssetType(customerAccount, siteId, assetTypeName);
        assertEquals(expResult, result);
        System.out.println("getByCustomerSiteAssetType Done");
       
    }
    
    /**
     * Test of getByCustomerSiteAssetType method, of class SiteAssetPresetController.
     */
    @Test
    public void test8_GetByCustomerSiteAssetType() throws Exception {
        System.out.println("getByCustomerSiteAssetType");
        String customerAccount = "77777";
        String siteId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        String assetTypeName = "Asset Type";
        
        String expResult = "{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"assetTypeName\":\"Test Asset Type\",\"SiteAssetPreset\":[{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"assetTypeName\":\"Test Asset Type\",\"assetPresetId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"pointLocationName\":\"Point Location Name\",\"assetPresetName\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\"devicePresetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\"alarmEnabled\":true,\"sampleInterval\":30,\"ffSetIds\":\"Test ffSEt Ids\",\"rollDiameter\":10.0,\"rollDiameterUnits\":\"Test\",\"speedRatio\":0.55,\"tachId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"description\":\"Celcius\"}],\"outcome\":\"GET worked successfully.\"}";
        
        String result = instance.getByCustomerSiteAssetType(customerAccount, siteId, assetTypeName);
        assertNotEquals(expResult, result);
        System.out.println("getByCustomerSiteAssetType");
       
    }
    
    /**
     * Test of getByCustomerSiteAssetType method, of class SiteAssetPresetController.
     */
    @Test
    public void test9_GetByCustomerSite() throws Exception {
        System.out.println("getByCustomerSiteAssetType");
        String customerAccount = "77777";
        String siteId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        
        String expResult = "{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"SiteAssetPreset\":[{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"assetTypeName\":\"Test Asset Type\",\"assetPresetId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"pointLocationName\":\"Point Location Name\",\"assetPresetName\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\"devicePresetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\"devicePresetType\":\"MistLx\",\"sampleInterval\":30,\"ffSetIds\":\"Test ffSEt Ids\",\"rollDiameter\":10.0,\"rollDiameterUnits\":\"Test\",\"speedRatio\":0.55,\"tachId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"description\":\"Celcius\"}],\"outcome\":\"GET worked successfully.\"}";
        
        String result = instance.getByCustomerSite(customerAccount, siteId);
        System.out.println("getByCustomerSiteAssetType result - "+ result);
        assertEquals(expResult, result);
        System.out.println("getByCustomerSiteAssetType");
       
    }
    
    /**
     * Test of getByCustomerSiteAssetType method, of class SiteAssetPresetController.
     */
    @Test
    public void test10_GetByCustomerSite() throws Exception {
        System.out.println("getByCustomerSiteAssetType");
        String customerAccount = "77777";
        String siteId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        
        String expResult = "{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"assetTypeName\":\"Test Asset Type\",\"SiteAssetPreset\":[{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"assetTypeName\":\"Test Asset Type\",\"assetPresetId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"pointLocationName\":\"Point Location Name\",\"assetPresetName\":\"d623b71a-6d3b-49dd-bd51-d5b0647cfc5f\",\"devicePresetId\":\"79c2e5b9-7328-4fcf-aa4c-fba233481152\",\"alarmEnabled\":true,\"sampleInterval\":30,\"ffSetIds\":\"Test ffSEt Ids\",\"rollDiameter\":10.0,\"rollDiameterUnits\":\"Test\",\"speedRatio\":0.55,\"tachId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"description\":\"Celcius\"}],\"outcome\":\"GET worked successfully.\"}";
        
        String result = instance.getByCustomerSite(customerAccount, siteId);
        assertNotEquals(expResult, result);
        System.out.println("getByCustomerSiteAssetType");
       
    }

    

    /**
     * Test of updateSiteAssetPreset method, of class SiteAssetPresetController.
     */
    @Test
    public void test11_UpdateSiteAssetPreset() {
        System.out.println("updateSiteAssetPreset");
        String expResult = "{\"outcome\":\"Updated SiteAssetPreset successfully.\"}";
        String result = instance.updateSiteAssetPreset(content);
        assertEquals(expResult, result);
        System.out.println("updateSiteAssetPreset");
       
    }
    
    /**
     * Test of updateSiteAssetPreset method, of class SiteAssetPresetController.
     */
    @Test
    public void test12_UpdateSiteAssetPreset() {
        System.out.println("updateSiteAssetPreset");
        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.updateSiteAssetPreset(insuffDataPayload);
        assertEquals(expResult, result);
       
    }

    /**
     * Test of deleteSiteAssetPreset method, of class SiteAssetPresetController.
     */
    @Test
    public void test13_DeleteSiteAssetPreset() {
        System.out.println("deleteSiteAssetPreset");
        String expResult = "{\"outcome\":\"Deleted Site Asset Preset successfully.\"}";
        String result = instance.deleteSiteAssetPreset(content);
        //assertEquals(expResult, result);
        System.out.println("deleteSiteAssetPreset Done"+result);
        
    }
    
    /**
     * Test of deleteSiteAssetPreset method, of class SiteAssetPresetController.
     */
    @Test
    public void test14_DeleteSiteAssetPreset() {
        System.out.println("deleteSiteAssetPreset");
        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.deleteSiteAssetPreset(insuffDataPayload);
        assertEquals(expResult, result);
        System.out.println("deleteSiteAssetPreset Done");
        
    }
}
