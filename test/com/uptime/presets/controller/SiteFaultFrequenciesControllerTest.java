/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.presets.controller;

import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author Joseph
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SiteFaultFrequenciesControllerTest {

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//presets.cql", true, true, "worldview_dev1"));
    private static SiteFaultFrequenciesController instance;
    private static String content;

    public SiteFaultFrequenciesControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        instance = new SiteFaultFrequenciesController();
        content = "[\n"
                + "	{\n"
                + "		\"customerAccount\": \"77777\",\n"
                + "		\"siteName\": \"UPTIME SITE 1\",\n"
                + "		\"ffSetName\": \"Set name 55\",\n"
                + "		\"ffSetId\": \"b026aa07-3e5c-4c24-b24e-0dc01779375e\",\n"
                + "		\"ffId\": \"f9dbaf10-e394-418a-ad91-381700ee0850\",\n"
                + "		\"ffName\": \"fname123\",\n"
                + "		\"ffSetDesc\": \"Set name 55 desc\",\n"
                + "		\"ffValue\": 0.0,\n"
                + "		\"ffUnit\": \"RPM\",\n"
                + "		\"siteId\": \"7ac748c0-afca-400c-a315-53c94a83aa67\"\n"
                + "	}\n"
                + "]";
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of createSiteFaultFrequencies method, of class
     * SiteFaultFrequenciesController.
     */
    @Test
    public void test10_CreateSiteFaultFrequencies() {
        System.out.println("createSiteFaultFrequencies");
        String expResult = "{\"outcome\":\"New Site Fault Frequencies created successfully.\"}";
        String result = instance.createSiteFaultFrequencies(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of createSiteFaultFrequencies method, of class
     * SiteFaultFrequenciesController.
     */
    @Test
    public void test11_CreateSiteFaultFrequencies() {
        System.out.println("createSiteFaultFrequencies");
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.createSiteFaultFrequencies(null);
        assertEquals(expResult, result);
    }

    /**
     * Test of createSiteFaultFrequencies method, of class
     * SiteFaultFrequenciesController.
     */
    @Test
    public void test12_CreateSiteFaultFrequencies() {
        System.out.println("createSiteFaultFrequencies");
        String contentJSON = "[\n"
                + "	{\n"
                + "		\"customerAccount\": \"77777\",\n"
                + "		\"siteName\": \"UPTIME SITE 1\",\n"
                + "		\"ffSetName\": \"Set name 55\",\n"
                + "		\"ffSetId\": \"b026aa07-3e5c-4c24-b24e-0dc01779375e\",\n"
                + "		\"ffId\": \"f9dbaf10-e394-418a-ad91-381700ee0850\",\n"
                + "		\"ffName\": \"fname123\",\n"
                + "		\"ffSetDesc\": \"Set name 55 desc\",\n"
                + "		\"ffValue\": 0.0,\n"
                + "		\"ffUnit\": \"RPM\"\n"
                + "	}\n"
                + "]";
        String expResult = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        String result = instance.createSiteFaultFrequencies(contentJSON);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateSiteFaultFrequencies method, of class
     * SiteFaultFrequenciesController.
     */
    @Test
    public void test20_UpdateSiteFaultFrequencies() {
        System.out.println("updateSiteFaultFrequencies");
        String expResult = "{\"outcome\":\"Updated Site Fault Frequencies successfully.\"}";
        String result = instance.updateSiteFaultFrequencies(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateSiteFaultFrequencies method, of class
     * SiteFaultFrequenciesController.
     */
    @Test
    public void test21_UpdateSiteFaultFrequencies() {
        System.out.println("updateSiteFaultFrequencies");
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.updateSiteFaultFrequencies(null);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateSiteFaultFrequencies method, of class
     * SiteFaultFrequenciesController.
     */
    @Test
    public void test22_UpdateSiteFaultFrequencies() {
        System.out.println("updateSiteFaultFrequencies");
       String contentJSON = "[\n"
                + "	{\n"
               // + "		\"customerAccount\": \"77777\",\n"
                + "		\"siteName\": \"UPTIME SITE 1\",\n"
                + "		\"ffSetName\": \"Set name 55\",\n"
                + "		\"ffSetId\": \"b026aa07-3e5c-4c24-b24e-0dc01779375e\",\n"
                + "		\"ffId\": \"f9dbaf10-e394-418a-ad91-381700ee0850\",\n"
                + "		\"ffName\": \"fname123\",\n"
                + "		\"ffSetDesc\": \"Set name 55 desc\",\n"
                + "		\"ffValue\": 0.0,\n"
                + "		\"ffUnit\": \"RPM\",\n"
                + "		\"siteId\": \"7ac748c0-afca-400c-a315-53c94a83aa67\"\n"
                + "	}\n"
                + "]";
        String expResult = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        String result = instance.updateSiteFaultFrequencies(contentJSON);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByPK method, of class SiteFaultFrequenciesController.
     * @throws java.lang.Exception
     */
    @Test
    public void test30_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = "77777";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String ffSetId = "b026aa07-3e5c-4c24-b24e-0dc01779375e";
        String ffId = "f9dbaf10-e394-418a-ad91-381700ee0850";
        String expResult = "{\"customerAccount\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"ffSetId\":\"b026aa07-3e5c-4c24-b24e-0dc01779375e\",\"ffId\":\"f9dbaf10-e394-418a-ad91-381700ee0850\",\"SiteFaultFrequencies\":[{\"customerAccount\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"ffSetId\":\"b026aa07-3e5c-4c24-b24e-0dc01779375e\",\"ffId\":\"f9dbaf10-e394-418a-ad91-381700ee0850\",\"ffSetName\":\"Set name 55\",\"ffName\":\"fname123\",\"ffSetDesc\":\"Set name 55 desc\",\"ffValue\":0.0,\"ffUnit\":\"RPM\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByPK(customerAccount, siteId, ffSetId, ffId);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByPK method, of class SiteFaultFrequenciesController.
     * @throws java.lang.Exception
     */
    @Test
    public void test31_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = "77788";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String ffSetId = "b026aa07-3e5c-4c24-b24e-0dc01779375e";
        String ffId = "f9dbaf10-e394-418a-ad91-381700ee0850";
        String expResult = "{\"outcome\":\"Site Fault Frequencies not found.\"}";
        String result = instance.getByPK(customerAccount, siteId, ffSetId, ffId);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByPK method, of class SiteFaultFrequenciesController.
     * @throws java.lang.Exception
     */
    @Test
    public void test32_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = null;
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String ffSetId = "b026aa07-3e5c-4c24-b24e-0dc01779375e";
        String ffId = "f9dbaf10-e394-418a-ad91-381700ee0850";
        String expResult = "Argument customerAccount cannot be null or empty.";
        String expMsg = "";
        try {
            instance.getByPK(customerAccount, siteId, ffSetId, ffId);
        } catch (Exception e) {
            expMsg = e.getMessage();
            System.out.println("Exception e - " + expMsg);
        }
        assertEquals(expResult, expMsg);
    }

    /**
     * Test of getByPK method, of class SiteFaultFrequenciesController.
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test33_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = "777777";
        String siteId = null;
        String ffSetId = "b026aa07-3e5c-4c24-b24e-0dc01779375e";
        String ffId = "f9dbaf10-e394-418a-ad91-381700ee0850";
        fail(instance.getByPK(customerAccount, siteId, ffSetId, ffId));
    }

    /**
     * Test of getByPK method, of class SiteFaultFrequenciesController.
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test34_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = "777777";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String ffSetId = null;
        String ffId = "f9dbaf10-e394-418a-ad91-381700ee0850";
        fail(instance.getByPK(customerAccount, siteId, ffSetId, ffId));
    }

    @Test(expected = NullPointerException.class)
    public void test35_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = "777777";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String ffSetId = "b026aa07-3e5c-4c24-b24e-0dc01779375e";
        String ffId = null;
        fail(instance.getByPK(customerAccount, siteId, ffSetId, ffId));
    }

    /**
     * Test of findByCustomerSiteSetId method, of class
     * SiteFaultFrequenciesController.
     * @throws java.lang.Exception
     */
    @Test
    public void test40_FindByCustomerSiteSetId() throws Exception {
        System.out.println("findByCustomerSiteSetId");
        String customerAccount = "77777";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String ffSetId = "b026aa07-3e5c-4c24-b24e-0dc01779375e";
        String expResult = "{\"customerAccount\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"ffSetId\":\"b026aa07-3e5c-4c24-b24e-0dc01779375e\",\"SiteFaultFrequencies\":[{\"customerAccount\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"ffSetId\":\"b026aa07-3e5c-4c24-b24e-0dc01779375e\",\"ffId\":\"f9dbaf10-e394-418a-ad91-381700ee0850\",\"ffSetName\":\"Set name 55\",\"ffName\":\"fname123\",\"ffSetDesc\":\"Set name 55 desc\",\"ffValue\":0.0,\"ffUnit\":\"RPM\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.findByCustomerSiteSetId(customerAccount, siteId, ffSetId);
        assertEquals(expResult, result);
    }

    /**
     * Test of findByCustomerSiteSetId method, of class
     * SiteFaultFrequenciesController.
     * @throws java.lang.Exception
     */
    @Test
    public void test41_FindByCustomerSiteSetId() throws Exception {
        System.out.println("findByCustomerSiteSetId");
        String customerAccount = "77788";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String ffSetId = "b026aa07-3e5c-4c24-b24e-0dc01779375e";
        String expResult = "{\"outcome\":\"Site Fault Frequencies not found.\"}";
        String result = instance.findByCustomerSiteSetId(customerAccount, siteId, ffSetId);
        assertEquals(expResult, result);
    }

    /**
     * Test of findByCustomerSiteSetId method, of class
     * SiteFaultFrequenciesController.
     * @throws java.lang.Exception
     */
    @Test
    public void test42_FindByCustomerSiteSetId() throws Exception {
        System.out.println("findByCustomerSiteSetId");
        String customerAccount = null;
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String ffSetId = "b026aa07-3e5c-4c24-b24e-0dc01779375e";
        String expResult = "Argument customerAccount cannot be null or empty.";
        String expMsg = "";
        try {
            instance.findByCustomerSiteSetId(customerAccount, siteId, ffSetId);
        } catch (Exception e) {
            expMsg = e.getMessage();
            System.out.println("Exception e - " + expMsg);
        }
        assertEquals(expResult, expMsg);
    }

    /**
     * Test of findByCustomerSiteSetId method, of class
     * SiteFaultFrequenciesController.
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test43_FindByCustomerSiteSetId() throws Exception {
        System.out.println("findByCustomerSiteSetId");
        String customerAccount = "77788";
        String siteId = null;
        String ffSetId = "b026aa07-3e5c-4c24-b24e-0dc01779375e";
        fail(instance.findByCustomerSiteSetId(customerAccount, siteId, ffSetId));
    }

    /**
     * Test of findByCustomerSiteSetId method, of class
     * SiteFaultFrequenciesController.
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test44_FindByCustomerSiteSetId() throws Exception {
        System.out.println("findByCustomerSiteSetId");
        String customerAccount = "77788";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String ffSetId = null;
        fail(instance.findByCustomerSiteSetId(customerAccount, siteId, ffSetId));
    }

    /**
     * Test of findByCustomerSiteId method, of class
     * SiteFaultFrequenciesController.
     * @throws java.lang.Exception
     */
    @Test
    public void test50_FindByCustomerSiteId() throws Exception {
        System.out.println("findByCustomerSiteId");
        String customerAccount = "77777";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String expResult = "{\"customerAccount\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"SiteFaultFrequencies\":[{\"customerAccount\":\"77777\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"ffSetId\":\"b026aa07-3e5c-4c24-b24e-0dc01779375e\",\"ffId\":\"f9dbaf10-e394-418a-ad91-381700ee0850\",\"ffSetName\":\"Set name 55\",\"ffName\":\"fname123\",\"ffSetDesc\":\"Set name 55 desc\",\"ffValue\":0.0,\"ffUnit\":\"RPM\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.findByCustomerSiteId(customerAccount, siteId);
        assertEquals(expResult, result);
    }

    /**
     * Test of findByCustomerSiteId method, of class
     * SiteFaultFrequenciesController.
     * @throws java.lang.Exception
     */
    @Test
    public void test51_FindByCustomerSiteId() throws Exception {
        System.out.println("findByCustomerSiteId");
        String customerAccount = "77788";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String expResult = "{\"outcome\":\"Site Fault Frequencies not found.\"}";
        String result = instance.findByCustomerSiteId(customerAccount, siteId);
        assertEquals(expResult, result);
    }

    /**
     * Test of findByCustomerSiteId method, of class
     * SiteFaultFrequenciesController.
     * @throws java.lang.Exception
     */
    @Test
    public void test52_FindByCustomerSiteId() throws Exception {
        System.out.println("findByCustomerSiteId");
        String customerAccount = null;
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String expResult = "Argument customerAccount cannot be null or empty.";
        String expMsg = "";
        try {
            instance.findByCustomerSiteId(customerAccount, siteId);
        } catch (Exception e) {
            expMsg = e.getMessage();
            System.out.println("Exception e - " + expMsg);
        }
        assertEquals(expResult, expMsg);
    }

    /**
     * Test of findByCustomerSiteId method, of class
     * SiteFaultFrequenciesController.
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test53_FindByCustomerSiteId() throws Exception {
        System.out.println("findByCustomerSiteId");
        String customerAccount = "77788";
        String siteId = null;
        fail(instance.findByCustomerSiteId(customerAccount, siteId));
    }

    /**
     * Test of deleteSiteFaultFrequencies method, of class
     * SiteFaultFrequenciesController.
     */
    @Test
    public void test60_DeleteSiteFaultFrequencies() {
        System.out.println("deleteSiteFaultFrequencies");
        String expResult = "{\"outcome\":\"Deleted Site Fault Frequencies successfully.\"}";
        String result = instance.deleteSiteFaultFrequencies(content);
        assertEquals(expResult, result);
    }

    
    /**
     * Test of deleteSiteFaultFrequencies method, of class
     * SiteFaultFrequenciesController.
     */
    @Test
    public void test61_DeleteSiteFaultFrequencies() {
        System.out.println("deleteSiteFaultFrequencies");
        content = "[\n"
                + "	{\n"
                + "		\"customerAccount\": \"77788\",\n"
                + "		\"siteName\": \"UPTIME SITE 1\",\n"
                + "		\"ffSetName\": \"Set name 55\",\n"
                + "		\"ffSetId\": \"b026aa07-3e5c-4c24-b24e-0dc01779375e\",\n"
                + "		\"ffId\": \"f9dbaf10-e394-418a-ad91-381700ee0850\",\n"
                + "		\"ffName\": \"fname123\",\n"
                + "		\"ffSetDesc\": \"Set name 55 desc\",\n"
                + "		\"ffValue\": 0.0,\n"
                + "		\"ffUnit\": \"RPM\",\n"
                + "		\"siteId\": \"7ac748c0-afca-400c-a315-53c94a83aa67\"\n"
                + "	}\n"
                + "]";
        String expResult = "{\"outcome\":\"No Site Fault Frequencies found to delete.\"}";
        String result = instance.deleteSiteFaultFrequencies(content);
        assertEquals(expResult, result);
    }

    
    /**
     * Test of deleteSiteFaultFrequencies method, of class
     * SiteFaultFrequenciesController.
     */
    @Test
    public void test62_DeleteSiteFaultFrequencies() {
        System.out.println("deleteSiteFaultFrequencies");
        String expResult = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        content = "[\n"
                + "	{\n"
                + "		\"customerAccount\": \"77777\",\n"
                + "		\"siteName\": \"UPTIME SITE 1\",\n"
                + "		\"ffSetName\": \"Set name 55\",\n"
                + "		\"ffSetId\": \"b026aa07-3e5c-4c24-b24e-0dc01779375e\",\n"
                + "		\"ffId\": \"f9dbaf10-e394-418a-ad91-381700ee0850\",\n"
                + "		\"ffName\": \"fname123\",\n"
                + "		\"ffSetDesc\": \"Set name 55 desc\",\n"
                + "		\"ffValue\": 0.0,\n"
                + "		\"ffUnit\": \"RPM\"\n"
                + "	}\n"
                + "]";
        String result = instance.deleteSiteFaultFrequencies(content);
        assertEquals(expResult, result);
    }

}
