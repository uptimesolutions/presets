/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.presets.controller;

import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SitePtLocationNamesControllerTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//presets.cql", true, true, "worldview_dev1"));    
    private static String content = "";
    private static SitePtLocationNamesController instance;
    
    public SitePtLocationNamesControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = new SitePtLocationNamesController();
        content = "{\n" +
"\"customerAccount\": \"77777\",\n" +
"\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n" +
"\"pointLocationName\":\"pointLocationName Test 1\",\n" +
"\"description\":\"pointLocationName description Test 1\"\n" +
"}";
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of createSitePtLocationNames method, of class SitePtLocationNamesController.
     */
    @Test
    public void test10_CreateSitePtLocationNames() {
        System.out.println("createSitePtLocationNames");
        String expResult = "{\"outcome\":\"New SitePtLocationNames created successfully.\"}";
        String result = instance.createSitePtLocationNames(content);
        System.out.println("createSitePtLocationNames result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of createSitePtLocationNames method, of class SitePtLocationNamesController.
     * Unhappy path: Passing an invalid input json with missing siteId. 
     */
    @Test
    public void test11_CreateSitePtLocationNames() {
        System.out.println("createSitePtLocationNames");
        String contentJSON = "{\n" +
                        "\"customerAccount\": \"77777\",\n" +
                       // "\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n" +
                        "\"pointLocationName\":\"pointLocationName Test 1\",\n" +
                        "\"description\":\"pointLocationName description Test 1\"\n" +
                        "}";
        String expResult = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        String result = instance.createSitePtLocationNames(contentJSON);
        System.out.println("createSitePtLocationNames result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of createSitePtLocationNames method, of class SitePtLocationNamesController.
     * Unhappy path: Passing an invalid input json as null.  
     */
    @Test
    public void test13_CreateSitePtLocationNames() {
        System.out.println("createSitePtLocationNames");
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.createSitePtLocationNames(null);
        System.out.println("createSitePtLocationNames result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByPK method, of class SitePtLocationNamesController.
     * @throws java.lang.Exception
     */
    @Test
    public void test20_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = "77777";
        String siteId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        String pointLocationName = "pointLocationName Test 1";
        String expResult = "{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"pointLocationName\":\"pointLocationName Test 1\",\"SitePtLocationNames\":[{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"pointLocationName\":\"pointLocationName Test 1\",\"description\":\"pointLocationName description Test 1\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByPK(customerAccount, siteId, pointLocationName);
        System.out.println("findByPK result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByPK method, of class SitePtLocationNamesController.
     * Unhappy path: When customerAccount is passed is not available in DB, no data is received from the database.
     * @throws java.lang.Exception
     */
    @Test
    public void test21_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = "77788";
        String siteId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        String pointLocationName = "pointLocationName Test 1";
        String expResult = "{\"outcome\":\"SitePtLocationNames not found.\"}";
        String result = instance.getByPK(customerAccount, siteId, pointLocationName);
        System.out.println("findByPK result 21 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByPK method, of class SitePtLocationNamesController.
     * Unhappy path: When customer is passed as null
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test22_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = null;
        String siteId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        String pointLocationName = "pointLocationName Test 1";
        fail(instance.getByPK(customerAccount, siteId, pointLocationName));
    }

    /**
     * Test of getByPK method, of class SitePtLocationNamesController.
     * Unhappy path: When siteId is passed as null
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test23_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = "77777";
        String siteId = null;
        String pointLocationName = "pointLocationName Test 1";
        fail(instance.getByPK(customerAccount, siteId, pointLocationName));
    }

    /**
     * Test of findByCustomer method, of class SitePtLocationNamesController.
     * @throws java.lang.Exception
     */
    @Test
    public void test30_FindByCustomer() throws Exception {
        System.out.println("findByCustomerSite");
        String customerAccount = "77777";
        String siteId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        String expResult = "{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"SitePtLocationNames\":[{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"pointLocationName\":\"pointLocationName Test 1\",\"description\":\"pointLocationName description Test 1\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByCustomerSite(customerAccount, siteId);
        System.out.println("findByCustomerSite result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of findByCustomer method, of class SitePtLocationNamesController.Unhappy path: Passing a siteId that does not exist in DB
     * @throws java.lang.Exception
     */
    @Test
    public void test31_FindByCustomer() throws Exception {
        System.out.println("findByCustomerSite");
        String customerAccount = "77777";
        String siteId = "dee5158a-30b0-4c12-b6b9-40cb91d1206a";
        String expResult = "{\"outcome\":\"SitePtLocationNames not found.\"}";
        String result = instance.getByCustomerSite(customerAccount, siteId);
        System.out.println("findByCustomerSite result 31 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of findByCustomer method, of class SitePtLocationNamesController.
     * Unhappy path: Controller throws IllegalArgumentException when 
     * siteId is passed as "null".
     * @throws java.lang.Exception 
     */
    @Test(expected = NullPointerException.class)
    public void test32_FindByCustomer() throws Exception {
        System.out.println("findByCustomerSite");
        String customerAccount = "77777";
        String siteId = null;
        fail(instance.getByCustomerSite(customerAccount, siteId));
    }

    /**
     * Test of findByCustomer method, of class SitePtLocationNamesController.
     * Unhappy path: Controller throws IllegalArgumentException when 
     * customerAccount is passed as "null".
     * @throws java.lang.Exception 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test33_FindByCustomer() throws Exception {
        System.out.println("findByCustomerSite");
        String customerAccount = null;
        String siteId = "dee5158a-30b0-4c12-b6b9-40cb91d1206a";
        fail(instance.getByCustomerSite(customerAccount, siteId));
    }

    /**
     * Test of updateSitePtLocationNames method, of class SitePtLocationNamesController.
     */
    @Test
    public void test40_UpdateSitePtLocationNames() {
        System.out.println("updateSitePtLocationNames");
        String expResult = "{\"outcome\":\"Updated SitePtLocationNames successfully.\"}";
        String result = instance.updateSitePtLocationNames(content);
        System.out.println("updateSitePtLocationNames result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateSitePtLocationNames method, of class SitePtLocationNamesController.
     * Unhappy path: Missing required field 'siteId' in input json. 
     */
    @Test
    public void test41_UpdateSitePtLocationNames() {
        System.out.println("updateSitePtLocationNames");
        String contentJSON = "{\n" +
                        "\"customerAccount\": \"77777\",\n" +
                       // "\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n" +
                        "\"pointLocationName\":\"pointLocationName Test 1\",\n" +
                        "\"description\":\"pointLocationName description Test 1\"\n" +
                        "}";
        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.updateSitePtLocationNames(contentJSON);
        System.out.println("updateSitePtLocationNames result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateSitePtLocationNames method, of class SitePtLocationNamesController.
     * Unhappy path: Passing empty json. 
     */
    @Test
    public void test42_UpdateSitePtLocationNames() {
        System.out.println("updateSitePtLocationNames");
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.updateSitePtLocationNames("");
        System.out.println("updateSitePtLocationNames result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteSitePtLocationNames method, of class SitePtLocationNamesController.
     */
    @Test
    public void test50_DeleteSitePtLocationNames() {
        System.out.println("deleteSitePtLocationNames");
        content = "{\n" +
                        "\"customerAccount\": \"77777\",\n" +
                        "\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n" +
                        "\"pointLocationName\":\"pointLocationName Test 1\",\n" +
                        "\"description\":\"pointLocationName description Test 1\"\n" +
                        "}";
        String expResult = "{\"outcome\":\"Deleted SitePtLocationNames successfully.\"}";
        String result = instance.deleteSitePtLocationNames(content);
        System.out.println("deleteSitePtLocationNames result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteSitePtLocationNames method, of class SitePtLocationNamesController.
     * Unhappy path: Missing required field 'siteId' in input json. 
     */
    @Test
    public void test51_DeleteSitePtLocationNames() {
        System.out.println("deleteSitePtLocationNames");
        content = "{\n" +
                        "\"customerAccount\": \"77777\",\n" +
                        //"\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n" +
                        "\"pointLocationName\":\"pointLocationName Test 1\",\n" +
                        "\"description\":\"pointLocationName description Test 1\"\n" +
                        "}";
        String expResult = "{\"outcome\":\"Insufficient data given in json\"}";
        String result = instance.deleteSitePtLocationNames(content);
        System.out.println("deleteSitePtLocationNames result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteSitePtLocationNames method, of class SitePtLocationNamesController.
     * Unhappy path: Missing required field 'siteId' in input json. 
     */
    @Test
    public void test52_DeleteSitePtLocationNames() {
        System.out.println("deleteSitePtLocationNames");
        content = "{\n" +
                        "\"customerAccount\": \"77777\",\n" +
                        //"\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n" +
                        "\"pointLocationName\":\"pointLocationName Test 1\",\n" +
                        "\"description\":\"pointLocationName description Test 1\"\n" +
                        "}";
        String expResult = "{\"outcome\":\"Insufficient data given in json\"}";
        String result = instance.deleteSitePtLocationNames(content);
        System.out.println("deleteSitePtLocationNames result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteSitePtLocationNames method, of class SitePtLocationNamesController.
     * Unhappy path: Passing a  null input json. 
     */
    @Test
    public void test53_DeleteSitePtLocationNames() {
        System.out.println("deleteSitePtLocationNames");
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.deleteSitePtLocationNames(null);
        System.out.println("deleteSitePtLocationNames result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteSitePtLocationNames method, of class SitePtLocationNamesController.
     * Unhappy path: Passing an invalid siteId in input json. 
     */
    @Test
    public void test54_DeleteSitePtLocationNames() {
        System.out.println("deleteSitePtLocationNames");
        content = "{\n" +
                        "\"customerAccount\": \"77777\",\n" +
                        "\"siteId\":\"dee5158a-30b0-4c12-\",\n" +
                        "\"pointLocationName\":\"pointLocationName Test 1\",\n" +
                        "\"description\":\"pointLocationName description Test 1\"\n" +
                        "}";
        String expResult = "{\"outcome\":\"Insufficient data given in json\"}";
        String result = instance.deleteSitePtLocationNames(content);
        System.out.println("deleteSitePtLocationNames result - " + result);
        assertEquals(expResult, result);
    }
    
}
