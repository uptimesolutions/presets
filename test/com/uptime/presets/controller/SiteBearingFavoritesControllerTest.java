/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.presets.controller;

import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SiteBearingFavoritesControllerTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//presets.cql", true, true, "worldview_dev1"));    
    private static String content = "";
    private static SiteBearingFavoritesController instance;
    
    public SiteBearingFavoritesControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = new SiteBearingFavoritesController();
        content = "[{\n" +
                    "\"customerAcct\": \"77777\",\n" +
                    "\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n" +
                    "\"vendorId\":\"Test Vendor\",\n" +
                    "\"bearingId\":\"Test Bearing\",\n" +
                    "\"ftf\":2,\n" +
                    "\"bsf\":10,\n" +
                    "\"bpfo\":20,\n" +
                    "\"bpfi\":30\n" +
                    "}]";
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }


    /**
     * Test of createSiteBearingFavorites method, of class SiteBearingFavoritesController.
     */
    @Test
    public void test10_CreateSiteBearingFavorites() {
        System.out.println("createSiteBearingFavorites");
        String expResult = "{\"outcome\":\"New Site Bearing Favorites created successfully.\"}";
        String result = instance.createSiteBearingFavorites(content);
        System.out.println("createSiteBearingFavorites result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of createSiteBearingFavorites method, of class SiteBearingFavoritesController.
     * Unhappy path: Passing an invalid input json with missing siteId. 
     */
    @Test
    public void test11_CreateSiteBearingFavorites() {
        System.out.println("createSiteBearingFavorites");
        content = "[{\n" +
                    "\"customerAccount\": \"77777\",\n" +
//                    "\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n" +
                    "\"vendorId\":\"Test Vendor\",\n" +
                    "\"bearingId\":\"Test Bearing\",\n" +
                    "\"ftf\":2,\n" +
                    "\"bsf\":10,\n" +
                    "\"bpfo\":20,\n" +
                    "\"bpfi\":30\n" +
                    "}]";
        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.createSiteBearingFavorites(content);
        System.out.println("createSiteBearingFavorites 11 result - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of createSiteBearingFavorites method, of class SiteBearingFavoritesController.
     * Unhappy path: Passing an invalid input json as null.  
     */
    @Test
    public void test12_CreateSiteBearingFavorites() {
        System.out.println("createSiteBearingFavorites");
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.createSiteBearingFavorites(null);
        System.out.println("createSiteBearingFavorites 12 result - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getByPK method, of class SiteBearingFavoritesController.
     * @throws java.lang.Exception
     */
    @Test
    public void test20_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = "77777";
        String siteId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        String vendorId = "Test Vendor";
        String bearingId = "Test Bearing";
        String expResult = "{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"vendorId\":\"Test Vendor\",\"bearingId\":\"Test Bearing\",\"SiteBearingFavorites\":[{\"customerAcct\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"vendorId\":\"Test Vendor\",\"bearingId\":\"Test Bearing\",\"ftf\":2.0,\"bsf\":10.0,\"bpfo\":20.0,\"bpfi\":30.0}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByPK(customerAccount, siteId, vendorId, bearingId);
        System.out.println("findByPK result - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of findByPK method, of class SiteBearingFavoritesController.
     * Unhappy path: When customerAccount is passed is not available in DB, no data is received from the database.
     * @throws java.lang.Exception
     */
    @Test
    public void test21_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = "88888";
        String siteId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        String vendorId = "Test Vendor";
        String bearingId = "Test Bearing";
        String expResult = "{\"outcome\":\"SiteBearingFavorites not found.\"}";
        String result = instance.getByPK(customerAccount, siteId, vendorId, bearingId);
        System.out.println("findByPK 21 result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of findByPK method, of class SiteBearingFavoritesController.
     * Unhappy path: When customer is passed as null
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test22_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = null;
        String siteId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        String vendorId = "Test Vendor";
        String bearingId = "Test Bearing";
        fail(instance.getByPK(customerAccount, siteId, vendorId, bearingId));
    }
    
    /**
     * Test of findByPK method, of class SiteBearingFavoritesController.
     * Unhappy path: When siteId is passed as null
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test23_FindByPK() throws Exception {
        System.out.println("findByPK");
        String customerAccount = "77777";
        String siteId = null;
        String vendorId = "Test Vendor";
        String bearingId = "Test Bearing";
        fail(instance.getByPK(customerAccount, siteId, vendorId, bearingId));
    }
    
    /**
     * Test of getByCustomerSite method, of class SiteBearingFavoritesController.
     * @throws java.lang.Exception
     */
    @Test
    public void test30_FindByCustomerSite() throws Exception {
        System.out.println("findByCustomerSite");
        String customerAccount = "77777";
        String siteId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        String expResult = "{\"customerAccount\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"SiteBearingFavorites\":[{\"customerAcct\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"vendorId\":\"Test Vendor\",\"bearingId\":\"Test Bearing\",\"ftf\":2.0,\"bsf\":10.0,\"bpfo\":20.0,\"bpfi\":30.0}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByCustomerSite(customerAccount, siteId);
        System.out.println("findByCustomerSite result - " + result);
        assertEquals(expResult, result);

    }
    
    /**
     * Test of getByCustomerSite method, of class SiteBearingFavoritesController.Unhappy path: Passing a siteId that does not exist in DB
     * @throws java.lang.Exception
     */
    @Test
    public void test31_FindByCustomerSite() throws Exception {
        System.out.println("findByCustomerSite");
        String customerAccount = "88888";
        String siteId = "dee5158a-30b0-4c12-b6b9-40cb91d1206d";
        String expResult = "{\"outcome\":\"SiteBearingFavorites not found.\"}";
        String result = instance.getByCustomerSite(customerAccount, siteId);
        System.out.println("findByCustomerSite result - " + result);
        assertEquals(expResult, result);

    }

    /**
     * Test of findByCustomerSite method, of class SiteBearingFavoritesController.
     * Unhappy path: Controller throws IllegalArgumentException when 
     * siteId is passed as "null".
     * @throws java.lang.Exception 
     */
    @Test(expected = NullPointerException.class)
    public void test32_FindByCustomerSite() throws Exception {
        System.out.println("findByCustomerSite");
        String customerAccount = "77777";
        String siteId = null;
        fail(instance.getByCustomerSite(customerAccount, siteId));
    }

    /**
     * Test of findByCustomerSite method, of class SiteBearingFavoritesController.
     * Unhappy path: Controller throws IllegalArgumentException when 
     * customerAccount is passed as "null".
     * @throws java.lang.Exception 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test33_FindByCustomer() throws Exception {
        System.out.println("findByCustomerSite");
        String customerAccount = null;
        String siteId = "dee5158a-30b0-4c12-b6b9-40cb91d1206a";
        fail(instance.getByCustomerSite(customerAccount, siteId));
    }
    
    /**
     * Test of updateSiteBearingFavorites method, of class SiteBearingFavoritesController.
     */
    @Test
    public void test40_UpdateSiteBearingFavorites() {
        System.out.println("updateSiteBearingFavorites");
        content = "{\n"
                    + "\"createSiteBearingFavoritesVOList\":[{\n" +
                    "\"customerAcct\": \"77777\",\n" +
                    "\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n" +
                    "\"vendorId\":\"Test Vendor\",\n" +
                    "\"bearingId\":\"Test Bearing 111\",\n" +
                    "\"ftf\":2,\n" +
                    "\"bsf\":10,\n" +
                    "\"bpfo\":20,\n" +
                    "\"bpfi\":30\n" +
                    "}], " +
                    "\n"
                    + "\"deleteSiteBearingFavoritesVOList\":[{\n" +
                    "\"customerAcct\": \"77777\",\n" +
                    "\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n" +
                    "\"vendorId\":\"Test Vendor\",\n" +
                    "\"bearingId\":\"Test Bearing\",\n" +
                    "\"ftf\":2,\n" +
                    "\"bsf\":10,\n" +
                    "\"bpfo\":20,\n" +
                    "\"bpfi\":30\n" +
                    "}]}";
        String expResult = "{\"outcome\":\"Updated Site Bearing Favorites successfully.\"}";
        String result = instance.updateSiteBearingFavorites(content);
        System.out.println("updateSiteBearingFavorites result - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of updateSiteBearingFavorites method, of class SiteBearingFavoritesController.
     * Unhappy path: Missing required field 'siteId' in input json. 
     */
    @Test
    public void test41_UpdateSiteBearingFavorites() {
        System.out.println("updateSiteBearingFavorites");
        content = "{\n"
                    + "\"createSiteBearingFavoritesVOList\":[{\n" +
                    "\"customerAcct\": \"77777\",\n" +
//                    "\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n" +
                    "\"vendorId\":\"Test Vendor\",\n" +
                    "\"bearingId\":\"Test Bearing\",\n" +
                    "\"ftf\":2,\n" +
                    "\"bsf\":10,\n" +
                    "\"bpfo\":20,\n" +
                    "\"bpfi\":30\n" +
                    "}]}";
        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.updateSiteBearingFavorites(content);
        System.out.println("updateSiteBearingFavorites result 41 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateSiteBearingFavorites method, of class SiteBearingFavoritesController.
     * Unhappy path: Passing empty json. 
     */
    @Test
    public void test42_UpdateSiteBearingFavorites() {
        System.out.println("updateSiteBearingFavorites");
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.updateSiteBearingFavorites("");
        System.out.println("updateSiteBearingFavorites result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteSiteBearingFavorites method, of class SiteBearingFavoritesController.
     */
    @Test
    public void test50_DeleteSiteBearingFavorites() {
        System.out.println("deleteSiteBearingFavorites");
        content = "{\n" +
                    "\"customerAcct\": \"77777\",\n" +
                    "\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n" +
                    "\"vendorId\":\"Test Vendor\",\n" +
                    "\"bearingId\":\"Test Bearing 111\",\n" +
                    "\"ftf\":2,\n" +
                    "\"bsf\":10,\n" +
                    "\"bpfo\":20,\n" +
                    "\"bpfi\":30\n" +
                    "}";
        String expResult = "{\"outcome\":\"Deleted SiteBearingFavorites successfully.\"}";
        String result = instance.deleteSiteBearingFavorites(content);
        System.out.println("deleteSiteBearingFavorites result - " + result);
        assertEquals(expResult, result);
    }
    

    /**
     * Test of deleteSiteBearingFavorites method, of class SiteBearingFavoritesController.
     * Unhappy path: Missing required field 'siteId' in input json. 
     */
    @Test
    public void test51_DeleteSiteBearingFavorites() {
        System.out.println("deleteSiteBearingFavorites");
        content = "{\n" +
                        "\"customerAcct\": \"77777\",\n" +
                        //"\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n" +
                        "\"vendorId\":\"Test Vendor\",\n" +
                        "\"bearingId\":\"Test Bearing\",\n" +
                        "\"ftf\":2,\n" +
                        "\"bsf\":10,\n" +
                        "\"bpfo\":20,\n" +
                        "\"bpfi\":30\n" +
                        "}";
        String expResult = "{\"outcome\":\"Insufficient data given in json\"}";
        String result = instance.deleteSiteBearingFavorites(content);
        System.out.println("deleteSiteBearingFavorites 51 result - " + result);
        assertEquals(expResult, result);
    }

   
    /**
     * Test of deleteSiteBearingFavorites method, of class SiteBearingFavoritesController.
     * Unhappy path: Passing a  null input json. 
     */
    @Test
    public void test52_DeleteSiteBearingFavorites() {
        System.out.println("deleteSiteBearingFavorites");
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.deleteSiteBearingFavorites(null);
        System.out.println("deleteSiteBearingFavorites 52 result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteSiteBearingFavorites method, of class SiteBearingFavoritesController.
     * Unhappy path: Passing an invalid siteId in input json. 
     */
    @Test
    public void test53_DeleteSiteBearingFavorites() {
        System.out.println("deleteSiteBearingFavorites");
        content = "{\n" +
                        "\"customerAccount\": \"77777\",\n" +
                        "\"siteId\":\"dee5158a-30b0-4c12-\",\n" +
                        "\"vendorId\":\"Test Vendor\",\n" +
                        "\"bearingId\":\"Test Bearing\",\n" +
                        "\"ftf\":2,\n" +
                        "\"bsf\":10,\n" +
                        "\"bpfo\":20,\n" +
                        "\"bpfi\":30\n" +
                        "}";
        String expResult = "{\"outcome\":\"Insufficient data given in json\"}";
        String result = instance.deleteSiteBearingFavorites(content);
        System.out.println("deleteSiteBearingFavorites 53 result - " + result);
        assertEquals(expResult, result);
    }    
}
