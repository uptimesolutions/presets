/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.presets.controller;

import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GlobalAssetTypeControllerTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//presets.cql", true, true, "worldview_dev1"));
    private static GlobalAssetTypeController instance;
    
    public GlobalAssetTypeControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = new GlobalAssetTypeController();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getByCustomer method, of class GlobalAssetTypeController.
     * @throws java.lang.Exception
     */
    @Test
    public void test10_GetByCustomer() throws Exception {
        System.out.println("getByCustomer");
        String customerAccount = "UPTIME";
        String expResult = "{\"customerAccount\":\"UPTIME\",\"GlobalAssetTypes\":[{\"customerAccount\":\"UPTIME\",\"assetTypeName\":\"Bearing\",\"description\":\"Bearing\"},{\"customerAccount\":\"UPTIME\",\"assetTypeName\":\"Motor\",\"description\":\"Motor\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByCustomer(customerAccount);
        System.out.println("test10_GetByCustomer result - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getByCustomer method, of class GlobalAssetTypeController.
     * @throws java.lang.Exception
     */
    @Test
    public void test11_GetByCustomer() throws Exception {
        System.out.println("getByCustomer");
        String customerAccount = "FEDEX";
        String expResult = "{\"outcome\":\"Global Asset Types not found.\"}";
        String result = instance.getByCustomer(customerAccount);
        System.out.println("test11_GetByCustomer result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByCustomer method, of class GlobalAssetTypeController.
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test12_GetByCustomer() throws Exception {
        System.out.println("getByCustomer");
        String customerAccount = null;
        instance.getByCustomer(customerAccount);
    }

    /**
     * Test of getByPK method, of class GlobalAssetTypeController.
     * @throws java.lang.Exception
     */
    @Test
    public void test20_GetByPK() throws Exception {
        System.out.println("getByPK");
        String customerAccount = "UPTIME";
        String assetTypeName = "Motor";
        String expResult = "{\"customerAccount\":\"UPTIME\",\"assetTypeName\":\"Motor\",\"GlobalAssetTypes\":[{\"customerAccount\":\"UPTIME\",\"assetTypeName\":\"Motor\",\"description\":\"Motor\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByPK(customerAccount, assetTypeName);
        System.out.println("test20_GetByPK result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByPK method, of class GlobalAssetTypeController.
     * @throws java.lang.Exception
     */
    @Test
    public void test21_GetByPK() throws Exception {
        System.out.println("getByPK");
        String customerAccount = "FEDEX";
        String assetTypeName = "Motor";
        String expResult = "{\"outcome\":\"Global Asset Types not found.\"}";
        String result = instance.getByPK(customerAccount, assetTypeName);
        System.out.println("test21_GetByPK result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByPK method, of class GlobalAssetTypeController.
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test22_GetByPK() throws Exception {
        System.out.println("getByPK");
        String customerAccount = null;
        String assetTypeName = "Motor";
        instance.getByPK(customerAccount, assetTypeName);
    }
    
}
